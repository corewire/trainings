#!/bin/sh
set -e

MY_ADDRESS="`hostname -i | cut -f 1 -d ' '`"
echo "*** Presentation container address is: http://$MY_ADDRESS:8080/"

npm start
