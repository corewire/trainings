import jinja2
import os
from pathlib import Path

rootdir = Path.cwd().parent.parent
loaderObj = jinja2.FileSystemLoader(rootdir)
jinjaEnv = jinja2.Environment(loader=loaderObj)

# german templates
template_path = Path.cwd() / "index.md.jinja"
jTemplate = jinjaEnv.get_template(str(template_path.relative_to(rootdir)))
output = jTemplate.render()
with open("index.md", "w") as index_file:
    index_file.write(output)

# english templates
template_path = Path.cwd() / "index_en.md.jinja"
if template_path.exists():
    jTemplate = jinjaEnv.get_template(str(template_path.relative_to(rootdir)))
    output = jTemplate.render()
    with open("index_en.md", "w") as index_file:
        index_file.write(output)
