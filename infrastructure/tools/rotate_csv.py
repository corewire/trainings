# This rotates csv files, this is expecially usefull to get the google trends
# into the reveal chart plugin
import csv
from itertools import zip_longest
a = zip_longest(*csv.reader(open("input.csv", "rt")))
csv.writer(open("output.csv", "wt")).writerows(a)
