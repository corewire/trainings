#!/usr/bin/python3
"""
Generate pdfs.


Example usage:

Generate all modified pdfs:
$ ./tools/pdfgen 172.17.0.2:8000

Generate all pdfs:
$ ./tools/pdfgen 172.17.0.2:8000 --force

Generate pdfs only for git slides:
$ ./tools/pdfgen 172.17.0.2:8000 --root git

Generate pdfs as Din A:
$ ./tools/pdfgen 172.17.0.2:8000 --din_a4
"""

import asyncio
import argparse
import os
import logging
import subprocess
import requests
import itertools
from pathlib import Path


def main():
    args = get_commandline_arguments()
    initialize_logging(args)

    # Check if reveal is available
    reveal_uri = get_reveal_uri(getattr(args, "ip:port"))
    if not is_reveal_reachable(reveal_uri):
        logging.error("Cannot reach reveal under {}. Please check your input".format(reveal_uri))
        return
    logging.info("Check reveal succeeded ({})".format(reveal_uri))

    # Generate jobs
    jobs = [ (pdf, args.reveal_prefix + index) for pdf, index in get_files(args.root, force_build=args.force) ]
    logging.info("Found {} files to generate".format(len(jobs)))
    if len(jobs) == 0:
        return

    # Ensure image is present
    logging.info("Pulling image:")
    subprocess.run(['sudo', 'docker', 'pull', 'astefanutti/decktape'])

    if args.din_a4:
        dimensions = "1782x1260"
    else:
        dimensions = "1920x1080"

    # Run jobs
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_jobs(reveal_uri, jobs, dimensions=dimensions, concurrency=args.concurrency))


def get_commandline_arguments():
    """ Commandline argument parser for this module
    :returns: namespace with parsed arguments
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "ip:port",
        help="Specify the ip:port target for the docker" +
        " container where the reveal.js server is running")
    parser.add_argument(
        "--root",
        default=".",
        help="Specify the root directory for the search")
    parser.add_argument(
        "--din_a4",
        default=False, action="store_true",
        help="Generate pdfs in DIN A4 format, default is full HD"
    )
    parser.add_argument(
        "--reveal_prefix",
        default="",
        help="Specify a prefix for the reveal uri")
    parser.add_argument(
        "-f", "--force",
        default=False, action="store_true",
        help="Regenerate all printed presentations even when already present")
    parser.add_argument(
        "-n", "--concurrency",
        type=int,
        default=8,
        help="Number of parallel jobs")

    # Logging Args
    parser.add_argument(
        "--logfile",
        help="Specify the log file")
    logging = parser.add_mutually_exclusive_group()
    logging.add_argument(
        "-v", "--verbose",
        help="Verbose logging output",
        action="store_true")
    logging.add_argument(
        "-q", "--quiet",
        help="No logging output except errors",
        action="store_true")

    return parser.parse_args()


def initialize_logging(args):
    """Initialize logging as given in the commandline arguments
    :returns: None

    """
    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG
    if args.quiet:
        loglevel = logging.ERROR

    # If logfile is given, generate a new logger with file handling
    if args.logfile:
        filehandler = logging.FileHandler(args.logfile, 'a')
        formatter = logging.Formatter()
        filehandler.setFormatter(formatter)
        logger = logging.getLogger()
        for handler in logger.handlers:
            logger.removeHandler(handler)
        logger.addHandler(filehandler)

    logging.getLogger().setLevel(loglevel)


def get_reveal_uri(uri):
    return uri if uri.startswith('http') else f'http://{uri}'

def is_reveal_reachable(uri):
    """ Check if reveal can be reached.
    """
    try:
        response = requests.get(uri)
        if response.status_code == 200:
            return True
    except:
        pass
    return False


def get_files(root_dir, force_build):
    """Get all files for which slides should be generated
    :returns: (pdf_file, index_file)

    """
    p = Path(root_dir)

    # Actual glob expression: **/index.html
    # Python bug for symlinks and globs containing '**'
    # https://bugs.python.org/issue33428
    # Therefore chain the following expressions
    files = itertools.chain(p.glob('*/index.html'), p.glob('*/*/index.html'),p.glob('*/*/*/index.html'))
    for index_file in files:
        pdf_file_name = index_file.parent.name + '.pdf'
        pdf_file = index_file.parent.parent.joinpath('pdf', pdf_file_name)

        md_file = index_file.with_suffix('.md')

        # Skip directories where md file is missing. E.g. templates/
        if not md_file.is_file():
            continue

        # Modification check (did the source file change after the PDF creation?)
        if pdf_file.is_file() and not force_build:
            mod_time_html = index_file.stat().st_mtime
            mod_time_md = md_file.stat().st_mtime
            mod_time_pdf = pdf_file.stat().st_mtime
            if mod_time_pdf > mod_time_html and mod_time_pdf > mod_time_md:
                # Nothing changed in source file and PDF is there,
                # nothing to do!
                logging.debug(f"Skip file: {index_file}")
                continue

        logging.debug(f"Found file: {index_file}")
        yield (str(pdf_file), str(index_file))


async def run_jobs(reveal_uri, jobs, dimensions, concurrency = 5):
    active = set()
    while len(jobs) > 0 or len(active) > 0:
        # Add tasks
        while len(jobs) > 0 and len(active) < concurrency:
            pdf, index = jobs.pop()
            task = asyncio.ensure_future(generate_pdf(
                reveal_uri, pdf, index, dimensions
            ))
            active.add(task)

        # Wait for next task to finish
        logging.info("Running: {}, Waiting: {}".format(len(active), len(jobs)))
        _, active = await asyncio.wait(active, return_when=asyncio.FIRST_COMPLETED)


async def generate_pdf(container_target, pdf_file, index_file, dimensions):
    """TODO: Docstring for function.

    :arg1: TODO
    :returns: TODO

    """
    url = "{}/{}".format(container_target, index_file)
    abs_path = os.path.abspath(pdf_file)
    norm_path = os.path.normpath(pdf_file)
    abs_rootdir = abs_path[:-len(norm_path)]

    command = "sudo docker run --rm --net=host -v{}:/slides:z astefanutti/decktape\
        reveal --size {} {} /slides/{}".format(abs_rootdir, dimensions, url, norm_path)

    print(command)

    pdf_dir = os.path.dirname(pdf_file)
    if not os.path.exists(pdf_dir):
        os.mkdir(pdf_dir)

    logging.info("Start pdf generation for {}".format(index_file))
    logging.debug("Executing command: {}".format(command))

    stdout = asyncio.subprocess.DEVNULL if logging.getLogger().level != logging.DEBUG else None
    proc = await asyncio.create_subprocess_shell(command, stdout=stdout)
    returncode = await proc.wait()

if __name__ == "__main__":
    main()
