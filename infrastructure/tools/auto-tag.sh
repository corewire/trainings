#!/bin/bash

set -euxo pipefail

# Get the latest tag that starts with "v"
latest_tag=$(git tag -l "v*" --sort=-v:refname | head -n 1)

# Generate new tag
version_number=$(echo $latest_tag | sed 's/v//')
new_version_number=$((version_number + 1))
new_tag="v${new_version_number}"

# Tag the current commit with the new tag
git tag $new_tag
echo "Tagged with $new_tag"

git push origin $new_tag
