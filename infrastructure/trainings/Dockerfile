######################
### Build hands-on ###
######################

FROM registry.gitlab.com/corewire/trainings/mkdocs:latest AS handson

ENV HOST_DOMAIN=labs.corewire.de

RUN wget https://github.com/mikefarah/yq/releases/download/v4.25.3/yq_linux_amd64 -O /usr/bin/yq \
  && chmod +x /usr/bin/yq

COPY ./hands-on /hands-on

RUN find /hands-on -type f -name "*.jinja" -delete \
  # Delete all Makefiles templates
  && find /hands-on -name "Makefile" -delete \
  # Build mkdocs sites (de and en) into /public/demos
  && mkdocs build -f /hands-on/config/de/mkdocs.yml --site-dir /public/demos/de \
  && mkdocs build -f /hands-on/config/en/mkdocs.yml --site-dir /public/demos/en \
  # Delete demos and knowledge-checks for the main mkdocs site
  && yq -i 'del(.nav[].*[].Demos, .nav[].*[].Knowledge-Checks)' /hands-on/config/de/mkdocs.yml \
  && sed -i 's/- {}//g' /hands-on/config/de/mkdocs.yml \
  && yq -i 'del(.nav[].*[].Demos, .nav[].*[].Knowledge-Checks)' /hands-on/config/en/mkdocs.yml \
  && sed -i 's/- {}//g' /hands-on/config/en/mkdocs.yml \
  # Build mkdocs sites (de and en) into /public/de and /public/en
  && mkdocs build -f /hands-on/config/de/mkdocs.yml --site-dir /public/de \
  && mkdocs build -f /hands-on/config/en/mkdocs.yml --site-dir /public/en

####################
### Build slides ###
####################

FROM registry.gitlab.com/corewire/trainings/revealjs:latest AS slides

RUN npm run build \
  && mkdir -p /public \
  && cp -r /revealjs/dist /public/dist \
  && cp -r /revealjs/plugin /public/plugin

COPY ./slides /public/de
COPY ./slides /public/en

RUN find /public -type f -name "*.jinja" -delete \
  # Delete all Makefiles templates
  && find /public -name "Makefile" -delete \
  # Rename all index_en.md files to index.md in /public/en
  && find /public/en -name index_en.md -exec sh -c 'mv "$0" "${0%_en.md}.md"' {} \;

###################
### Final image ###
###################

FROM nginx:latest AS final

COPY --from=handson /public /usr/share/nginx/html
COPY --from=slides /public /usr/share/nginx/html
COPY ./infrastructure/trainings/index.html /usr/share/nginx/html/index.html

RUN chmod -R 755 /usr/share/nginx/html
