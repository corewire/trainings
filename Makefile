# Variables
DOCKER_COMPOSE = docker compose
DOCKER = docker

# include Makefile.local if it exists
# Use this local makefile to overwrite docker command
-include Makefile.local

# 0 if rootless, 1 otherwise
export IS_ROOTLESS := $(shell $(DOCKER) info 2>/dev/null | grep "Context" | grep "rootless" > /dev/null 2>&1 ; echo $$?)
ifeq ($(IS_ROOTLESS), 0)
# For rootless setups keep UID/GID as 0(root) because docker maps the
# root user inside the container to the current user outside
export HOST_UID ?= 0
export HOST_GID ?= 0
else
export HOST_UID ?= $(shell id -u)
export HOST_GID ?= $(shell id -g)
endif


RUN_TEXLIVE= HOST_UID=$(HOST_UID) HOST_GID=$(HOST_GID) $(DOCKER_COMPOSE) -f infrastructure/texlive-full/docker-compose.yml run --rm -T texlive-full
RUN_MERMAID= $(DOCKER) run --rm -u $(HOST_UID):$(HOST_GID) -v .:/data minlag/mermaid-cli

all:
	$(RUN_TEXLIVE) sh -c "find /trainings/hands-on -type d -exec test -f {}/Makefile \; -exec make -C {} all \;"
	$(RUN_TEXLIVE) sh -c "find /trainings/slides -type d -exec test -f {}/Makefile \; -exec make -C {} all \;"

clear:
	$(RUN_TEXLIVE) sh -c "find /trainings/hands-on -type d -exec test -f {}/Makefile \; -exec make -C {} clear \;"
	$(RUN_TEXLIVE) sh -c "find /trainings/slides -type d -exec test -f {}/Makefile \; -exec make -C {} clear \;"

install_hooks:
	cat .git_hooks/post-checkout >> .git/hooks/post-checkout
	chmod u+x .git_hooks/post-checkout

build_texlive:
	HOST_UID=$(HOST_UID) HOST_GID=$(HOST_GID) $(DOCKER_COMPOSE) -f infrastructure/texlive-full/docker-compose.yml build

.PHONY: all clear

watch-slides:
	while true; do \
		inotifywait -re close_write ./slides/ | while read directory action file; do \
			case $$file in \
				*.jinja) \
					$(RUN_TEXLIVE) sh -c "make -C $$directory all" ;; \
				*.tex) \
					$(RUN_TEXLIVE) sh -c "make -C $$directory all" ;; \
				*.mmd) \
					$(RUN_MERMAID) -i "$$directory/$$file"; \
			esac \
		done \
	done

watch-hands-on:
	while true; do \
		inotifywait -re close_write ./hands-on/ | while read directory action file; do \
			case $$file in \
				*.jinja) \
					$(RUN_TEXLIVE) sh -c "make -C $$directory all" ;; \
				*.tex) \
					$(RUN_TEXLIVE) sh -c "make -C $$directory all" ;; \
				*.mmd) \
					$(RUN_MERMAID) -i "$$directory/$$file"; \
			esac \
		done \
	done

final-image:
	$(DOCKER) build -f infrastructure/trainings/Dockerfile --target final -t trainings:dev .
	$(DOCKER) run -it --rm -p 8001:80 trainings:dev
