# Trainings

## Installation

1. Install docker. For debian-based systems use:
  [https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)

1. Install dependencies. For debian-based systems use:
    ```
    apt-get install git-lfs
    git lfs install
    git lfs pull
    ```

1. Generate all images
    ```
    make

    # In case of: permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock
    echo "DOCKER_COMPOSE = sudo -E docker compose" > Makefile.local
    echo "DOCKER = sudo -E docker" >> Makefile.local
    make

    # In case you want to build the images automatically after every checkout, install the post-checkout hook
    make install_hooks
    ```

1. Start applications and visit (with dev-server and hot reloading)
    ```
    docker compose up -d
    ```
    
    - [localhost:8000/\<foldername\>/](localhost:8000/docker/) for the slides
    - [localhost:8080](localhost:8080) for the hands-on tasks

1. Visit final site without dev-servers but slides+hands-ons combined
    ```
    make final-image
    # Visit localhost:8001
    ```

## Repository structure

```
.
├── hands-on               # Hands-on tasks and demos
│   └── {...}
├── infrastructure         # Container images and tools
│   └── {...}
└── slides                 # Slide content
    ├── {...}
    └── templates          # Base files, logos, styles, ...
```

## Developing hands-on

The hands-on part is based on [MKDocs](https://www.mkdocs.org/). Changes to `*.md` files get automatically picked up by the dev server. For changes to `*.tex` images see [developing tex-images](#developing-tex-images)

```
# Watch and rebuild tex-images on changes
make watch-hands-on
```

## Developing slides

The slides are based on [revealjs](https://revealjs.com/). For more control and less code-duplication, we use Jinja templates to generate the actual markdown files. Use `make` for one-time generation or `make watch-slides` to continously watch for file changes.

For changes to `*.tex` images see [developing tex-images](#developing-tex-images)

TODO: Browser refresh is required to see the changes on the slide. This should be improved fixed in the future

```
# Watch and rebuild tex-images and Jinja-templates on changes
make watch-slides
```

## Developing tex-images

1. change <name>.multi.tex-files that generate multiple images from one tikz-file

1. generate images with

  ``` make ```
  
1. use <name><number>.svg to load image into slides

1. generate liveview while editing with

  ``` make liveview <name>.multi ```

TODO:
- Explain watcher

## Developing knowledge checks

The knowledge checks are made with a h5p-Editor and then exported to a .html-file. A possible h5p-Editor is [Lumi](https://app.lumi.education/#download) ([Github-Link](https://github.com/Lumieducation)). You have to manually export the .html-file to use it in your slides.
