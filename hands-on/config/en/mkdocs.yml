# Project information
site_name: Corewire
site_author: Corewire
docs_dir: '../../docs/en'
site_dir: '../../public/en'

# Copyright
copyright: Copyright &copy; 2024 corewire

# Configuration
theme:
  name: material
  custom_dir: '../../overrides/'
  font: false
  language: en
  logo: assets/corewire-logo-light.svg
  features:
    - tabs
    - navigation.indexes
    - content.code.copy
    - content.code.annotate

extra_css:
  - stylesheets/extra.css
  - stylesheets/fonts.css

markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences
  - pymdownx.tabbed:
      alternate_style: true
  # Search for emojis here: https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/#search
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg

# Plugins
plugins:
  - search:
      lang:
        - de
        - en
  - minify:
      minify_html: false
  - macros

# Page tree
nav:
  - Home: ./index.md
  - Docker:
    - docker/index.md
    - Exercises:
      - First steps: docker/exercises/01-first-steps.md
      - Images: docker/exercises/02-images.md
      - Volumes: docker/exercises/03-volumes.md
      - Networks: docker/exercises/04-networks.md
      - Docker Compose: docker/exercises/05-docker-compose.md
      - Dockerfile: docker/exercises/06-dockerfile.md
      - Caching and Multistage: docker/exercises/07-caching-and-multistage.md
      - Debugging: docker/exercises/08-debugging.md
      - Development Environments: docker/exercises/09-dev-environment.md
    - Cheatsheets:
      - First steps: docker/cheatsheets/01-first-steps.md
      - Images: docker/cheatsheets/02-images.md
      - Volumes: docker/cheatsheets/03-volumes.md
      - Networks: docker/cheatsheets/04-networks.md
      - Docker Compose: docker/cheatsheets/05-docker-compose.md
      - Dockerfile: docker/cheatsheets/06-dockerfile.md
      - Debugging: docker/cheatsheets/07-debugging.md
    - Slides:
      - Introduction: docker/01-einfuehrung/#/
      - First steps: docker/02-erste-schritte/#/
      - Images: docker/03-images/#/
      - Volumes: docker/04-volumes/#/
      - Netzwerk: docker/05-netzwerk/#/
      - Docker Compose: docker/06-docker-compose/#/
      - Create images: docker/07-images-erstellen/#/
      - Caching and Multistage: docker/08-caching-und-multistage/#/
      - Docker Architecture: docker/09-docker-architecture/#/
      - Security: docker/10-security/#/
      - Docker on Windows: docker/11-docker-on-windows/#/
      - Debugging: docker/12-debugging/#/
      - Runtime Interna: docker/13-runtime-interna/#/
      - Rootless Docker: docker/14-rootless-docker/#/
      - Development Environment: docker/15-dev-environments/#/
      - CI/CD: docker/16-CI-CD/#/
      - Orchestration: docker/17-Orchestration/#/
  - Exoscale Foundation:
    # - exoscale_foundation/index.md
    - Exercises:
      - Virtual Machines: exoscale_foundation/exercises/01-virtual-machines.md
      - IAM and CLI:     exoscale_foundation/exercises/02-iam-cli.md
      - Simple Object Storage: exoscale_foundation/exercises/03-simple-object-storage.md
    - Demos:
      - First Steps: exoscale_foundation/demos/01-first-steps.md
      - Virtual Machines: exoscale_foundation/demos/02-virtual-machines.md
      - IAM CLI API: exoscale_foundation/demos/03-iam-cli-api.md
      - Simple Object Storage: exoscale_foundation/demos/04-simple-object-storage.md
      - Databases: exoscale_foundation/demos/05-databases.md
  - Git:
    - git/index.md
    - Exercises:
      - My first Project: git/exercises/01-my-first-project.md
      - Basic Commands: git/exercises/02-basic-commands.md
      - Reset and Revert: git/exercises/03-reset-and-revert.md
      - Remote Branches: git/exercises/04-remote-branches.md
      - Merge: git/exercises/05-merge.md
      - Merge with Conflicts: git/exercises/06-merge-with-conflict.md
      - Rebase: git/exercises/07-rebase.md
      - Interactive Rebase: git/exercises/08-interactive-rebase.md
      - Git LFS & Bisect: git/exercises/09-git-lfs.md
      - Gitlab - Issues and Merge Requests: git/exercises/10-gitlab.md
    - Cheatsheets:
      - Introduction: git/cheatsheets/1-introduction.md
      - Changes: git/cheatsheets/2-changes.md
      - Branches: git/cheatsheets/3-branches.md
      - History Rewriting: git/cheatsheets/4-history-rewriting.md
      - Advanced Git Features: git/cheatsheets/5-advanced-git-features.md
    - Slides:
      - Introduction: git/1-introduction/#/
      - Undo changes: git/2-changes/#/
      - Branches 1: git/3-branches/#/
      - Branches 2: git/4-branches-2/#/
      - History Rewriting: git/5-history-rewriting/#/
      - Branching Workflows: git/6-branching-workflows/#/
      - Advanced Features: git/7-advanced-git-features/#/
      - Server & Hosting: git/8-hosting-and-protocols/#/
      - Outro: git/9-outro/#/
  - Impressum: about.md

extra:
  domain: !ENV HOST_DOMAIN
  training_name: !ENV CW_TRAINING_NAME
  alternate:

    # Switch to English
    - name: English
      link: /en/
      lang: en

    # Switch to German
    - name: Deutsch
      link: /de/
      lang: de
