# Cheatsheet 4 - Netzwerke

| Befehl                                                                                       | Aktion                                                                     |
|----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| ```docker run --network <name> <image>```                                                    | Container im festgelegten Netzwerk starten                                 |
| ```docker network create <name>```                                                           | Netzwerk erstellen                                                         |
| ```docker network create --subnet <Netzadresse>/<Maske> <name>```                            | Netzwerk mit bestimmtem Subnetz erstellen                                  |
| ```docker network create --ipv6 --subnet <Netzadresse>/<Maske> <name>```                     | IPv6-Netzwerk mit bestimmtem Subnetz erstellen                             |
| ```docker network connect <name> <containername>```                                          | Bestehenden Container einem Netzwerk hinzufügen                            |
| ```docker network ls```                                                                      | Netzwerke anzeigen                                                         |
| ```docker network inspect <name>```                                                          | Low-level Details zu Netzwerk anzeigen                                     |
| ```docker network rm <name>```                                                               | Einzelnes Netzwerk löschen                                                 |
| ```docker network prune```                                                                   | Alle ungenutzten Netzwerke löschen                                         |

