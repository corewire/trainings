# Cheatsheet 2 - Docker Images

| Befehl                                          | Aktion                                                  |
|-------------------------------------------------|---------------------------------------------------------|
| ```docker pull <image>```                       | Ein Image `<image>` mit dem Tag `@latest` herunterladen |
| ```docker pull <image>:<tag>```                 | Ein Image `<image>` mit dem Tag `<tag>` herunterladen   |
| ```docker pull <server>/<repo>/<image>:<tag>``` | Ein Image von einer privaten Registry herunterladen     |
| ```docker image ls```                           | Lokal vorhandene Images anzeigen                        |
| ```docker login [--username] <server>```        | Bei privater Docker-Registry anmelden                   |
