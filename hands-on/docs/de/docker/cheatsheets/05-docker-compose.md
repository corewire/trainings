# Cheatsheet 5.1 - Docker Compose Befehle

Services werden in der `docker-compose.yml` definiert. Diese muss sich im aktuellen
Verzeichnis befinden. `<Service...>` kann durch einen oder mehrere Services ersetzte werden. Man kann
es auch komplett weg lassen, dann wird der Befehl auf alle Services angewendet.

| Befehl                                              | Aktion                                                            |
|-----------------------------------------------------|-------------------------------------------------------------------|
| ```docker compose up -d <Service...>```             | Einen oder mehrere Services erstellen und starten                 |
| ```docker compose down```                           | Alle Services stoppen und löschen (inkl. Netzwerke)               |
| ```docker compose down -v```                        | Alle Services, Netzwerke, Volumes stoppen und löschen             |
| ```docker compose build <Service...>```             | Einen oder mehrere Services bauen                                 |
| ```docker compose pull <Service...>```              | Die Images von einem oder mehreren Services pullen                |
| ```docker compose up -d --build --pull always <Service...>``` | Services bauen oder pullen; Services starten            |
| ```docker compose logs <Service...>```              | Die Logs von einem oder mehreren Services ausgeben                |
| ```docker compose ps```                             | Die laufenden Container anzeigen lassen                           |
| ```docker compose top <Service...>```               | Die laufenden Prozesse von einem oder mehreren Services auflisten |
| ```docker compose create <Service...>```            | Einen oder mehrere Services erstellen                             |
| ```docker compose start <Service...>```             | Einen oder mehrere Services starten                               |
| ```docker compose stop <Service...>```              | Einen oder mehrere Services stoppen                               |
| ```docker compose rm <Service...>```                | Einen oder mehrere Services löschen                               |
| ```docker compose exec <Service> <Befehl>```        | Befehl in Container mit TTY ausführen                             |
| ```docker compose exec -T <Service> <Befehl>```     | Befehl in Container ohne TTY ausführen                            |


# Cheatsheet 5.2 - docker-compose.yml

Alle Einträge, die mit `cw-` beginnen, sind frei wählbare Namen. Alle Zeilen die
mit `#` anfangen, sind Kommentare.

## Struktur

```yml
services:
  # Einen oder mehrere Services anlegen
  cw-webapp:
    # build verweist auf ein Verzeichnis mit dem Dockerfile.
    # Diese Datei wird gebaut
    build: ./path/to/directory
    # Definiert das Image mit optionalem Tag. Wird heruntergeladen, falls nicht vorhanden
    image: <image>:<tag>
    # Meistens wird entweder 'build' oder 'image' verwendet. Beide können aber auch kombiniert werden. Für mehr Informationen: https://docs.docker.com/reference/compose-file/build/

    # Optional: überschreibt den Entrypoint
    entrypoint: /bin/bash
    # Optional: überschreibt den Befehl
    command: echo $MYSQL_USER

    # Liste abhängiger Dienste
    ## Dieser Dienst (cw-webapp) wird erst gestartet, nachdem alle
    ## abhängigen Dienste gestartet wurden
    depends_on:
      - cw-database

    # Liste der Portweiterleitungen
    ports:
      -  5000:5000

    # Liste der Volumes
    volumes:
      - cw-webapp-volume:/app/data/notes

    # Liste der Netzwerke
    networks:
      - cw-database-network


    # Liste der Umgebungsvariablen
    environment:
      - "MYSQL_USER=example-user"
      - "MYSQL_PASSWORD=..."

  # Zweiter Service:
  cw-database:
    [...]

# (Optional) Ein oder mehrere Managed Volumes anlegen
volumes:
  cw-webapp-volume:

# (Optional) Ein oder mehrere Netzwerke anlegen
networks:
  cw-database-network:
```
