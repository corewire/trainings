# Cheatsheet 3 - Docker Volumes

| Befehl                                                                                       | Aktion                                                                     |
|----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| ```docker run --mount type=bind,source=<Hostpfad>,target=<Containerpfad> <image>```          | Pfad vom Host in einen Container mounten                                   |
| ```docker run --mount type=bind,source=<Hostpfad>,target=<Containerpfad>,readonly <image>``` | Pfad vom Host in einen Container als **readonly** mounten                  |
| ```docker run --mount type=bind,source=${PWD}/<Hostpfad>,target=<Containerpfad> <image>```   | Pfad vom Host relativ zum aktuellen Verzeichnis in einen Container mounten |
| ```docker run --mount source=<Volume>,target=<Containerpfad> <image>```                      | Volume in einen Container mounten                                          |
| ```docker container inspect -f "{% raw %}{{.Mounts}}{% endraw %}" <container>```             | Volumes eines Containers anzeigen                                          |
| ```docker volume create <name>```                                                            | Volume erstellen                                                           |
| ```docker volume ls```                                                                       | Volumes anzeigen                                                           |
| ```docker volume inspect <name>```                                                           | Low-level Details zu Volume anzeigen                                       |
| ```docker volume rm <name>```                                                                | Einzelnes Volume löschen                                                   |
| ```docker volume prune```                                                                    | Alle ungenutzten Volumes löschen                                           |
| ```docker run -v ${PWD}/<Hostpfad>:<Containerpfad>:ro <image>```                             | Alte Schreibweise: Nutzung wie --mount                                     |
