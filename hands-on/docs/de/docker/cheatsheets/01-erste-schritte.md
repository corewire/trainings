# Cheatsheet 1 - Erste Schritte

| Befehl                                                   | Aktion                                                       |
|----------------------------------------------------------|--------------------------------------------------------------|
| ```docker run <image>```                                 | Container im Vordergrund starten                             |
| ```docker run -d <image>```                              | Container im Hintergrund (`detached`) starten                |
| ```docker run --name <container-name> <image>```         | Container starten und mit einem Namen versehen               |
| ```docker run -p <host-port>:<container-port> <image>``` | Container mit Port-Mapping vom Host-System starten           |
| ```docker run -i <image>```                              | Container interaktiv starten                                 |
| ```docker run --rm <image>```                            | Container nach Beendigung direkt löschen                     |
| ```docker ps```                                          | Laufende Container anzeigen                                  |
| ```docker ps -a```                                       | Alle Container anzeigen (laufend, gestoppt)                  |
| ```docker logs <container>```                            | Logs eines Containers anzeigen                               |
| ```docker logs -f <container>```                         | Logs eines Containers anzeigen und auf neue Einträge warten  |
| ```docker inspect <container>```                         | Low-level Informationen eines Containers anzeigen            |
| ```docker start <container>```                           | Einen gestoppten Container starten                           |
| ```docker stop <container>```                            | Einen laufenden Container stoppen                            |
| ```docker restart <container>```                         | Einen Container neustarten                                   |
| ```docker rm <container>```                              | Einen gestoppten Container löschen                           |
