# Cheatsheet 6.1 - Images erstellen

| Befehl                                                     | Aktion                                                   |
|------------------------------------------------------------|----------------------------------------------------------|
| ```docker build .```                                       | Ein Image aus dem Dockerfile im aktuellen Ordner bauen   |
| ```docker build -f <file> .```                             | Ein Image aus <file\> im aktuellen Ordner bauen          |
| ```docker build -t <name> .```                             | Ein Image bauen und mit <name\>:latest taggen            |
| ```docker build -t <name>:<tag> .```                       | Ein Image bauen und mit <name\>:<tag\> taggen            |
| ```docker build --no-cache .```                            | Ein Image ohne Cache bauen                               |
| ```docker build --target <stage> .```                      | Ein Image bis zur Stage <stage\> bauen                   |
| ```docker build --build-arg MYARG=myvalue .```             | Ein Image mit dem Build-Argument MYARG bauen             |
| ```docker tag <image> <name>:<tag>```                      | Ein Image <image\> mit einem weiteren Tag <tag\> taggen  |
| ```docker tag <image> <registry>/<project>/<name>:<tag>``` | Ein Image <image\> für eine private Registry taggen      |
| ```docker image push <name>:<tag>```                       | Ein Tag an die Registry pushen                           |
| ```docker image push --all-tags <name>```                  | Alle Tags an die Registry pushen                         |
| ```docker image prune -a --filter "until=24h"```           | Alle ungenutzten Images, die älter als 24h sind, löschen |
| ```docker system prune```                                  | Alle ungenutzten Docker-Objekte löschen                  |
| ```docker run <image> <args>```                            | Ein Image mit Argumenten starten. Überschreibt CMD       |
| ```docker run --entrypoint="<command>" <image> ```         | Ein Image mit einem anderen Entrypoint starten           |

# Cheatsheet 6.2 - Befehle im Dockerfile

| Befehl                                      | Aktion                                                                     |
|---------------------------------------------|----------------------------------------------------------------------------|
| ```FROM <baseimage>```                      | Baseimage festlegen                                                        |
| ```FROM <baseimage> as <name>```            | Baseimage festlegen und der Stage einen Namen geben                        |
| ```RUN <command>```                         | Einen Befehl im Container ausführen                                        |
| ```ENTRYPOINT ["<arg1>", "<arg2>"]```       | Den Entrypoint festlegen. Wird bei jedem Start eines Containers ausgeführt |
| ```CMD ["<arg1>", "<arg2>"]```              | Default Argumente für den Entrypoint festlegen                             |
| ```WORKDIR <path>```                        | Das Workdir im Container für alle nachfolgenden Befehle festlegen          |
| ```USER <user/user-id>```                   | Den Benutzer im Container für alle nachfolgenden Befehle festlegen         |
| ```EXPOSE <port>```                         | Benötigte Ports dokumentieren                                              |
| ```COPY <source> <target>```                | <source\> vom Host in den Container kopieren                               |
| ```COPY --from=<stage> <source> <target>``` | <source\> von einer vorherigen Stage in die aktuelle Stage kopieren        |
| ```ADD <source> <target>```                 | <source\> in den Container kopieren. Kann ein Host-Pfad oder URL sein      |
| ```ARG <name>[=<default>]```                | Build Argument mit optionalem Defaultwert definieren                       |
| ```ENV <name>=<default>```                  | Umgebungsvariable im Container definieren                                  |
