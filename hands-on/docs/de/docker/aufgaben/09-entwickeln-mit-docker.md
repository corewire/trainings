# Entwickeln mit Docker

!!! goal "Ziel"
    In diesem Projekt geht es darum, für verschiedene Programmiersprachen 
    auf Docker basierende Entwicklungsumgebungen aufzusetzen oder zu nutzen. Sie werden:
    
    - Demoapplikationen in Containern kompilieren
    - die Version von Docker unter der Demoapplikation ändern

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Demoapplikation mit Docker entwickeln

- Wechseln Sie in den Ordner `DEV-docker-demoapp`. In diesem befindet sich ein Setup für die docker-demoapp. Hier sind bereits Scripte für das Ausführen der Tests, Linter und der Applikation selbst vorhanden
- Schauen Sie sich an, wie diese Scripte funktionieren
- Lassen sie zuerst die Tests laufen:

```shell
./run-tests.sh
```

- Anschließend die Linter:

```shell
./run-linter.sh
```

- Starten Sie nun die Anwendung:

```
./run-app.sh
```

Die Demoanwendung ist nun auf Port 5000 erreichbar.

## Aufgabe 2 - Go Demoanwendung

Wechseln Sie in den Ordner `Go-demoapp`. Hier findet sich eine einfaches Go Hello World. Go ist eine Sprache die sich zu ausführbaren Dateien compilieren lässt. Da wir Go nicht installieren wollen, führen wir `go build` nicht direkt sondern in Docker aus.

### 2.1 - Compilieren mit Docker

- Erstellen Sie eine neue Datei `build-app.sh`
- Fügen Sie folgenden Docker Befehl ein:

```shell
docker run --rm -v "$PWD":/usr/src/hello -w /usr/src/hello golang:1.20 go build
```

Das Script mountet den Ordner in den Docker Container und führt dann `go build` aus.

- Machen Sie das Script ausführbar:

```shell
chmod u+x build-app.sh
```

- Bauen Sie die Anwendung:

```shell
./build-app.sh
```

- Führen Sie die Go Anwendung aus:

```shell
./hello
```

### 2.2 Anwendung Entwickeln mit Docker

Während der Entwicklung wollen wir die Anwendung in einem Schritt bauen und ausführen. Erstellen Sie dafür eine neue Datei `run-app.sh` mit folgendem Inhalt:

```shell
docker run --rm -v "$PWD":/usr/src/hello -w /usr/src/hello golang:1.20 go run .
```

- Machen Sie die Datei ausführbar:

```shell
chmod u+x run-app.sh
```

- Führen Sie die Anwendung ohne vorheriges Bauen aus:

```shell
./run-app.sh
```

Ändern Sie den Text der ausgegeben wird und führen Sie die Anwendung erneut aus.

## Aufgabe 3 - Node Demoanwendung

In diesem Teil des Hands-On wollen wir bei einer Node Demoanwendung das eingesetzte Node aktualisieren. Wechseln Sie in den Ordner `Node-Demoapp`. Machen Sie sich mit der Anwendung vertraut. Die Scripte `install.sh`, `run-app.sh` und `run-tests.sh` nutzen hier `docker compose` um die Anwendung auszuführen.

### 3.1 - Entwicklungsumgebung nutzen

- Installieren Sie die Abhängigkeiten der Anwendung:

```shell
./install.sh
```

- Starten Sie die Anwendung

```shell
./run-app.sh
```

- Die Anwendung ist nun auf Port 5173 erreichbar.
- Sie können die Anwendung mit `Strg`+ `C` wieder stoppen.
- Führen Sie die Tests aus:

```shell
./run-tests.sh
```

### 3.2 - Node Updaten

Wir wollen nun unser Projekt auf eine neue Node Version updaten. Die Node Version ist in der Datei `docker-compose.yml` festgelegt.

- Ändern Sie die Node Version auf `20.0.0`.
- Führen Sie die Anwendung erneut aus:

```shell
./run-app.sh
```

- Die Anwendung ist wieder auf Port 5173 erreichbar.

Das Update scheint funktioniert zu haben, aber funktioniert auch alles?

- Schauen Sie sich die Tests an:
    
```shell
./run-tests.sh
```

- Die Tests schlagen fehl. Das liegt daran, dass die neue Node Version nicht mit der Anwendung kompatibel ist. Wir haben hier einen dummy test eingebaut der nun fehlschlägt. Ändern Sie den test so, dass er wieder funktioniert.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Die Tests finden Sie in der Datei `src/__tests__/App.test.tsx`. Ändern Sie in dieser die Version von `v18.16.0` auf `v20.0.0`.

- Führen Sie die Tests erneut aus:

```shell
./run-tests.sh
```

- Die Tests sollten nun wieder grün sein.

Wir haben nun erfolgreich unser Projekt auf eine neue Node Version aktualisiert. Das ganze ohne Node zu installieren.
