# Erste Schritte mit Docker

!!! goal "Ziel"
    In diesem Projekt geht es um die grundlegende Funktionalität von Docker. Sie werden:

    - Container starten und stoppen
    - erste Befehle ausführen
    - den Lebenszyklus von Containern nachverfolgen

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../02-erste-schritte/#/)
    und des [Cheatsheets](../cheatsheets/01-erste-schritte.md) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Schulungsmaschine einrichten

- Die Schulungsmaschinen sind unter `code-{ZAHL}.{{ domain }}` zu erreichen.
Ersetzen Sie `{ZAHL}` mit der Ihnen zugewiesenen Zahl.
  - Beispiel für die Zahl 5: `code-5.{{ domain }}`.
  - Beispiel für die Zahl 11: `code-11.{{ domain }}`.
- Geben Sie danach das Ihnen zugewiesene Passwort ein.
- Sie haben nun Zugriff auf die Schulungsmaschine.

## Aufgabe 2 - Die Demoanwendung

### 2.1: Terminal starten

- Öffnen Sie ein Terminal (Menü > Terminal > New Terminal)

### 2.2: Demoanwendung starten

- Starten Sie die Demo-Anwendung `corewire/docker-demoapp` über das Terminal. Sie
  wird automatisch von Dockerhub heruntergeladen und gestartet.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie folgenden Befehl aus:
    ```
    docker run corewire/docker-demoapp
    ```
    - Mit `Strg + C` können Sie den Container wieder beenden.

### 2.2: Demoanwendung von extern erreichbar machen

Die Demoanwendung läuft nun, ist aber noch nicht von extern erreichbar. Stoppen
Sie die Anwendung und starten Sie sie erneut. Dieses Mal aber mit einer Portweiterleitung
von 8080 -> 5000.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Mit `Strg + C` können Sie den Container wieder beenden.
    - Führen Sie folgenden Befehl aus:
    ```
    docker run -p 8080:5000 corewire/docker-demoapp
    ```

### 2.3: Demoanwendung besuchen

- Besuchen Sie die Demo-Anwendung im Browser
- Sie ist über Ihre URL (`code-{ZAHL}.labs.corewire.de`)
  auf dem Port 8080 zu erreichen. Verwenden Sie `http://` und nicht `https://`.
- Geben Sie 1-2 Notizen ein.
- Schauen Sie im VSCode Terminal nach den Logs. Sie können Ihre Zugriffe auf die Webseite sehen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Öffnen Sie, die Webapp in einem neuen Tab unter `http://code-0.labs.corewire.de:8080/`.
    - Wichtig:
        - `http` **nicht** `https`
        - `code-0` durch Ihre Instanz ersetzen.
    - Bei Erfolg können Sie wieder zur VSCode Instanz wechseln. Im Terminal sehen
      sie in den Logeinträgen, dass Sie auf die Webapp zugegriffen haben.

## Aufgabe 3 - Container starten und stoppen

### 3.1: Container im Vordergrund stoppen

- Stoppen Sie Ihren Container.
- Überprüfen Sie, dass die Webseite nun nicht mehr erreichbar ist.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Klicken Sie auf Ihr Terminal, in dem der Container läuft.
    - Mit `Strg + C` können Sie den Container nun beenden.

### 3.2: Im Hintergrund starten

- Starten Sie die Demoanwendung nun im Hintergrund, sodass die Logs nicht auf dem
  Terminal erscheinen.
- Besuchen Sie die Webseite erneut und überprüfen Sie, dass sie erreichbar ist.
- Fügen Sie Notizen ein.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Container im **Hintergrund** starten:
    ```
    docker run -d -p 8080:5000 corewire/docker-demoapp
    ```

### 3.3 Container im Hintergrund stoppen

- Stoppen Sie den Container nun wieder. Dazu müssen Sie zuerst herausfinden, wie der
  Container heißt.

!!! attention "VSCode Container"
    Die Container `code-server` und `vscode-traefik` stellen das VSCode Programm bereit.
    Diese Container **nicht** stoppen, da ansonsten die Instanz nicht mehr erreichbar ist.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Container stoppen, wenn er im **Hintergrund** gestartet wurde:
        - `docker stop <ID oder Name des Containers>`
        - Den Containernamen können Sie per `docker ps` nachschauen.


## Aufgabe 4 - Container neu starten

`docker run` erstellt jedes Mal einen neuen Container. Das erkennen Sie zum Beispiel
daran, dass die Notizen bei jedem neuen Container nicht mehr vorhanden waren.

In Aufgabe 3.3 haben Sie den Namen des Containers nachgeschaut. Starten Sie diesen
Container nun erneut (nicht über `docker run`), sodass die Notizen wieder angezeigt
werden.

Überprüfen Sie die Erreichbarkeit der Webseite und ob Ihre Notizen wieder angezeigt werden.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Container neu starten: `docker start <ID oder Name des Containers>`.


## Aufgabe 5 - Den Container stoppen und löschen

Stoppen Sie nun den Container wieder. Löschen Sie ihn anschließend.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Stoppen Sie den Container, wie zuvor:
        - `docker stop <ID oder Name des Containers>`
        - Den Containernamen können Sie per `docker ps` nachschauen.
    - Löschen Sie den Container:
        - `docker rm <ID oder Name des Containers>`
