# Docker Compose

!!! goal "Ziel"
    In diesem Projekt geht es um Docker Compose. Sie werden:

    - ein Docker Compose Deployment schreiben
    - sich mit den grundlegenden Befehlen der Compose CLI `docker compose` vertraut machen


!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../06-docker-compose/#/)
    und des [Cheatsheets](../cheatsheets/05-docker-compose.md) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.


## Aufgabe 1 - Erstellen und Verwenden einer docker-compose.yml

In diesem Abschnitt machen Sie sich mit dem [Compose Dateiformat](https://docs.docker.com/reference/compose-file/) vertraut und starten die Docker Demo App mithilfe des Compose CLI `docker compose`.

1. Erstellen Sie einen neuen Unterordner `deployment` des Workspace-Ordners `/home/coder/workspace`.
1. Erstellen Sie im Ordner `deployment` eine neue Datei `compose.yml`.
1. Öffnen Sie die Datei `compose.yml`.
1. Kopieren Sie folgenden Inhalt in die Datei:
    ```yaml
    ---
    services:
      webapp:
        image: corewire/docker-demoapp:1.0.0
        ports:
          -  8080:5000
    ```
    - Im Block `services` definiert man alle Container eines Compose Deployments.
    - `webapp` definiert einen Service. In diesem Service definieren Sie,
        1. ) dass das Image `corewire/docker-demoapp:1.0.0` genutzt wird und
        1. ) ein Portmapping des Container-Ports `5000` auf den Host-Port `8080`, sodass die Anwendung unter `http://code-X.labs.corewire.de:8080` erreichbar ist.


1. Wechseln Sie im Terminal in den Ordner `deployment`.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        - Wenn Sie bereits im Ordner `Workspace` sind können Sie mit `cd` direkt in den neuen Ordner wechseln:
        ```
        cd deployment
        ```
        - Wenn Sie noch in einem Unterordner des `Workspace` befinden, navigieren Sie zunächst in den Workspace-Ordner:
          ```
          cd ..
          ```
          Anschließend wechseln Sie in den richtigen Unterordner:
          ```
          cd deployment
          ```

1. Starten Sie Ihren Service im Hintergrund.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        - Mit `-d` wird der Container im Hintergrund gestartet.
          ```
          docker compose up -d
          ```

    - Sie können sich alle laufenden Container eines Compose Deployments mit dem Befehl `docker compose ps` anzeigen lassen.

    !!! tipp "docker compose up -d"
        - Falls Sie mehr als einen Service definiert haben, startet `docker compose up -d` alle Services.
        - Falls Sie nur einzelne Services starten wollen, nutzen Sie `docker compose up -d <service_name>`

1. Lassen Sie sich die Logs des Services `webapp`  anzeigen.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        - Sie können sich die Logs mit oder ohne die Option `-f` anzeigen lassen. Mit `-f` werden neu erscheinende Logs angezeigt.
          ```
          docker compose logs -f webapp
          ```

1. Öffnen Sie im Browser die URL `http://code-X.labs.corewire.de:8080`, wobei X Ihre Nummer ist. Sie sollten nun die Demo App sehen.
1. Stoppen und entfernen sie den Service wieder:
    ```
    docker compose stop webapp
    docker compose rm webapp
    ```
    oder
    ```
    docker compose down
    ```

    !!! attention "Warnung zu docker compose down"
        - `docker compose down` ist ein sehr destruktiver Befehl.
        - Falls Sie mehr als einen Service definiert haben, wird `docker compose down` alle Services stoppen, alle Container entfernen und alle lokalen Netzwerke entfernen.
        - Falls Sie also nur einzelne Services stoppen wollen, nutzen Sie `docker compose stop` und `docker compose rm`.



## Aufgabe 2 - Volumes in Docker Compose

Sie haben in vorherigen Kapiteln der Schulung gelernt, dass Container `stateless` sind.
Stateless bedeutet, dass Daten, die während der Laufzeit eines Containers geschrieben werden, verschwinden, sobald der Container gelöscht wird.
In diesem Abschnitt werden Sie lernen, wie man im Compose Dateiformat Volumes definiert.

1. Die Docker Demo App legt Notizen, die im Webinterface erstellt werden, standardmäßig unter dem Pfad `/app/data/notes` ab.
1. Fügen Sie zu Ihrer `compose.yml` Datei dem Service `webapp` einen Abschnitt `volumes` hinzu.

    ???+ summary "Volume für Service 'webapp'  "
        === "Volume"
            - Typ: **bind-mount**
            - Lokaler Pfad: **./volumes/webapp-data**
            - Mount Pfad im Container: **/app/data/notes**

    - Wenn Sie die genaue Syntax vergessen haben, nutzen Sie die Schulungsunterlagen oder die Dokumentation des Compose Dateiformats zu [Volumes](https://docs.docker.com/reference/compose-file/volumes/).

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        Die Syntax ist volumes: und darunter mit Einrückung und `-` `<Hostpfad>:<Containerpfad>`:
        ```yaml
        ---
        services:
          webapp:
            image: corewire/docker-demoapp:1.0.0
            ports:
              -  8080:5000
            volumes:
              - ./volumes/webapp-data:/app/data/notes
        ```
        Alternativ gibt es auch eine lange Form, die allerdings voraussetzt, dass der Ordner `/volumes/webapp-data` bereits vorhanden ist:
        ```yaml
        ---
        services:
          webapp:
            image: corewire/docker-demoapp:1.0.0
            ports:
              -  8080:5000
            volumes:
              - type: bind
                source: ./volumes/webapp-data
                target: /app/data/notes
        ```

1. Starten Sie den Webapp Service.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        ```
        docker compose up -d webapp
        ```

1. Sie sollten nun sehen, dass in Ihrem Verzeichnis ein neuer Ordner `volumes` auftaucht, in dem ein Ordner `webapp-data` liegt. Dieser Ordner sollte leer sein.
1. Öffnen Sie nun im Browser die URL `http://code-X.labs.corewire.de:8080`, wobei X Ihre Nummer ist. Sie sollten nun die Demo App sehen.
1. Erstellen eine Notiz im Webinterface der Demo App.
1. Sie sollten nun im Ordner `volumes/webapp-data` sehen, dass eine neue Datei angelegt wurde, ähnlich zu `note_2022-06-16T13:43:23.690467.txt`.
1. Stoppen und entfernen Sie den Service `webapp`.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        Zum Stoppen und Entfernen benutzen Sie folgenden Befehl:
        ```
        docker compose stop webapp
        docker compose rm webapp
        ```
        oder
        ```
        docker compose down
        ```

1. Starten Sie den Webapp Service.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        ```
        docker compose up -d webapp
        ```
1. Öffnen Sie nun wieder das Webinterface der Demo App (`http://code-X.labs.corewire.de:8080`).
1. Sie sollten nun Ihre gerade angelegte Notiz wieder sehen können.


## Aufgabe 3 - Datenbank Service

In diesem Abschnitt werden Sie einen neuen Service definieren, der einen MariaDB Datenbank Server startet.

1. Fügen Sie Ihrer `compose.yml` Datei einen Service `database` hinzu:

    ???+ summary "Datenbank Service 'database' "
        === "Service"
            - Name: **database**
        === "Container image"
            - Image Name: **mariadb**
            - Image Tag: **latest**
            - Referenz:  [https://hub.docker.com/_/mariadb](https://hub.docker.com/_/mariadb)
        === "Environment Variablen"
            - **"MYSQL_USER=example-user"**
            - **"MYSQL_PASSWORD=password"**
            - **"MYSQL_ROOT_PASSWORD=root_password"**
        === "Volume"
            - Typ: **named volume**
            - Name: **database-volume**
            - Mount Pfad im Container: **/var/lib/mysql**
        === "Netzwerke"
            - Name: **database-network**

    - Nutzen Sie dabei die Schulungsunterlagen und die Dokumentation von Compose:
         - [Environment Variablen](https://docs.docker.com/reference/compose-file/services/#environment)
         - [Volumes](https://docs.docker.com/reference/compose-file/volumes/)
         - [Netzwerke eines Services](https://docs.docker.com/reference/compose-file/services/#networks)
         - [Netzwerke in Compose](https://docs.docker.com/reference/compose-file/networks/)

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        ```yaml
        ---
        services:
          webapp:
            image: corewire/docker-demoapp:1.0.0
            ports:
              -  8080:5000
            volumes:
              - ./volumes/webapp-data:/app/data/notes

          database:
            image: mariadb:latest
            environment:
              - "MYSQL_USER=example-user"
              - "MYSQL_PASSWORD=password"
              - "MYSQL_ROOT_PASSWORD=root_password"
            volumes:
              - database-volume:/var/lib/mysql
            networks:
              - database-network

        volumes:
          database-volume:

        networks:
          database-network:
        ```

1. Starten Sie den Service `database`.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        Zum Starten des Services benutzen Sie folgende Eingabe:
        ```
        docker compose up -d database
        ```

1. Lassen Sie sich die Logs des Service `database` ausgeben.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        Zum Ausgeben der Logs benutzen Sie folgende Eingabe:
        ```
        docker compose logs -f database
        ```

1. Wenn der Datenbank Container erfolgreich gestartet hat, sehen Sie folgende Ausgabe am Ende des Logstreams:
    ```
    database_1  | 2022-06-16 14:42:06 0 [Note] Server socket created on IP: '0.0.0.0'.
    database_1  | 2022-06-16 14:42:06 0 [Note] Server socket created on IP: '::'.
    database_1  | 2022-06-16 14:42:06 0 [Note] mariadbd: ready for connections.
    database_1  | Version: '10.8.3-MariaDB-1:10.8.3+maria~jammy'  socket: '/run/mysqld/mysqld.sock'  port: 3306  mariadb.org binary distribution
    ```

## Aufgabe 4 - Verbinden der Webapp mit der Datenbank
In diesem Abschnitt verbinden wir die Docker Demo App mit einer Datenbank, das bedeutet den Service `webapp` mit dem Service `database`.

1. Auf dem Webinterface der Docker Demo App (http://code-X.labs.corewire.de:8080) sehen Sie folgende Fehlermeldung:
    ```
    Connection to MariaDB Server could not be established: Unknown MySQL server host 'database' (-5)
    ```
    Das liegt daran, dass die Docker Demo App mit keiner Datenbank verbunden ist und die Applikation standardmäßig erwartet, dass ein Datenbank Server unter dem DNS Namen "database" erreichbar ist.

1. Passen Sie den Service `webapp` an, indem Sie Environment Variablen und ein Netzwerk hinzufügen:

    ???+ summary "Service 'webapp' "
        === "Environment Variablen"
            - **"DATABASE_HOST=database"**
            - **"DATABASE_PORT=3306"**
            - **"DATABASE_USER=example-user"**
            - **"DATABASE_USER_PASSWORD=password"**
        === "Netzwerke"
            - Name: **database-network**

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        ```yaml
        ---
        services:
          webapp:
            image: corewire/docker-demoapp:1.0.0
            ports:
              -  8080:5000
            volumes:
              - ./volumes/webapp-data:/app/data/notes
            networks:
              - database-network
            environment:
              - "DATABASE_HOST=database"
              - "DATABASE_PORT=3306"
              - "DATABASE_USER=example-user"
              - "DATABASE_USER_PASSWORD=password"

          database:
            image: mariadb:latest
            environment:
              - "MYSQL_USER=example-user"
              - "MYSQL_PASSWORD=password"
              - "MYSQL_ROOT_PASSWORD=root_password"
            volumes:
              - database-volume:/var/lib/mysql
            networks:
              - database-network

        volumes:
          database-volume:

        networks:
          database-network:
        ```


1. Starten Sie den Service `webapp` neu.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        Zum Starten des Services benutzen Sie folgende Eingabe:
        ```
        docker compose up -d webapp
        ```

1. Öffnen Sie `http://code-X.labs.corewire.de:8080`, die Fehlermeldung sollte nun verschwunden sein.
