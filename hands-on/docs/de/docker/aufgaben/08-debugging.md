# Debugging

!!! goal "Ziel"
    In diesem Projekt geht es darum, ein kaputtes Docker Setup zu debuggen und zu
    reparieren. Sie werden:

    - eine nicht-funktionale Demo-App zum Laufen bringen


!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../10-debugging/#/)
    und des [Cheatsheets](../cheatsheets/07-debugging.md) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe - Demoapplikation reparieren

- Wechseln Sie in den Ordner `BROKEN-docker-demoapp`. In diesem befindet sich ein Setup für die docker-demoapp. Leider hat Ihr Kollege, beim Versuch die Anwendung zu verbessern, einiges kaputt gemacht und braucht jetzt Ihre Hilfe um das Ganze wieder zum Laufen zu bringen.

Versuchen Sie die Fehler selbständig Schritt für Schritt zu beheben, bis die Anwendung online erreichbar ist und die Datenbankverbindung funktioniert. Wenn Sie die Anwendung mit `docker compose up -d` starten, wird der Container automatisch gebaut und Sie sehen die Fehler, die im Build Prozess auftreten. Wenn diese behoben sind, können Sie mit `docker compose logs -f` live sehen, was im Container geloggt wird. Nutzen Sie für alles Weitere das [Cheatsheet](../cheatsheets/07-debugging.md).


??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Hier finden Sie Lösungen zu verschiedenen Fehlermeldungen, die im Laufe dieses Prozesses auftauchen können.

    - Fehler bei beliebigem Docker Compose Befehl:

    ??? failure "ERROR: The Compose file './docker-compose.yml' is invalid..."
        In der `docker-compose.yml` fehlt in Zeil 6 ein `-`. Verändern Sie `./volumes/webapp-data:/data/notes` zu `- ./volumes/webapp-data:/data/notes`.

    - Fehler beim Bauen des Containers:

    ??? failure "Error response from daemon: dockerfile parse error line 18: unknown instruction: COPI"
        Ändern Sie in Zeile 18 im Dockerfile den Befehl `COPI` zu `COPY`.

    ??? failure "ERROR: Could not open requirements file: [Errno 2] No such file or directory: 'requirements.txt'"
        In Zeile 14 im Dockerfile hat sich ein Tippfehler eingeschlichen. Ändern Sie `WORKDIR /ap` zu `WORKDIR /app`. Damit wird der `pip` Befehl dann in dem Ordner ausgeführt in den auch die `requirements.txt` kopiert wurde.

    ??? failure "ERROR: Could not install packages due to an OSError: [Errno 13] Permission denied: '/home/app-runner'"
        Der Befehl `pip install` sollte hier noch als `root` ausgeführt werden, damit er die Rechte für die Installation hat. Verschieben Sie dafür die Zeile 12 mit `USER app-runner` unter den `pip install` Befehl.

    - Fehler nach dem Starten des Containers:

    ??? failure "ERROR: Connection to MariaDB Server could not be established: Unknown MySQL server host 'database' (-3)"
        Das Netzwerk der Datenbank ist nicht an die Webapp angeschlossen. Erweitern Sie die Definition des `webapp` services in der `docker-compose.yml` um das Netzwerk:
        ```yaml
        networks:
          - database-network
        ```

    - Fehler nach Eingabe einer Notiz:

    ??? failure "PermissionError: [Errno 13] Permission denies: '/data/notes/note_....txt'"
        Da der gemountete Ordner `webapp-data` nur eine Freigabe für root und nicht für den Nutzer besitzt, können die Notizen hier nicht gesichert werden. Die Rechte des Ordners müssen also auf die ID des Nutzers geändert werden.
        Wechseln Sie deshalb in den Ordner `volumes/webapp-data` und geben Sie folgenden Befehl ein:
        ```
        chown 10100:10100 .
        ```
