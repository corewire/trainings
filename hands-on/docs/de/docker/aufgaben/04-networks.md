# Umgang mit Netzwerken

!!! goal "Ziel"
    In diesem Projekt geht es darum, grundlegende Netzwerkkonzepte von Docker kennen zu lernen. Sie werden:

    - ein eigenes Netzwerk erstellen
    - die Verbindung zwischen zwei Containern herstellen
    - ein eigenes IPv6-Netzwerk mit bestimmten Subnetzen erstellen


!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../05-netzwerk/#/) und des [Cheatsheets](../cheatsheets/04-networks.md) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Vorbereitung - VSCode einstellen

Vergewissern Sie sich zuerst, dass Sie wieder im workspace Ordner sind. In diesem Hands-On werden wir die Verbindung zwischen zwei Containern in verschiedenen Szenarien betrachten. Das ist am einfachsten, wenn die zwei Container in gesplitteten Terminal Fenstern nebeneinander offen sind. Öffnen Sie dafür VSCode, öffnen Sie ein Terminal und klicken Sie auf  ![Split Button](./img/split_button.png). Ihr VSCode sollte danach etwa so aussehen:
![Split View](./img/split_cmd.png)

## Aufgabe 1 - Default Bridge Netzwerke

### 1.1: Container starten

Starten Sie im linken und rechten Terminal jeweils einen Container mit:
```shell
docker run -it --rm corewire/network-playground /bin/bash
```
Für dieses Hands-On haben wir einen kleinen Alpine basierten Container erstellt, in dem Netzwerk Diagnose Tools vorinstalliert sind.

### 1.2: Netzwerkinformationen herausfinden

Finden Sie für beide Container die IP Adressen heraus mit:
```shell
ip a
```

### 1.3: Containerverbindung testen

- Führen Sie im Container im rechten Terminal folgenden Befehl aus:
```
tcpdump icmp
```
Der Container überwacht jetzt live allen ICMP Traffic, also alle PING Pakete.
- Führen Sie im Container im linken Terminal einen Ping zum Container im rechten Terminal aus:
```
ping <container ip>
```
Sie sehen nun im rechten Terminal, wie der Traffic beim Container ankommt.

## Aufgabe 2 - User-defined bridge Netzwerke

### 2.1: Eigenes Netzwerk anlegen

- Beenden Sie den `tcpdump` im rechten Terminal mit `Strg`+`c` und verlassen Sie den Container mit `exit`.
- Erstellen Sie ein neues Netzwerk mit dem Namen `my-network`.
??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Führen Sie in einem der beiden Terminals folgenden Befehl aus:
    ```shell
    docker network create my-network
    ```
- Inspizieren Sie Ihr Netzwerk und prüfen Sie, welches Subnetz in my-network verwendet wird.
??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Führen Sie in einem der beiden Terminals folgenden Befehl aus:
    ```shell
    docker network inspect my-network
    ```

### 2.2: Container mit Netzwerk starten

- Starten Sie im rechten Terminal wieder den Container, diesmal aber mit dem gerade erstellten Netzwerk:
```shell
docker run -it --rm --network=my-network corewire/network-playground /bin/bash
```
- Starten Sie wieder `tcpdump`, fragen Sie die **neue** IP-Adresse mit `ip a` ab und versuchen Sie, wie oben, den rechten Container aus dem linken heraus zu erreichen. Das ist in diesem Fall nicht erfolgreich, da die Container in unterschiedlichen Netzwerken sind.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Führen Sie im Container im rechten Terminal folgenden Befehl aus:
    ```
    tcpdump icmp
    ```
    Finden Sie nun die **neue** IP mit folgendem Befehl heraus:
    ```
    ip a
    ```
    Der Container überwacht jetzt live allen ICMP Traffic, also alle PING Pakete.
    Führen Sie im Container im linken Terminal einen Ping zum Container im rechten Terminal aus:
    ```
    ping <container ip>
    ```
    Sie sehen hier keinen Traffic beim Container ankommen, da die Container in unterschiedlichen Netzwerken sind.

- Stoppen Sie den linken Container mit `exit`.
- Starten Sie den linken Container im neuen Netzwerk.
??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Um das Netzwerk im linken Container zu starten, nutzen Sie folgenden Befehl:
    ```shell
    docker run -it --rm --network=my-network corewire/network-playground /bin/bash
    ```
- Testen Sie erneut die Verbindung zwischen den Containern mit `tcpdump` und `ping`.
- Die Verbindung ist nun wieder möglich.

### 2.3: Nutzung des DNS

- Stoppen Sie beide Container wieder und starten Sie diese zusätzlich mit einem Namen. Dieser kann über `--name=<container name>` gesetzt werden.
- Testen Sie die Verbindung erneut mit `tcpdump` und `ping`, nutzen Sie dabei den vergebenen Namen als Zieladresse.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Linkes Terminal:
    ```shell
    docker run -it --rm --network=my-network --name=left corewire/network-playground /bin/bash
    ```

    Rechtes Terminal:
    ```shell
    docker run -it --rm --network=my-network --name=right corewire/network-playground /bin/bash
    ```

    ```shell
    tcpdump icmp
    ```

    Linkes Terminal:
    ```shell
    ping right
    ```

## Aufgabe 3: IPv6-Netzwerke

- Stoppen Sie wieder beide Container.
- Legen Sie ein neues Netzwerk `my-ipv6-network` an mit dem spezifizierten Subnetz `fd00:0:0:1::/64` an
??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Um das Netzwerk in einem der Container zu erstellen, nutzen Sie folgenden Befehl:
    ```shell
    docker network create --ipv6 --subnet fd00:0:0:1::/64 my-ipv6-network
    ```
- Starten Sie die beiden Container nun im `my-ipv6-network` Netzwerk.
??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Linkes Terminal:
    ```shell
    docker run -it --rm --network=my-ipv6-network --name=left corewire/network-playground /bin/bash
    ```

    Rechtes Terminal:
    ```shell
    docker run -it --rm --network=my-ipv6-network --name=right corewire/network-playground /bin/bash
    ```
- Mit `tcpdump icmp6` können Sie nun die ipv6-Pakete beobachten.
??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Rechtes Terminal:
    ```shell
    tcpdump icmp6
    ```

    Linkes Terminal:
    ```shell
    ping6 right
    ```
