# Docker & Container

- [Zu den Aufgaben](./aufgaben/01-erste-schritte-mit-docker.md)
- [Zu den Cheatsheets](./cheatsheets/01-erste-schritte.md)
- [Zu den Folien](./01-einfuehrung/#/)

## Dauer

3 Tage

## Zielgruppe

- Softwareentwickler:innen
- Software-Architekt:innen
- Systemadministrator:innen
- DevOps-Engineers

## Voraussetzungen

- Erste Erfahrungen mit dem Terminal/Bash
- Arbeiten mit Dateien und Verzeichnissen im Terminal
- Grundbegriffe der Netzwerktechnologie (IP-Adressen, Ports, DNS)
- Grundlegendes Verständnis von Softwareentwicklung und IT-Infrastrukturen
- Vertrautheit mit den Konzepten der Virtualisierung

## Kursziel

Der Kurs vermittelt die Verwendung von Docker in der Praxis und die technischen Hintergründe dazu.
Die Nutzung wird sowohl unter Windows als auch Linux erklärt, wobei der Schwerpunkt auf Linux liegt.
Die Inhalte lassen sich jedoch problemlos auf Windows übertragen.
Am ersten Tag werden Anwendungsfälle und Grundlagen von Containern behandelt.
Die Anwendungsfälle geben einen Einblick, in welchen Situationen die Verwendung von Containern sinnvoll ist.
Die Grundlagen ermöglichen den Teilnehmer:innen den sicheren Umgang mit Containern.
Der zweite Tag beinhaltet Docker Compose, das Erstellen von eigenen Images sowie das Absichern von Containern.
Mit diesem Wissen lassen sich Container selbst entwickeln und sicher betreiben.
Der dritte Tag behandelt einen tiefen Einblick in das Debugging mit Containern, die Docker-Architektur und den Einsatz von Containern in Entwicklungsumgebungen, CI/CD Systemen und Clustern.
Der Kurs richtet sich somit auch explizit an Teams mit unterschiedlichen Erfahrungsleveln zu Docker und Container.

## Schulungsform

Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt

### 1. Tag
- Motivation, Grundbegriffe und Anwendungsfälle für Container
- Vergleich von Entwicklungsumgebungen mit und ohne Container
- Vergleich von Deploymentszenarien mit und ohne Container
- Abgrenzung Bare Metal, VMs und Container
- Container vs. Docker
- Container-Lifecycle, bestehende Container starten/stoppen
- Container-Images, Tags, Registry
- Volumes, Volume-Types und Persistenz
- Netzwerken und interne/externe Erreichbarkeit

### 2. Tag
- Service-Architekturen mit Docker Compose abbilden
- Designen von containerbasierten Anwendungen
- Erstellen von eigenen Images (Dockerfile)
- Baseimage, Layer und Caching, Multistage, Dockerfile Best Practices
- Grundlagen zur Docker-Architektur (Docker CLI, Docker Daemon)
- Security Best Practices mit dem Absichern von Images, Containern, dem Docker-Host, dem Docker Daemon
- Docker unter Windows mit Linux-/Windows-Container, WSL2, Docker Desktop

### 3. Tag
- Erweitertes Debugging von Containern
- Docker Architektur mit containerd, runc, Kernel (Namespaces, cgroups)
- Container ohne Docker/alternativen Runtimes
- Rootless Docker
- Containerbasierte Entwicklungsumgebungen
- CI/CD mit und für Container
- Ausblick Container Orchestrierung (Kubernetes)
