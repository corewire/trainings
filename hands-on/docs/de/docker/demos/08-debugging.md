# Debugging

Das hier auschecken:
<https://gitlab.com/corewire/hands-on/docker-demoapp/-/merge_requests/4>

Dann folgende Probleme beheben:

## Vorbereitung

```
# Prepare repo
mkdir /tmp/debuggingdemo
cd /tmp/debuggingdemo
git clone git@gitlab.com:corewire/hands-on/docker-demoapp.git
cd docker-demoapp
git switch broken-version-showcase

# Remove old image
docker image rm webapp
docker image prune
```

## Dockerfile

```
docker compose build
```

- db-volume -> database-volume
- apt install -y Flag fehlt
- fehlendes gcc in apt

```
docker compose ps
```
- Tippfehler in ENV FLASK_APP=docker/app -> ENV FLASK_APP=dockerdemo/app

## app.py

```
docker compose up
```

- Syntaxfehler in Zeile 13

## Dockerfile

Zeige
```
docker compose exec webapp sh
```
- Container nur auf localhost erreichbar
  - CMD um --host=0.0.0.0 erweitern

## Docker Compose

Zeige
```
docker compose logs
```
- Datenbank Passwort falsch

## File permissions

- Falsch gesetzt auf lokalem volume ordner

```
chown 10100:10100 -R volumes
```

