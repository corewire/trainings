# Entwickeln mit Docker

In dieser Demo soll gezeigt werden wie Docker basierte Entwicklungsumgebungen das Onboarding neuer Mittarbeiter:innen vereinfachen und generell den Entwicklungsworkflow verbessern.

## Entwicklungsumgebung manuell aufsetzen

Erst zeigen wir wie aufwändig es ist das ganze von Hand aufzusetzen.

- Öffnen der `docker-demoapp`.
- Installieren der Abhängigkeiten wie in der README beschrieben.
- Im Virtualenv:

```shell
pytest
```

```shell
black --check .
```

```shell
cd dockerdemo
flask run
```

Hier erwähnen, dass das ganze einen Demoapplikation ist und in der Realität alles noch viel aufwändiger ist.

## Entwicklungsumgebung mit Docker

Wir erstellen jetzt gemeinsam eine Entwicklungsumgebung mit Docker.

- Dockerfile anpassen, so dass die test requirements in eigener Test-Stage installiert werden:

```dockerfile
# build stage
FROM python:3.10-slim AS build

RUN apt-get update && apt-get install -y \
        gcc \
        libmariadb3 \
        libmariadb-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY ./dockerdemo ./dockerdemo
ENV FLASK_APP=dockerdemo/app

# testEnv stage
FROM build AS testEnv

COPY test-requirements.txt /app/test-requirements.txt
RUN pip install -r test-requirements.txt

# prodEnv stage
FROM build AS prodEnv

CMD ["flask", "run", "--host=0.0.0.0"]
```

Manuell Container bauen:

```shell
docker build --target=testEnv -t testdemoapp .
```

Manuell Container starten und rein verbinden:

```shell
docker run -it testdemoapp /bin/bash
```

```shell
pytest
```

```shell
black --check .
```

- Aus dem container ausloggen.
- Neue Datei `run-tests.sh` erstellen:

```
docker build --target=testEnv -t testdemoapp .
docker run testdemoapp pytest
```

```shell
chmod u+x run-tests.sh
```

```shell
./run-tests.sh
```

Das ganze wiederholen für

- `run-linter.sh`:

```shell
docker build --target=testEnv -t testdemoapp .
docker run testdemoapp black --check .
```

- `run-app.sh` in der Production-Stage mit docker compose für die Datenbank:

```shell
docker build --target=prodEnv -t testdemoapp .
docker compose up
```

- Erklären warum jetzt alles toll für neue Entwickler:innen ist.




