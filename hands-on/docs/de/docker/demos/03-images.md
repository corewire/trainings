# Images

## Docker Registry zeigen

- Images auf Docker Hub mit Tags zeigen
  nginx (https://hub.docker.com/_/nginx)


## Docker pull ohne Tag

- pull ohne Tag nutzt default-Tag (latest)

```
docker pull nginx
```
```
docker image ls
```

## Docker pull mit Tags

- pull mit Tag empfohlen, Beispiel nginx:1.24.0

```
docker pull nginx:1.24.0
```
```
docker image ls
```

## Docker pull zeigt auf selben Tag

- pull mit respective Link hat selbe ID und Größe

```
docker pull nginx:1.24
```
```
docker image ls
```
