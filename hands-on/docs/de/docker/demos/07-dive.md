# Dive Demo

- Dockerfile aus den Folien bauen und testen:

```
FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y git
RUN rm -rf /var/lib/apt/lists/*
```

```
docker build -t dive-demo .
dive dive-demo
```

- Dockerfile anpassen und erneut bauen und checken.

```
FROM ubuntu:20.04

RUN apt-get update \
  && apt-get install -y git \
  && rm -rf /var/lib/apt/lists/*
```
