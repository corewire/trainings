# Intro und Dockerhub

## Intro

Motivation: Erste Schritte mit Docker sollen gezeigt werden, um ein Gefühl dafür zu bekommen: starten, stop, alles sehr leichtgewichtig und einfach

- Ersten Container starten und im Browser zeigen:
  ```
  docker run -d -p 8000:80 nginx
  ```

- Weitere Container starten:
  ```
  docker run -d -p 8001:80 nginx
  ```

  ```
  docker run -d -p 8002:80 httpd
  ```

- Geht sehr schnell, wenig Overhead, ...

- Laufende Container anzeigen

  ```
  docker ps
  ```

- Container stoppen, Effekt im Browser zeigen
  ```
  docker stop
  ```

  ```
  docker ps
  ```

- Unterschiede der Innen- und Außenansicht zeigen:

  ```
  # Auf dem Host:
  htop
  ```

  Im Container

  ```
  docker exec -it <container> bash
  ```

  ```
  apt update && apt install htop
  ```

  ```
  htop
  ```

  Alternativ
  
  ```
  docker top
  ```

## Dockerhub zeigen

<https://hub.docker.com/>