#  Docker Architektur und Runtimes

## Gvisor Showcase

- [Gvisor](https://gvisor.dev/) ist eine Sandbox Runtime für Container
- Gvisor ist eine Alternative zu default runtime runC
- GVisor installieren:
  1. Dependencies
  ```bash
  sudo apt-get update && \
  sudo apt-get install -y \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg
  ```
  1. Apt-Key hinzufügen
  ```bash
  curl -fsSL https://gvisor.dev/archive.key | sudo gpg --dearmor -o /usr/share/keyrings/gvisor-archive-keyring.gpg
  ```
  1. Apt-Source hinzufügen
  ```bash
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/gvisor-archive-keyring.gpg] https://storage.googleapis.com/gvisor/releases release main" | sudo tee /etc/apt/sources.list.d/gvisor.list > /dev/null
  ```
  1. Gvisor installieren
  ```
  sudo apt-get update && sudo apt-get install -y runsc
  ```
  1. Runtimeargs der runsc runtime konfigurieren in `/etc/docker/daemon.json`:
    ```json
      "runtimes": {
          "runsc": {
              "path": "/usr/bin/runsc",
               "runtimeArgs": [
                  "-overlay2=all:memory"
              ]
          }
      }
    ```
    - "-overlay2=all:memory" isoliert das host filesystem vom container filesystem
  1. docker daemon neu starten
  ```
  sudo service docker restart
  ```
  1. Gibt es jetzt eine neue Runtime?
  ```
  $ docker info | grep "Runtimes"
  Runtimes: runc runsc io.containerd.runc.v2
  ```
  1. Wir können nun runsc als Runtime für einen Container verwenden.


### dmesg vergleichen
- RunC als Runtime für einen Container verwenden:
  ```
  $ docker run --rm --runtime=runc alpine dmesg
  dmesg: klogctl: Operation not permitted
  ```
- runSC als Runtime:
  ```
  $ docker run --rm --runtime=runsc alpine dmesg
  [   0.000000] Starting gVisor...
  [   0.316254] Gathering forks...
  [   0.588455] Singleplexing /dev/ptmx...
  [   0.812510] Recruiting cron-ies...
  [   0.868245] Mounting deweydecimalfs...
  [   0.904474] Moving files to filing cabinet...
  [   1.387169] Searching for socket adapter...
  [   1.481630] Creating process schedule...
  [   1.897891] Reading process obituaries...
  [   1.900038] Consulting tar man page...
  [   2.290297] Synthesizing system calls...
  [   2.364210] Setting up VFS...
  [   2.773802] Setting up FUSE...
  [   2.996845] Ready!
  ```

### Kernel Versionen vergleichen
- RunC als Runtime für einen Container verwenden:
  ```
  $ docker run --rm --runtime=runc alpine uname -r
  5.10.16.3-microsoft-standard-WSL2
  ```
- GVisor runsc als Runtime für einen Container verwenden:
  ```
  $ docker run --rm --runtime=runsc alpine uname -r
  4.4.0
  ```
- uname -r gibt die Kernel Version aus
- Was ist der Unterschied?
- GVisor ist Sandbox Runtime
- Nutzt eigene Kernel Implementation
- Das sieht man an der Kernel Version die ausgegeben wird.

### Root mount angriff
1. Erzeuge container mit default runtime runC
```
docker run --rm -it --runtime=runc -v /:/hostfs ubuntu /bin/bash
```
1. Erzeuge ein file in /hostfs/tmp
```
echo "malicious" > /hostfs/tmp/malicious
```
1. container verlassen
1. Zeige dass auf dem host nun ein file geschrieben wurde:
  ```
  cat /tmp/malicious
  ```
1. Erzeuge container mit default runtime runSC
```
docker run --rm -it --runtime=runsc -v /:/hostfs ubuntu /bin/bash
```
1. Erzeuge ein file in /hostfs/tmp
```
echo "malicious2" > /hostfs/tmp/malicious2
```
1. container verlassen
1. Zeige dass auf dem host kein file geschrieben wurde:
  ```
  cat /tmp/malicious2
  ```
1. Was ist passiert?
1. GVisor isoliert das host filesystem vom container filesystem
1. Man kann zwar noch lesend zugreifen, aber nicht mehr schreibend.


## Dirty cow Showcase
 TODO
 // JL: Wäre mega nice, aber schwer umzusetzen für den moment - man braucht zumindest einen kernel der anfällig ist für dirty cow.
 // Ggf geht das mit einer microVM.


## Podman Showcase

- Podman ist eine Alternative zu Docker
- Podman ist ein daemonless container engine
- Podman ist ein drop-in replacement für `docker`
- Podman kann auch mit `docker` images umgehen

### Podman installieren
- https://podman.io/docs/installation

```
sudo apt-get install podman
```

### Demo

- Ähnliche Befehle wie bei Docker
- Clone https://gitlab.com/corewire/hands-on/docker-demoapp
- Image bauen:
  ```
  podman build -t demoapp-podman .
  ```
- Image ausführen:
  ```
  podman run --rm -p 5000:5000 demoapp-podman
  ```
- Selbes Image mit Docker ausführen:
  ```
  docker run --rm -p 5000:5000 demoapp-podman
  ```
- Umgekehrt geht auch:
  ```
  docker build -t demoapp-docker .
  ```
- Selbes Image mit Docker ausführen:
  ```
  podman run --rm -p 5000:5000 demoapp-docker


