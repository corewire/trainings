# Pipelines mit Docker

In dieser Demo soll eine einfache Pipeline mit Docker gezeigt werden.

Die Umgebung für die Demo findet sich unter:

- [https://gitlab.com/corewire/hands-on/docker-demoapp/-/merge_requests/13](https://gitlab.com/corewire/hands-on/docker-demoapp/-/merge_requests/13)

## .gitlab-ci.yml

- Erkläre, dass die Pipeline in der Datei `.gitlab-ci.yml` definiert wird.
- Öffne die Datei `.gitlab-ci.yml` aus dem Merge request heraus:
    - Changes -> Klick auf die drei Punkte -> View file @ xxxxxxxx
- Erkläre, die Inhalte der Datei.

## Pipeline starten

- Starte die Pipeline manuell:
    - CI/CD ->  Pipelines -> Run Pipeline
    - Branch: `example-pipeline`

## Pipeline Ergebnis

- Zeige wie die Pipeline läuft
- Zeige die Ergebnisse der einzelnen Jobs insbesondere die Ausgabe des Jobs `Lint`






