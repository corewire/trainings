# Networks

## Netzwerke anzeigen

IP-Adressbereiche der Netzwerke anzeigen

```
docker network ls
docker network inspect bridge
docker network create demo
docker network inspect demo
```

## Container starten

```
docker run -d -p 8080:80 nginx
docker inspect <container-id>
```

Container erreichbar über `localhost:8080` oder `<ip>:80`

```
docker network connect demo <container-id>
docker inspect <container-id>
```

Container hat nun 2 IP-Adressen, Alias wird angezeigt. Wenn man möchte kann man noch einen Container mit --network starten oder 2 im gleichen Netz und dann über Aliase pingen.
