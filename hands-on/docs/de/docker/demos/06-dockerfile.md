# Dockerfile

Dockerfile liegt im [Repo](https://gitlab.com/corewire/hands-on/docker-demoapp/-/blob/main/Dockerfile). Kann einfach Schritt für Schritt aufgebaut werden:

Vorschlag 1. Version:

```docker
FROM python:3.10-slim

RUN apt-get update && apt-get install -y \
        gcc \
        libmariadb3 \
        libmariadb-dev \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r requirements.txt

COPY ./dockerdemo ./dockerdemo

CMD ["flask", "run"]
```

Starten, geht kaputt mit 'Could not locate a Flask application. You did not provide the "FLASK_APP" environment variable, [...]'. Folgendes hinzufügen

```docker
ENV FLASK_APP=dockerdemo/app
```

Ist aber noch nicht erreichbar. CMD erweitern:

```docker
CMD ["flask", "run", "--host=0.0.0.0"]
```
