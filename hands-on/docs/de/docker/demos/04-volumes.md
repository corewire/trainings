# Volumes

## Volumes mit Nginx

```
mkdir -p /tmp/dockerdemo
cd /tmp/dockerdemo
docker run -d -p 8080:80 --mount type=bind,source=/tmp/dockerdemo,target=/usr/share/nginx/html,readonly nginx:latest
```

Beispiel Index.html
```
<html>
<body>
<h1>Hello World</h1>
</body>
</html>
```

- Zeigen wie Nginx die Datei ausliefert, Datei kann auf Host verändert werden und Nginx liefert entsprechend den aktuellen Stand aus.

## Unterschied --mount und -v

```
docker run -d -p 8080:80  --mount type=bind,source=/tmp/dockerdemo/directory,target=/usr/share/nginx/html,readonly   nginx:latest
# -> Fehler
docker run -d -p 8080:80  -v /tmp/dockerdemo/directory:/usr/share/nginx/html:ro   nginx:latest
ls -la
# -> Verzeichnis angelegt
```

## Nutzerrechte bei Linux

Volume erstellen

```
docker volume create my-vol
docker volume ls
docker inspect my-vol
```

Ubuntu als Root starten, Datei anlegen
```
docker run -it --rm --user root -v my-vol:/input ubuntu bash
```

Ubuntu als User 1000 starten, Datei anlegen, versuchen Datei von root zu lesen/schreiben
```
docker run -it --rm --user 1000 -v my-vol:/input ubuntu bash

# Ohne nano/vi schreiben
echo "Test" >> /input/test.txt
```
