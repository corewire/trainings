# Rootless Docker

In dieser Demo soll gezeigt werden wie rootless Docker funktioniert und welche Sicherheitsvorteile es bietet.

!!! goal "Vorbereitung"
    Für die Demo muss rootless Docker eingerichtet werden. Zu Beginn der Demo sollte Docker noch rootful sein. Sollte docker bereits rootless sein, kann es wie folgt gestoppt und im rootfull Modus gestartet werden:

    ```shell
    dockerd-rootless-setuptool.sh uninstall
    sudo systemctl start docker
    ```

```shell
sudo su
```

## Rootfull Docker demonstrieren

- Container starten

```shell
docker run -d -p 8080:80 nginx
```

- Container id ausgeben

```shell
docker ps
```

- Container Prozesse anzeigen

```shell
docker top <container-id>
```

- Nginx Prozess anzeigen

```shell
ps u -q <pid>
```

- Nginx stoppen
- Interaktiven Container starten

```shell
docker run -it --rm -v /etc:/host-etc ubuntu /bin/bash
```

- Host Dateien anzeigen

```shell
cd /host-etc
ls
cat shadow
```

Hier erklären, dass das ein Sicherheitsproblem ist.

- Aus dem Container ausloggen

## Rootless Docker einrichten

- Rootshell verlassen
- Docker command ohne root ausführen:

```shell
docker ps
```
- Container starten

```shell
docker run -d -p 8080:80 nginx
```

- Container id ausgeben

```shell
docker ps
```

- Container Prozesse anzeigen

```shell
docker top <container-id>
```

- Nginx Prozess anzeigen

```shell
ps u -q <pid>
```

- Nginx stoppen
- Interaktiven Container starten

```shell
docker run -it --rm -v /etc:/host-etc ubuntu /bin/bash
```

- Host Dateien anzeigen

```shell
cd /host-etc
ls
cat shadow
```

Hier die Sicherheitsimplikationen erklären.

