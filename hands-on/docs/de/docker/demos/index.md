# Timetable

Connect to code-<x>.labs.corewire.de via SSH:

```
ssh -i id_training_key root@code-0.labs.corewire.de
```

where id_training_key is the SSH-Key for this training.

## 1. Tag
- Gesamt: **5h 35min ohne Pausen**
- 01-Einführung (Dauer: 1,25h)
    - Showcase: [Intro und Dockerhub](./01-erste-schritte/)
- 02-Erste Schritte (Dauer: 30 min + Hands-on: 20 min)
    - Showcase: [Schulungsumgebung](./02-schulungsumgebung/)
    - Hands-on: [Erste Schritte](../aufgaben/01-erste-schritte-mit-docker/)
- 03-Images (Dauer: 30 min + Hands-on: 30 min)
    - Showcase: [Images](./03-images/)
    - Hands-on: [Umgang mit Images](../aufgaben/02-images/)
- 04-Volumes (Dauer: 45 min + Hands-on: 30 min)
    - Showcase: [Volumes](./04-volumes/)
    - Hands-on: [Umgang mit Volumes](../aufgaben/03-volumes/)
- 05-Networks (Dauer: 45 min + Hands-on: 30 min)
    - Showcase: [Networks](./05-networks/)
    - Hands-on: [Umgang mit Netzwerken](../aufgaben/04-networks/)

## 2. Tag

- Gesamt: **6h 15min ohne Pausen**
- Rückblick (Dauer: 15min)
- 06-Docker-Compose (Dauer: 1h + Hands-on: 30 min)
    - Showcase: [Docker Compose](./05-docker-compose/)
    - Hands-on: [Docker Compose](../aufgaben/05-docker-compose/)
- 07-Dockerfile (Dauer: 1h + Hands-on: 30 min)
    - Showcase: [Dockerfile](./06-dockerfile/)
    - Hands-on: [Dockerfile](../aufgaben/06-dockerfile/)
- 08-Caching und Multistage (Dauer: 1,5h + Hands-on: 30 min)
    - Showcase: [Dive-Demo](./07-dive/)
    - Showcase: [Multistage Build](./07-caching-und-multistage/)
    - Hands-on: [Caching und Multistage](../aufgaben/07-caching-und-multistage/)
- 09-Docker Architektur (Dauer: 15min)
- 10-Security (Dauer: 30min)
- 11-Docker on Windows (Dauer: 15min)

## 3. Tag

- Gesamt: **5h 45min ohne Pausen**
- Rückblick (Dauer: 15min)
- 12-Debugging (Dauer: 1h + Hands-on: 45 min)
    - Showcase: [Debugging](./08-debugging/)
    - Hands-on: [Debugging](../aufgaben/08-debugging/)
- 13-Internals (Dauer: 1h)
    - Showcase: [Docker Architektur und Runtimes](./13-arch-runtimes/)
- 14-Rootless-Docker (Dauer: 30 min)
    - Showcase: [Rootless Docker](./11-rootless-docker/)
- 15-Dev-Envs (Dauer: 30 min + Hands-on: 30 min)
    - Showcase: [Entwickeln mit Docker](./10-entwickeln-mit-docker/)
- 16-CI/CD (Dauer: 30 min)
    - Showcase: [Pipelines mit Docker](./12-pipelines/)
- 17-Orchestrierung (Dauer: 45 min)
    - Showcase: [Orchestrierung](./14-orchestrierung/)
