# AWS - Relational Database Service

- [Zu den Aufgaben](./aufgaben/01-mysql-datenbank-erstellen.md)

## Dauer
1 Tag

## Zielgruppe
- AWS-RDS-Nutzer:innen

## Voraussetzungen
- Erste Erfahrungen mit AWS allgemein
- (optional) Erfahrungen mit AWS RDS Grundlagen

## Kursziel
Der Kurs vermittelt den Umgang mit AWS RDS in der Praxis.

## Schulungsform
Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **40% theoretischen Inhalten** und **60% praktischen Übungen**.

## Inhalt
- RDS MySQL Instanz erstellen
- Patchmanagement und Scaling
- RDS IAM Authentifizierung
- Backup und Restore
- RDS Monitoring
- Ressourcen mit Terraform anlegen
- Netzwerk und Erreichbarkeit