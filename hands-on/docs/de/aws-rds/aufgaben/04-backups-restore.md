
# Backup und Restore

!!! goal "Ziele"
    - In diesem Projekt werden Sie eine Datenbank Cluster erstellen.
    - Sie werden die Datenbank mit Daten befüllen.
    - Sie werden Daten aus der Datenbank löschen und dann ein Backup mithilfe von Backtracking einspielen.

!!! tipp "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Vorbereitung - Datenbank erstellen

- Erstellen Sie eine Amazon Aurora MySQL Datenbank mit Multi-AZ Deployment
    - Name: `multi-az-db`
    - Instanz: `db.t3.medium`
- Denken Sie an den Admin-Nutzer und die Netzwerkeinstellungen
- Konfigurieren Sie zusätzlich eine Backup Retention Period und aktivieren sie Backtracking

    ???+ summary "Backup Konfiguration"
        === "Additional Configuration"
            - Backup retention period: Wählen Sie **1 Day**
            - Enable Backtrack: Aktivieren Sie die Option indem die auf die Checkbox klicken.
            - Target Backtrack window: Schreiben Sie **2** in die Textbox.


??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Suchen Sie in der Management Console nach RDS
    - Klicken Sie auf "Create database"

    ???+ summary "Datenbank Instanz Einstellungen"
        === "Engine options"
            - Type: **Amazon Aurora**
            - Edition: **Amazon Aurora MySQL-Compatible Edition**
            - Capacity Type: **Provisioned**
        === "Settings"
            - DB cluster identifier: **multi-az-db**
            - Master username: `admin`
            - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
        === "DB instance class"
            - DB instance class: Burstable classes (includes t classes)
            - Dropdown: **db.t3.medium**
        === "Availability & durability"
            Multi AZ deployment: **Create an Aurora Replica or Reader ...** auswählen.
        === "Connectivity"
            - Virtual private cloud (VPC): **{{ training_name }}-labs-vpc**
            - Subnet Groups: Wählen Sie die Subnetz Gruppe **{{ training_name }}-labs-rds-subnet-group** aus.
            - VPC security groups: Wählen Sie die Security Gruppe **{{ training_name }}-labs-rds-sec-group** aus und entfernen Sie die Gruppe <del>**default**</del>.
        === "Additional Configuration"
            - Backup retention period: Wählen Sie **1 Day**
            - Enable Backtrack: Aktivieren Sie die Option.
            - Target Backtrack window: Schreiben Sie **2** in die Textbox.


## Aufgabe 1 - `multi-az-db` - Snapshots.

- Prüfen Sie, dass Sie sich von der VSCode Umgebung mit der Datenbank `multi-az-db` verbinden können
- Verwenden Sie folgenden Befehl und ersetzten Sie die Felder `${USER}` und `${URL_TO_DB}`

```bash
mysql -u ${USER} -p -h ${URL_TO_DB}
```

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Die URL zur Datenbank finden Sie in der Management Console
    - Klicken Sie auf das **Regional cluster** mit dem Namen `multi-az-db`
    - Im Tab **Connectivity & security** unter **Endpoints**
    - Kopieren Sie die URL der **Writer instance**
    - Führen Sie anschließend den Befehl im Terminal im VSCode aus

- Laden Sie die Daten mit dem Script `run_load_data.sh` in die Datenbank
- Sie müssen vorher noch die Variablen anpassen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Öffnen Sie in der linken Navigation die Datei `run_load_data.sh`
    - Passen Sie die Variablen an (Zeile 4-6, ohne Leerzeichen)
    - Speichern Sie die Datei
    - Führen Sie im Terminal folgenden Befehl aus:
      ```
      cd scripts && ./run_load_data.sh
      ```
    - Dies dauert ein paar Sekunden. Sie sollten zum Schluss eine kleine Tabelle
      mit zwei Einträgen sehen
        ```
        +--------+----------+
        | gender | count(*) |
        +--------+----------+
        | M      |   179973 |
        | F      |   120051 |
        +--------+----------+
        ```
- Öffnen Sie in der Management Console Ihr Datenbank-Cluster.
- Klicken Sie auf den Reiter **Maintenance & Backup**.
- Klicken Sie am Ende der Website unter **Snapshots** auf  **Take Snapshot**.
- Geben Sie dem Snapshot den Namen **first-snapshot**.
- Klicken Sie nun "Take Snapshot".
- Es wird nun ein Snapshot der Daten Ihres Datenbank-Clusters erzeugt, dies kann einige Minuten dauern.

## Aufgabe 2 - `multi-az-db` - Löschen von Daten.
- Starten Sie einen Befehl, der alle 2 Sekunden eine Query an die Datenbank sendet
- Verwenden Sie dafür das Script `run_read_every_2_sec.sh`
- Sie müssen vorher noch die Variablen anpassen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Passen Sie im Script `run_read_every_2_sec.sh` ebenfalls die Variablen an
    - Führen Sie das Script aus:
        ```
        ./run_read_every_2_sec.sh
        ```

- Öffnen sie nun ein neues Terminal, dies können Sie tun indem Sie rechts in der Navigationsleiste Ihres Terminals auf "+" drücken.
- Starten Sie nun einen Befehl, der Daten aus der Datenbank löscht.
- Verwenden Sie dafür das Script `run_erase_data.sh`.
- Sie müssen vorher noch die Variablen anpassen.

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Passen Sie im Script `run_erase_data.sh` ebenfalls die Variablen an
    - Führen Sie das Script aus:
        ```
        ./run_erase_data.sh
        ```

- Es werden nun alle Männer aus der Datenbank gelöscht.
- Wenn das Script beendet wurde, wird ein Zeitstempel ausgegeben, beispielsweise "17.03.2022 13:37", notieren Sie sich diesen, da er für eine spätere Aufgabe höchst wichtig ist.
- Beobachten Sie nun den Output des Scripts `run_read_every_2_sec.sh`
- Nachdem das Löschen erfolgreich beendet wurde, sollten Sie folgenden Output sehen:
        ```
        +--------+----------+
        | gender | count(*) |
        +--------+----------+
        | F      |   120051 |
        +--------+----------+
        ```


## Aufgabe 3 - `multi-az-db` - Backtracking.
- Sie haben in Aufgabe 2 erfolgreich Daten aus Ihrer Datenbank gelöscht.
- Sie wollen nun die Datenbank zurückspulen.
- Öffnen Sie in der Management Console Ihr Datenbank Cluster.
- Klicken Sie oben rechts auf "Actions".
- Wählen Sie "Backtrack".
- Wählen Sie einen Zeitstempel aus, der Kurz vor dem Löschen der Daten liegt, den Zeitstempel dafür sollten Sie sich notiert haben. Wenn das löschen Ihrer Daten beispielsweise um "17.03.2022 13:37" beendet wurde, könnten sie "17.03.2022 13:35" wählen.
- Klicken Sie nun "Backtrack DB Cluster".
- Nun wird Ihr Datenbank-Cluster zurückgespult, dieses Vorgang kann ein paar Minuten dauern. Sie erkennen dies daran, dass der Status Ihres Datenbank-Clusters **backtracking** ist.
- Beobachten Sie während dem Backtracking nun den Output des Scripts `run_read_every_2_sec.sh`
- Sie sollten beobachten können, dass Ihr Datenbank Cluster für kurze Zeit nicht erreichbar ist und dann wieder folgenden Output sehen:
        ```
        +--------+----------+
        | gender | count(*) |
        +--------+----------+
        | M      |   179973 |
        | F      |   120051 |
        +--------+----------+
        ```
- Sie haben erfolgreich Ihr Datenbank-Cluster Zurückgespult.

## Ende

- Löschen Sie das Datenbank-Cluster
