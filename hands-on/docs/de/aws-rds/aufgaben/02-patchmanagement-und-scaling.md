# Patchmanagement und Scaling

!!! goal "Ziele"
    - In diesem Projekt werden Sie zwei Datenbanken erstellen
    - Bei der ersten Datenbank müssen Sie die Datenbank-Engine aktualisieren
    - Die zweite Datenbank müssen Sie vertikal Skalieren

!!! tipp "Hilfsmittel"
    - Sollten Sie Probleme haben die Aufgaben zu lösen, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.
    - Versuchen Sie die Aufgaben erst einmal ohne die Lösungshilfen zu bearbeiten.

## Vorbereitung - Datenbanken erstellen

- Erstellen Sie eine Amazon Aurora MySQL Datenbank mit Multi-AZ Deployment
    - Name: `multi-az-db`
    - Instanz: `db.t3.medium`
- Denken Sie an den Admin-Nutzer und die Netzwerkeinstellungen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Suchen Sie in der Management Console nach RDS
    - Klicken Sie auf "Create database"

    ???+ summary "Datenbank Instanz Einstellungen"
        === "Engine options"
            - Type: **Amazon Aurora**
            - Edition: **Amazon Aurora MySQL-Compatible Edition**
            - Capacity Type: **Provisioned**
        === "Settings"
            - DB cluster identifier: **multi-az-db**
            - Master username: `admin`
            - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
        === "DB instance class"
            - DB instance class: Burstable classes (includes t classes)
            - Dropdown: **db.t3.medium**
        === "Availability & durability"
            Multi AZ deployment: **Create an Aurora Replica or Reader ...** auswählen.
        === "Connectivity"
            - Virtual private cloud (VPC): **{{ training_name }}-labs-vpc**
            - Subnet Groups: Wählen Sie die Subnetz Gruppe **{{ training_name }}-labs-rds-subnet-group** aus.
            - VPC security groups: Wählen Sie die Security Gruppe **{{ training_name }}-labs-rds-sec-group** aus und entfernen Sie die Gruppe <del>**default**</del>.


- Erstellen Sie nun eine Amazon Aurora MySQL Datenbank **ohne** Multi-AZ Deployment
    - Name: `single-az-db`
    - Instanz: `db.t3.medium`
- Denken Sie an den Admin-Nutzer und die Netzwerkeinstellungen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Suchen Sie in der Management Console nach RDS
    - Klicken Sie auf "Create database"

    ???+ summary "Datenbank Instanz Einstellungen"
        === "Engine options"
            - Type: **Amazon Aurora**
            - Edition: **Amazon Aurora MySQL-Compatible Edition**
            - Capacity Type: **Provisioned**
        === "Settings"
            - DB cluster identifier: **multi-az-db**
            - Master username: `admin`
            - Master password: Vergeben Sie ein Passwort und notieren Sie es sich.
        === "DB instance class"
            - DB instance class: Burstable classes (includes t classes)
            - Dropdown: **db.t3.medium**
        === "Availability & durability"
            Multi AZ deployment: **Don't create an Aurora Replica** auswählen.
        === "Connectivity"
            - Virtual private cloud (VPC): **{{ training_name }}-labs-vpc**
            - Subnet Groups: Wählen Sie die Subnetz Gruppe **{{ training_name }}-labs-rds-subnet-group** aus.
            - VPC security groups: Wählen Sie die Security Gruppe **{{ training_name }}-labs-rds-sec-group** aus und entfernen Sie die Gruppe <del>**default**</del>.

## Aufgabe 1 - `multi-az-db` aktualisieren

- Prüfen Sie, dass Sie sich von der VSCode Umgebung mit der Datenbank `multi-az-db` verbinden können
- Verwenden Sie folgenden Befehl und ersetzten Sie die Felder `${USER}` und `${URL_TO_DB}`

```bash
mysql -u ${USER} -p -h ${URL_TO_DB}
```

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Die URL zur Datenbank finden Sie in der Management Console
    - Klicken Sie auf das **Regional cluster** mit dem Namen `multi-az-db`
    - Im Tab **Connectivity & security** unter **Endpoints**
    - Kopieren Sie die URL
    - Führen Sie anschließend den Befehl im Terminal im VSCode aus

- Laden Sie die Daten mit dem Script `run_load_data.sh` in die Datenbank
- Sie müssen vorher noch die Variablen anpassen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Öffnen Sie in der linken Navigation die Datei `run_load_data.sh`
    - Passen Sie die Variablen an (Zeile 4-6, ohne Leerzeichen)
    - Speichern Sie die Datei
    - Führen Sie im Terminal folgenden Befehl aus:
      ```
      cd scripts && ./run_load_data.sh
      ```
    - Dies dauert ein paar Sekunden. Sie sollten zum Schluss eine kleine Tabelle
      mit zwei Einträgen sehen
        ```
        +--------+----------+
        | gender | count(*) |
        +--------+----------+
        | M      |   179973 |
        | F      |   120051 |
        +--------+----------+
        ```

- Starten Sie einen Befehl, der alle 2 Sekunden eine Query an die Datenbank sendet
- Verwenden Sie dafür das Script `run_read_every_2_sec.sh`
- Sie müssen vorher noch die Variablen anpassen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Passen Sie im Script `run_read_every_2_sec.sh` ebenfalls die Variablen an
    - Führen Sie das Script aus:
        ```
        ./run_read_every_2_sec.sh
        ```

- Aktualisieren Sie die Datenbank-Version von `Aurora (MySQL 5.7) 2.07.2` auf `Aurora (MySQL 5.7) 2.10.2`

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Gehen Sie in die Management Console
    - **Tip**: Ordnen Sie die Management Console und das Terminal in VSCode so an, dass sie
    beides gleichzeitig sehen.

    - Wählen Sie das Cluster `multi-az-db` aus und klicken auf den Button **Modify**
    - Wählen Sie unter Version `Aurora (MySQL 5.7) 2.10.2` aus
    - Klicken Sie ganz unten auf **Continue**
    - Wählen Sie **Apply Immediately** aus und bestätigen Sie mit **Modify cluster**

- Das Cluster wird nun aktualisiert
- Beobachten Sie währendessen die Erreichbarkeit der Datenbank über das Script `run_read_every_2_sec.sh`
- Sie müssen vorher noch die Variablen anpassen
- Warten Sie bis das Upgrade erfolgreich beendet wurde

## Aufgabe 2 - `single-az-db` vertikal Skalieren

- Prüfen Sie, dass Sie sich von der VSCode Umgebung mit der Datenbank `single-az-db` verbinden können
- Verwenden Sie folgenden Befehl und ersetzten Sie die Felder `${USER}` und `${URL_TO_DB}`

```bash
mysql -u ${USER} -p -h ${URL_TO_DB}
```

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Die URL zur Datenbank finden Sie in der Management Console
    - Klicken Sie auf das **Regional cluster** mit dem Namen `single-az-db`
    - Im Tab **Connectivity & security** unter **Endpoints**
    - Kopieren Sie die URL
    - Führen Sie anschließend den Befehl im Terminal im VSCode aus

- Laden Sie die Daten mit dem Script `run_load_data.sh` in die Datenbank
- Sie müssen vorher noch die Variablen anpassen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Öffnen Sie in der linken Navigation die Datei `run_load_data.sh`
    - Passen Sie die Variablen an (Zeile 4-6, ohne Leerzeichen)
    - Speichern Sie die Datei
    - Führen Sie im Terminal folgenden Befehl aus:
      ```
      cd scripts && ./run_load_data.sh
      ```
    - Dies dauert ein paar Sekunden. Sie sollten zum Schluss eine kleine Tabelle
      mit zwei Einträgen sehen
        ```
        +--------+----------+
        | gender | count(*) |
        +--------+----------+
        | M      |   179973 |
        | F      |   120051 |
        +--------+----------+
        ```

- Starten Sie einen Befehl, der alle 2 Sekunden eine Query an die Datenbank sendet
- Verwenden Sie dafür das Script `run_read_every_2_sec.sh`
- Sie müssen vorher noch die Variablen anpassen

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Passen Sie im Script `run_read_every_2_sec.sh` ebenfalls die Variablen an
    - Führen Sie das Script aus:
        ```
        ./run_read_every_2_sec.sh
        ```

- Verkleinern Sie die Datenbank-Instanz von `db.t3.medium` auf `db.t3.small` mit
  möglichst **kurzer** Downtime

- Fügen Sie dazu eine neue Reader-Instanz hinzu.

???+ summary "Datenbank Instanz Einstellungen"
    === "Settings"
        - DB instance identifier: **single-az-db-small**
    === "DB instance class"
        - DB instance class: Burstable classes (includes t classes)
        - Dropdown: **db.t3.small**

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Wählen Sie das Cluster `single-az-db` in der Management Console aus
    - Fügen Sie eine Reader-Instanz über **Actions > Add reader** hinzu
    - Setzen Sie den **DB instance identifier** und die **DB instance class** wie oben beschrieben
    - Starten Sie den Reader mit **Add reader**

- Während der Reader startet, ist Ihre Datenbank durchgehend erreichbar.
- Warten Sie bis der Reader **Available** ist.
- Befördern Sie die Instanz `single-az-db-small` zum neuen Writer.
- Achten Sie währendessen auf die Erreichbarkeit Ihrer Datenbank

??? help "Lösung (Klicken Sie auf den Pfeil für eine Schritt für Schritt Anleitung)"
    - Wählen Sie das Instanz `single-az-db-small` in der Management Console aus
    - Befördern Sie die Instanz über **Actions > Failover**.
    - Der Failover verursacht eine kurze Downtime von wenigen Sekunden bis zu einer Minute.

- Löschen Sie die Instanz `single-az-db-instance-1`. Sie wird nicht mehr benötigt
- Sie haben die Datenbank nun erfolgreich verkleinert.

## Ende

- Löschen Sie die beiden Datenbank-Cluster
