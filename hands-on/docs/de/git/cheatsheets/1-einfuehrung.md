# Cheatsheet 1 - Einführung

| Befehl                                         | Aktion                                                                         |
|------------------------------------------------|--------------------------------------------------------------------------------|
| ```apt update && apt install git```            | Installiert git auf Linux (Debian-Derivaten)                                   |
| ```git clone <adresse>```                      | Repository an angegebener Adresse holen und in lokalen Ordner ablegen          |
| ```git status```                               | Anzeigen, welche Dateien modified/staged/untracked sind                        |
| ```git diff```                                 | Änderungen anzeigen                                                            |
| ```git diff <datei>```                         | Änderungen in \<datei\> anzeigen                                               |
| ```git diff --staged```                        | Änderungen im Stage-Bereich anzeigen                                           |
| ```git log```                                  | Historie anzeigen                                                              |
| ```git log --graph```                          | Historie als Graph anzeigen                                                    |
| ```git config <einstellung> <wert>```          | Setzt eine Einstellung für das aktuelle Repository                             |
| ```git config --global <einstellung> <wert>``` | Setzt eine Einstellung systemweit                                              |
| ```git config --list```                        | Alle aktuellen Einstellungen mit Wert anzeigen                                 |
| ```git add <pfad>```                           | Eine Datei als staged markieren                                                |
| ```git restore --staged <pfad>```              | Die Datei wieder unstaged markieren                                            |
| ```git restore <pfad>```                       | Eine Datei im modified state wieder auf den Originalinhalt zurücksetzen        |
| ```git commit```                               | Einen Commit mit allen Dateien in der Staging Area erstellen                   |
| ```git mv datei1 datei2```                     | Eine Dateiumbenennung stagen                                                   |
| ```git rm datei```                             | Eine Dateientfernung stagen                                                    |
| ```git push```                                 | Commits auf Server schieben                                                    |
| ```git pull```                                 | Commits vom Server holen                                                       |
| ```git help <befehl>```                        | Hilfe zum Git-Befehl anzeigen                                                  |
