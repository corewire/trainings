# Remote Branches

!!! goal "Ziel"
    - In diesem Projekt geht es um den Umgang mit Branches und wie man diese mit Anderen teilt.

!!! tipp "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
    [Folien](../../3-branches/#/) und des
    [Cheatsheets](../cheatsheets/3-branches.md) zu lösen
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

- Suchen Sie im Gitlab nach dem Projekt `remote-branches` und klonen Sie es.
    Stellen Sie sicher, dass Sie davor das vorherige Repository verlassen haben.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Suchen Sie im Gitlab links oben unter `Projects` nach dem Projekt `remote-branches`.
    - Kopieren Sie den Link unter `Clone with HTTPS`.
    - Gehen Sie zurück ins Terminal in der VSCode Instanz.
    - Stellen Sie sicher, dass Sie sich in `/root/workspace` befinden.
      Verlassen Sie dafür etwaige vorherige Repositories mit `cd ..`
      (alternativ können Sie mit `cd /root/workspace` direkt in das Verzeichnis wechseln).
    - Clonen Sie das Projekt mit `git clone {URL}`.

## Aufgabe 2

- Legen Sie einen Branch mit Ihrem Nutzernamen an und wechseln Sie auf diesen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Wechseln Sie mit `cd remote-branches` in das neue Projekt.
    - Legen Sie einen Branch mit `git switch -c user-{ZAHL}` an.
    Sie wechseln automatisch auf den Branch.
    - Wenn Sie nun `git status` ausführen sehen Sie ebenfalls, dass Sie auf dem neuen Branch sind.

## Aufgabe 3

- Committen Sie eine neue Datei mit einem kreativen Text.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie wie im vorherigen Hands-On eine Datei mit etwas Kreativem und committen Sie ihre Änderung.

## Aufgabe 4

- Pushen Sie den neuen Branch auf den Server.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Pushen Sie ihre Änderungen mit `git push`.
    - Sie erhalten einen Fehler, da ihr Branch aktuell nur lokal vorhanden ist und
      git nicht weiß, wohin Sie ihn pushen möchten.
    - Führen Sie deshalb den in der Ausgabe angegebenen Befehl aus.
    Dieser teilt git mit, dass Sie den Branch auf `origin` schieben möchten.

## Aufgabe 5

- Holen Sie die Branches von allen anderen und betrachten Sie einige der Änderungen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git fetch` aus. Die Ausgabe zeigt ihnen, wenn `origin` einen neuen Branch enthält.
    - Sie können sich auch alle Branches mit `git branch -a` anzeigen lassen.
    - Checken Sie einen beliebigen Branch mit `git switch {branch}` aus.
      Beispiel: Remote enthält einen Branch `test`.

      ```shell
      $ git branch -a
      main
      remotes/origin/test
      ```

      Checken Sie `test` mit `git switch test` aus. Git erstellt dann automatisch den
      lokalen Branch `test` und aktiviert das Tracking für `origin/test`.
    - Schauen Sie sich den kreativen Inhalt im Dateibrowser an.
    Wiederholen Sie diese Schritte bei weiteren Teilnehmern.
    Führen Sie auch nochmal `git fetch` aus. Evtl. sind neue Branches hinzugekommen.
