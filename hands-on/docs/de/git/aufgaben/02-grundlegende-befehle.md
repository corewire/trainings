# Grundlegende Befehle

!!! goal "Ziel"
    - In diesem Projekt geht es um die grundlegende Funktionalität von git. Sie
      sollen Dateien bearbeiten/verändern.
    - Führen Sie dabei nach jeder Aktion `git status` aus,
    um die Auswirkungen sehen zu können. Zum Schluss dürfen Sie dann mit allen Änderungen
    einen Commit erstellen und mit dem Server synchronisieren ("pushen").

!!! tipp "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
     [Folien](../../1-introduction/#/) und des
     [Cheatsheets](../cheatsheets/1-einfuehrung.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.


Bearbeiten Sie folgende Aufgaben und führen Sie dabei nach jeder Aktion `git
status` aus.

## Aufgabe 1

- Verändern Sie den Inhalt von `files/change_me.txt` und speichern Sie die Datei.
- Lassen Sie sich die Änderung von git anzeigen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Öffnen Sie die Datei `change_me.txt` links im Dateibrowser unter `my-first-project/files`.
    - Ändern Sie den Inhalt der Datei und speichern Sie mit `Strg + s`.

    - Klicken Sie in das Terminal und stellen sicher, dass Sie sich im Verzeichnis
      `~/workspace/my-first-project` befinden.

    - Führen Sie `git status` aus. Die Ausgabe sollte `modified:
      files/change_me.txt` enthalten.

## Aufgabe 2

- Verschieben Sie `files/move_me.txt` an eine andere Stelle.
 Verwenden Sie dazu das von git bereitgestellten Kommando.
- Lassen Sie sich die Änderung von git anzeigen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git mv files/move_me.txt files/new_location.txt` aus.
    - Führen Sie `git status` aus.
    Die Datei wurde umbenannt und automatisch zur Staging-Area hinzugefügt.

## Aufgabe 3

- Löschen Sie `files/delete_me.txt`.
- Lassen Sie sich die Änderung von git anzeigen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Suchen Sie im Dateibrowser die Datei `delete_me.txt`.
    - Löschen Sie die Datei mit `Rechtsklick > Delete Permanently`.
    - Führen Sie `git status` aus. Die Datei wird nun als `deleted` aufgelistet.

## Aufgabe 4

- Benennen Sie die Datei `files/rename_me.txt` links über den Dateibrowser
von VSCode um.
- Lassen Sie sich die Änderung von git anzeigen. Die alte Datei sollte als
`deleted` und die neue als `untracked` erscheinen.
- Fügen Sie beide Dateien zur Staging-Area hinzu.
- Schauen Sie sich erneut die Auswirkungen über `git status` an. Die beiden
Dateien sollten nun unter `Changes to be committed` mit dem Status `renamed`
angezeigt werden.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Suchen Sie im Dateibrowser die Datei `rename_me.txt`.
    - Bennen Sie die Datei über `Rechtsklick > Rename` um.
    - Gehen Sie ins Terminal und führen `git status` aus. Die alte Datei sollte als
      `deleted` und die neue als `untracked` erscheinen.
    - Fügen Sie beide Dateien zur Staging-Area hinzu, indem Sie `git add
      files/rename_me.txt` und `git add files/{neuer name}` ausführen.
    - Schauen Sie sich erneut die Auswirkungen über `git status` an. Die beiden
    Dateien sollten nun unter `Changes to be committed` mit dem Status `renamed`
    angezeigt werden.

## Aufgabe 5

- Erstellen Sie eine neue Datei `files/new.txt` und fügen
Sie sie dem Repository zu.
- Lassen Sie sich die Änderung von git anzeigen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie eine neue Datei mit `Rechtsklick > New File` auf den Ordner `files`.
    - Nennen Sie die Datei `new.txt`.
    - Führen Sie `git status` aus. Die Datei wird nun unter `Untracked files` gelistet.

## Aufgabe 6

- Erstellen Sie einen Commit mit allen bisherigen Änderungen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Fügen Sie alle bisherigen Änderungen im Ordner `files` mit `git add files`
    zur Staging-Area hinzu.
    - Mit `git diff --staged` können Sie sich noch einmal alle Änderungen,
    die im Commit enthalten sein werden, anschauen. Mit `q` können Sie diese Ansicht verlassen.
    - Erstellen Sie einen Commit mit `git commit -m "Mein erster Commit"`.

## Aufgabe 7

- Schauen Sie sich den Commit in der Historie an.
    Der Name und die Email-Adresse sind aktuell noch automatisch generiert.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git log` aus. Neueste Commits stehen oben. Mit `q` können Sie diese Ansicht verlassen.
    - Es wird Ihnen der eben erstellte Commit angezeigt.
    Der Autor ist allerdings noch der automatisch generierte.

## Aufgabe 8

- Ändern Sie ihre git-Identität zu ihrem Namen und einer beliebigen Email-Adresse.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Ändern Sie ihren Namen mit `git config --global user.name <Name>`.
    - Ändern Sie ihre Email-Adresse mit `git config --global user.email beliebig@beispiel.de`.

## Aufgabe 9

- Erstellen Sie die Datei `files/ignored.txt`. Sie sollte **nicht** im Commit
    landen und auch nicht durch `git status` angezeigt werden.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Erstellen Sie die Datei `ignored.txt` im Ordner `files`.
    - Führen Sie `git status` aus. Die Datei wird unter `Untracked files` gelistet.
    - Erstellen Sie die Datei `.gitignore` im Verzeichnis `my-first-project`.
    - Fügen Sie der Datei `.gitignore` folgende Zeile als Inhalt hinzu:

        ```text
        files/ignored.txt
        ```

    - Führen Sie erneut `git status` aus. Die Datei `ignored.txt` sollte nun
    nicht mehr gelistet werden.

## Aufgabe 10

- Erstellen Sie einen zweiten Commit. Er sollte nur die `.gitignore` Datei enthalten.
    `git status` sollte außerdem einen sauberen Workspace zeigen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Fügen Sie die Änderungen mit `git add files` zur Staging-Area hinzu.
    - Mit `git diff --staged` können Sie sich noch einmal alle Änderungen, die im
      Commit enthalten sein werden, anschauen.
    - Führen Sie `git commit` aus. Es öffnet sich der Terminal-Editor `nano`.
    - Schreiben Sie eine Commit-Nachricht wie `"Add .gitignore"`.
    - Speichern Sie mit `Strg + o` und `Enter`.
    - Verlassen Sie `nano` mit `Strg + x`.

## Aufgabe 11

- Überprüfen Sie erneut ihre Commit-Historie. Nun sollte ihr Name als Autor
    eingetragen sein.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git log` aus.
    - Es wird Ihnen der eben erstellte Commit angezeigt. Dieses sollte der Autor
    den von ihnen eingegebenen Daten entsprechen.

## Aufgabe 12

- Pushen Sie ihre Änderungen zum Server. Sie können im Gitlab verifizieren,
    dass Ihre Änderungen erfolgreich angekommen sind.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Führen Sie `git push` aus.
    - Gehen Sie auf die Gitlab-Instanz.
    - Suchen Sie links oben unter `Projects` nach `my-first-project`.
    - Wenn `my-first-project` nicht direkt erscheint, klicken Sie auf `Your projects` um es dort zu finden.
    - Schauen Sie sich ihren Commit an.
