# Merge

!!! goal "Ziel"
    - In diesem Projekt geht es um den Umgang mit Branches und wie man Änderungen aus mehreren Branches wieder vereint.

!!! tipp "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
    [Folien](../../3-branches/#/) und des
    [Cheatsheets](../cheatsheets/3-branches.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

- Für dieses kleine Projekt bietet es sich an, ein Plugin zur Graph-Visualisierung zu nutzen. Klicken Sie dazu auf das Graph-Symbol ![](img/Plugin.png) auf der linken Seitenleiste.
- Es erscheint eine SOURCE CONTROL Leiste, in der jedes bereits in Ihrem VSCode vorhandene Git-Repository zu finden ist.
- Klicken Sie bei verschiedenen vorherigen Projekten auf das Graph-Symbol ![](img/Plugin_symbol.png) ganz rechts neben dem Ordnernamen
- Sie sollten einen Graphen analog Ihres `git log --graph`-Befehls sehen können

## Aufgabe 2

- Verlassen Sie das Repository `remote-branches` und legen Sie ein neues an:

  ```bash
  cd ..        # Den Order remote-branches verlassen
  mkdir merge  # Einen neuen Ordner anlegen
  cd merge     # In den neuen Ordner wechseln
  git init     # Ein Git-Repository anlegen
  ```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verlassen Sie den Ordner `remote-branches` mit dem Kommando `cd ..`.
    - Führen Sie `pwd` aus. Sie sollten die Augabe `/root/workspace` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie mit `cd /root/workspace` in das Verzeichnis.
    - Wenn Sie im richtigen Verzeichnis sind, führen Sie `mkdir merge` aus.
    - Wechseln Sie mit `cd merge` in den neu angelegten Ordner.
    - Führen Sie `git init` aus. Dadurch wird das Verzeichnis zu einem
      Git-Repository und Sie können Dateien/Änderungen committen.

## Aufgabe 3

- Bauen Sie den untenstehenden Graphen nach.

![Graph](tikz/project_graph.svg)

Jeder grüne Knoten steht dabei für einen Commit. Gehen Sie dazu für jeden Commit/Knoten folgendermaßen vor:

1. Legen Sie eine Datei mit dem Knotennamen als Detainamen an (sofern es kein Merge Commit ist). Sie können dafür über die Seitenleiste vorgehen, oder im Terminal den Befehl `touch <Dateiname>` nutzen.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den ersten Commit `C1`:
        ```
        touch C1
        ```

1. Fügen Sie die Datei der Staging Area hinzu und committen Sie Ihre Änderung mit dem Knotennamen als Commitnachricht.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den ersten Commit `C1`:
        ```
        git add C1
        git commit -m "C1"
        ```

1. Gehen von einem Knoten mehrere Branches ab, fügen Sie gegebenenfalls einen neuen Branch hinzu, auf den Sie später mit `git switch` zurückgreifen können.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den dritten Commit `C3` ausgehend von Commit `C1`:
        ```
        git branch branch1
        ```
        Dann kann der zweite Commit `C2` erstellt werden. Und anschließend der Branch gewechselt werden.
        ```
        touch C2
        git add C2
        git commit -m "C2"
        git switch branch1
        ```
        Dort kann C3 wie bekannt angelegt werden.

1. Ist der Knoten von zwei vorherigen Knoten abhängig, ist er ein Merge Commit. Für ihn muss keine Datei angelegt werden, aber die Commitnachricht soll mit dem Knotennamen übereinstimmen.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den fünften Commit `C5` ausgehend von Commit `C2` und `C3`:
        ```
        git switch main
        git merge branch1
        ```

 1. Vervollständigen Sie den Graphen. In VSCode sollte es im Git Graph Plugin dann folgendermaßen oder ähnlich aussehen:

    ![](img/merge.png)

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Die vollständige Lösung sieht folgendermaßen aus:
    ```
    touch C1
    git add C1
    git commit -m "C1"
    git branch branch1
    touch C2
    git add C2
    git commit -m "C2"
    git switch branch1
    touch C3
    git add C3
    git commit -m "C3"
    git switch main
    git merge branch1 -m "C5"
    git switch branch1
    git branch branch2
    touch C4
    git add C4
    git commit -m "C4"
    git switch branch2
    touch C6
    git add C6
    git commit -m "C6"
    git switch branch1
    git merge branch2 -m "C7"
    git switch main
    git merge branch1 -m "C8"
    git branch -d branch1
    git branch -d branch2
    ```
