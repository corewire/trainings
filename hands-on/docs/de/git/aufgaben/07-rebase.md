# Rebase

!!! goal "Ziel"
    - In diesem Projekt sollen Sie sich mit dem Rebase, der Alternative zum Mergen, vertraut machen.

!!! tipp "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
     [Folien](../../4-branches-2/#/) und des
     [Cheatsheets](../cheatsheets/3-branches.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1

Verlassen Sie das Repository `conflict` und legen Sie ein neues an:

```bash
cd ..         # Den Order conflict verlassen
mkdir rebase  # Einen neuen Ordner anlegen
cd rebase     # In den neuen Ordner wechseln
git init      # Ein Git-Repository anlegen
```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verlassen Sie den Ordner `conflict` mit dem Kommando `cd ..`.
    - Führen Sie `pwd` aus. Sie sollten die Augabe `/root/workspace` erhalten.
    - Sollten Sie die Ausgabe nicht erhalten, wechseln Sie mit `cd /root/workspace` in das Verzeichnis.
    - Wenn Sie im richtigen Verzeichnis sind, führen Sie `mkdir rebase` aus.
    - Wechseln Sie mit `cd rebase` in den neu angelegten Ordner.
    - Führen Sie `git init` aus. Dadurch wird das Verzeichnis zu einem
      Git-Repository und Sie können Dateien/Änderungen committen.

## Aufgabe 2

- Bauen Sie den untenstehenden Graphen nach.

![Graph](tikz/rebase_graph.svg)

Jeder grüne Knoten steht dabei für einen Commit. Gehen Sie dazu für jeden Commit/Knoten folgendermaßen vor:

1. Legen Sie den branch `us-42` an.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        ```
        git switch -c us-42
        ```

1. Legen Sie eine Datei mit dem Knotennamen als Namen an. Sie können dafür über die Seitenleiste vorgehen, oder im Terminal den Befehl `touch <Dateiname>` nutzen.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den ersten Commit `C1`:
        ```
        touch C1
        ```

1. Fügen Sie die Datei der Staging Area hinzu und committen Sie Ihre Änderung mit dem Knotennamen als Commitnachricht.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den ersten Commit `C1`:
        ```
        git add C1
        git commit -m "C1"
        ```

1. Gehen von einem Knoten mehrere Branches ab, fügen Sie gegebenenfalls einen neuen Branch hinzu.

    ??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
        z.B. für den dritten Commit `C3` ausgehend von Commit `C2`:
        ```
        git switch -c testing
        ```
        Dann kann der dritte Commit `C3` erstellt werden. Und anschließend der Branch gewechselt werden.
        ```
        touch C3
        git add C3
        git commit -m "C3"
        git switch us-42
        ```

 1. Vervollständigen Sie den Graphen. In VSCode sollte es im Git Graph Plugin dann folgendermaßen oder ähnlich aussehen:

    ![](img/rebase_1.png)

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Die vollständige Lösung sieht folgendermaßen aus:
    ```
    git switch -c us-42
    touch C1
    git add C1
    git commit -m "C1"
    touch C2
    git add C2
    git commit -m "C2"
    git switch -c testing
    touch C3
    git add C3
    git commit -m "C3"
    git switch us-42
    touch C4
    git add C4
    git commit -m "C4"
    ```

## Aufgabe 3

Führen Sie einen Rebase aus, sodass die Änderungen von `C3` auf `C4`
 angewendet werden und der Branch `us-42` auf den neuen Commit `C3` zeigt.

In VSCode soll es im Git Graph Plugin dann folgendermaßen aussehen:
![](img/rebase_2.png)

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Sie haben nun einen Branch `us-42` und einen Branch `testing`. Der
      `testing`-Branch beinhaltet leider nicht die neusten Änderungen von `us-42`.
      Das wollen wir nun ändern, indem wir einen Rebase ausführen.
    - Wechseln Sie dazu auf den Branch `testing` mit `git switch testing`.
    - Starten Sie den Rebase mit `git rebase us-42` damit die Änderungen von `testing`
      nun wieder auf den aktuellen Änderungen von `us-42` basieren.
    - Als Letztes wollen wir nun die Änderungen von `testing` in `us-42`
      integrieren. Wechseln Sie dazu mit `git switch us-42` auf den Branch `us-42`.
    - Führen Sie mit `git merge testing` einen Fast-Forward-Merge aus.
    - Sehen Sie sich mit `git log` das Ergebnis an.
