# Interaktiver Rebase

!!! goal "Ziel"
    - In diesem Projekt lernen Sie den Umgang und die Möglichkeiten des interaktiven Rebase.

!!! tipp "Hilfsmittel"
    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der
     [Folien](../../5-history-rewriting/#/) und des
     [Cheatsheets](../cheatsheets/4-history-rewriting.md) zu lösen
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe
    einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Vorbereitung

Suchen Sie im Gitlab das Projekt `interactive-rebase` und klonen Sie es:

- Suchen Sie im Gitlab links oben unter `Menu->Projects` nach dem Projekt `interactive-rebase`.
  Wenn Sie das Projekt hier nicht direkt sehen, finden Sie es unter `Your projects`.

- Kopieren Sie den Link unter `Clone with HTTPS`.

- Gehen Sie zurück ins Terminal in der VSCode Instanz.

- Stellen Sie sicher, dass Sie sich in `/root/workspace` befinden. Verlassen
  Sie etwaige vorherige Repositories mit `cd ..` (alternativ können Sie mit
  `cd /root/workspace` direkt in das Verzeichnis wechseln).

- Klonen Sie das Projekt mit `git clone {URL}`.

## Aufgabe 1

Ein Entwickler hat auf dem Branch `feature-1` ein einfaches Hello-World Python-Script
geschrieben. Die Aufgabe ist es, diesen Feature-Branch in den `main`-Branch zu integrieren.
Deshalb soll der Feature-Branch zunächst aufgeräumt werden. Dafür sollen alle Commits
in einen einzelnen 'gesquashed' werden.

1. Wechseln Sie auf den Branch `feature-1`.

1. Schauen Sie sich die Historie an, um zu sehen, was getan wurde.

1. Nutzen Sie den interaktiven Rebase, sodass nur noch ein Commit mit genau einer
   sinnvollen Commit-Nachricht übrig bleibt.

1. Überprüfen Sie die Historie, um zu sehen, ob alles so ist, wie Sie es erwarten.

1. Mergen Sie `feature-1` in den `main`-Branch ohne fast-forward.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Teil:
        - Wechseln Sie in den `interactive-rebase` Ordner.
        - Führen Sie zum Wechseln des Branches `git switch feature-1` aus, um in den bestehenden Branch zu wechseln.
    2. Teil:
        - Schauen Sie sich den Dateibrowser an. Es gibt dort eine Datei `hello-world.py`.
        - Schauen Sie sich die Historie mit `git log --graph` an.
        - Schauen Sie sich einzelne Commits mit `git show HEAD` bzw `git show HEAD~1` und `git show HEAD~2` an.
    3. Teil:
        - Das Feature enthält insgesamt 3 Commits, die wir nun in einen Einzigen vereinen möchten.
         Führen Sie deshalb `git rebase -i HEAD~3` aus.
        - Es erscheint eine Liste mit den Commits. Da wir nur noch einen Commit erhalten möchten, ändern wir die Liste zu:
             ```bash
             pick cf3b080 Add file where the feature should be implemented
             squash b88e281 Adds hello-world functionality
             squash 1d6f112 Fit typo in hello-world output
             ```
          So werden die letzten beiden Commits in den Ersten integriert.
        - Sie erhalten nun eine Liste mit den kombinierten Commit-Nachrichten.
         Löschen Sie alle nicht auskommentierten Zeilen und geben Sie eine sinnvolle Nachricht ein.
        - Speichern und schließen Sie die Commit-Nachricht.
    4. Teil:
        - Überprüfen Sie die Historie um zu sehen, ob alles so ist, wie Sie es erwarten.
         Führen Sie dazu `git log --graph` aus.
    5. Teil:
        - Führen Sie `git switch main` und dann `git merge feature-1` aus.

## Aufgabe 2

1. Nutzen Sie `git reflog` um ihren interaktiven Rebase rückgängig zu machen.
   Suchen Sie dazu im Reflog die Zeile `rebase -i (start)`. Hier wurde der
   Rebase gestartet. Die Zeile darunter ist die Referenz zum Zustand davor.

1. Führen Sie ein `git reset` aus um wieder auf diesen Zustand zu gelangen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    1. Teil:
        - Führen Sie `git reflog` aus.
        - Suchen Sie die Zeile mit dem Inhalt `rebase -i (start)`. Hier wurde der Rebase gestartet.
        - Die Zeile darunter ist die Referenz zum Zustand davor. Merken Sie sich die angegebene Referenz (`HEAD@{??}`).
    2. Teil:
        - Führen Sie `git reset --hard HEAD@{??}` aus. Ersetzen Sie dabei `??` mit der Zahl aus dem Reflog.
        - Führen Sie `git log --graph` aus. Sie sind nun wieder auf dem Ausgangszustand.

## Aufgabe 3

Falls noch Zeit übrig ist können Sie den `feature-2`-Branch auschecken und
anschauen. Er enthält ein einfaches Kommandozeilentool.

1. Wechseln Sie auf den Branch `feature-2`.

1. Erstellen Sie einen neuen Branch, sodass Sie nach einem Rebase einfach auf
   diesen Stand zurück springen können.

1. Schauen Sie sich die Historie an.

1. Versuchen Sie unterschiedliche Varianten von `rebase -i` aus.
