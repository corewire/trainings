# Einführung

In diesem Foliensatz gibt es zwei Demos. Eine zum Kennenlernen der Oberflächen und der Versionierung und eine, um ein add, commit, push nachzuvollziehen.

## Demo 1: Workspace und grafische Oberfläche

### Demo 1 - Workspace

- VSCode öffnen
- Ein öffentliches Git-Repo klonen, z.B. <https://gitlab.com/corewire/hands-on/git/demo-project.git>
  ```
  git clone git@gitlab.com:corewire/hands-on/git/demo-project.git
  cd demo-project
  ```
- Dateien in der Oberfläche zeigen
- Auf eine vorherige Version, z.B. Version 1, wechseln
  ```
  git switch --detach c35f2663
  ```
  Die Datei version_2.txt wird zu version_1.txt und der Text in my_text_file verändert sich.
- Wieder auf den aktuellen Stand wechseln
  ```
  git switch main
  ```
  
### Demo 1 - grafische Oberfläche

- Anmelden auf <https://gitlab.com/>
- Ein Projekt, z.B. <https://gitlab.com/corewire/hands-on/git/demo-project.git> zeigen und Historie zeigen
- Projektmanagement mit Issues zeigen
    - ein Issue anlegen
    - Issue board zeigen
    - Issue schließen
- eine CI/CD-Pipeline zeigen, z.B. <https://gitlab.com/corewire/trainings/-/pipelines>
- Zeigen, dass es auch <https://github.com/> gibt


## Demo 2: Erste Befehle

### Demo 2 - add, commit, push

- Einen neuen Branch anlegen
  ```
  git switch -c development
  ```
- *Hinweis: falls der Branch aus vorherigen Schulungen noch existieren sollte, folgende Befehle einfügen und ausführen:*
  ```
  git switch main
  git branch -d development
  git push origin --delete development
  ```
- Eine neue Datei hinzufügen
  ```
  touch my-new-file
  ```
- Mit `git status` den Status betrachten
- Die neue Datei dem Repository hinzufügen, wieder Status betrachten und commiten
  ```
  git add .
  git status
  git commit -m "create file my-new-file"
  git status
  ```
- In Gitlab zeigen, dass die Datei noch nicht vorhanden ist
- Den Commit pushen
  ```
  git push -u origin development
  ```
- Logs anzeigen lassen
  ```
  git log --graph
  ```
- In Gitlab zeigen, dass die Datei im neuen Branch vorhanden ist, aber im main nicht
