# Änderungen rückgängig machen

## Demo - Commits anpassen oder rückgängig machen

- Wir bleiben in unserem Development-Branch aus der ersten Demo.
- Verändere eine Datei, z.B. `files/file1`
- Adden und committen
  ```
  git add .
  git commit -m "das ist die falsche Nachricht"
  ```
- Mit `git log` die Commitnachricht anschauen
- Die Commit-Nachricht anpassen
  ```
  git commit --amend
  ```
- Mit `git log` die Commitnachricht anschauen
- Den letzten Commit durch einen neuen Commit rückgängig machen
  ```
  git revert HEAD
  ```
- Der Text des Files ist nun wieder wie vorher.
- Die letzten 2 Commits (der selbst erstellte sowie der Revert-Commit) nacheinander entfernen, dabei den Text beobachten
  ```
  git reset --hard HEAD~1
  git log
  git reset --hard HEAD~1
  ```
- Im `git log` ist nun nichts mehr von den Änderungen zu sehen
