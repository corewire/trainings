# Branches

Die erste Demo ist für die Remote Brnaches, die zweite für den merge.

## Demo 1 - Remote Branches

- Starte wieder im Development-Branch
- Alle möglichen Branches anschauen
  ```
  git branch
  ```
- Erzeuge einen neuen Branch `dev2`
  ```
  git switch -c dev2
  ```
- Mit `git log` den aktuellen Stand anschauen.
- Mit `git branch` sehen, dass der Branch existiert
- Zurück wechseln auf development und den neuen Branch wieder löschen.
  ```
  git switch development
  git branch -d dev2
  ```
- Verändere etwas, committe und pushe es
  ```
  git switch development
  <etwas verändern>
  git add .
  git commit -m "change something"
  git push
  ```
- Zeige das Git Graph-Plugin in VSCode

## Demo 2 - Merge

- Merge nun development und main2 in development zusammen
  ```
  git merge main2
  git push
  ```
- Zeige das Git Graph-Plugin in VSCode (**Achtung**: manchmal muss ein Refresh gemacht werden)
