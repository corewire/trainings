# History Rewriting

## Demo - Force push

- Verändere etwas im development-Branch und committe
  ```
  git add .
  git commit -m "change made"
  ```
- Pushe die Änderungen
  *Hinweis:* Die Änderungen werden aufgrund des rebase fehlschlagen. Hier können Vorschläge zur Auflösung eingeholt werden.
  Eine Lösung:
  ```
  git config pull.rebase true
  git pull
  git push
  ```
- Die Commitnachricht verändern
  ```
  git commit --amend
  ```
- pushen funktioniert nicht
- Zum forcen
  ```
  git push --force-with-lease
  ```
- ab hier kann der Branch aufgeräumt werden für das nächste Mal
  ```
  git switch main
  git branch -d development
  git push origin --delete development
  ```
