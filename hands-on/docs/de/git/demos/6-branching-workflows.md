# Branching Workflows

## Demo - Forks und merge requests

Zeige das Gitlab Repository <https://gitlab.com/gitlab-org/gitlab> und darin

- Forks
- Merge requests
- Approves

Falls noch nicht passiert kann nun der development-Branch gelöscht werden:
  ```
  git switch main
  git branch -d development
  git push origin --delete development
  ```
