# Advanced Features

## Demo: Git Bisect

- Checke branch bisect aus
  ```
  git switch bisect
  ```
- Starte bisect-Suche
  ```
  git bisect start
  ```
- Commit mit Nachricht bisect start war noch in Ordnung
  ```
  git bisect good c3e523
  ```
- HEAD ist nicht in Ordnung
  ```
  git bisect bad HEAD
  ```
- in der Datei bisect_file steht "good", wenn es good ist und "bad" wenn es nicht mehr in Ordnung ist. Davon abhängig:
  ```
  git bisect <good | bad>
  ```
  Solange, bis ein Commit erscheint

Falls noch nicht passiert kann nun der development-Branch gelöscht werden:
  ```
  git switch main
  git branch -d development
  git push origin --delete development
  ```
