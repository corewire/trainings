# Server und Hosting

## Demo - Zugriffe mit SSH / HTTPS

- ssh Schlüssel erstellen
  ```
  ssh-keygen
  ```
  **Achtung**: nicht bereits vorhandene Schlüssel überschreiben! 
- in Gitlab zeigen, wo er hinterlegt werden muss
  Profilbild anklicken ⇨ Preferences ⇨ SSH-Keys
- einen Unterordner `ssh-clone` erstellen und mit `cd ssh-clone` betreten
- ein beliebiges git Repo mit ssh auschecken, z.B. 
  ```
  git@gitlab.com:corewire/hands-on/git/my-first-project.git
  ```
- mit `cd ..` den Ordner `ssh-clone` verlassen
- einen Unterordner `https-clone` erstellen und mit `cd https-clone` betreten
- mit https auschecken zeigen, z.B. 
  ```
  git clone https://gitlab.com/corewire/hands-on/git/my-first-project.git
  ```

Falls noch nicht passiert kann nun der development-Branch im demo-Repo gelöscht werden:
  ```
  git switch main
  git branch -d development
  git push origin --delete development
  ```
