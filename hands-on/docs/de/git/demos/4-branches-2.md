# Branches 2

## Demo 1 - Local, Remote, Base

- Tippe ein git merge von main2 auf den develop. **Frage:** Welcher Branch wird in welchen integriert?
  ```
  git merge main2
  ```
  (es sollte aus der vorherigen Aufgabe bereits aktuell sein)
- Auf dem development-Branch weiterarbeiten, z.B. einen Text einfügen
  ```
  git add .
  git commit -m "do something"
  ```
  *Hinweis:* Die Datei test_version_3.txt sollte nicht verändert werden, da sonst der rebase Konflikte erstellt.
- main2 bleibt stehen, wohingegen development sich weiterbewegt im Plugin


## Demo 2 - Rebase

- In main3 steht Text in test_version_3.txt
- Auf dem development-Branch:
  ```
  git rebase main3
  ```
