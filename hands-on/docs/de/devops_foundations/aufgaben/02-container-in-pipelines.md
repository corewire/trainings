# Container in Pipelines

!!! goal "Ziel"
    In diesem Projekt geht darum mit Hilfe eines Containers einen Linter in eine Pipeline zu integrieren.

!!! tipp "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der Folien zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Issue-MR-Workflow

### 1.1 Gitlab Projekt öffnen

- Melden Sie sich auf der Gitlab-Instanz für diese Schulung an: [https://git.labs.corewire.de/](https://git.labs.corewire.de/)
- Öffnen Sie das Projekt `mk-docs`

Wie auch im letzten Hands-On, starten wir mit dem Erstellen eines Issues und eines Merge Requests.

### 1.1 Issue anlegen

- Eröffnen Sie ein Issue für das Hinzufügen eines Linters in die Pipeline.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Stellen Sie sicher, dass Sie im Projekt `mk-docs` sind. Oben links im Menü steht das aktuell geöffnete Projekt
    - Klicken Sie links im Menü auf **Issues**
    - Auf dieser Seite können Sie rechts oben über "New Issue" ein Issue anlegen
    - Geben Sie einen Title (z.B. "Linter zur Pipeline hinzufügen") ein
    - Weitere Eingaben sind nicht erforderlich
    - Erstellen Sie das Issue unten mit "Create Issue"

### 1.2 Merge Request anlegen

- Das eben erstellte Issue möchten Sie nun bearbeiten/lösen.
- Erstellen Sie aus dem Issue heraus einen Merge Request:
    - Weisen Sie sich den Merge Request zu
    - Erstellen Sie den Merge Request

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Mit dem Erstellen des Issues wurden Sie automatisch auf das Issue weitergeleitet
    - Hier finden Sie den Button "Create Merge Request" mit einem **Pfeil** daneben.
    - Klicken Sie auf "Create Merge Request"
    - Auf der Seite "New merge request", die sich geöffnet hat, können Sie sich unter "Assignee" den Merge Request zuweisen.
    - Erstellen Sie anschließend den Merge Request


## Aufgabe 2 - Linter in Pipeline einbauen

### 2.1 Source Code bearbeiten

- Gehen Sie zurück zu ihrem Merge Request
- Öffnen Sie ihren Branch in der Web IDE von Gitlab
- Öffnen Sie die Datei `.gitlab-ci.yml`
- Finden Sie den Job `lint`, er sieht wie folgt aus:

```yaml
lint:
  stage: test
  script:
    - echo "Dummy run linter"
```

- Ersetzen Sie den Inhalt des Jobs durch den folgenden Code:

```yaml
lint:
  image: markdownlint/markdownlint #(1)
  stage: test
  script:
    - mdl basic-blog #(2)
```

1. Das Dockerimage `markdownlint/markdownlint` enthält den Linter `mdl`
2. Der Linter wird auf den Ordner `basic-blog` ausgeführt

- Erstellen Sie mit Ihrer Änderung einen Commit


Der neue Job enthält ein Dockerimage, in dem der Linter `mdl` installiert ist. Dieser wird dann im Abschnitt `script` aufgerufen und prüft die Markdown-Dateien im Ordner `basic-blog`.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Auf dem Merge Request finden Sie oben rechts den Button **Code**
    - Klicken Sie ihn und wählen Sie "Open in Web IDE"
    - Es öffnet sich der in Gitlab integrierte Text-Editor
    - Suchen Sie links im Dateibrowser die Datei **.gitlab-ci.yml**
    - Führen Sie die oben beschriebenen Änderungen durch
    - Wählen Sie in der linken Menü-Leiste den Tab **Source Control** aus. Das Icon sollte bereits mit einer lila Benachrichtigung hervorgehoben sein.
    - In diesem Tab können Sie sich nochmals Ihre Änderungen ansehen.
    - Wenn Sie mit den Änderungen zufrieden sind, klicken Sie auf "Commit to '<Branchname>'"
    - Sie haben erfolgreich einen Commit erstellt.

### 2.2 Pipeline-Logs ansehen

!!! warning "Achtung"
    Der Linter wird fehlschlagen, da die Dateien im Ordner `basic-blog` nicht den Anforderungen des Linters entsprechen. Das ist so gewollt und wird im nächsten Schritt behoben.

- Verlassen Sie die Gitlab Web IDE und gehen Sie zurück zu ihrem Merge Request
- Schauen Sie im Tab "Pipelines" nach der von ihrem Commit getriggerten Pipeline
- Schauen Sie sich das Job-Log an
- Merken Sie sich die Fehlermeldungen des Linters für die nächste Aufgabe
    - **Tipp**: Lassen Sie den Tab mit den Logs geöffnet

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Verlassen Sie die Gitlab Web IDE indem Sie oben links auf den Projektnamen **mkdocs** klicken
    - Öffnen Sie ihren Merge Request links über das Menü **Merge requests**
    - Gehen Sie im Merge Request auf den Tab Pipelines (direkt unter dem Titel)
    - Hier sehen Sie die zu diesem Merge Request gehörenden Pipelines
    - Schauen Sie sich den Job im Detail mit Klick auf den **roten Kreis** unter **Stages** an.
    - Hier sehen Sie die Logausgabe des Jobs sowie weitere Informationen.

## Aufgabe 3 Linter Fehler beheben

### 3.1 Linter Fehler beheben

- Gehen Sie zurück in die Web IDE
- Beheben Sie die Fehler, die der Linter gefunden hat
- Erstellen Sie mit Ihrer Änderung einen Commit
- Prüfen Sie die Pipeline erneut
- Der Linter sollte nun erfolgreich durchlaufen

### 3.2 Mergerequest schließen

- Markieren Sie den Merge Request als "ready" zur Überprüfung
- Mergen Sie den Merge Request
