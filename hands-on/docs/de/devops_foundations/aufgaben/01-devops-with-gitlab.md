# CI/CD-Pipelines in Gitlab

!!! goal "Ziel"
    In diesem Projekt geht es um den Ablauf des Entwicklungsprocesses mit einer bestehenden CI/CD-Pipeline.

!!! tipp "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](../../1-introduction/#/) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Setup

### 1.1 Bei Gitlab anmelden

- Melden Sie sich auf der Gitlab-Instanz für diese Schulung an: [https://git.labs.corewire.de/](https://git.labs.corewire.de/)
- Username: `user-{ZAHL}`
- Passwort: wird mitgeteilt
- Öffnen Sie das Projekt `mk-docs`
- Das Projekt enthält die eben gezeigte Demo-Anwendung

### 1.2 Demo-Anwendung aufrufen

Die Anwendung wird über Gitlab-Pages gehostet. Damit lassen sich statische Webseiten dirket über Gitlab sehr einfach ausliefern. Dieses Tool wird gerne für Dokumentation oder ähnliches genutzt.

- Navigieren Sie links im Menü zu `Deploy` -> `Pages`
- Öffnen Sie den Link in einem neuen Tag und testen Sie kurz die Demo-Anwendung.

## Aufgabe 2 - Issue-MR-Workflow

### 2.1 Issue anlegen

- Eröffnen Sie ein Issue für einen neuen Blogpost (stellvertretend für ein neues Feature).

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Stellen Sie sicher, dass Sie im Projekt `mk-docs` sind. Oben links im Menü steht das aktuell geöffnete Projekt
    - Klicken Sie links im Menü auf **Issues**
    - Auf dieser Seite können Sie rechts oben über "New Issue" ein Issue anlegen
    - Geben Sie einen Title (z.B. "Neuer Blogpost: ...") ein
    - Weitere Eingaben sind nicht erforderlich
    - Erstellen Sie das Issue unten mit "Create Issue"

### 2.2 Merge Request anlegen

- Das eben erstellte Issue möchten Sie nun bearbeiten/lösen.
- Erstellen Sie aus dem Issue heraus einen Merge Request:
    - Weisen Sie sich den Merge Request zu
    - Erstellen Sie den Merge Request

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Mit dem Erstellen des Issues wurden Sie automatisch auf das Issue weitergeleitet
    - Hier finden Sie den Button "Create Merge Request" mit einem **Pfeil** daneben.
    - Klicken Sie auf "Create Merge Request"
    - Auf der Seite "New merge request", die sich geöffnet hat, können Sie sich unter "Assignee" den Merge Request zuweisen.
    - Erstellen Sie anschließend den Merge Request

### 2.3 Issue überprüfen

- Öffnen Sie Ihr erstelltes Issue in einem neuen Tab
- Unter "Related merge requests" sollte ihr Merge Request verknüpft sein
- In den Aktivitäten sehen Sie, dass ein Branch angelegt wurde

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - In die Beschreibung des Merge Requests wurde automatisch **Closes #{Issue Nummer}** geschrieben.
    - Per Rechtsklick auf die Issuenummer, können Sie ihr Issue in einem neuen Tab öffnen.
    - Schauen Sie sich die entsprechenden Stellen an. Im Issue wurde genau dokumentiert, dass Sie es bearbeiten und einen Merge Request erstellt haben.
    - Schließen Sie anschließend den Tab wieder.

### 2.4 Source Code bearbeiten

- Gehen Sie zurück zu ihrem Merge Request
- Öffnen Sie ihren Branch in der Web IDE von Gitlab
- Erstellen Sie einen neuen Blogpost in dem Sie eine neue Datei unter `basic-blog/blog/posts` anlegen
- Füllen Sie die Datei mit Inhalt. Orientieren Sie sich dabei an der Struktur der bestehenden Posts
- Erstellen Sie mit ihrer Änderung einen Commit

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Auf dem Merge Request finden Sie oben rechts den Button **Code**
    - Klicken Sie ihn und wählen Sie "Open in Web IDE"
    - Es öffnet sich der in Gitlab integrierte Text-Editor
    - Suchen Sie links im Dateibrowser den Ordner **basic-blog/blog/posts**.
    - Mit Rechtsklick auf den Ordner `posts` können Sie eine neue Datei anlegen. Der Name ist frei wählbar. Die Datei muss aber auf `.md` enden.
    - Kopieren Sie den Inhalt eines anderen Blogposts in die neu erstellte Datei. Passen Sie Datum, Kategorie, Überschrift und Text beliebig an.
    - Wählen Sie in der linken Menü-Leiste den Tab **Source Control** aus. Das Icon sollte bereits mit einer lila Benachrichtigung hervorgehoben sein.
    - In diesem Tab können Sie sich nochmals Ihre Änderungen ansehen.
    - Wenn Sie mit den Änderungen zufrieden sind, klicken Sie auf "Commit to '<Branchname>'"
    - Sie haben erfolgreich einen Commit erstellt.

### 2.5 Pipeline-Logs ansehen

- Verlassen Sie die Gitlab Web IDE und gehen Sie zurück zu ihrem Merge Request
- Schauen Sie im Tab "Pipelines" nach der von ihrem Commit getriggerten Pipeline
- Schauen Sie sich das Job-Log an

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Verlassen Sie die Gitlab Web IDE indem Sie oben links auf den Projektnamen **mkdocs** klicken
    - Öffnen Sie ihren Merge Request links über das Menü **Merge requests**
    - Gehen Sie im Merge Request auf den Tab Pipelines (direkt unter dem Titel)
    - Hier sehen Sie die zu diesem Merge Request gehörenden Pipelines
    - Schauen Sie sich den Job im Detail mit Klick auf den **grünen Kreis** unter **Stages** an.
    - Hier sehen Sie die Logausgabe des Jobs sowie weitere Informationen.

### 2.6 Mergerequest schließen

Normalerweise passiert an dieser Stelle ein Code-Review durch weitere Entwickler:innen. Wir überspringen diesen Schritt.

- Markieren Sie den Merge Request als "ready" zur Überprüfung
- Mergen Sie den Merge Request

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Klicken Sie auf die drei Punkte rechts oben im Merge Request neben dem blauen Button `Code`. Dort finden Sie die Option **Mark as ready**.
    - Klicken Sie anschließend auf den Button **Merge** auf der Merge Request Seite

## Aufgabe 3 - Pipeline auf dem Main Branch

### 3.1 Pipeline auf dem Main Branch öffnen

- Öffnen Sie die Pipeline auf dem Main Branch

Auf dem main Branch wird nun zusätzlich der deploy Job ausgeführt. Dieser deployt die Änderungen auf Gitlab Pages.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Klicken Sie in der Linke Menüleiste auf **Build** -> **Pipelines**
    - Öffnen Sie die oberste Pipeline indem Sie auf den button links unter **Status** klicken.

### 3.2 Demo-Anwendung erneut ansehen

- Schauen Sie sich den Gitlab Pages Link erneut an, wenn die Pipeline erfolgreich durchgelaufen ist
- Ihre Änderungen sollten nun sichtbar sein.