# Monitoring und Logging

!!! goal "Ziel"
    In diesem Projekt geht darum mit Hilfe eines Containers einen Linter in eine Pipeline zu integrieren.

!!! tipp "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der Folien zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - In Grafana Einloggen

### 1.1 Grafana öffnen

- Öffnen Sie die Grafana-Instanz für diese Schulung: [http://grafana.code-X.labs.corewire.de/](http://grafana.labs.corewire.de/)
- Ersetzen Sie `code-X` durch Ihren Benutzernamen
- Loggen Sie sich mit den Zugangsdaten ein, die Sie von Ihrem Trainer erhalten haben

## Aufgabe 2 - Dashboard erstellen

### 2.1 Data Source hinzufügen

Im Moment gibt es noch nicht viel zu sehen. Um zu starten müssen wir eine Datenquelle hinzufügen.

- Klicken Sie auf `Connections` in der linken Menüleiste
- Klicken Sie auf `Add new connection`
- Suchen Sie nach `TestData`
- Klicken Sie auf `Add new connection` oben rechts
- **Wichtig:** Aktivieren Sie die Checkbox `Default`
- Klicken Sie auf `Save & Test`

### 2.2 Dashboard erstellen

Die Test Data Data Source bringt bereits ein erstes Dashboard mit. Dieses möchen wir hier anlegen.

- Klicken Sie auf den Reiter `Dashboards` in der neuen Datasorce
- Klicken Sie auf `Import` für das `Simple Streaming Example`

### 2.3 Dashboard öffnen

- Klicken Sie auf `Dashboards` in der linken Menüleiste
- Wählen Sie das Dashboard `Simple Streaming Example` aus

## Aufgabe 3 - Logs monitoren

### 3.1 Logs einsehen

- Klicken Sie auf `Explore` in der linken Menüleiste
- Wählen Sie die Datenquelle `Loki` aus
- Klicken Sie oben rechts auf `Builder`
- Klicken Sie auf `Select label` und wählen Sie `container`
- Klicken Sie auf `Select value` und wählen Sie `gitlab`
- Klicken Sie auf `Run query`

### 3.2 Logs finden

- Loggen Sie sich in einem neuen Tag in Gitlab ein
- Erzeugen Sie einen 404 Fehler indem Sie einen frei ausgedachten Prad aufrufen
- Gehen Sie zurück zu Grafana und suchen Sie im Log nach dem 404 Fehler:
    - Klicken Sie auf `Code` in ihrem Querry
    - Fügen Sie folgenden Code ein:

```sh
{container="gitlab"} | json | status="404"
```

- Hier werden auch die Requests der anderen Teilnehmer:innen angezeigt. Um nur die eigenen Requests zu sehen, können Sie direkt nach dem eigenen Pfad suchen:

```sh
{container="gitlab"} | json | uri="<ihr ausgedachter pfad>"
```

- Im weiteren werden wir Logs aus dem Container `flog` betrachten. Flog ist ein Service der Dummy Logs generiert. Um die Logs zu sehen, können Sie folgende Query verwenden:

```sh
{container="flog"}
```

- Diese lassen sich ebenfalls filtern. Probieren Sie verschiedenes aus. Sie können auch mehrere Filter kombinieren, zum Beispiel:

```sh
{container="flog"} | json | request="/content" status="501"
```

## Aufgabe 4 - Eigenes Dashboard erstellen

### 4.1 Dashboard erstellen

- Klicken Sie auf `Dashsboards` in der linken Menüleiste
- Klicken Sie auf `New`
- Klicken Sie auf `New Dashboard`
- Klicken Sie auf `Add visualisation`
- Wählen Sie `Loki` als Datenquelle aus
- Geben sie folgende Query ein:

```sh
sum by (status) (count_over_time({container="flog"} | json [10m]))
```
- Klicken Sie `Run query`
- Im rechen Menü geben Sie `Requests nach Statuscode` als Titel ein
- Klicken Sie auf `Apply` um das Panel zu erstellen

Sie sehen nun die Anzahl der Requests nach Status Code gruppiert. Da es sich hier um Dummy Daten handelt, können wir keine Pattern erkennen.

### 4.2 Zweites Panel hinzufügen

- Klicken Sie auf `Add`
- Wählen Sie `Visualisation`
- Geben Sie folgende Query ein:

```sh
sum by (method) (count_over_time({container="flog"} | json [10m]))
```
- Klicken Sie `Run query`
- Im rechten Menü geben Sie `Requests nach Methode` als Titel ein
- Klicken Sie auf `Apply` um das Panel zu erstellen

### 4.3 Dashboard speichern

- Klicken Sie auf das Disketten-Symbol oben rechts
- Geben Sie einen Namen für das Dashboard ein
- Klicken Sie auf `Save`