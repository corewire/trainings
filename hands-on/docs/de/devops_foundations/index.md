# DevOps Grundlagen

- [Zu den Aufgaben](./aufgaben/01-devops-with-gitlab.md)
- [Zu den Folien](./01-introduction/#/)

## Dauer

2 Tage

## Zielgruppe

- Softwareentwickler:innen
- Software-Architekt:innen
- Systemadministrator:innen
- Projektmanager:innen
- QS-Teams
- Führungskräfte

## Voraussetzungen

- IT Grundlagen
- Verständnis für Abläufe in der Softwareentwicklung

## Kursziel

Der Kurs vermittelt die grundlegenden Prinzipien und Praktiken von DevOps, einer modernen Herangehensweise an die Zusammenarbeit zwischen Entwicklung und Betrieb. Zu Beginn werden die Kernprinzipien und der kulturelle Wandel behandelt, der durch die Integration von Entwicklung und Betrieb notwendig ist. Anschließend lernen die Teilnehmer:innen, wie sie durch Automatisierung, kontinuierliche Integration und kontinuierliches Deployment (CI/CD) die Effizienz und Qualität in der Softwareentwicklung steigern können. Der zweite Teil des Kurses fokussiert sich auf Observability und das Management von Infrastruktur als Code (IaC). Am Ende des Kurses sind die Teilnehmer:innen in der Lage, DevOps-Methoden erfolgreich in ihre Entwicklungs- und Betriebsprozesse zu integrieren und kontinuierliche Verbesserungen im Team zu fördern. Der Kurs richtet sich an Teams und Einzelpersonen, die ihre Effizienz durch den Einsatz von DevOps verbessern möchten.

## Schulungsform

Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt

### 1. Tag
- Einführung in DevOps: Motivation, Ziele und Anwendungsfälle
- Kulturwandel: Zusammenarbeit zwischen Entwicklung und Betrieb
- Grundlegende DevOps-Prinzipien: Automatisierung, kontinuierliche Integration (CI) und kontinuierliche Bereitstellung (CD)
- Einführung in CI/CD-Pipelines: Aufbau und Implementierung
- Einführung in die Containervirtualisierung: Unterschiede zu virtuellen Maschinen, Vorteile von Containern (Isolierung, Effizienz, Portabilität) Container-Technologien (z.B. Docker) und deren Bedeutung für DevOps-Prozesse
- Automatisierte Tests und kontinuierliche Qualitätskontrolle
- Überblick über DevOps-Tools: Jenkins, GitLab CI, Travis CI, etc.

### 2. Tag

- Bedeutung von Cloud-Computing und Ressourcen on Demand: Elastische Skalierung, Kosteneffizienz, Flexibilität
- Infrastructure as Code (IaC): Einführung in Tools wie Terraform und Ansible
- Deployment-Strategien: Blue-Green, Rolling, Canary-Deployments
- Monitoring und Logging: Grundlegende Konzepte und Tools (z.B. Prometheus, Grafana)
- Automatisierung von Infrastruktur- und Anwendungsmanagement
- Monitoring und Alerts: Systeme zur Überwachung von Produktionsumgebungen
- Skalierung und Hochverfügbarkeit von Anwendungen in DevOps-Umgebungen
- Sicherheitsaspekte in DevOps: DevSecOps und Security Best Practices
- Ausblick: GitOps
