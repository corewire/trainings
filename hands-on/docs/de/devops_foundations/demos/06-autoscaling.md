# Autoscaling

- Von Hand in AWS ein Launchtemplate + ASG erstellen
- In die Maschine einloggen

```shell
sudo apt update
sudo apt install cpulimit
```

- Folgendes Script anlegen und anschließend ausführen:

```bash
#!/bin/bash

# Start an infinite loop to consume CPU
while :
do
    :
done &

# Get the process ID of the loop
PID=$!

# Use cpulimit to limit the CPU usage to 80%
cpulimit -p $PID -l 80
```