# CI/CD mit Gitlab

- Hintergrund: Zeigen wann und wie eine Pipeline läuft. Inhalt der Pipeline noch nicht relevant.
- Fokus: Wie ist eine Pipeline im Kontext eines Projektes eingebettet.

## Gitlab zeigen

- Projekt `mkdocs` suchen
- Projekt kurz zeigen
- Gitlab-Pages dazu kurz zeigen
- Issue erstellen
- MR erstellen
- Datei verändern
  - z.B. Neuen Blog-post anlegen (Datei unter blog/posts)
  - oder bestehenden verändern
- Pipeline anschauen
- MR anschauen
  - Issue closing Pattern (z.B. `closes` zu `relates` ändern)
  - Tabs: Commits, Pipelines,Changes
  - Approval, Draft
- Issue geschlossen
