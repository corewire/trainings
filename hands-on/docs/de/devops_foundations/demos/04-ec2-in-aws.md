# EC2 in AWS

- Hintergrund: Wir zeigen einmal generell was Cloud ist und wie es funktioniert am Beispiel von EC2. Im nächsten Kapitel zeigen wir Terraform und wie wir die gleich Infrastruktur mit Terraform aufbauen können.
- Fokus: Komplexität von Cloud-Infrastruktur und wie schnell man Fehler machen kann.
- Story: Wir haben eine neue Anwendung entwickelt und wollen diese in der Cloud deployen. Wir haben uns für AWS entschieden und wollen eine EC2-Instanz aufsetzen.

## Vorbereitung

- AWS User erstellen

## Content

### AWS

- AWS kurz vorstellen
- VPC erstellen
- Subnet erstellen
- Internet Gateway erstellen
- Internet Gateway an VPC anhängen
- Route Table erstellen
- Route Table configurieren
- Route Table an Subnet anhängen
- Security Group erstellen
- Instanz erstellen

### Instanz erstellen

- Ubuntu
- Mit SSH-Key
- Userdata:

```bash
#!/bin/bash

apt-get update
apt-get install -y nginx curl unzip

curl --output mkdocs_artifact.zip --location "https://gitlab.com/corewire/hands-on/devops/mkdocs/-/jobs/artifacts/main/download?job=pages"

unzip mkdocs_artifact.zip -d /tmp/mkdocs
rm -rf /var/www/html/*
mv /tmp/mkdocs/public/* /var/www/html/
chown -R www-data:www-data /var/www/html
systemctl restart nginx
```
