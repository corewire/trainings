# Gitlab CI

- Hintergrund: Inhalt einer Pipeline zeigen, was tut sie, was kann sie

## Gitlab-ci im Hands-On Repo zeigen
- `.gitlab-ci.yaml` in Web IDE öffnen und zeigen, Fokus auf Stages und Jobs
- Test-Job bearbeiten
    ```
      script:
        - pwd
        - ls -la
    ```
- Job-Log ansehen, man sieht auch einen `public`-Ordner, der eig. nicht Teil des Repos ist.
- Artefacts vom `build` Job erklären
- "Wollen wir uns mal ansehen" -> `- tree .` zum Test-Job hinzufügen
- Job schlägt fehl, weil `tree` fehlt
    -> "Roter" Job, Return-Code relevant
    -> "Was können wir eig. alles ausführen?"
- Fixen mit `apt update && apt install -y tree`:
    ```
      script:
        - pwd
        - ls -la
        - apt update && apt install -y tree
        - tree .
    ```
- Job-Log und Inhalt von `public` anschauen

## Überleitung zu Container-Kapitel

- "Was läuft da eig" ->
    ```
    test:
        stage: test
        script:
            - uname -a
            - cat /etc/os-release
    ```
- Job-Log-Output ansehen
- Image ändern
    ```
    test:
        stage: test
        image: alpine
        script:
            - uname -a
            - cat /etc/os-release
    ```
