# EC2+ECS in Terraform

- Hintergrund/Ziel: Die gleich Infrastruktur wie im Showcase davor aufbauen, aber mit Terraform.
- Fokus: Terraform als Lösung für das Problem der manuellen Fehler/fehlenden Reproduzierbarkeit.
- Optional: Anstelle von EC2 einen ECS-Service verwenden. Beispiel Code ist ebenfalls im unten verlinkten Repo.
- [https://gitlab.com/corewire/technology-and-research/aws/ec2-in-terraform](https://gitlab.com/corewire/technology-and-research/aws/ec2-in-terraform)

## Vorbereitung

- Terraform ordner erstellen und eine `secret.auto.tfvars` Datei erstellen
- Credentials in `secret.auto.tfvars` speichern
    ```
    AWS_ACCESS_KEY_ID="..."
    AWS_SECRET_ACCESS_KEY="..."
    AWS_SESSION_TOKEN="..."
    ```

## Content

### Teraform

- Terraform kurz vorstellen
- Terraform nach und nach aufbauen
    - immer wieder `terraform plan` und `terraform apply` ausführen
    - provider.tf
    - variables.tf
    - main.tf

Ein fertiges Beispiel kann hier gefunden werden:

https://gitlab.com/corewire/technology-and-research/aws/ec2-in-terraform

Das Projekt kann an die Teilnehmer:innen raus gegeben werden.
