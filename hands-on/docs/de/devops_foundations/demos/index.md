# Timetable

## 1. Tag
- Einführung (Dauer: 2h)
    - Showcase: [CI/CD in Gitlab](./01-ci-cd-with-gitlab/)
    - Hands-on: [DevOps Workflows](../aufgaben/01-devops-with-gitlab/)
- Pipelines: (Dauer: 1h)
    - Showcase: [Einfache Pipeline](./02-gitlab-ci/)
- Container: (Dauer: 2,5h)
    - Showcase: [Container](./03-container/)
    - Hands-on: [Container in Pipelines](../aufgaben/02-container-in-pipelines/)
- Cloud: (Dauer: 1h)
    - Showcase: [EC2 in AWS](./04-ec2-in-aws/)

## 2. Tag
- X as Code: (Dauer: 1,5h)
    - Showcase: [EC2+ECS in Terraform](./05-ec2+ecs-in-terraform/)
- Skalieren und Microservices: (Dauer: 1,5h)
    - Showcase: [Autoscaling](./06-autoscaling/)
- Observability: (Dauer: 1,5h)
    - Hands-on: [Monitoring und Logging](../aufgaben/03-monitoring/)
- Ausblick: (Dauer: TBD)
    - Showcase: [GitOps](./08-gitops/)
