# Cluster Setup

!!! goal "Ziel"
    In diesem Projekt setzen Sie einen Kubernetes-Cluster auf. Dabei werden Sie:
    - `containerd`, `runc` und die `cni-plugins` auf allen Cluster-Nodes installieren
    - `kubeadm` auf allen Cluster-Nodes installieren
    - die Worker Nodes joinen
    - ein Netzwerk-Plugin installieren
    - ein Deployment und einen Service erstellen

## Aufgabe 1: Installation von containerd

In dieser Aufgabe installieren Sie `containerd`, `runc` und die cni-plugins auf allen Cluster-Nodes.

- Erstellen Sie ein Shell-Skript `install-containerd.sh`, das die Installation von `containerd` auf durchführt:

```bash
CONTAINERD_VERSION=2.0.2
RUNC_VERSION=1.2.4
CNI_PLUGIN_VERSION=1.6.2
# Install Containerd
wget https://github.com/containerd/containerd/releases/download/v${CONTAINERD_VERSION}/containerd-${CONTAINERD_VERSION}-linux-amd64.tar.gz
tar Cxzvf /usr/local containerd-${CONTAINERD_VERSION}-linux-amd64.tar.gz
rm containerd-${CONTAINERD_VERSION}-linux-amd64.tar.gz
mkdir -p /usr/local/lib/systemd/system/
wget https://raw.githubusercontent.com/containerd/containerd/main/containerd.service -O /usr/local/lib/systemd/system/containerd.service
systemctl daemon-reload
systemctl enable --now containerd

# Install runc
wget https://github.com/opencontainers/runc/releases/download/v${RUNC_VERSION}/runc.amd64
install -m 755 runc.amd64 /usr/local/sbin/runc

# Install CNI plugins
mkdir -p /opt/cni/bin
wget https://github.com/containernetworking/plugins/releases/download/v${CNI_PLUGIN_VERSION}/cni-plugins-linux-amd64-v${CNI_PLUGIN_VERSION}.tgz
tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v${CNI_PLUGIN_VERSION}.tgz
rm cni-plugins-linux-amd64-v${CNI_PLUGIN_VERSION}.tgz
```

- Um das Skript nicht auf jeder Node einzeln ausführen zu müssen, erstellen wir ein Skript `run-on-nodes.sh`, dass das Skripte auf allen Nodes ausführt:

```bash
#!/bin/bash
servers=("controlplane1" "worker1" "worker2")
script_path="$1"

for server in "${servers[@]}"; do
  echo "Running script on $server"
  ssh root@"$server" 'bash -s' < "$script_path"
done
```

- Machen Sie das Skript ausführbar:

```bash
chmod +x run-on-nodes.sh
```

- Führen Sie das Skript aus:

```bash
./run-on-nodes.sh install-containerd.sh
```

## Aufgabe 2: Installation von Kubeadm

In dieser Aufgabe installieren Sie `kubeadm` auf allen Cluster-Nodes.

- Erstellen Sie ein Shell-Skript `install-kubeadm.sh`, das die Installation von `kubeadm` durchführt:

```bash
# Disable swap (required for kubeadm)
sudo swapoff -a
sed -i '/swap/d' /etc/fstab

# Set sysctl parameters for Kubernetes networking
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF

sudo sysctl --system
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gpg
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.32/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.32/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```

- Führen Sie das Skript auf allen Nodes aus:

```bash
./run-on-nodes.sh install-kubeadm.sh
```

## Aufgabe 3: Cluster erstellen

- Verbinden Sie sich per ssh auf die Controlplane-Node und führen Sie folgenden Befehl aus:

```bash
kubeadm init
```

- Kopieren Sie den Befehl zum Hinzufügen von Nodes in den Cluster in eine Datei. Wir werden die Worker Nodes später hinzufügen.

### Aufgabe 3.1: Zum Cluster verbinden

- Konfigurieren Sie `kubectl` die Cluster Konfiguration zu verwenden:

```bash
export KUBECONFIG=/etc/kubernetes/admin.conf
```

- Überprüfen Sie, ob der Cluster funktioniert:

```bash
kubectl cluster-info
```

- Überprüfen Sie, ob die Nodes im Cluster vorhanden sind:

```bash
kubectl get nodes
```

Die Controlplane-Node ist noch nicht ready. Das ist normal, da wir noch keine Worker Nodes hinzugefügt haben.

- Die Controlplane Komponenten `kube-apiserver`, `kube-controller-manager`, `kube-scheduler` und `etcd` sind auf der Controlplane-Node gestartet. Überprüfen Sie, ob die Pods laufen:

```bash
kubectl get pods -n kube-system
```

### Aufgabe 3.2: Netzwerk Setup

Damit der Cluster funktioniert, müssen wir noch ein Netzwerk-Plugin installieren. Wir verwenden `Cillium`.  Führen Sie die folgenden Befehle auf der Controlplane aus um die `cillium cli` zu installieren:

```bash
CILIUM_CLI_VERSION=$(curl -s https://raw.githubusercontent.com/cilium/cilium-cli/main/stable.txt)
CLI_ARCH=amd64
curl -L --fail --remote-name-all https://github.com/cilium/cilium-cli/releases/download/${CILIUM_CLI_VERSION}/cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
sha256sum --check cilium-linux-${CLI_ARCH}.tar.gz.sha256sum
sudo tar xzvfC cilium-linux-${CLI_ARCH}.tar.gz /usr/local/bin
rm cilium-linux-${CLI_ARCH}.tar.gz{,.sha256sum}
```

- Installieren Sie `cilium` im Cluster:

```bash
cilium install --version 1.17.1
```

- Cilium wurde als DaemonSet installiert. Überprüfen Sie, ob die Pods laufen:

```bash
kubectl get pods -n kube-system
```

## Aufgabe 4: Worker Nodes hinzufügen

- Verbinden Sie sich per ssh auf die Worker Nodes und fügen Sie die Worker Nodes dem Cluster hinzu. Verwenden Sie dazu den Befehl, den Sie in der Datei aus Aufgabe 3 gespeichert haben.
- Loggen Sie sich wieder in die Controlplane-Node ein und überprüfen Sie, ob die Worker Nodes hinzugefügt wurden:

```bash
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl get nodes
```

Die Worker Nodes sind eventuell noch nicht ready, Sie starten gerade noch. Nach kurzer Zeit sollten alle Nodes ready sein.

## Aufgabe  5: Cluster nutzen

### Aufgabe 5.1: Kubeconfig lokal nutzen

- Kopieren Sie die Kubeconfig Datei von der Controlplane-Node auf Ihren lokalen Rechner:

```bash
scp root@controlplane1:/etc/kubernetes/admin.conf ~/.kube/my-cluster.conf
```

- Setzen Sie die Umgebungsvariable `KUBECONFIG`:

```bash
export KUBECONFIG=~/.kube/my-cluster.conf
```

### Aufgabe 5.2: Deployment erstellen

- Erstellen Sie ein Deployment `nginx`:

```bash
kubectl create deployment nginx --image=nginx
```

- Überprüfen Sie, ob das Deployment erstellt wurde:

```bash
kubectl get all
```

### Aufgabe 5.3: Service erstellen

- Erstellen Sie einen Service, um das Deployment zu erreichen:

```bash
kubectl expose deployment nginx --port=80 --target-port=80 --type=NodePort
```

- Überprüfen Sie, ob der Service erstellt wurde:

```bash
kubectl get services
```

- Merken Sie sich den Nodeport des Services
- Lassen Sie sich die IP-Adresse der Worker Nodes anzeigen:

```bash
kubectl get nodes -o wide
```

- Öffnen Sie einen Browser und geben Sie die IP-Adresse einer Worker Node und den Nodeport des Services ein. Sie sollten die Nginx-Willkommensseite sehen.