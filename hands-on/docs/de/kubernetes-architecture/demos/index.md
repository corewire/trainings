# Timetable

TODO: noch nicht fertig, muss noch angepasst werden

## 1. Tag
- 01-Einführung (Dauer: 1h)
- 02-Erste Schritte (Dauer: 2h)
    - Showcase: [Erste Schritte](./01-first-steps/)
    - Hands-on: [Erste Schritte](../aufgaben/01-first-steps/)
- 03-Pod Management (Dauer: 1h)
    - Showcase: [Pod Management](./02-pod-management/)
    - Hands-on: [Pod Management](../aufgaben/02-pod-management/)
- 04-Konfiguration (Dauer: 1h)
    - Showcase: [Konfiguration](./03-configuration/)
    - Hands-on: [Konfiguration](../aufgaben/03-configuration/)
- 05-Volumes (Dauer: 1h)
    - Folien + Showcase
    - Showcase: [Volumes](./04-volumes/)

## 2. Tag

- 05-Volumes (Dauer: 0.5h)
    - Hands-on: [Volumes](../aufgaben/04-volumes/)
- 06-Pod Management 2 (Dauer: 1.5h + Handon)
    - Showcase: [Pod Management 2](./06-pod-management-2/)
    - Hands-on: [Pod Management 2](../aufgaben/05-pod-management-2/)
- 07-Architektur (Dauer: 3h + Handson)
- 08-Netzwerk (Dauer: 1h)
    - Showcase: [Netzwerk](./05-networking/)
    - Hands-on: [Netzwerk](../aufgaben/07-networking/)

## 3. Tag

- 09-Nodes und Pods (Dauer: 2h)
    - Showcase: [Nodes und Pods](./09-nodes-and-pods/)
    - Hands-on: [Nodes und Pods](../aufgaben/08-nodes-and-pods/)
- 10-Access Control (Dauer: 2h)
    - Showcase: [Access Control](./10-access-control/)
    - Hands-on: [Access Control](../aufgaben/11-access-control/)
- 11-Security und Compliance (Dauer: 1.5h)
- 12-Ausblick (Dauer: 30 min)
