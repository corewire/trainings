# OICD mit K8s

## Vorraussetzungen

- Keycloak Zugang
- Kubelogin ist installiert

## Keycloak einrichten

- Relm in Keycloak anlegen
- Client in Keycloak anlegen
- User im Keycloak anlegen
- UserID notieren
- Client ID notieren
- Issuer URL notieren: https://keycloak.example.com/realms/REALM_NAME

## Kubernetes Apiserver konfigurieren

- In der Datei `/etc/kubernetes/manifests/kube-apiserver.yaml` folgende Zeilen hinzufügen:

```yaml
--oidc-issuer-url=ISSUER_URL
--oidc-client-id=YOUR_CLIENT_ID
```

- Warten bis der Apiserver neugestartet ist

## Clusterrolle einrichten

- Clusterrolle anlegen:

```bash
kubectl create clusterrolebinding oidc-cluster-admin --clusterrole=cluster-admin --user='ISSUER_URL#YOUR_SUBJECT'
```

## Lokalen Login einrichten

- Konfig vom Apiserver holen:

```bash
scp root@master:/etc/kubernetes/admin.conf ~/.kube/your-cluster-name.conf
```

- Kubelogin zeigen: https://github.com/int128/kubelogin
- OIDC Authentifizierung in K8s:

```bash
kubectl oidc-login setup --oidc-issuer-url=ISSUER_URL --oidc-client-id=YOUR_CLIENT_ID
```

## Testen

```bash
kubectl --user=oidc-user --kubeconfig=/path/to/kubeconfig get nodes
```

