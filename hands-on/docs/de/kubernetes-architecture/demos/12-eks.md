# Running an AWS EKS Cluster

## Vorbereitung

- AWS EKS Cluster vorher schon starten, start dauter lange...
- VPC erstellen
- Subnetze erstellen
- Subnetze labeln
  - `kubernetes.io/role/internal-elb=1` für private netze
  - `kubernetes.io/role/elb=1` für public netze

## EKS Cluster starten

- In der Oberfläche zusammen klicken
  - Public Subnetzer wählen (für ELB)
  **TODO:** hier sollten private und public verwendet werden 

## Zum Cluster verbinden

Ab hier den vorher gestarteten Cluster verwenden

- AWS-Zugangsdaten als Umgebungsvariablen, aus dem AWS Access Portal setzen
- Kubenconfig setzen:

```bash
aws eks update-kubeconfig --region eu-central-1 --name 
```

- Cluster testen:

```bash
kubectl get nodes
```

## Pod starten

- Pod starten:

```bash
kubectl run nginx --image=nginx
```

- Zeigen das pod pending ist

```
kubectl get pods
```

- Oberfläche zeigen wie maschinen gestartet werden

## Deployment erstellen

- Deployment erstellen:

```bash
kubectl create deployment nginx --image=nginx --replicas=3
```

- Deployment zeigen:

```bash
kubectl get deployments
```

## Service erstellen

- Service Datei erstellen:

```bash
kubectl create service loadbalancer nginx-lb --tcp=80:80 -oyaml --dry-run=client > /tmp/ekslb.yaml
```

- Service bearbeiten:

```yaml
  annotations:
    service.beta.kubernetes.io/aws-load-balancer-scheme: "internet-facing"
```

```yaml
  selector:
    bla: blubb
```

- Service erstellen:

```bash
k apply -f /tmp/ekslb.yaml
```

- In Oberfläche zeigen, dass LB erstellt wurde
- Domain abrufen:

```bash
kubectl get svc
```

- Domain im Browser öffnen





