# Nutzung von Docker und containerd unter Linux

In diesem Showcase demonstrieren wir, wie man unter Linux einen Container mit
Docker starten kann und anschließend den Wechsel zu containerd durchführt. Der
nginx-Container wird in beiden Fällen als Beispiel verwendet, wobei der Port 80
am Host verfügbar gemacht wird.

## Voraussetzungen
- Ein Linux-System mit installiertem Docker und containerd.
- Nutzerrechte mit `sudo` oder Zugriff auf die Docker- und containerd-Daemons.

---

## Teil 1: Container mit Docker starten

1. Überprüfen, ob Docker läuft:
   ```bash
   systemctl status docker
   ```

1. Einen nginx-Container starten und Port 80 bereitstellen:
   ```bash
   docker run -d -p 8080:80 nginx
   ```
   - Überprüfen, ob der Container läuft:
     ```bash
     docker ps
     ```

1. Container stoppen und entfernen:
   ```bash
   docker stop <container id>
   docker rm <container id>
   ```

---

## Teil 2: Wechsel zu containerd


1. Überprüfen, ob containerd läuft:
   ```bash
   systemctl status containerd
   ```

1. Container Image herunterladen:
  ```bash
  ctr image pull docker.io/library/nginx:latest
  ```
1. Container starten und Port 80 freigeben:
  ```bash
  ctr run -d --net-host docker.io/library/nginx:latest hello-containerd
  ```
1. Überprüfen, ob der Container läuft:
  ```bash
  ctr tasks ls
  ```

1. Container stoppen (falls nicht automatisch entfernt):
  ```bash
  ctr task kill hello-containerd
  ```
