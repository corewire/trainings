# Kubernetes Architektur

- [Zu den Aufgaben](./aufgaben/01-first-steps/)
- [Zu den Cheatsheets](./cheatsheets/01-first-steps/)
- [Zu den Folien](./01-introduction/#/)

## Dauer

3 Tage

## Zielgruppe

- Softwareentwickler:innen
- Software-Architekt:innen
- Systemadministrator:innen
- DevOps-Engineers

## Voraussetzung

- Arbeiten mit Linux
- Arbeiten auf der Konsole
- Containerisierung (Docker oder Podman)
    - Verwalten von Containern (starten, stoppen, protokollieren, ausführen)
    - Erstellen von Container-Images

## Kursziel

- Lernen, wie man Anwendungen auf Kubernetes bereitstellt
- Verstehen der Konzepte von Kubernetes

## Schulungsform

Der Trainer führt in jedes Thema mit einem Foliensatz ein und führt die Anwendung des Themas vor. Die Teilnehmer wenden ihr Verständnis durch das Lösen der Übungen an und vertiefen es.

## Kursinhalt

TODO
