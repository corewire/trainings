# Einführung in CI/CD-Pipelines mit Gitlab

- [Zu den Aufgaben](./aufgaben/01-ci-cd-pipelines-in-gitlab.md)
- [Zu den Folien](./1-introduction/#/)


## Dauer
1 Tag

## Zielgruppe
- DevOps-Engineers
- Softwareentwickler:innen
- Systemadministrator:innen

## Voraussetzungen
- Grundlagen in Git
- Grundlagen in Docker
- Grundlagen mit dem Terminal/Bash

## Kursziel
Der Kurs vermittelt den Umgang und technische Hintergründe zu CI/CD-Pipelines mit Gitlab. Zu Beginn werden Motivation, Hintergründe und die Einbindung in bestehende Entwicklungsprozesse besprochen. Anschließend werden anhand von Szenarien und Beispielen die Möglichkeiten einer CI/CD-Pipeline von einfach bis komplex vorgestellt. Der letzte Teil des Kurses geht auf Tips und Best Practices in Bezug auf Sicherheit, Troubleshooting und Adminstration ein.

## Schulungsform
Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt
- Grundkonzepte CI/CD und DevOps, Anwendungsfälle für CI/CD
- Verwendung einer CI/CD-Pipeline in Gitlab
- Grundlagen einer einfachen Pipeline (.gitlab-ci.yml)
- Gitlab Runner und unterschiedliche Executors
- Komplexes Szenario mit Review-Umgebung und SSH-Deplyment
- Komplexes Szenario mit Artefakten, Docker und Container-Registry
- Unterschiedliche Pipeline-Architekturen
- Security-Hinweise, Best Practices und Troubleshooting
- Gitlab Runner einrichten und verwalten
