# Administration

!!! goal "Ziel"
    In diesem Projekt geht es um Gitlab Runner. Sie sollen:

    - Einen Runner starten
    - Den Runner registrieren
    - Einen Job auf dem Runner ausführen


!!! tipp "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](../../6-administration/#/) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Runner starten

- Gehen Sie auf ihre VSCode Instanz: `code-{ZAHL}.labs.corewire.de`
- Erstellen Sie einen Ordner `gitlab-runner` in ihrem Workspace
- Starten Sie einen Runner. Verwenden Sie für die Config den eben erstellten Ordner.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Links im Menü können Sie den Dateiexplorer (Symbol über der Lupe) öffnen
    - Erstellen Sie mit Rechtsklick auf den Dateiexplorer den Ordner `gitlab-runner`
    - Öffnen Sie nun über das Menu (Symbol über dem Dateiexplorer) ⇨ Terminal ⇨ New Terminal ein Terminal
    - Führen Sie folgenden Befehl aus:
    ```
    docker run -d --name gitlab-runner --restart always  -v /home/coder/workspace/gitlab-runner:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock  gitlab/gitlab-runner:latest
    ```

## Aufgabe 2 - Runner registrieren

### 2.1 Runner registrieren

- Registrieren Sie den Runner als Specific Runner für ihr gitlab-ci-demo Projekt
    - Fügen Sie ein Tag (z.B. user-0-runner) hinzu
    - Wählen Sie als Executor `docker`

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Führen Sie folgenden Befehl aus:
    ```
    docker run --rm -it -v /home/coder/workspace/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register
    ```
    - Geben Sie als Gitlab instance URL `https://git.labs.corewire.de` an.
    - Den Token finden Sie im Gitlab in ihrem Projekt unter Settings ⇨ CI/CD ⇨ Runners
    - Geben Sie dem Runner einen Namen
    - Geben Sie dem Runner ein Tag
    - Die Maintenance Note können Sie überspringen
    - Executor: `docker`
    - Docker image: `ubuntu:22.04`

### 2.2 Runner überprüfen

- Überprüfen Sie, dass der Runner erfolgreich registriert wurde.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Auf der Seite, auf der Sie den Token gefunden haben, sollte nun auch der Runner mit dem von ihnen angegebenen Namen angezeigt werden
    - Gegebenfalls müssen Sie die Seite neu laden

## Aufgabe 3 - Job auf Runner starten

- Starten Sie einen Job auf dem neuen Runner
- Passen Sie dafür einen Job in der `.gitlab-ci.yml` entsprechend an

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Gehen Sie auf den Pipeline-Editor ihres gitlab-ci-demo Projekts.
    - Passen Sie die `.gitlab-ci.yml` zum Beispiel wie folgt an. Ggfs. müssen Sie den Tag anpassen.
    ```yml
        Demo:
            tags:
                - user-0-runner
            script:
                - echo $CI_PIPELINE_SOURCE
                - echo $CI_PIPELINE_ID
                - echo $CI_PIPELINE_IID
                - sleep 60
    ```
    - Commiten Sie die Änderung und triggern Sie dadurch die Pipeline
    - In den Job-Details können Sie oben rechts sehen, auf welchem Runner ihr Job ausgeführt wird.

