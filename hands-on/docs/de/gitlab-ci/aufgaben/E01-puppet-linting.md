# Puppet Pipeline

!!! goal "Ziel"
    In diesem Projekt geht es darum lokal und in der Pipeline Puppetcode zu linten.

!!! tipp "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben selbständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Lokal linten

- Gehen Sie auf ihre VSCode Instanz: `code-{ZAHL}.labs.corewire.de`
- Wechseln Sie in den Ordner `puppet-linting` in ihrem Workspace
- Schauen Sie sich die Projektdateien an. Die wichtigsten Inhalte befinden sich in:
    - `Makefile`
    - `.gitlab-ci.yml`
    - `modules/examples/manifests/init.pp`
- Führen Sie `make validate` aus, um den Linter lokal laufen zu lassen
- Schauen sie sich die auftretenden Fehler an.

## Aufgabe 2 - Linten in der Pipeline

### 2.1 Issue anlegen

- Wechseln Sie zu Gitlab und erstellen Sie ein neues Issue im Repository `puppet-linting`.
- Geben Sie als Titel des Issues `Linter Fehler beheben` ein.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Stellen Sie sicher, dass Sie im Projekt `puppet-linting` sind. Oben links im Menü steht das aktuell geöffnete Projekt
    - Klicken Sie links im Menü auf **Issues**
    - Auf dieser Seite können Sie rechts oben über "New Issue" ein Issue anlegen
    - Geben Sie den Title ein
    - Weitere Eingaben sind nicht erforderlich
    - Erstellen Sie das Issue unten mit "Create Issue"

### 2.2 Merge Request anlegen

- Das eben erstellte Issue möchten Sie nun bearbeiten/lösen.
- Erstellen Sie aus dem Issue heraus einen Merge Request:
    - Weisen Sie sich den Merge Request zu
    - Erstellen Sie den Merge Request

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Mit dem Erstellen des Issues wurden Sie automatisch auf das Issue weitergeleitet
    - Hier finden Sie den Button "Create Merge Request" mit einem **Pfeil** daneben.
    - Klicken Sie auf den **Pfeil**
    - Klicken Sie auf "Create Merge Request"
    - Auf der Seite "New merge request", die sich geöffnet hat, können Sie sich unter "Assignee" den Merge Request zuweisen.
    - Erstellen Sie anschließend den Merge Request

### 2.3 Source Code bearbeiten

- Wechseln Sie zurück in VSCode und checken Sie den zum Merge Request gehörenden Branch aus
- Machen Sie eine kleine Änderung an der `README.md` der Inhalt ist hier nicht wichtig
- Erstellen Sie mit ihrer Änderung einen Commit
- Pushen Sie Ihre Änderungen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Klicken Sie im Merge Request oben rechts auf den blauen Button **Code**
    - Klicken Sie auf **Check out Branch**
    - Kopieren Sie die Befehle unter Step 1
    - Wechseln Sie in das Terminal in VSCode.
    - Stellen Sie sicher, dass Sie sich im Ordner `puppet-linting` befinden.
    - Führen Sie die kopierten Befehle aus
    - Bearbeiten Sie die `README.md`
    - Commiten Sie Ihre Änderungen mit:
        - `git add README.md`
        - `git commit -m "Improve README"`
    - Pushen Sie die Änderungen: `git push`

### 2.4 Pipeline-Logs ansehen

- Schauen Sie sich das Job-Log an
- Lassen Sie sich die Linter Fehler in der Weboberfläche ausgeben

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Gehen Sie im Merge Request auf den Tab Pipelines (direkt unter dem Titel)
    - Hier sehen Sie die zu diesem Merge Request gehörenden Pipelines
    - Schauen Sie sich den Job im Detail mit Klick auf den **grünen Kreis** unter **Stages** an.
    - Hier sehen Sie die Logausgabe des Jobs sowie weitere Informationen.


## Aufgabe 3 - Beheben der Linterfehler

- Führen sie im VSCode `make validate-fix` aus, um alle Linterfehler zu beheben, die automatisch behoben werden können.
- Beheben Sie die übrigen Linterfehler.
- Pushen Sie ihre Änderungen auf den zum Merge Request gehörenden Branch
- Prüfen Sie ob die Pipeline nun feherfrei durchläuft.