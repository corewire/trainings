# Sync-Deployment

!!! goal "Ziel"
    In diesem Projekt geht es um:

    - bedingte Jobs
    - Zugriff per SSH
    - und die Review-Umgebung


!!! tipp "Hilfsmittel"

    - Versuchen Sie zuerst, die unten stehenden Aufgaben mit Hilfe der [Folien](../../3-sync-deployment/#/)
      und des [Cheatsheets](../cheatsheets/03-sync-deployment.md) zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1 - Bedingte Jobs

### 1.1 Pipeline Editor öffnen

- Öffnen Sie den Pipeline-Editor für ihr `gitlab-ci-demoapp` Projekt.
- Löschen Sie den Inhalt der aktuelle `.gitlab-ci.yml`.
- Fügen Sie folgende Vorlage ein:

```yml
stages:
  - build
  - test
  - deploy

build:
  stage: build
  script:
    - echo $CI_PIPELINE_SOURCE
    - echo "Building..."

test:
  stage: test
  script:
    - echo "Testing..."

deploy_review:
  stage: deploy
  script:
    - echo "Deploy review environment..."

deploy_production:
  stage: deploy
  script:
    - echo "Deploy production environment..."
```

### 1.2 Separate Deployments

Erweitern Sie die Vorlage so, dass:

- `deploy_review` nur in einem Merge Request ausgeführt wird

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Erweiten Sie `deploy_review` mit:
    ```yml
    rules:
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    ```

- `deploy_production` nur auf dem default Branch ausgeführt wird

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Erweiten Sie `deploy_production` mit:
    ```yml
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    ```

### 1.3 Deployment testen

- Triggern Sie die Pipeline so, dass einmal `deploy_production` und einmal `deploy_review` ausgeführt wird
- Beobachten Sie das Ergebnis

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Durch den Commit mit ihren Änderungen wird bereits `deploy_production` ausgeführt
    - `deploy_review` auszuführen benötigt etwas mehr Schritte
    - Erstellen Sie ein Issue oder verwenden sie ein bestehendes Issue von vorher.
    - Erstellen Sie über das Issue einen Merge Request


??? info "Beobachtung: Was ist beim Erstellen des Merge Requests passiert?"
    - Ein Commit auf den default Branch "main" hat wie erwartet die Pipeline mit `deploy_production` ausgeführt
    - Das Erstellen eines Merge Requests hat nur "build" und "test" ausgeführt.
    - **Erklärung**: Beim Erstellen einens Merge Requests wird ein Branch angelegt (= "push"-Event). Es folgt aber noch kein "merge_request_event", dss für `deploy_review` notwendig ist.
    - **Aufgabe**: Erstellen Sie über die Web IDE einen Commit in ihrem Merge Requests und beobachten Sie erneut das Ergebnis


??? info "Beobachtung 2: Was ist bei einem Commit im Merge Request passiert?"
    - Es wurden zwei Pipelines erstellt. Eine für das Push-Event und eine für das Merge-Request-Event
    - **Lösung**: Das Verhalten lässt sich zum Beispiel durch folgende Workflow-Regel beheben:
    ```yml
        workflow:
            rules:
                - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Execute jobs in merge request context
                - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH      # Execute jobs when a new commit is pushed to master branch
    ```
    - Testen Sie die Lösung


## Aufgabe 2 - Zugriff per SSH

### 2.1 Vorbereitung

- Stellen Sie sicher, dass ihre `.gitlab-ci.yml` auf dem Main-Branch wie folgt aussieht.
```yml
stages:
  - build
  - test
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Execute jobs in merge request context
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH      # Execute jobs when a new commit is pushed to master branch

build:
  stage: build
  script:
    - echo $CI_PIPELINE_SOURCE
    - echo "Building..."

test:
  stage: test
  script:
    - echo "Testing..."

deploy_review:
  stage: deploy
  script:
    - echo "Deploy review environment..."
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

deploy_production:
  stage: deploy
  script:
    - echo "Deploy production environment..."
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

### 2.2 SSH Konfigurieren

- Konfigurieren Sie den SSH Zugang für `deploy_production`

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Kopieren Sie die entsprechenden Zeilen aus den Folien oder dem Cheatsheet
    - Setzten Sie die Variablen `SSH_PRIVATE_KEY` und `SSH_KNOWN_HOSTS`
    - Der private Schlüssel für `SSH_PRIVATE_KEY` liegt auf ihrer VSCode Instanz unter `demo-ssh-key`
    - Den Wert für `SSH_KNOWN_HOSTS` erhalten Sie über:
    ```
    ssh-keyscan code-{ZAHL}.labs.corewire.de
    ```

- Legen Sie eine Datei auf ihrer VSCode Instanz an:
```
    - ssh $SSH_USER@$SSH_HOST "touch /home/coder/workspace/production.txt"
```
- Setzten Sie die Variablen wie folgt:
    - `SSH_USER`: root
    - `SSH_HOST`: code-{ZAHL}.labs.corewire.de

- Commiten Sie ihre Änderungen und überprüfen sie, ob die Datei auf ihrer VSCode Instanz angelegt wurde.

## Aufgabe 3 - Review Umgebung

### 3.1 Vorbereitung

- Stellen Sie sicher, dass ihre `.gitlab-ci.yml` auf dem Main-Branch wie folgt aussieht.
```yml
stages:
  - build
  - test
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event" # Execute jobs in merge request context
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH      # Execute jobs when a new commit is pushed to master branch

build:
  stage: build
  script:
    - echo $CI_PIPELINE_SOURCE
    - echo "Building..."

test:
  stage: test
  script:
    - echo "Testing..."

deploy_review:
  stage: deploy
  script:
    - echo "Deploy review environment..."
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

deploy_production:
  stage: deploy
  script:
    - echo "Deploy production environment..."
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y &&
        apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh $SSH_USER@$SSH_HOST "touch /home/coder/workspace/production.txt"
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

### 3.2 Review-Umgebung konfigurieren

- Konfiguriren Sie die Review-Umgebung für den `deploy_review` Job.
- Es soll eine Datei mit dem `CI_COMMIT_REF_NAME` im Namen erstellt werden.
- Die Konfiguration der URL soll nicht beachtet werden

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Der `deploy_review` Job soll wie folgt aussehen:
    ```yml
    deploy_review:
        stage: deploy
        rules:
            - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        environment:
            name: review/$CI_COMMIT_REF_NAME
            url: https://$CI_ENVIRONMENT_SLUG.example.com
        script:
            - echo "Deploy production environment..."
            - 'command -v ssh-agent >/dev/null || ( apt-get update -y &&
                apt-get install openssh-client -y )'
            - eval $(ssh-agent -s)
            - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
            - mkdir -p ~/.ssh
            - chmod 700 ~/.ssh
            - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
            - chmod 644 ~/.ssh/known_hosts
            - ssh $SSH_USER@$SSH_HOST "touch /home/coder/workspace/review_$CI_COMMIT_REF_NAME.txt"
    ```

- Testen Sie ihre Review-Umgebung, indem Sie einen Merge Request mit einem Commit erstellen. Es sollte nun eine neue Datei auf ihrer VSCode Instanz liegen
