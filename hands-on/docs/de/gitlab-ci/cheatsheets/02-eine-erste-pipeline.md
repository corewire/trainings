# Cheatsheet - Eine erste Pipeline

Alle Namen, die mit `my` anfangen können frei gewählt werden. Alle anderen Einträge sind definierte Keywords.

```yml
stages: # Reihenfolge der Stages
    - myFirstStage
    - mySecondStage

variables: # Variablem, die für alle Jobs gültig sind
    MyFirstVariable: ...
    MySecondVariable: ...

MyJob:
    image: ...   # Tag des Images, in dem alle Befehle des Jobs ausgeführt werden sollen
    stage: ...   # Referenz zu einer Stage aus "stages"
    needs:
        - ...    # Liste an Jobs, die vor diesem Jobs läufen müssen
    variables:   # Variable, die nur für diesen Job gültig ist
        MyJobVariable: ...
    script:
        - ...    # Liste an Befehlen
        - ...
```
