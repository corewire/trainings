# Cheatsheet - Async Deployment

Alle Namen, die mit `my` anfangen können frei gewählt werden. Alle anderen Einträge sind definierte Keywords.

```yml
workflow: # Bedingung für die Ausführung der gesamten Datei
    rules:
        - ... # Liste von Regeln, wann die Pipeline ausgeführt werden soll

stages: # Reihenfolge der Stages
    - myFirstStage
    - mySecondStage

variables: # Variablem, die für alle Jobs gültig sind
    MyFirstVariable: ...
    MySecondVariable: ...

default:  # Defaults für alle Jobs in dieser Datei
    before_script: # Liste an Befehlen, die vor script ausgeführt werden
        - ...
    after_script:  # Liste an Befehlen, die nach script ausgeführt werden
        - ...

MyJob:
    image: ...   # Tag des Images, in dem alle Befehle des Jobs ausgeführt werden sollen
    stage: ...   # Referenz zu einer Stage aus "stages"
    needs:
        - ...    # Liste an Jobs, die vor diesem Jobs läufen müssen
    variables:   # Variable, die nur für diesen Job gültig ist
        MyJobVariable: ...
    rules:
        - if: ...            # Bedingung
          changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
          exists: ...        # Prüft, ob bestimmte Dateien existieren
          allow_failure: ... # Job darf fehlschlagen
          variables: ...     # Variablen setzen
          when: ...          # "Wann" wird der Job ausgeführt
    artifacts:
        name: ...            # Name des Artefakts
        paths:
            - ...            # Liste von Pfaden/Verzeichnissen/Dateien
        expire_in: ...
        exclude:
            - ...            # Liste mit Pfaden/Dateien, die nicht im Artefakt enthalten sein sollen
        untracked: ...       # Dateien, die im Repository als "untracked" gelten mit einbeziehen
        when: ...            # Wann das Artefakt hochgeladen werden soll (on_success, on_failure, always)
        reports:             # Speziell untersützte Reports hochladen
    services:
        - ...                # Liste an Services, die dem Container/Job zur Verfügung gestellt werden sollen

    before_script: # Liste an Befehlen, die vor script ausgeführt werden
        - ...
    script:
        - ...      # Liste an Befehlen
        - ...
    after_script:  # Liste an Befehlen, die nach script ausgeführt werden
        - ...
```

