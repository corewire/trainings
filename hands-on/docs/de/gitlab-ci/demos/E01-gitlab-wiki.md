# Wiki

## Scenario

Es gab einen Securitry Incident, wir wollen ihn dokumentieren

- Incident aufmachen
- Ticket aufmachen
- Merge Request aufmachen

## Beispielseite aufbauen

- Generell Wiki erklären
  - Repo im Hintergrund
- Seite Anlegen `incident-overview`
- Folgende Tabelle nach und nach aufbauen:

```markdown

| Incident | Ausfall von/bis      | Issues | Mergerequests | Verantwortlicher Commit                  | Wiki Seite                  |
|----------|----------------------|--------|---------------|------------------------------------------|-----------------------------|
| #2       | 01.04.23 2:00 - 3:00 | #1     | !1            | 5715d1ab2dcf5103e7c9548ffd6fe8389af5b510 | [hier](incidents/incident1) |
|          |                      |        |               |                                          |                             |
|          |                      |        |               |                                          |                             |

```

Vorschlag für Aufbaustufen:

Mit [](https://www.tablesgenerator.com) leere Tabelle erstellen:

```markdown

| Incident | Ausfall von/bis      | Issues | Mergerequests | Verantwortlicher Commit                  | Wiki Seite                  |
|----------|----------------------|--------|---------------|------------------------------------------|-----------------------------|
|          |                      |        |               |                                          |                             |
|          |                      |        |               |                                          |                             |
|          |                      |        |               |                                          |                             |

```

- Incident verlinken
- Ausfall eintragen
- Issue verlinken
- Mergerequest verlinken
- Commit raussuchen
- Wiki Seite verlinken
- Wiki Seite durch klick auf Link erstellen