# Helm

!!! goal "Ziel"
    In diesem Projekt geht es darum, dass Sie sich mit dem Paketmanager Helm vertraut machen.

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../13-advanced/#/)
    und der [Cheatsheets](../../cheatsheets/12-helm/) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.


## Vorbereitung

- Erstellen Sie ein einfaches PV und PVC, das in ein HostPath-Volume schreibt:

```yaml
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: postgres-pv
  labels:
    exercise: helm
spec:
  capacity:
    storage: 1Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: ""
  hostPath:
    path: "/mnt/data"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: postgres-pvc
  labels:
    exercise: helm
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  storageClassName: ""
```

### Aufgabe 1: Repository mit Helm installieren

- Schauen Sie sich auf [Artifact Hub](https://artifacthub.io/packages/search) nach verfügbaren Helm-Chart-Repositories um.
- Machen Sie sich mit dem [Bitnami-Repository](https://artifacthub.io/packages/helm/bitnami/postgresql) vertraut.
- Erstellen Sie eine `values.yaml`-Datei und setzen Sie die folgenden Werte:

```yaml
primary:
  persistence:
    # This setting enables persistence for the primary database
    enabled: true
    # This is the name of the PVC created in the preparation step
    existingClaim: postgres-pvc
```

- Sie können die [möglichen Werte und die Standardwerte hier nachschlagen](https://artifacthub.io/packages/helm/bitnami/postgresql#postgresql-primary-parameters)

- Testen Sie Ihre Werte, indem Sie den Chart mit dem folgenden Befehl rendern:

```shell
helm template my-release oci://registry-1.docker.io/bitnamicharts/postgresql -f values.yaml > rendered.yaml
```

- Schauen Sie sich die `rendered.yaml`-Datei an
- Überprüfen Sie, ob die Ausgabe den Abschnitt `persistentVolumeClaim` mit `existingClaim` auf `postgres-pvc` enthält.
- Nutzen Sie den Befehl `helm install` um den Chart zu installieren:

```shell
helm install my-release oci://registry-1.docker.io/bitnamicharts/postgresql -f values.yaml
```

- Überprüfen Sie, ob der Chart erfolgreich installiert wurde:

```shell
helm list
```

- Überprüfen Sie den Status des PostgreSQL Charts:

```shell
helm status <release-name>
```

- Überprüfen Sie die bereitgestellten Ressourcen:

```shell
kubectl get pods
```

- Geben Sie alle Werte + Manifeste des PostgreSQL-Charts mit dem folgenden Befehl aus:

```shell
helm get all <release-name>
```

### Aufgabe 2: Helm-Charts verstehen (Optional)

- Beim Ausführen des `helm install`-Befehls zieht Helm den Chart aus dem Repository und speichert ihn im lokalen Cache. Sie können den Chart mit dem folgenden Befehl anzeigen:

```shell
helm pull oci://registry-1.docker.io/bitnamicharts/postgresql
```

- Dieser Befehl lädt den Chart auf Ihren lokalen Rechner herunter. Sie sollten
eine Datei namens `postgresql-<version>.tgz` sehen.
- Entpacken Sie die Datei und schauen Sie sich die Dateien und Ordner an:

```shell
tar -xvf postgresql-<version>.tgz
```

- Schauen Sie sich die Dateien und Ordner an:
    - `Chart.yaml`: Enthält die Metadaten des Charts wie Name, Version und Beschreibung.
    - `values.yaml`: Enthält die Standardwerte des Charts.
    - `templates/`: Enthält die Go-Templates, die die Kubernetes-Ressourcen definieren.
    - `charts/`: Enthält die Abhängigkeiten des Charts.
    - `README.md`: Enthält die Dokumentation des Charts.

Sie können auch den Quellcode des Charts anschauen, indem Sie das Repository des
Charts aufrufen. Sie finden den Quellcode des PostgreSQL-Charts [hier](https://github.com/bitnami/charts/tree/main/bitnami/postgresql).


### Clean up

- Löschen Sie den PostgreSQL-Chart mit dem folgenden Befehl:

```shell
helm uninstall my-release
```

- Löschen Sie das PVC und PV