# Mehrere Container im Pod nutzen

!!! goal "Ziel"
    In diesem Projekt geht es darum mehrere Container innerhalb eines Pods zu starten. Sie werden:

    - initContainer verwenden
    - Sidecars verwenden

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../10-multiple-containers/#/)
    und der [Cheatsheets](../../cheatsheets/09-multiple-containers/) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.


## Aufgabe 1: Init-Container nutzen

- Erstellen Sie eine Datei `init.yaml`, die die Beschreibung für einen Pod mit dem Namen `pod-with-init` enthält. Verwenden Sie das nginx-Image wie zuvor.
- Der Pod sollte ein persistentes Volume verwenden, sodass die Dateien in `/usr/share/nginx/html` geändert werden können
- Der Pod sollte einen zusätzlichen initContainer enthalten, der:
    - Das gleiche persistente Volume-Setup wie der Hauptcontainer verwendet
    - die folgenden Befehle ausführt:
    - `echo "Overriding index.html file..."`
    - `echo "Hello from $(hostname)" | tee /usr/share/nginx/html/index.html`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    ---
    apiVersion: v1
    kind: Pod
    metadata:
      name: pod-with-init
      labels:
        app: nginx
        exercise: initcontainer
    spec:
      volumes:
      - name: data
        emptyDir: {}
      initContainers:
      - name: init
        image: nginx
        command:
        - bash
        - -c
        args:
        - |
          echo "Overriding index.html file..." && echo "Hello from $(hostname)" | tee /usr/share/nginx/html/index.html
        volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: data
      containers:
      - name: web
        image: nginx
        volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: data
    ```

- Deployen Sie den Pod

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```bash
    kubectl apply -f init.yaml
    ```

- Schauen Sie sich das Ergebnis mit `kubectl get pods` an
- Schauen Sie sich die Logs des initContainers mit `kubectl logs` mit der Option `-c` an

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl logs pod-with-init -c init
    ```

- Holen Sie sich Details zum Pod mit `kubectl describe`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl describe pod pod-with-init
    ```

- Beachten Sie dabei vor allem die Abschnitte `Init Container` und `Container` und vergleichen Sie die Unterabschnitte `State`
- Schauen Sie sich an, welche Events ausgelöst wurden. Das geht mit `kubectl events` oder seinem Äquivalent `kubectl get events --sort-by='.metadata.creationTimestamp'`
- Machen Sie sich mit `kubectl events` vertraut und probieren Sie einige weitere Optionen aus, z.B. `kubectl events -o yaml`

- Führen Sie den Befehl `curl localhost` innerhalb des Pods mit `kubectl exec` aus

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl exec -it pod-with-init -- curl localhost
    ```

## Aufgabe 2: Sidecar nutzen

- Erstellen Sie eine Datei `secret.yaml`, die die Beschreibung für ein Secret
mit dem Namen `db` enthält. Die Daten sollten einen Schlüssel
`mysql-root-password` mit dem Wert `not-the-secret` enthalten.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    ---
    apiVersion: v1
    kind: Secret
    metadata:
      name: db
    type: Opaque
    stringData:
      mysql-root-password: "not-the-secret"
    ```

- Wenden Sie das Secret mit `kubectl apply` an.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl apply -f secret.yaml
    ```

- Erstellen Sie eine Datei `pod.yaml`, die die Beschreibung für einen Pod mit dem Namen `mysql` enthält. Verwenden Sie das `mysql:5` Image wie zuvor.
- Der Pod sollte einen Hauptcontainer enthalten, der:
    - eine Umgebungsvariable `MYSQL_ROOT_PASSWORD` enthält, die auf den Schlüssel `mysql-root-password` im Secret zeigt
    - einen Port für den Container auf Port `3306` öffnen
- Der Pod sollte auch einen zusätzlichen Container (Sidecar) mit dem Namen `metrics` enthalten, der Metriken aus der mysql-Datenbank exportiert.
  Er sollte:
    - das Image `prom/mysqld-exporter:v0.15.1` verwenden
    - eine Umgebungsvariable `MYSQLD_EXPORTER_PASSWORD` enthalten, die auf den Schlüssel `mysql-root-password` im Secret zeigt
    - einen Port zum ContainerPort `9104` öffnen.
    - den folgenden Befehl ausführen:
      ```
      sh
      -c
      /bin/mysqld_exporter --mysqld.address=mysql:3306 --mysqld.username=root
      ```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: v1
    kind: Pod
    metadata:
      name: mysql
      labels:
        app: mysql
    spec:
      containers:
      - name: mysql
        image: mysql:5
        env:
        - name: MYSQL_ROOT_PASSWORD
          valueFrom:
            secretKeyRef:
              name: db
              key: mysql-root-password
        ports:
        - name: mysql
          containerPort: 3306
      - name: metrics
        image: prom/mysqld-exporter:v0.15.1
        env:
        - name: MYSQLD_EXPORTER_PASSWORD
          valueFrom:
            secretKeyRef:
              name: db
              key: mysql-root-password
        command:
        - sh
        - -c
        - /bin/mysqld_exporter --mysqld.address=mysql:3306 --mysqld.username=root
        ports:
        - name: metrics
          containerPort: 9104
    ```

- Deployen Sie den Pod mit `kubectl apply`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl apply -f pod.yaml
    ```

- Erstellen Sie eine Datei `service.yaml`, die die Beschreibung für einen Service mit dem Namen `db` enthält.
   Sie sollte die folgenden TCP-Port-Zuordnungen enthalten:
   ```
   - Port: 3306
     targetPort: 3306
   - port: 9104
     targetPort: 9104
   ```

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: db
      labels:
        exercise: sidecar
    spec:
      selector:
        app: mysql
      ports:
      - name: mysql
        protocol: TCP
        port: 3306
        targetPort: 3306
      - name: metrics
        protocol: TCP
        port: 9104
        targetPort: 9104
    ```

- Wenden Sie den Service mit `kubectl apply` an.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl apply -f service.yaml
    ```

- Untersuchen Sie die Container des Pods mit `kubectl describe`
- Untersuchen Sie die Logs der beiden Container des Pods mit `kubectl logs` mit der Option `-c` für jeden Container
- Verwenden Sie `curl`, um die Metriken vom mysqld_exporter abzufragen

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
    curl db:9104/metrics
    ```

## Cleanup

- Löschen Sie alle Objekte die Sie in dieser Übung erstellt haben

