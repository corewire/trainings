# Lab: Wordpress mit Frontend und Backend

!!! goal "Ziel"
    In diesem Projekt geht es um das Zusammenführen des bisher Gelerntem. Sie werden:

    - Eine Anwendung mit Frontend und Backend bereitstellen

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der bisherigen Folien und des [Cheatsheets](../../cheatsheets/05-wordpress-lab/) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.


## Aufgabe 1: Datenbank erstellen

### Aufgabe 1.1: Datenbank-Deployment erstellen

- Erstellen Sie ein Secret, das einen Schlüssel `password` mit dem Wert "mypassword" enthält

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    - Erstellen Sie eine Datei `secret.yaml` mit dem folgenden Inhalt:

    ```yaml
    ---
    apiVersion: v1
    kind: Secret
    metadata:
      name: db
      labels:
        exercise: wordpress
    type: Opaque
    stringData:
      password: "mypassword"
    ```

    - Deployen Sie es:

    ```
    kubectl apply -f secret.yaml
    ```

- Erstellen Sie ein Deployment für die DB
    - Das Deployment sollte einen einzelnen Pod erstellen
    - Der Container sollte das Image `mysql:5` verwenden
    - Ein HostPath-Volume vom Typ `DirectoryOrCreate` sollte unter dem Pfad `/data/blog/mysql` erstellt werden
    - Das Volume sollte unter dem Mountpfad `/var/lib/mysql` eingehängt werden
    - Die folgenden Umgebungsvariablen sollten gesetzt werden:
        - `MYSQL_RANDOM_ROOT_PASSWORD: "yes"`
        - `MYSQL_DATABASE: "wordpress"`
        - `MYSQL_USER: "wordpress"`
        - `MYSQL_PASSWORD`, das auf das oben erstellte geheime Passwort verweist
    - Der ContainerPort 3306 sollte konfiguriert werden
    - Vergessen Sie nicht die Labels und LabelSelectors!

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: db
      labels:
        app: blog
        component: db
        exercise: wordpress
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: blog
          component: db
      template:
        metadata:
          labels:
            app: blog
            component: db
        spec:
          volumes:
          - name: data
            hostPath:
              path: /data/blog/mysql
              type: DirectoryOrCreate
          containers:
          - name: mysql
            image: mysql:5
            env:
            - name: MYSQL_RANDOM_ROOT_PASSWORD
              value: "yes"
            - name: MYSQL_DATABASE
              value: wordpress
            - name: MYSQL_USER
              value: wordpress
            - name: MYSQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: db
                  key: password
            volumeMounts:
            - mountPath: /var/lib/mysql
              name: data
            ports:
            - containerPort: 3306
    ```
- Deployen Sie die Datenbank

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl apply -f db-deployment.yaml
    ```

- Überprüfen Sie, ob die Datenbank läuft indem Sie die Logs des Pods abrufen

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```
    kubectl logs deployments/db
    ```

    ```
    (...)
    [Note] mysqld: ready for connections.
    ```

### Aufgabe 1.2: Datenbank-Service erstellen

- Erstellen Sie einen Service für die Datenbank
  - Der Dienst sollte Port 3306 auf targetPort 3306 abbilden
- Vergessen Sie die labelSelectors nicht!

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: db
      labels:
        exercise: wordpress
    spec:
      selector:
        app: blog
        component: db
      ports:
      - protocol: TCP
        port: 3306
        targetPort: 3306
    ```

- Deployen Sie den Service

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```
    kubectl apply -f db-service.yaml
    ```


## Aufgabe 2: Wordpress Frontend erstellen

- Erstellen Sie ein Deployment für das Wordpress-Frontend
    - Das Deployment sollte einen einzelnen Pod erstellen
    - Der Container sollte das Image "wordpress:5" verwenden
    - Die folgenden Umgebungsvariablen sollten gesetzt werden:
        - `WORDPRESS_DB_HOST: "db"`
        - `WORDPRESS_DB_NAME: "wordpress"`
        - `WORDPRESS_DB_USER: "wordpress"`
        - `WORDPRESS_DB_PASSWORD`, das auf das oben erstellte Secret verweist
    - Der ContainerPort 80 sollte konfiguriert werden
- Vergessen Sie nicht die Labels und LabelSelectors!

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: server
      labels:
        exercise: wordpress
    spec:
      replicas: 1
      selector:
        matchLabels:
          app: blog
          component: web
      template:
        metadata:
          labels:
            app: blog
            component: web
        spec:
          containers:
          - name: wordpress
            image: wordpress:5
            imagePullPolicy: Always
            env:
            - name: WORDPRESS_DB_HOST
              value: db
            - name: WORDPRESS_DB_NAME
              value: wordpress
            - name: WORDPRESS_DB_USER
              value: wordpress
            - name: WORDPRESS_DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: db
                  key: password
            ports:
            - name: web
              containerPort: 80
    ```

- Deployen Sie das Frontend

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```
    kubectl apply -f web-deployment.yaml
    ```

- Prüfen Sie in den Wordpress-Logs, ob die Anwendung läuft

```
kubectl logs deployments/server
```

```
WordPress not found in /var/www/html - copying now...
Complete! WordPress has been successfully copied to /var/www/html
No 'wp-config.php' found in /var/www/html, but 'WORDPRESS_...' variables supplied; copying 'wp-config-docker.php' (WORDPRESS_DB_HOST WORDPRESS_DB_NAME WORDPRESS_DB_PASSWORD WORDPRESS_DB_USER)
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 10.42.130.234. Set the 'ServerName' directive globally to suppress this message
AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using 10.42.130.234. Set the 'ServerName' directive globally to suppress this message
[Wed Feb 28 15:46:34.797478 2024] [mpm_prefork:notice] [pid 1] AH00163: Apache/2.4.53 (Debian) PHP/7.4.29 configured -- resuming normal operations
[Wed Feb 28 15:46:34.797511 2024] [core:notice] [pid 1] AH00094: Command line: 'apache2 -D FOREGROUND'
```

- Erstellen Sie einen Service für die Wordpress-Anwendung
    - Der Dienst sollte Port 80 auf targetPort "web" abbilden.
- Vergessen Sie nicht die labelSelectors!

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: server
      labels:
        exercise: wordpress
    spec:
      selector:
        app: blog
        component: web
      ports:
      - protocol: TCP
        port: 80
        targetPort: web
    ```

- Fügen Sie im Abschnitt `spec` des Services die folgende Zeile hinzu, um den Service für den Zugriff von außen freizugeben:

    ```yaml
    spec:
      type: NodePort
    ```

Wir werden später behandeln, was hier im Detail passiert.

- Deployen Sie den Service

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```
    kubectl apply -f web-service.yaml
    ```

## Aufgabe 3: Wordpress aufrufen

- Verwenden Sie `kubectl get all`, um einen Überblick über alle Dinge zu erhalten, die bereitgestellt wurden.
- Verwenden Sie `curl`, um sicherzustellen, dass der Webserver Wordpress ausliefert

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```
    kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
    curl -sv server
    ```

- Setzen Sie den Namen Ihres Wordpress Frontend Services als Umgebungsvariable:

```
FRONTEND_SERVICE=server
```

- Das folgende Kommando gibt Ihnen die URL, unter der Sie auf Wordpress zugreifen können:

```bash
echo Wordpress Adresse: $(kubectl get nodes -o json | jq -r '.items[] | select(.metadata.labels["node-role.kubernetes.io/control-plane"] | not) | .status.addresses[] | select(.type=="ExternalIP") | .address' | head -n 1):$(k get service ${FRONTEND_SERVICE} -o json | jq .spec.ports[].nodePort)
```

Sie müssen es nicht vestehen, wir werden später mehr darüber reden wie NodePorts funktionieren.

- Öffnen Sie einen Wordpress im Browser

## Cleanup

- Löschen Sie alle Objekte, die Sie erstellt haben.
- Beobachten Sie, wie sie mit `watch kubectl get all` verschwinden.