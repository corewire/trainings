# Replicasets und Deployments

Dieser showcase zeigt die Verwendung von ReplicaSets und Deployments in Kubernetes.
Er kann entweder komplett durchgeführt werden oder in Teilen, an den entsprechenden Stellen in den Folien.

## ReplicaSets

- Cluster Status ausgeben

```bash
watch -n 1 kubectl get all
```

- Erstellen Sie ein ReplicaSet mit 3 Pods

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: my-replicaset
spec:
  replicas: 3
  selector:
    matchLabels:
      my-label: my-app
  template:
    metadata:
      labels:
        my-label: my-app
    spec:
      containers:
      - name: my-container
        image: nginx:latest
```

- Erstellen Sie das ReplicaSet

```bash
kubectl apply -f replicaset.yaml
```

### Skalieren

- Pods hoch skallieren
- Anpassen des Images auf ubuntu
  - Erklären warum die Pods nicht aktualisiert werden
  - Erklären warum
- Hochskalieren
  - jetzt wird ubuntu genutzt
- Wieder auf nginx zurück gehen

### Externe Pods Addoptieren

- Erstellen eines Pods der auf die gleichen Labels hört

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-manual-pod
  labels:
    my-label: my-app
spec:
  containers:
  - name: my-container
    image: nginx:latest
```

- Erklären warum der Pod sofort gelöscht wird
- Löschen des ReplicaSets
- Starten des Pods
- Starten des ReplicaSets
- Erklären warum der Pod jetzt da bleibt
- ReplicaSet löschen

## Deployments

Hier können zuerst die Folien gezeigt werden und dann die Demo durchgeführt werden.

```bash
k create deployment my-deployment --image=nginx:latest -o yaml --dry-run=client
```

- **Oder** von Hand erstellen:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      my-label: my-app
  template:
    metadata:
      labels:
        my-label: my-app
    spec:
      containers:
      - name: my-container
        image: nginx:latest
```

- Image anpassen
- Erklären warum das Deployment das Image aktualisiert
- Skalieren
  - Erklären warum hier kein neues Replicaset erstellt wird
- Image anpassen auf etwas das nicht existiert
- Status des Deployments anzeigen

```bash
kubectl rollout status deployment my-deployment
```

```bash
kubectl describe deployment my-deployment
```

- Rollout History zeigen:

```bash
kubectl rollout history deployment my-deployment
```

- Version anzeigen

```bash
kubectl rollout history deployment my-deployment --revision=2
```

- Deployment zurück rollen

```bash
kubectl rollout undo deployment my-deployment
```

- Deployment löschen

```bash
kubectl delete deployment my-deployment
```



