# Showcase: Kubernetes Volumes

Dieses Showcase zeigt grundlegende Konzepte und Befehle im Umgang mit Volumes in Kubernetes. Die Beispiele sind unterschiedlich zum Hands-On, decken jedoch dieselben Themen ab.

---

## Beispiel 1: emptyDir Volumes

Ein Pod wird mit einem `emptyDir`-Volume gestartet, das Daten für die Lebensdauer des Pods speichert.

### Manifest
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: showcase-emptydir
spec:
  volumes:
  - name: temp-storage
    emptyDir: {}
  containers:
  - name: writer
    image: busybox
    command: ["sh", "-c", "while true; do echo $(date) >> /tmp/log.txt; sleep 5; done"]
    volumeMounts:
    - mountPath: /tmp
      name: temp-storage
  - name: reader
    image: busybox
    command: ["sh", "-c", "tail -f /tmp/log.txt"]
    volumeMounts:
    - mountPath: /tmp
      name: temp-storage
```

### Befehle

- Pod erstellen: `kubectl apply -f showcase-emptydir.yaml`
- Logs prüfen: `kubectl logs showcase-emptydir reader`

---

## Beispiel 2: hostPath Volumes

Ein Pod wird gestartet, der ein Verzeichnis auf dem Host mountet.

### Manifest
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: showcase-hostpath
spec:
  volumes:
  - name: host-storage
    hostPath:
      path: /tmp/k8s-hostpath-demo
      type: DirectoryOrCreate
  containers:
  - name: nginx
    image: nginx
    volumeMounts:
    - mountPath: /data
      name: host-storage
```

### Befehle

- Pod erstellen: `kubectl apply -f showcase-hostpath.yaml`
- Describe Pod: `kubectl describe pod showcase-hostpath`
- In Container einloggen: `kubectl exec -it showcase-hostpath -- bash`
- Mount prüfen: `df --exclude-type=tmpfs`
- Datei erstellen: `echo "Hello, Kubernetes!" > /data/file.txt`
- Prüfen wo der Pod läuft: `kubectl get pod -o wide`
- Pod löschen: `kubectl delete pod showcase-hostpath`
- Neuen Pod erstellen: `kubectl apply -f showcase-hostpath.yaml`
- Schauen auf welchem Node der Pod läuft: `kubectl get pod -o wide`
- Pod auf Node forcieren: `spec`: `nodeName: <node-name>`

---

## Beispiel 3: PersistentVolume und PersistentVolumeClaim

### Manifest (PersistentVolume)

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: dummy-pv
  labels:
    storage: dummy
spec:
  capacity:
    storage: 5Gi
  storageClassName: ""
  volumeMode: Filesystem
  accessModes:
  - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  hostPath:
    path: /data/pv/dummy
    type: DirectoryOrCreate
```

### Manifest (PersistentVolumeClaim)

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: dummy-pvc
spec:
  volumeName: dummy-pv
  storageClassName: ""
  accessModes:
  - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 5Gi
```

### Manifest (Pod)

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: storage-demo
spec:
  volumes:
    - name: pvc-volume
      persistentVolumeClaim:
        claimName: dummy-pvc
  containers:
    - name: nginx
      image: nginx
      volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: pvc-volume
```

### Befehle

- PersistentVolume erstellen: `kubectl apply -f showcase-pv.yaml`
- PersistentVolumeClaim erstellen: `kubectl apply -f showcase-pvc.yaml`
- Pod erstellen: `kubectl apply -f showcase-pv-pod.yaml`
- In Container einloggen: `kubectl exec -it storage-demo -- bash`
- Html Datei von einer Chat AI einfügen
- Port forward: `kubectl port-forward storage-demo 8080:80`
- Pod löschen: `kubectl delete pod storage-demo`
- Pod wieder erstellen: `kubectl apply -f showcase-pv-pod.yaml`

## Beispiel 4: PersistentVolumeClaim mit CSI Plugin

- pv und pvc anzeigen: `kubectl get pv,pvc`
- Storrage Class anzeigen: `kubectl get sc`

```yaml
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: hetzner-pvc
spec:
  accessModes:
  - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 5Gi
```

```
kubectl describe persistentvolumeclaims hetzner-pvc
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: hetzner-storage-demo
  labels:
    exercise: storage
spec:
  volumes:
    - name: pvc-volume
      persistentVolumeClaim:
        claimName: hetzner-pvc
  containers:
    - name: nginx
      image: nginx
      volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: pvc-volume

```

```
kubectl get pv,pvc
```

---

## Cleanup

- Alle Ressourcen löschen:
  ```bash
  kubectl delete pod,configmap,secret,pvc,pv --all
  
