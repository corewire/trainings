# Kubernetes Showcase: Pod Management 2

## Ziele des Showcases

In diesem Showcase zeigt der Trainer alternative Beispiele zu den Themen des Hands-On:

- Erstellung und Verwaltung von StatefulSets
- Jobs und CronJobs
- DaemonSets
- Deployment-Strategien

Die Beispiele sollen die Konzepte verdeutlichen und den Einstieg in das Hands-On erleichtern.

## StatefulSets: Skalierung und Updates

### Vorbereitung
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: redis
spec:
  serviceName: "redis"
  replicas: 2
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
      - name: redis
        image: redis:6
        ports:
        - containerPort: 6379
```

### Schritte
1. Deploye das StatefulSet:
   ```bash
   kubectl apply -f redis-statefulset.yaml
   ```
2. Skaliere es auf 4 Pods:
   ```bash
   kubectl scale statefulset redis --replicas=4
   ```
3. Aktualisiere das Image auf `redis:7`:
   ```bash
   kubectl edit statefulset redis
   ```

---

## Jobs: Parallelisierung und Fehlerbehandlung

### Einfacher Job
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: print-date
spec:
  template:
    spec:
      containers:
      - name: print-date
        image: busybox
        command: ["date"]
      restartPolicy: Never
```

1. Deploye den Job:
   ```bash
   kubectl apply -f job-print-date.yaml
   ```
2. Prüfe die Logs:
   ```bash
   kubectl logs job/print-date
   ```

### Parallelisierter Job
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: print-parallel
spec:
  parallelism: 3
  completions: 6
  template:
    spec:
      containers:
      - name: print-parallel
        image: ubuntu
        command: ["sh", "-c", "echo Parallel Job run on $HOSTNAME at $(date)"]
      restartPolicy: Never
```

1. Deploye den Job:
   ```bash
   kubectl apply -f job-parallel.yaml
   ```
2. Beobachte die Pods:
   ```bash
   watch kubectl get pods
   ```
3. Prüfe die Logs:
   ```bash
   kubernetes logs jobs/print-parallel --all-pods
   ```

---

## DaemonSets: Clusterweiter Einsatz (Optional)

### Beispiel: Node Logging Agent
```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: logging-agent
spec:
  selector:
    matchLabels:
      app: logging-agent
  template:
    metadata:
      labels:
        app: logging-agent
    spec:
      containers:
      - name: fluentd
        image: fluentd
        ports:
        - containerPort: 24224
```

1. Deploye das DaemonSet:
   ```bash
   kubectl apply -f logging-daemonset.yaml
   ```
2. Prüfe die laufenden Pods:
   ```bash
   kubectl get pods -o wide
   ```

---

## Deployment-Strategien: Rolling Update

### Deployment
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web-app
spec:
  replicas: 25
  selector:
    matchLabels:
      app: web
  template:
    metadata:
      labels:
        app: web
    spec:
      containers:
      - name: nginx
        image: nginx:1.21
```

- Deploye das Deployment:
   ```bash
   kubectl apply -f rolling-update-deployment.yaml
   ```
- Aktualisiere das Image auf `nginx:1.23`:
   ```bash
   kubectl set image deployment/web-app nginx=nginx:1.23
   ```


- Rollback auf `nginx:1.21`:
   ```bash
   kubectl rollout undo deployment/web-app
   ```

- `MaxSurge` und `MaxUnavailable`  in `.spec` einfügen:
   ```yaml

   strategy:
     type: RollingUpdate
     rollingUpdate:
        maxSurge: 1
        maxUnavailable: 0
   ```

- Beobachte den Rollout:
   ```bash
   kubectl rollout status deployment/web-app
   ```
- `Recreate` Strategie erklären und zeigen

---

## Cleanup

- Alles löschen