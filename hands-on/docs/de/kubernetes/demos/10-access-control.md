# Kubernetes Access Control Showcase

## Ziel

Dieser Showcase demonstriert die wichtigsten Konzepte der Zugriffskontrolle in Kubernetes. Es werden Namespaces, RBAC und Kubeconfig behandelt. 

---

## Namespaces

### Namespaces untersuchen und erstellen

- **Bestehende Namespaces anzeigen:**
   ```bash
   kubectl get namespace
   ```

- **Namespace `team` erstellen:**
   ```yaml
   apiVersion: v1
   kind: Namespace
   metadata:
     name: team
   ```

- **Deployment im Namespace `team` erstellen:**
   ```bash
   kubectl create deployment demo-app --image=nginx --namespace=team
   kubectl get pods --namespace=team
   ```

---

## Role-Based Access Control (RBAC)

### ServiceAccount und RBAC


- **ServiceAccount `developer` erstellen:**
   ```yaml
   apiVersion: v1
   kind: ServiceAccount
   metadata:
     name: developer
     namespace: team
   ```

- **Rolle `read-pods` erstellen:**
   ```yaml
   apiVersion: rbac.authorization.k8s.io/v1
   kind: Role
   metadata:
     name: read-pods
     namespace: team
   rules:
   - apiGroups: [""]
     resources: ["pods"]
     verbs: ["get", "list"]
   ```

- **RoleBinding erstellen:**
   ```yaml
   apiVersion: rbac.authorization.k8s.io/v1
   kind: RoleBinding
   metadata:
     name: developer-binding
     namespace: team
   roleRef:
     apiGroup: rbac.authorization.k8s.io
     kind: Role
     name: read-pods
   subjects:
   - kind: ServiceAccount
     name: developer
     namespace: team
   ```


- **Aktionen als `developer` ausführen:**
   ```bash
   kubectl get pods --namespace team --as system:serviceaccount:team:developer
   ```

   ```bash
   kubectl get all --namespace team --as system:serviceaccount:team:developer
   ```

---

## Kubeconfig

### Kontextverwaltung

1. **Neuen Kontext `team-context` erstellen:**
   ```bash
   kubectl config set-context team-context \
     --cluster=$(kubectl config view -o jsonpath='{.clusters[0].name}') \
     --user=$(kubectl config view -o jsonpath='{.users[0].name}') \
     --namespace=team
   ```

2. **Zu Kontext wechseln und Pods anzeigen:**
   ```bash
   kubectl config use-context team-context
   kubectl get pods
   ```

3. **Kontext zurücksetzen:**
   ```bash
   kubectl config use-context $(kubectl config current-context)
   ```

---

## Cleanup

1. **Erstellte Ressourcen entfernen:**
   ```bash
   kubectl delete namespace example
   kubectl delete namespace team
   ```

