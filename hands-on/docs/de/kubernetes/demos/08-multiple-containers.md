# Showcase: Mehrere Container im Pod

Dieser Showcase gibt eine Einführung in die Nutzung von Init-Containern und Sidecars, wobei andere Beispiele als im Hands-On verwendet werden.

## Init-Container Beispiel

Hier demonstrieren wir die Nutzung eines Init-Containers, um Dateien in einem Pod vorzubereiten.

### Manifest

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: showcase-init
spec:
  volumes:
  - name: showcase-data
    emptyDir: {}
  initContainers:
  - name: setup
    image: busybox
    command:
    - sh
    - -c
    args:
    - |
      echo "Preparing environment..." && echo "Welcome to $(hostname)" > /data/welcome.txt
    volumeMounts:
    - mountPath: /data
      name: showcase-data
  containers:
  - name: app
    image: nginx
    volumeMounts:
    - mountPath: /usr/share/nginx/html
      name: showcase-data
```

### Befehle

-  Pod erstellen:

   ```bash
   kubectl apply -f showcase-init.yaml
   ```

- Port Forward

    ```bash
    kubectl port-forward pod/showcase-init 5000:80
    ```

- Browser öffnen
- Logs des Init-Containers anzeigen:

   ```bash
   kubectl logs showcase-init -c setup
   ```

## Sidecar Beispiel

Ein Sidecar-Container wird verwendet, um Logs eines Hauptcontainers bereitzustellen.

### Manifest

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: showcase-sidecar
spec:
  containers:
  - name: app
    image: busybox
    command:
    - sh
    - -c
    args:
    - |
      while true; do echo "Log entry at $(date)" >> /logs/app.log; sleep 5; done
    volumeMounts:
    - mountPath: /logs
      name: log-volume
  - name: logger
    image: busybox
    command:
    - tail
    - -f
    args:
    - /logs/app.log
    volumeMounts:
    - mountPath: /logs
      name: log-volume
  volumes:
  - name: log-volume
    emptyDir: {}
```

### Befehle


```bash
kubectl apply -f showcase-sidecar.yaml
```

```bash
kubectl logs showcase-sidecar
```

```bash
kubectl logs showcase-sidecar -c logger
```

```bash
kubectl describe pod showcase-sidecar
```

## Cleanup

Am Ende der Demonstration können alle erstellten Ressourcen gelöscht