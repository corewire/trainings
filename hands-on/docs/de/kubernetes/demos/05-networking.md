# Showcase: Kubernetes Networking

Demonstrieren Sie verschiedene Netzwerkfunktionen in Kubernetes, bevor die Teilnehmer das Hands-On bearbeiten. Der Showcase verwendet unterschiedliche Beispiele, um dieselben Konzepte zu illustrieren.

---

## Port-Forwarding

### Vorbereitung
Erstellen Sie ein Deployment `app` mit einem einfachen HTTP-Server und einem zugehörigen Service `app-service`.

**Manifest-Datei:** `port-forwarding-showcase.yaml`
```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: app
  template:
    metadata:
      labels:
        app: app
    spec:
      containers:
      - name: http-server
        image: hashicorp/http-echo
        args: ["-text=Hello, Kubernetes!"]
        ports:
        - containerPort: 5678
---
apiVersion: v1
kind: Service
metadata:
  name: app-service
spec:
  selector:
    app: app
  ports:
  - protocol: TCP
    port: 80
    targetPort: 5678
```

**Deployment:**
```shell
kubectl apply -f port-forwarding-showcase.yaml
```

### Port-Forward

1. Führen Sie folgenden Befehl aus:
   ```shell
   kubectl port-forward service/app-service 5000:80
   ```
2. Testen Sie die Verbindung:
   ```shell
   curl http://127.0.0.1:5000
   ```

---

## Node-Ports

### Vorbereitung
Nutzen Sie das vorherige Deployment `app` und erstellen Sie einen NodePort-Service.

**Manifest-Datei:** `node-port-showcase.yaml`
```yaml
apiVersion: v1
kind: Service
metadata:
  name: app-nodeport
spec:
  type: NodePort
  selector:
    app: app
  ports:
  - protocol: TCP
    port: 80
    targetPort: 5678
```

**Deployment:**
```shell
kubectl apply -f node-port-showcase.yaml
```

### Zugriff auf NodePort
1. Ermitteln Sie den NodePort:
   ```shell
   kubectl get svc app-nodeport
   ```
2. Finden Sie die externe IP eines Workers:
   ```shell
   kubectl get nodes -o wide
   ```
3. Öffnen Sie im Browser: `http://<Worker-IP>:<NodePort>`

---

## Loadbalancer

- Service vom Typ LoadBalancer zum bestehenden Manifest hinzufügen:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: loadbalancer-service
  annotations:
    load-balancer.hetzner.cloud/location: fsn1
    load-balancer.hetzner.cloud/name: herribert
spec:
  type: LoadBalancer
  selector:
    app: app
  ports:
  - protocol: TCP
    port: 80
    targetPort: 5678
```

- Hetzner Konsole öffnen
- Deployen
- Public IP abrufen:

```shell
kubectl get svc loadbalancer-service -o wide
```

- Ip im Browser öffnen
- Loadbalancer in der Hetzner Konsole zeigen


---

## Zusammenfassung
Der Showcase demonstriert, wie man Port-Forwarding, NodePort und Ingress verwendet. Nutzen Sie diese Beispiele, um die Konzepte klar und effektiv zu erklären.

