# Showcase: Kubernetes-Scheduling-Funktionen

## Ziele des Showcases
- Veranschaulichung der Kubernetes-Scheduling-Konzepte.
- Vermittlung der wichtigsten Befehle und Manifeste.

---

## Node-Selector Showcase

### Vorbereitung

- Labeln Sie einen Node mit `node-role=database`:

```bash
kubectl label node <node_name> node-role=database
```

```bash
kubectl get nodes --show-labels
```

### Deployment
- Erstellen Sie eine Deployment-Datei `node-selector-showcase.yaml` mit folgendem Inhalt:
  ```yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: database-deployment
  spec:
    replicas: 2
    selector:
      matchLabels:
        app: database
    template:
      metadata:
        labels:
          app: database
      spec:
        nodeSelector:
          node-role: database
        containers:
        - name: postgres
          image: postgres:15-alpine
          env:
            - name: POSTGRES_PASSWORD
              value: "password"
  ```
- Deployment anwenden:
  ```bash
  kubectl apply -f node-selector-showcase.yaml
  ```
- Verteilung der Pods überprüfen:
  ```bash
  kubectl get pods -o wide
  ```

---

## Node-Affinity Showcase
### Vorbereitung
- Labeln Sie einen Node mit `node-type=high-memory`:
  ```bash
  kubectl label node <node_name> node-type=high-memory
  ```

### Deployment
- Erstellen Sie eine Deployment-Datei `node-affinity-showcase.yaml`:
  ```yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: memory-intensive-deployment
  spec:
    replicas: 3
    selector:
      matchLabels:
        app: memory-intensive
    template:
      metadata:
        labels:
          app: memory-intensive
      spec:
        affinity:
          nodeAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              nodeSelectorTerms:
              - matchExpressions:
                - key: node-type
                  operator: In
                  values:
                  - high-memory
        containers:
        - name: analytics
          image: python:3.10
          command: ["python"]
          args: ["-m", "http.server"]
  ```
- Deployment anwenden:
  ```bash
  kubectl apply -f node-affinity-showcase.yaml
  ```
- Verteilung der Pods überprüfen:
  ```bash
  kubectl get pods -o wide
  ```

---

## Pod Affinity und Anti-Affinity Showcase
### Vorbereitung
- Deployment-Datei mit Backend- und Frontend-Pods erstellen:
  ```yaml
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: backend
  spec:
    replicas: 2
    selector:
      matchLabels:
        app: backend
    template:
      metadata:
        labels:
          app: backend
      spec:
        containers:
        - name: backend
          image: busybox
          command: ["sh", "-c", "sleep infinity"]
  ---
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: frontend
  spec:
    replicas: 3
    selector:
      matchLabels:
        app: frontend
    template:
      metadata:
        labels:
          app: frontend
      spec:
        affinity:
          podAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchLabels:
                  app: backend
              topologyKey: "kubernetes.io/hostname"
        containers:
        - name: nginx
          image: nginx:1.25.4-alpine
  ```
- Deployment anwenden:
  ```bash
  kubectl apply -f pod-affinity-showcase.yaml
  ```
- Verteilung der Pods überprüfen:
  ```bash
  kubectl get pods -o wide
  ```
- Auf Anti-Affinity umstellen:
  ```yaml
  podAntiAffinity:
  ```

---

## Taints und Tolerations Showcase
### Vorbereitung
- Node tainten:
  ```bash
  kubectl taint node <node_name> dedicated=critical:NoSchedule
  ```

### Deployment
- Deployment-Datei ohne Toleration:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: critical-app
spec:
  replicas: 2
  selector:
    matchLabels:
      app: critical
  template:
    metadata:
      labels:
        app: critical
    spec:
      containers:
      - name: redis
        image: redis:7-alpine
      nodeSelector:
        dedicated: "critical"
```

- Verteilung zeigen
- Tollerations hinzufügen:

```yaml
    spec:
      tolerations:
      - key: "dedicated"
        operator: "Equal"
        value: "critical"
        effect: "NoSchedule"
```


---

## Resource Requests und Limits Showcase

### Deployment

- Pod-Datei mit Ressourcenanforderungen und -limits erstellen:

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: resource-demo
spec:
  replicas: 2
  selector:
    matchLabels:
      app: resource-demo
  template:
    metadata:
      labels:
        app: resource-demo
    spec:
      containers:
      - name: nginx
        image: nginx:1.25.4-alpine
        resources:
          requests:
            memory: "1Gi"
            cpu: "250m"
          limits:
            memory: "2Gi"
            cpu: "500m"

```

- Deployment anwenden
- Node Resourcen überprüfen:

```bash
kubectl describe node <node_name>
```

- Replicas auf 4 erhöhen
- Zeigen dass eine Node über 100% vergeben hat
- Requests auf `2Gi` erhöhen
- Erklären warum Pod nicht startet

---

## Clean-Up

- Entferne alle erstellten Ressourcen

