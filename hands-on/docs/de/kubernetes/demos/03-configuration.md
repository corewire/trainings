# Konfiguration

Dieser showcase zeigt die Verwendung von Environment Variablen, Secrets und ConfigMaps in Kubernetes.

## 1. Umgebungsvariablen

**Beispiel:** Deployment für eine Python-App mit einer Umgebungsvariablen.
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: echo-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: echo-app
  template:
    metadata:
      labels:
        app: echo-app
    spec:
      containers:
      - name: ubuntu
        image: ubuntu
        command: ["bash", "-c", "while true; do echo ${GREETING} It is $(date); sleep 5; done"]
        env:
        - name: GREETING
          value: "Hello, Kubernetes!"
```

**Befehle:**

1. Deployment anwenden:
   ```bash
   kubectl apply -f echo-app.yaml
   ```
2. Pod-Details inspizieren:
   ```bash
   kubectl describe pods
   ```
3. Logs des Pods anzeigen:
   ```bash
   kubectl logs deployment/echo-app
   ```

---

## 2. ConfigMaps

**Beispiel:** ConfigMap mit Anwendungskonfiguration.
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: app-config
data:
  LOG_LEVEL: debug
  FEATURE_FLAG: "true"
```
**Befehle:**
1. ConfigMap erstellen:
   ```bash
   kubectl apply -f app-config.yaml
   ```
2. ConfigMap in einem Deployment nutzen:
   ```yaml
   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: config-app
   spec:
     replicas: 1
     selector:
       matchLabels:
         app: config-app
     template:
       metadata:
         labels:
           app: config-app
       spec:
         containers:
         - name: app
           image: nginx
           envFrom:
           - configMapRef:
               name: app-config
   ```
3. Deployment anwenden:
   ```bash
   kubectl apply -f config-app.yaml
   ```
4. Umgebungsvariablen im Pod prüfen:
   ```bash
   kubectl exec -it deployment/config-app -- printenv | grep LOG_LEVEL
   ```

---

## 3. Secrets

**Beispiel:** Secret für eine Datenbankverbindung.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: db-secret
stringData:
  DB_USERNAME: admin
  DB_PASSWORD: supersecret
```
**Befehle:**
1. Secret erstellen:
   ```bash
   kubectl apply -f db-secret.yaml
   ```
2. Secret in einem Deployment nutzen:
   ```yaml
   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: db-app
   spec:
     replicas: 1
     selector:
       matchLabels:
         app: db-app
     template:
       metadata:
         labels:
           app: db-app
       spec:
         containers:
         - name: app
           image: postgres:15
           env:
           - name: POSTGRES_USER
             valueFrom:
               secretKeyRef:
                 name: db-secret
                 key: DB_USERNAME
           - name: POSTGRES_PASSWORD
             valueFrom:
               secretKeyRef:
                 name: db-secret
                 key: DB_PASSWORD
   ```
3. Deployment anwenden:
   ```bash
   kubectl apply -f db-app.yaml
   ```
4. Umgebungsvariablen im Pod prüfen:
   ```bash
   kubectl exec -it deployment/db-app -- printenv | grep POSTGRES
   ```

### 4. Cleanup

Alle erstellten Ressourcen entfernen:
```bash
kubectl delete deployment echo-app db-app config-app
kubectl delete secret db-secret
kubectl delete configmap app-config
```


