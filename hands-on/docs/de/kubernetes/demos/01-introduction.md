# Einführung

- Ziel: Ersten Pod starten und erreichbar machen
- Fokus: Ersten Eindruck von K8s gewinnen

## Pods starten

- VSCode öffnen und erklären
- Terminal öffnen

```
kubectl run pod apache-pod --image=httpd -o yaml --dry-run=client
```

- **Alternativ:** pod.yaml-Datei schreiben

```
apiVersion: v1
kind: Pod
metadata:
  name: apache-pod
  labels:
    app: apache
spec:
  containers:
  - name: apache-container
    image: httpd:2.4
```

- Pod starten `kubectl apply -f pod.yaml`
- anzeigen lassen `kubectl get pods`
- Beschreibung zeigen `kubectl describe pod apache-pod`
- löschen `kubectl delete pod apache-pod`

## Labels

- In pod.yaml Label hinzufügen:
```
apiVersion: v1
kind: Pod
metadata:
  name: apache-pod
  labels:
    app: web
spec:
  containers:
  - name: apache-container
    image: httpd:2.4

```
- Zeigen, dass bei apply ein Neustart passiert:
```
kubectl get pods
kubectl apply -f pod.yaml
kubectl get pods
```
- Dann Annotation hinzufügen:
```
apiVersion: v1
kind: Pod
metadata:
  name: apache-pod
  labels:
    app: web
  annotations:
    description: "Apache Pod for web app"
spec:
  containers:
  - name: apache-container
    image: httpd:2.4
```
- Zeigen, dass bei apply KEIN Neustart passiert:
```
kubectl apply -f pod.yaml
kubectl get pods
```
- Annotationen anzeigen:
`kubectl describe pod apache-pod`
- Nach Label filtern:
`kubectl get pods -l app=web`
- Erstelle weiteren Pod pod2.yaml mit demselben Label aber nginx statt apache:
```
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
  labels:
    app: web
spec:
  containers:
  - name: nginx-container
    image: nginx

```
- Apply und anzeigen
```
kubectl apply -f pod2.yaml
kubectl get pods
```
- Nach Label filtern
`kubectl get pods -l app=web`

## Service

- Erstelle Service-Datei:

```bash
kubectl create service clusterip web-service --tcp=80:80 -o yaml --dry-run=client
```

- **Oder**: Service-Datei erstellen:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: web-service
spec:
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
```
- Erstellen `kubectl apply -f service.yaml`
- Anzeigen `kubectl get services`
- `kubectl describe service web` Zeigt unterschiedliche Endpunkte an

```shell
kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
```

```shell
curl web-service
```

- So oft wiederholen bis einmal nginx und einmal apache kommt