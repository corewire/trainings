# Showcase: Kubernetes Pod Status and Related Features

## Ziel
Der Showcase bietet einen Überblick über die zentralen Themen des Hands-On. Der Trainer führt alternative Beispiele durch, um die Konzepte zu demonstrieren, und zeigt dabei wichtige Kubernetes-Befehle und Manifeste. Folgende Themen werden abgedeckt:

- Pod Status verstehen
- Image Pull Secrets verwenden
- Probes konfigurieren

---

## 1. Pod Status Verstehen

### Beispiel 1.1: Pod mit erfolgreich abgeschlossenem Container
Manifest-Datei `status-demo.yaml`:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: status-demo
spec:
  containers:
  - name: status-demo
    image: busybox
    command:
    - "echo"
    - "All done :)"
```

**Befehle:**

```bash
kubectl apply -f status-demo.yaml
```

```bash
kubectl describe pod status-demo
```

- `.spec` erweitern mit:

```yaml
  restartPolicy: Never
```

- Deployen

```bash
kubectl get pod status-demo -o json | jq .status.phase
```

```bash
kubectl get pod status-demo -o json | jq .status.conditions
```

- Verschiedene Sachen kaputt machen und Container Status erklären, z.B.:
  - Tippfehler im Image
  - Kaputter Command
- Erklären mit:

```bash
kubectl get pod status-demo -o json | jq .status.phase
```

```bash
kubectl get pod statu-demo -o json | jq .status.conditions
```

---

## 2. Image Pull Secrets

```bash
kubectl create secret docker-registry registry.gitlab.com \
--docker-server=registry.gitlab.com \
--docker-username=${COREWIRE_TRAINING_REGISTRY_USER} \
--docker-password=${COREWIRE_TRAINING_REGISTRY_TOKEN}
```

```bash
kubectl get secrets registry.gitlab.com -o yaml
```

```bash
kubectl get secrets registry.gitlab.com -o yaml | yq .data[] | base64 -d
```

### Beispiel: Pod mit privatem Image
Manifest-Datei `pull-secret-demo.yaml`:

```yaml
---
apiVersion: v1
kind: Pod
metadata:
  name: pull-secret
spec:
  imagePullSecrets:
  - name: registry.gitlab.com
  containers:
  - name: demoapp
    image: registry.gitlab.com/corewire/hands-on/k8s/images/docker-demoapp:1.0.0
    ports:
    - name: web
      containerPort: 5000
```

---

## 3. Probes

### Beispiel 3.1: Liveness Probe für einen Webserver
Manifest-Datei `liveness-probe-demo.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: liveness-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: liveness-demo
  template:
    metadata:
      labels:
        app: liveness-demo
    spec:
      containers:
      - name: web-server
        image: nginx:latest
        ports:
        - containerPort: 80
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 5
          periodSeconds: 10
```

**Befehle:**

```bash
kubectl apply -f liveness-probe-demo.yaml
kubectl describe pod -l app=liveness-demo
```

---

### Beispiel 3.2: Readiness Probe für eine Datenbank
Manifest-Datei `readiness-probe-demo.yaml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: readiness-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: readiness-demo
  template:
    metadata:
      labels:
        app: readiness-demo
    spec:
      containers:
      - name: database
        image: postgres:latest
        env:
        - name: POSTGRES_PASSWORD
          value: example
        readinessProbe:
          exec:
            command:
            - pg_isready
          initialDelaySeconds: 5
          periodSeconds: 10
```

**Befehle:**

```bash
kubectl apply -f readiness-probe-demo.yaml
kubectl describe pod -l app=readiness-demo
```

### Beispiel 3.3: Crashlooping Pod

Manifest-Datei `crashloop-demo.yaml`:

```yaml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: custom-app
  labels:
    exercise: probes
spec:
  replicas: 1
  selector:
    matchLabels:
      app: custom-app
  template:
    metadata:
      labels:
        app: custom-app
    spec:
      containers:
      - name: alpine-app
        image: alpine:latest
        command: ["/bin/sh", "-c"]
        args: ["touch /tmp/alive; sleep 15; rm -f /tmp/alive; sleep 600"]
        livenessProbe:
          exec:
            command:
            - cat
            - /tmp/alive
          initialDelaySeconds: 5
          periodSeconds: 5
          failureThreshold: 3
```

---

## Cleanup

Alles löschen