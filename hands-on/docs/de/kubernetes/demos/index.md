# Timetable

## 1. Tag
- 01-Einführung (Dauer: 1h)
- 02-Erste Schritte (Dauer: 2h)
    - Showcase: [Erste Schritte](./01-first-steps/)
    - Hands-on: [Erste Schritte](../aufgaben/01-first-steps/)
- 03-Pod Management (Dauer: 1.5h)
    - Showcase: [Pod Management](./02-pod-management/)
    - Hands-on: [Pod Management](../aufgaben/02-pod-management/)
- 04-Konfiguration (Dauer: 1.5h)
    - Showcase: [Konfiguration](./03-configuration/)
    - Hands-on: [Konfiguration](../aufgaben/03-configuration/)

## 2. Tag

- 05-Volumes (Dauer: 2h)
    - Showcase: [Volumes](./04-volumes/)
    - Hands-on: [Volumes](../aufgaben/04-volumes/)
- 06-Wordpress Lab (Dauer: 1h)
    - Hands-on: [Wordpress Lab](../aufgaben/05-wordpress-lab/)
- 07-Netzwerk (Dauer: 1h)
    - Showcase: [Netzwerk](./05-networking/)
    - Hands-on: [Netzwerk](../aufgaben/06-networking/)
- 08-Pod Management 2 (Dauer: 1h + Handon)
    - Showcase: [Pod Management 2](./06-pod-management-2/)
    - Hands-on: [Pod Management 2](../aufgaben/07-pod-management-2/)

## 3. Tag

- 09-Pod Status (Dauer: 2h)
    - Showcase: [Pod Status](./07-pod-status/)
    - Hands-on: [Pod Status](../aufgaben/08-pod-status/)
- 10-Multiple Container (Dauer: 1h)
    - Showcase: [Multiple Container](./08-multiple-containers/)
    - Hands-on: [Multiple Container](../aufgaben/09-multiple-containers/)
- 11-Nodes und Pods (Dauer: ??)
    - Showcase: [Nodes und Pods](./09-nodes-and-pods/)
    - Hands-on: [Nodes und Pods](../aufgaben/10-nodes-and-pods/)
- 12-Access Control (Dauer: ??)
    - Showcase: [Access Control](./10-access-control/)
    - Hands-on: [Access Control](../aufgaben/11-access-control/)
- 13-Advanced (Dauer: ??)
    - Showcase: [Advanced](./11-helm/)
    - Hands-on: [Advanced](../aufgaben/12-helm/)
