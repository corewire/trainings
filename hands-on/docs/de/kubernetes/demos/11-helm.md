# Helm Showcase

## Ziel

Dieser Showcase gibt eine kurze Demonstration der wichtigsten Helm-Features, die
im folgenden Hands-on behandelt werden. Das Ziel ist, den Teilnehmern einen
Überblick zu geben, wie Helm für das Management von Kubernetes-Ressourcen
verwendet werden kann.

---

## Vorbereitung

- Ein neues PV und PVC wird erstellt, diesmal mit einem anderen Pfad und Namen:

```yaml
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: demo-pv
  labels:
    showcase: helm
spec:
  capacity:
    storage: 2Gi
  volumeMode: Filesystem
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: ""
  hostPath:
    path: "/mnt/demo-data"
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: demo-pvc
  labels:
    showcase: helm
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi
  storageClassName: ""
```

## Helm Chart Installation

1. **Repository hinzufügen und Chart-Details ansehen:**

    ```shell
    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm search repo bitnami/nginx
    ```

2. **Eigene `values.yaml` erstellen:**

    ```yaml
    replicaCount: 2
    persistence:
      enabled: true
      existingClaim: demo-pvc
    ```

3. **Chart rendern und installieren:**

    ```shell
    helm template demo-release bitnami/nginx -f values.yaml > nginx-rendered.yaml
    helm install demo-release bitnami/nginx -f values.yaml
    ```

4. **Status und Ressourcen prüfen:**

    ```shell
    helm status demo-release
    kubectl get pods
    helm get all demo-release
    ```

## Helm-Charts Struktur

- Ein anderes Chart wird heruntergeladen und entpackt, z. B. Redis:

    ```shell
    helm pull bitnami/redis
    tar -xvf redis-*.tgz
    ```

- Struktur erklären:
  - `Chart.yaml`: Metadaten des Charts.
  - `values.yaml`: Standardwerte.
  - `templates/`: Ressourcen-Templates.
  - `README.md`: Dokumentation.

## Aufräumen

- Helm Release und PVC entfernen:

    ```shell
    helm uninstall demo-release
    kubectl delete pvc demo-pvc
    kubectl delete pv demo-pv
    
