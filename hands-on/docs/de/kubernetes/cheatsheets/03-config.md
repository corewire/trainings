# Cheatsheet 3 - Secrets und ConfigMaps

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```echo -n "<secret>" | base64```                          | Base64-Wert von <Secret> erhalten                          |
| ```echo -n "<encoded-secret>" | base64 -d```               | Klartext von encodiertem <Secret> erhalten                 |
| ```kubectl get secrets```                                  | Secrets anzeigen                                           |
| ```kubectl describe secret <name>```                       | Secret <name> anzeigen (ohne Secret-Werte)                 |
| ```kubectl get secrets <name> -o yaml```                   | Secret <name> anzeigen (mit Secret-Werten, Base64-Encoded) |
| ```kubectl get secrets <name> -o yaml```                   | Secret <name> anzeigen (mit Secret-Werten, Base64-Encoded) |
| ```kubectl get secrets <name> -o yaml | yq '.data.<secret-key>' | base64 -d``` | Secret im Klartext anzeigen (yq)       |
| ```kubectl get secrets <name> -o json | jq -r '.data.SECRET_PHRASE' | base64 -d``` | Secret im Klartext anzeigen (jq)   |
| ```kubectl get configmaps```                               | ConfigMaps anzeigen                                        |
| ```kubectl describe configmap <name>```                    | ConfigMap <name> anzeigen                                  |
| ```kubectl create secret generic <name> --from-literal=<key>=<value>``` | Key-Value als Secret <name> erstellen         |
| ```kubectl edit <resource> <name>```                       | Manifest einer Ressource (Secret, ConfigMap, Pod, ...) direkt bearbeiten |
| ```kubectl exec -it deployment/web -- sh```                | Interaktive Shell in laufendem Pod eines Deployments starten |

## Secret Beschreibung (Beispiel)

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
type: Opaque
data:
  password: c2VjcmV0  # base64 encoded value of 'secret'
```

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-secret-2
type: Opaque
stringData:
  password: secret # Klartext Wert, wird automatisch
                   # encodiert nach "data.password" geschrieben
```

## ConfigMap Beschreibung (Beispiel)

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-config-map
data:
  key: value
  file1.ext: |
    data1
  config.json: |
    {
      "key": "value"
    }
```
