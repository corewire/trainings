# Cheatsheet 4 - Volumes

## Volume Beschreibung

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  volumes:
  - name: <volume-name-1>
    <volume-type>:
      <volume-type-param-key>: <value>
  - name: <volume-name-2>
    <volume-type>:
      <volume-type-param-key>: <value>
  containers:
  - name: mycontainer
    image: myimage:latest
    volumeMounts:
    - name: <volume-name-1>
      mountPath: <first-path>
    - name: <volume-name-2>
      mountPath: <second-path>
```

## EmptyDir Beispiel:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  volumes:
  - name: myvolume
    emptyDir: {}
  containers:
  - name: mycontainer
    image: myimage:latest
    volumeMounts:
    - name: myvolume
      mountPath: /data
```

## HostPath Beispiel:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  volumes:
  - name: myvolume
    hostPath:
      path: /path/on/host
      type: DirectoryOrCreate
  containers:
  - name: mycontainer
    image: myimage:latest
    volumeMounts:
    - name: myvolume
      mountPath: /data
```

## Persistent Volume Claim Beispiel:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 5Gi
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  volumes:
    - name: my-storage
      persistentVolumeClaim:
        claimName: my-pvc
  containers:
    - name: my-container
      image: nginx
      volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: my-storage
```