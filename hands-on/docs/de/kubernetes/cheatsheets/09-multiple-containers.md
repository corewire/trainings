# Cheatsheet 9 - Multiple Container

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```kubectl logs <pod-name> -c <container-name>``` | Logs eines bestimmten Containers anzeigen                          |
| ```echo -n "<encoded-secret>" | base64 -d```               | Klartext von encodiertem <Secret> erhalten                 |


## initContainer Beispiel

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: example-pod
spec:
  initContainers:
  - name: init-container-1
    image: busybox
    command: ['sh', '-c', 'echo "Init Container 1 is running"; sleep 5']
  - name: init-container-2
    image: busybox
    command: ['sh', '-c', 'echo "Init Container 2 is running"; sleep 5']

  containers:
  - name: app-container
    image: nginx
    ports:
    - containerPort: 80
```

## Sidecars Beispiel

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: example-pod-sidecar
spec:
  containers:
  - name: app-container
    image: nginx
    ports:
    - containerPort: 80
    volumeMounts:
    - name: shared-data
      mountPath: /usr/share/nginx/html # Mount für geteilten Speicher mit Sidecar 1

  - name: sidecar-1
    image: busybox
    command: ['sh', '-c', 'echo "Hello from Sidecar 1" > /data/index.html; sleep 3600']
    volumeMounts:
    - name: shared-data
      mountPath: /data # Gemeinsamer Volume mit app-container

  - name: sidecar-2
    image: busybox
    command: ['sh', '-c', 'while true; do echo "Sidecar 2 is monitoring..." >> /var/log/monitor.log; sleep 10; done']
    volumeMounts:
    - name: log-data
      mountPath: /var/log

  volumes:
  - name: shared-data
    emptyDir: {}
  - name: log-data
    emptyDir: {}

```
