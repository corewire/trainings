# Cheatsheet 1 - Pods und Services

| Befehl                                                | Aktion                                            |
|-------------------------------------------------------|---------------------------------------------------|
| ```kubectl apply -f pod.yaml```                       | Pod aus der Pod-Definition pod.yaml starten       |
| ```kubectl run --image=nginx my-pod-name```           | Pod direkt starten                                |
| ```kubectl run --image=nginx my-pod-name --dry-run=client -o=yaml > pod.yaml``` | Pod-Definition pod.yaml generieren|
| ```kubectl run -it --image=alpine --rm my-first-pod``` | Interaktiven Pod starten                         |
| ```kubectl get pods```                                | Laufende Pods anzeigen                            |
| ```kubectl get pods -o=wide```                        | Laufende Pods mit zusätzlichen Details anzeigen   |
| ```kubectl get pod -l <label>```                      | Alle Pods mit dem Label <label> anzeigen          |
| ```kubectl get pod -l '<labelname> in (<list>)'```    | Alle Pods mit Labeln aus <list> anzeigen          |
| ```kubectl logs <pod-name>```                         | Logs eines Pods anzeigen                          |
| ```kubectl label pod <name> <newlabel> --overwrite``` | Label in Pod <name> mit <newlabel> überschreiben  |
| ```kubectl label pod <name> --list```                 | Liste aller Label in Pod <name> anzeigen          |
| ```kubectl describe pod <name>```                     | Details des Pods <name> anzeigen                  |
| ```kubectl delete pod <name>```                       | Pod <name> löschen                                |
| ```kubectl delete -f pod.yml```                       | Pod(s), die in pod.yaml definiert sind, löschen   |
| ```kubectl describe service <name>```                 | Details des Service <name> anzeigen               |
| ```kubectl delete service <name>```                   | Service <name> löschen                            |

## Pod Beschreibung (Beispiel)

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
  labels:
    my-label: my-value
    app: web
    environment: production
    tier: frontend
spec:
  containers:
  - name: my-container-name
    image: nginx
```

## Service Beschreibung (Beispiel)

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
```