# Cheatsheet 7 - Stateful-, DaemonSets, Jobs

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```kubectl scale statefulset <name> --replicas=<number>``` | Anzahl der Replicas im StatefulSet <name> ändern            |


## Headless-Servcie-Manifest Beispiel

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-headless-service
  labels:
    app: my-app
spec:
  ports:
  - port: 80
    name: web
  clusterIP: None
  selector:
    app: my-app
```

## StatefulSet-Manifest Beispiel

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: my-stateful-set
  labels:
    app: my-app
spec:
  serviceName: my-service
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: my-container
        image: nginx:latest
```

## Job-Manifest Beispiel

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: my-job
spec:
  parallelism: 2
  completions: 6
  backoffLimit: 3
  template:
    spec:
      containers:
      - name: echo-hello
        image: alpine
        command: ["echo", "Hello"]
      restartPolicy: Never
```

## CronJob-Manifest Beispiel

```yaml
---
apiVersion: batch/v1
kind: CronJob
metadata:
  name: my-cronjob
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          [...]
```

## DaemonSet-Manifest Beispiel:

```yaml
---
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: my-daemonset
spec:
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: node-exporter
        image: prom/node-exporter
        ports:
        - containerPort: 9100
```

## Rolling Update Beispiel

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rolling-update-deployment
spec:
  replicas: 3
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1   # A maximum of 1 Pod can be unavailable during the update process
      maxSurge: 1         # A maximum of 1 additional Pod can be created above the desired number during updates
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: app-container
        image: nginx:latest
        ports:
        - containerPort: 80
```
