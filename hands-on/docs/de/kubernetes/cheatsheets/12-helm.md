# Cheatsheet 12 - Helm

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```helm template <release-name> <repo-url> -f values.yaml > rendered.yaml``` | Resourcen eines Charts rendern           |
| ```helm install <release-name> <repo-url> -f values.yaml``` | Chart installieren                                        |
| ```helm list```                                             | Installierte Charts anzeigen lassen                       |
| ```helm status <release-name>```                            | Status eines Charts anzeigen lassen                       |
| ```helm get all <release-name>```                           | Details zu einem Chart anzeigen lassen                    |
| ```helm pull <repo-url>```                                  | Chart herunterladen                                       |
| ```helm uninstall <release-name>```                         | Chart deinstallieren                                      |

