# Cheatsheet 6 - Netzwerk

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```kubectl port-forward service <name> <local-port>:<service-port>``` | Anfragen von einem lokalen Port an Service-Port weiterleiten |

## ClusterIP Manifest

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-internal-service
spec:
  selector:
    app: my-app
  type: ClusterIP
  ports:
  - name: http
    port: 80
    targetPort: 80
    protocol: TCP
```

## NodePort Manifest

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-nodeport-service
spec:
  selector:
    app: my-app
  type: NodePort
  ports:
  - name: http
    port: 80
    targetPort: 80
    nodePort: 30001
    protocol: TCP
```

## Ingress Controller Manifest

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: my-ingress
spec:
  rules:
  - host: foo.mydomain.com
    http:
      paths:
      - backend:
          service:
            name: foo
            port:
              number: 8080
  - host: mydomain.com
    http:
      paths:
      - path: /bar/*
        backend:
          service:
            name: bar
            port:
              number: 8080
```

## Sidecars example

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: example-pod-sidecar
spec:
  containers:
  - name: app-container
    image: nginx
    ports:
    - containerPort: 80
    volumeMounts:
    - name: shared-data
      mountPath: /usr/share/nginx/html # Mount für geteilten Speicher mit Sidecar 1

  - name: sidecar-1
    image: busybox
    command: ['sh', '-c', 'echo "Hello from Sidecar 1" > /data/index.html; sleep 3600']
    volumeMounts:
    - name: shared-data
      mountPath: /data # Gemeinsamer Volume mit app-container

  - name: sidecar-2
    image: busybox
    command: ['sh', '-c', 'while true; do echo "Sidecar 2 is monitoring..." >> /var/log/monitor.log; sleep 10; done']
    volumeMounts:
    - name: log-data
      mountPath: /var/log

  volumes:
  - name: shared-data
    emptyDir: {}
  - name: log-data
    emptyDir: {}

```
