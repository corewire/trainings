# Cheatsheet 11 - Access Control

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```kubectl get namespace``` | Namespaces anzeigen |
| ```kubectl get pods --namespace=<namespace>``` | Pods in einem Namespace anzeigen |
| ```kubectl config view``` | Aktuelle Kubeconfig anzeigen |
| ```kubectl config get-contexts``` | Verfügbare Contexts anzeigen|
| ```kubectl config current-context``` | Aktuellen Context anzeigen |
| ```kubectl config set-context --current --namespace=<namespace> --user=<user>``` | Aktuellen Context setzen |
| ```kubectl apply -f pod.yaml --as system:serviceaccount:<namespace>:<serviceaccount>``` | Befehl als anderer Nutzer ausführen |


## Namespace-Manifest Beispiel

```yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: test
```

## Namespace in einer Resource definieren

```yaml
---
apiVersion: apps/v1
kind: Deployment  # Auch Pod, Job oder jegliche weitere Resource.
metadata:
  name: my-resource
  namespace: my-namespace
spec:
  [...]
```

## ServiceAccount-Manifest Beispiel

```yaml
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: my-service-account
  namespace: my-namespace
```

## Role-Manifest Beispiel

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: my-role
  namespace: my-namespace
rules:
- apiGroups: [""]
  resources:
  - pods
  verbs:
  - create
  - get
  - list
  - watch
  - delete
```

## RoleBinding-Manifest Beispiel

```yaml
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: my-role-binding
  namespace: my-namespace
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: my-role
subjects:
- kind: ServiceAccount
  name: my-service-account
  namespace: my-namespace
```