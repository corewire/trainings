# Cheatsheet 5 - Wordpress Lab

## Secrets Manifest

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-secret
type: Opaque
stringData:
  password: secret # Klartext Wert, wird automatisch
                   # encodiert nach "data.password" geschrieben
```

## Deployment Manifest

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
  labels:
    app: my-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      volumes:
      - name: my-volume
        hostPath:
          path: /data
          type: DirectoryOrCreate
      containers:
      - name: my-container
        image: nginx:latest
        env:
        - name: my-key
          value: my-value
          valueFrom:
            secretKeyRef:
              name: my-secret
              key: my-key
        volumeMounts:
        - mountPath: /data
          name: my-volume
        ports:
        - containerPort: <port>
```

## Service Manifest

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: web
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
```