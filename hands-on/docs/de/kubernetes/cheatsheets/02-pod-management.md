# Cheatsheet 2 - ReplicaSets und Deployments

| Befehl                                                     | Aktion                                                     |
|------------------------------------------------------------|------------------------------------------------------------|
| ```kubectl get all```                                      | Laufende Pods, Services und weitere Resourcen anzeigen     |
| ```kubectl get pods,deployments```                         | Laufende Pods und Deployments anzeigen                     |
| ```kubectl scale replicaset <name> --replicas=<number>```  | Anzahl der Replicas im ReplicaSet <name> ändern            |
| ```kubectl scale deployment <name> --replicas=<number>```  | Anzahl der Replicas im Deployment <name> ändern            |
| ```kubectl delete <type> --selector <label>```             | Alle ReplicaSets/Deployments/... mit Label <label> löschen |
| ```watch kubectl get all```                                | Änderungen in Echtzeit anzeigen lassen                     |
| ```kubectl rollout history deployment <name>```            | Historie des Deployments <name> anzeigen                   |
| ```kubectl rollout history deployment <name> --revision=<id>```| Details eines Deployments anzeigen                     |
| ```kubectl rollout undo deployment <name>```               | Änderung eines Deployments zurückrollen                    |
| ```kubectl describe <type> <name>```                       | Beschreibung von ReplicaSet/Deployment/... <name> anzeigen |

## ReplicaSet Beschreibung (Beispiel)

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: my-replicaset
  labels:
    app: my-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: my-container
        image: nginx:latest
```


## Deployment Beschreibung (Beispiel)

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-deployment
  labels:
    app: my-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: my-container
        image: nginx:latest
```


