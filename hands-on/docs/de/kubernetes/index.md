# Kubernetes

- [Zu den Aufgaben](./aufgaben/01-first-steps/)
- [Zu den Cheatsheets](./cheatsheets/01-first-steps/)
- [Zu den Folien](./01-introduction/#/)

## Dauer

3 Tage

## Zielgruppe

- Softwareentwickler:innen
- Software-Architekt:innen
- Systemadministrator:innen
- DevOps-Engineers

## Voraussetzung

- Arbeiten mit Linux
- Arbeiten auf der Konsole
- Containerisierung (Docker oder Podman)
    - Verwalten von Containern (starten, stoppen, protokollieren, ausführen)
    - Erstellen von Container-Images

## Kursziel

- Lernen, wie man Anwendungen auf Kubernetes bereitstellt
- Verstehen der Konzepte von Kubernetes
- Konfigurieren des Benutzerzugriffs auf Anwendungen mit Hilfe eines Ingress Controllers

## Schulungsform

Der Trainer führt in jedes Thema mit einem Foliensatz ein und führt die Anwendung des Themas vor. Die Teilnehmer wenden ihr Verständnis durch das Lösen der Übungen an und vertiefen es.

## Kursinhalt

### 1. Tag
- Motivation, Grundbegriffe und Anwendungsfälle für Orchestrierung
- Erste Schritte mit Pods, Labels und Services
- ReplicaSets und Deployments
- Umgebungsvariablen, ConfigMaps und Secrets
- Möglichkeiten der Speicherverwaltung in Kubernetes

### 2. Tag
- Aufbau eines Wordpress-Clusters
- Grundlagen von Netzwerken mit Loadbalancer und Ingress Controller
- Stateful Sets, Daemon Sets, Jobs und Cronjobs zur Erstellung von Pods
- Image Pull Secrets und Readiness Probe
- Init Container und Sidecars

### 3. Tag
- Cluster-Architektur mit Request und Limits von Pods
- Zugriffskontrollmöglichkeiten
- Kind und Helm
- Prüfungsvorbereitung CKAD
- Ausblick
