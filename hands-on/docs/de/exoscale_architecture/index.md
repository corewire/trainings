# Exoscale - Certified Solution Architect

**Wichtig:** Die Kursmaterialien sind bisher nur auf Englisch verfügbar:

- [Zu den Aufgaben](/en/exoscale/exercises/01-virtual-machines/)

## Dauer

2 Tage

## Zielgruppe

- Softwareentwickler:innen
- Software-Architekt:innen
- Systemadministrator:innen
- DevOps-Engineers

## Voraussetzungen

- (optional) Erste Erfahrungen mit dem Terminal/Bash
- (empfohlen) Certified Sales Professional - Kurs

## Kursziel

Verstehen und Anwenden von technischen Konzepten durch die Nutzung von Exoscale-Funktionen und -Produkten zur Erstellung und Ausführung moderner Anwendungen und Cloud-Native Workloads.

## Schulungsform

Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt

Dieser Kurs vermittelt die technischen Grundkenntnisse für den Aufbau von Cloud Computing-Architekturen mit den Exoscale-Plattformprodukten, die zu Beginn vorgestellt werden. Technologische Einblicke und der Aufbau des notwendigen Wissens über die Produkte und Funktionen der Exoscale-Plattform sowie eine strukturierte Herangehensweise an sie sind das Ziel dieses Level 200 Certified Solution Architect - Kurses. Er wird Ihnen helfen, die zentralen technischen Konzepte von Computing, Storage, Automation, Skalierung und Backup zu erlernen. Außerdem werden Sie sich mit Netzwerkkomponenten und -konfiguration befassen. Der Kurs schließt mit architektonischen Hinweisen zur Cloud-Computing-Infrastruktur und einem umfassenden Überblick über die wichtigsten Cloud-Themen. Einige Tipps zum Thema Database as a Service runden die Schulung ab, da die Datenbankfunktionalität oft eine wichtige Komponente bei der Implementierung von Cloud-Architekturen ist.

Sie lernen die grundlegenden technologischen Konzepte kennen:

- Compute
- Cloud-Init
- Automatisierung
- Skalierung
- Verkehr
- Speicherung
- Sicherung

Die notwendigen Netzwerkthemen:

- Grundlagen der Netzwerktechnik
- Switching/Routing
- Private Netzwerke
- Load Balancing

Und ausgewählte fortgeschrittene Themen: 

- Cloud-Herausforderungen
- Architektur
- Datenbank

## Zertifizierung

Dieser Kurs soll Sie auf die Prüfung zum Certified Solution Architect vorbereiten.

