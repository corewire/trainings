# Code-Build-Test-Stage

- Hintergrund: Aus der Planing-Phase fallen Issues, die umgesetzt werden müssen. Dies zeigen wir nun einmal
- Fokus: Umsetzung aus Sicht eines Entwicklers/einer Entwicklerin

## Vorbereitung

**Wichtig für alle Show-Cases**: [SETUP-SHOWCASE.md](https://gitlab.com/corewire/hands-on/devops/mkdocs-on-aws/-/blob/main/SETUP-SHOWCASE.md?ref_type=heads)

## MR erstellen oder öffnen:

- Ein Issue aus dem vorherigen Showcase umsetzen oder ein neues erstellen.
- Wir arbeiten ohne Code, deshalb wäre ein Beispiel: "Blogpost über Schulung anlegen"
- Create MR

## MR bearbeiten
- Open WebIDE (press "." in MR or use Code -> Open WebIDE)
- Add a file and commit
- Show Commits, Pipelines, Changes
- Show "visal" Part of CI -> Red/Green bubbles, Talk about automated tests, qa, ...
- Talk about "approve", Code Review

## MR abschließen
- Edit First Line "Merge branch 'xxx' into 'main'" to something meaningful
- Add `Changelog: added` to the bottom of the Merge Commit Message
- Show Container Registry, where there is a container image tag for each git commit which triggered a CI

## Issue zeigen

- Ist geschlossen