# Plan-Stage

- Hintergrund: Hier soll vermittelt werden, wie die Planung durch ein Tool wie Gitlab unterstütz werden kann.
- Fokus:Es ist wichtig nicht zu Gitlab spezifisch zu werden, sondern die allgemeinen Konzepte zu vermitteln. Es geht darum zu Zeigen was mit den standard Features eines Ticketsstems möglich ist und das diese Ausreichen.

## Vorbereitung

**Wichtig für alle Show-Cases**: [SETUP-SHOWCASE.md](https://gitlab.com/corewire/hands-on/devops/mkdocs-on-aws/-/blob/main/SETUP-SHOWCASE.md?ref_type=heads)

## Labels erstellen

- Type::Bug
- Type::Userstory
- State::Selected for Development
- State::In Progress
- State::Ready for Review

## Boards erstellen

- Development
- Sprint Planning

## Tickets zeigen

- Verschiedene Tickets erstellen und Features zeigen und erklären
- Sprintplanung machen
- In Development Board wechseln
- Ticket in "In Progress" verschieben
- Merge Request erstellen
- Verlinkung von Tickets und Merge Requests zeigen
