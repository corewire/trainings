# Release-Deploy-Stage

- Hintergrund: Issue wurde von Entwickler:in bearbeitet, jetzt soll eine/mehrere Änderungen released werden
- Fokus: Easy-to-use, wenn automatisiert. Kein Stress beim Release

## Vorbereitung

**Wichtig für alle Show-Cases**: [SETUP-SHOWCASE.md](https://gitlab.com/corewire/hands-on/devops/mkdocs-on-aws/-/blob/main/SETUP-SHOWCASE.md?ref_type=heads)

## Release erstellen:

- In diesem Fall: Release wird durch Git-Tag getriggert
    -> gibt auch andere Möglichkeiten
- Tag größer 1.0.0 erstellen -> z.B. "v1.1.0"
- Pipelines zeigen, -> Neue Pipeline mit mehr Schritten, weil es ein Release ist
- `build-release-notes` Schritt: Artefakt anzeigen, über automatisierte Releases sprechen
- Release ist erstellt unter Deploy -> Release; => andere könnten das Ergebnis jetzt nutzen

## Deployment

- Version vom ELB auf AWS zeigen
- Alternativ, wenn wir selber "Kunde" sind: Deployment triggern, Manual Step in der CI