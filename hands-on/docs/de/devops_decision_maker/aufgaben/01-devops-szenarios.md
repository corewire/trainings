# Devops Szenarien

!!! goal "Ziel"
    In diesem Projekt geht es darum die in dieser Schulung erlernten DevOps Konzepte in realistischen Szenarien anzuwenden.

- Finden Sie sich in einer Gruppe zusammen. Die Gruppengröße sollte zwischen 3 und 5 Personen liegen
- Wählen Sie ein Szenario aus der untenstehenden Liste aus
- Identifizieren Sie Methoden und Techniken aus der Schulung, die auf Ihr
Szenario anwendbar sind und Ihrer Meinung nach in der Aktuellen Situation am
schnellsten zu Verbesserungen führen
    - Beschreiben Sie Veränderungen die schnell und mit wenig Aufwand umsetzbar sind und eine möglichst große Wirkung haben
    - Beschreiben Sie langfristigere Ziele mit einer möglichst große Wirkung

## Szenario 1: E-Commerce Startup – DevOps am Anfang

- **Unternehmen**: Ein junges E-Commerce-Unternehmen, das stark auf die Markteinführung eines neuen Produkts fokussiert ist.
- **Mitarbeiter**: 30, kleines IT-Team mit 5 Entwicklern.
- **Aktueller Stand**: DevOps ist noch nicht eingeführt. Entwicklungs- und Operations-Teams arbeiten getrennt, Releases dauern 4-6 Wochen. Es gibt keinen automatisierten CI/CD-Prozess.
- **Technologie**: Webanwendung in der Cloud (AWS), Nutzung von manuell konfigurierten virtuellen Maschinen, vereinzelte Docker-Container.
- **Herausforderungen**: Lange Release-Zyklen, hohe Anzahl von Bugs, keine klare Kommunikationsstruktur zwischen Entwicklung und Operations.

## Szenario 2: Mittelständisches Unternehmen – DevOps-Transformation stagniert

- **Unternehmen**: Ein mittelständischer Hersteller von Industrieanlagen mit einer eigenen internen IT-Abteilung.
- **Mitarbeiter**: 500, IT-Team mit 20 Entwicklern.
- **Aktueller Stand**: DevOps wurde vor zwei Jahren eingeführt, doch es gibt Widerstände. Es bestehen noch Silos zwischen den Teams. Continuous Integration ist etabliert, aber es fehlen automatisierte Tests und ein durchgehender CI/CD-Prozess.
- **Technologie**: Hybride Infrastruktur (teilweise On-Premises, teilweise Cloud). Kubernetes wird in ersten Projekten genutzt, aber die Einführung ist noch nicht abgeschlossen.
- **Herausforderungen**: Die IT- und Entwicklungsteams arbeiten immer noch nicht effizient zusammen. Sicherheitsbedenken bremsen die Automatisierung. Der Kulturwandel ist ins Stocken geraten.

## Szenario 3: Großkonzern – DevOps skalieren

- **Unternehmen**: Ein multinationaler Finanzdienstleister, der stark regulierten Märkten unterliegt.
- **Mitarbeiter**: 50.000, global verteilte IT-Teams.
- **Aktueller Stand**: DevOps ist in einzelnen Bereichen erfolgreich etabliert, doch es gibt Herausforderungen bei der Skalierung auf das gesamte Unternehmen. Sicherheits- und Compliance-Anforderungen verzögern den Einsatz neuer Technologien.
- **Technologie**: Multi-Cloud-Umgebung mit komplexen Microservices-Architekturen. Kubernetes und Docker sind weit verbreitet, doch die Teams verwenden unterschiedliche DevOps-Tools.
- **Herausforderungen**: Schwierigkeiten bei der Standardisierung von Prozessen und Tools. Sicherheits- und Compliance-Anforderungen stehen oft im Konflikt mit der DevOps-Philosophie der schnellen Auslieferung.

## Szenario 4: SaaS-Unternehmen – Schnell wachsend, aber instabil

- **Unternehmen**: Ein wachsendes Software-as-a-Service (SaaS)-Unternehmen, das eine Plattform für Projektmanagement anbietet.
- **Mitarbeiter**: 150, IT-Team mit 50 Entwicklern.
- **Aktueller Stand**: DevOps wurde eingeführt, aber es gibt viele Produktionsausfälle und ungeplante Downtimes. Die Teams arbeiten unter hohem Druck, neue Features schnell zu veröffentlichen, was oft auf Kosten der Stabilität geht.
- **Technologie**: Cloud-native Architektur (Azure). Nutzung von CI/CD-Pipelines, aber keine konsequente Automatisierung von Sicherheits- und Lasttests. Die Infrastruktur wird manuell gemanagt.
- **Herausforderungen**: Hoher Release-Druck führt zu mangelnder Stabilität und unzureichendem Testen. Kundenbeschwerden nehmen zu.

## Szenario 5: Traditionsunternehmen – Legacy-Systeme und Widerstand gegen Veränderung

- **Unternehmen**: Ein traditionsreiches Versicherungsunternehmen, das seit Jahrzehnten auf ein Mainframe-basiertes IT-System setzt.
- **Mitarbeiter**: 5.000, IT-Team mit 200 Mitarbeitern, von denen viele auf Mainframes spezialisiert sind.
- **Aktueller Stand**: Erste Schritte in Richtung DevOps wurden unternommen, doch die Einführung neuer Technologien stößt auf großen Widerstand. Die IT-Infrastruktur basiert weitgehend auf monolithischen Legacy-Systemen, die schwer zu modernisieren sind.
- **Technologie**: Vorwiegend On-Premises, ältere monolithische Systeme und Mainframes. Es gibt keine durchgängige Cloud-Strategie.
- **Herausforderungen**: Hohe Abhängigkeit von Legacy-Systemen und mangelnde Bereitschaft der Mitarbeiter, neue Technologien zu übernehmen. Mangelnde Agilität und langsame Release-Zyklen.

## Szenario 6: Technologie-Startup – Cloud-native, aber unklare Prozesse

- **Unternehmen**: Ein kleines, aber innovatives Technologie-Startup, das KI-gestützte Lösungen anbietet.
- **Mitarbeiter**: 50, mit einem IT-Team von 30 Entwicklern, alle sind erfahren in modernen Technologien.
- **Aktueller Stand**: Das Unternehmen setzt vollständig auf eine Cloud-native Infrastruktur (GCP), hat aber bisher keine klaren DevOps-Prozesse definiert. Das schnelle Wachstum führt zu unstrukturierten Workflows, und es fehlt an zentralen Vorgaben für CI/CD und Monitoring.
- **Technologie**: Komplett Cloud-native, Kubernetes und Docker im Einsatz, aber ohne klare Richtlinien oder zentrale Steuerung. Es gibt keine zentrale Überwachung oder Log-Analyse.
- **Herausforderungen**: Hohe Innovationsgeschwindigkeit, aber mangelnde Prozessstruktur und fehlende Monitoring-Strategie. Kein standardisierter CI/CD-Ansatz.
