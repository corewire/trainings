# DevOps für Entscheider

- [Zu den Folien](./0_agenda/#/)

## Dauer

1 Tag

## Zielgruppe

- Führungskräfte
- Projektmanager:innen

## Voraussetzungen

- IT Grundlagen
- Verständnis für Abläufe in der Softwareentwicklung

## Kursziel

Der Kurs vermittelt Führungskräften und Entscheidungsträgern ein klares Verständnis der DevOps-Prinzipien und deren strategischer Bedeutung für moderne IT-Organisationen. Die Teilnehmer lernen, wie DevOps die Zusammenarbeit zwischen Entwicklung und Betrieb optimiert, Time-to-Market verkürzt und die Softwarequalität verbessert. Der Kurs bietet Einblicke in die Vorteile von Automatisierung, kontinuierlicher Integration und Bereitstellung (CI/CD) sowie die Bedeutung von Kulturwandel und agiler Zusammenarbeit. Am Ende des Kurses können die Teilnehmer:innen fundierte Entscheidungen über die Einführung und Umsetzung von DevOps-Methoden treffen und deren geschäftlichen Nutzen einschätzen.

## Schulungsform

Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt.

## Kursinhalt

- Einführung in die DevOps Grundlagen: Agile Kultur, Mindset und Vorteile
- Scrum/Kanban und DevOps im Produktlebenszyklus
- Systemgestützter agiler Entwicklungsprozess und agiles Produktmanagement
- Code & Review Management und Qualitäts- & Sicherheitsmanagement
- Continuous Integration, Delivery und Deployment
- Betrieb, Virtualisierung und Orchestrierung
- Automatisierung und kontinuierliche Bewertung & Monitoring
- Fail-Fast, Learn-Fast, Fix-Fast: Prinzipien der Prozessoptimierung
- Kundenintegration: Kommunikation, Feedback und Transparenz
- Integration des Entwicklungsteams: Arbeitsabläufe, Transparenz und Automatisierung
- Automatisiertes Bauen, Testen und Code-Analysen
- Cross-funktionale DevOps-Teams und ihre Produktivität
- DevOps-Toolchain: Tools für verschiedene Bereiche
- Seiteneffekte von DevOps: Social Engineering und Know-How Management
- Ausblick auf DevSecOps und BizDevOps
