# Mit anderen gemeinsam arbeiten

!!! goal "Ziel"
    - In diesem Hands-on geht es um die Zusammenarbeit mit anderen über Gitlab.
    - Sie werden ein Ticket erstellen und es jemandem zuweisen. Nach der Bearbeitung
    müssen Sie die Änderungen überprüfen und können anschließend das Ticket abschließen.

!!! attention "Hinweis"
    Sie müssen in diesem Hands-on mit anderen zusammenarbeiten. Wenn in einer Aufgabe von
    der nächsten Person die Rede ist, dann ist damit folgendes gemeint:

    | Sie | Die nächste Person |
    |-----|---------------------|
    | user_**5**@corewire.de | user_**6**@corewire.de|

## Aufgabe 1 - Ein Ticket erstellen

Die im vorhergehenden Hands-on angelegte Datei soll nun bearbeitet werden. Gehen Sie wie
folgt vor:

- Erstellen Sie ein Ticket mit einem beliebigen Titel
- Fügen Sie folgende Beschreibung ein:
```
Hallo,

das jährliche Sommerfest steht an. Vielen Dank, dass du dich um das Catering
kümmerst. Bitte trage noch ein paar Infos in folgende Datei ein: **Namen ihrer Datei**

Folgende Punkte sind noch offen:
- [ ] Als Ansprechpartner fürs Catering eintragen
- [ ] Kontaktdaten eines Caterers (mit Link) eintragen
- [ ] Geplante Essens- und Getränkeauswahl eintragen

```
- Ersetzen Sie in der Beschreibung `**Name ihrer Datei**` mit dem Namen der zuvor erstellten Datei.
- Hängen Sie das Ticket an das Epic "Gemeinsam arbeiten"
- Markieren Sie das Ticket als Userstory

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Ticket erstellen:

    - Ein Ticket können Sie über das Plus-Symbol in der Kopfzeile oder in der Ticketübersicht (Navigation unter "Issues") mit dem Knopf "New Issue" rechts oben erstellen
    - Geben Sie einen beliebigen Titel ein
    - Kopieren Sie die Beschreibung von oben in das Beschreibungsfeld
    - Passen Sie die Beschreibung an, indem Sie den `**Namen**` und `**Name ihrer Datei**` ersetzen
    - In dem Sie über dem Eingabefeld auf Preview klicken, können Sie sich anschauen, wie Gitlab den Text anzeigen wird
    - Wählen Sie unter "Select epic" das Epic "Gemeinsam arbeiten" aus
    - Wählen Sie unter Labels das Label "Userstory" aus
    - Erstellen Sie das Ticket mit einem Klick auf "Create issue"


## Aufgabe 2 - Bereit zur Entwicklung

- Gehen Sie auf das Board "Backlog refinement"
- Markieren Sie das Ticket als "Bereit zur Entwicklung"
- Weisen Sie das Ticket der nächsten Person zu. Wenn Sie User
  *user_***5***@corewire.de* sind, dann ist für Sie der nächste User
  *user_***6***@corewire.de*

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Gehen Sie links in der Navigation auf "Issues"
    - Im Untermenü finden Sie den Eintrag "Boards"
    - Wählen Sie über das Dropdown links oben das Board "Backlog refinement" aus
    - Über Drag-and-drop können Sie Tickets zwischen den Spalten hin und her bewegen.
    - Klicken Sie auf ihr Ticket und ziehen Sie es in die Spalte "Bereit zur Entwicklung"
    - Wenn Sie auf ihr Ticket klicken, öffnet sich rechts ein kleines Menü. Hier können Sie das Ticket zusätzlich bearbeiten
    - Weisen Sie das Ticket der nächsten Person (wie oben beschrieben) zu

## Aufgabe 3 - In Arbeit

Sie sollten nun ebenfalls ein Ticket zugewiesen bekommen haben. Schauen Sie auf
das Board nach ihren Tickets. Wenn das Board unübersichtlich ist, können Sie
zusätzlich nach ihrem Nutzer filtern.

- Gehen Sie auf das Board "Development"
- Markieren Sie das Ticket als "In Arbeit"
- Erstellen Sie einen Mergerequest
- Folgen Sie den Anweisungen im Ticket
- Erstellen Sie einen Commit mit ihren Änderungen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Wählen Sie über das Dropdown nun das Board "Development" aus
    - Sie können den Status des Tickets wieder über Drag-and-drop verändern.
    - Ziehen Sie ihr Ticket in die "In Arbeit"-Spalte
    - Öffnen Sie das Ticket durch einen Klick darauf
    - Erstellen Sie einen Mergerequest über den Knopf "Create merge request" in
      der Ticketübersicht
    - Im Mergerequest, öffen Sie die Web IDE über "Open in Web IDE".
    - Wählen Sie in der Web IDE links im Dateibrowser die Datei aus, die Sie bearbeiten sollen.
    - Ergänzen Sie die Angaben in der Datei, wie sie im Ticket beschrieben sind:
        - Ergänzen Sie die Zeile "Catering" in der Tabelle zu
        ```
        | Catering | **Name** | 0815 / 123456 |
        ```
        - Ergänzen Sie die Kontaktdaten für das Catering
        ```
        Firma ... Partyservice und mehr
        Kontakt: [www.partyservice.de](www.partyservice.de)
        ```
        - Ergänzen Sie die ein paar Getränke und Essensvorschläge
    - Schauen Sie sich ihre Änderungen an. Die Web IDE bietet ebenfalls eine Markdown Previewfunktion
    - Erstellen Sie einen Commit über einen Klick unten links auf "Commit..."

## Aufgabe 4 - Bereit zum Review

- Gehen Sie wieder auf ihren Mergerequest
- Markieren Sie ihren Mergerequest als "Bereit"
- Fügen Sie die Person, von der Sie das Ticket erhalten haben, als Reviewer hinzu
- Aktualisieren Sie das dazugehörige Ticket:
    - Ticket als "Bereit zum Review" markieren
    - Subtasks aktualisieren
    - Ticket wieder dem Ticketersteller zuweisen

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Zurück zum Mergerequest:

    - Schließen Sie die Web IDE. Das können Sie entweder über einen Klick oben Links auf das aktuelle Projekt oder
    unten in der Fußzeile. Hier wird ihnen angezeigt, mit welchem Mergerequest sie arbeiten. Mit einem Klick auf
    "Merge request !{nummer}" gelangen Sie direkt zum Mergerequest.
    - Unter "Changes" können Sie alle Änderungen innerhalb des Mergerequestes sehen

    Mergerequest als "Bereit" zu markieren

    - Klicken Sie oben rechts auf "Mark as ready"

    Reviewer hinzufügen:

    - Fügen Sie rechts die Person, die ihnen das Ticket zugewiesen hat, als Reviewer hinzu.

    Ticket aktualisieren:

    - Gehen Sie auf das "Development"-Board. Ziehen Sie dort das entsprechende Ticket in die Spalte "Bereit zum Review"
    - Markieren Sie mit einem Klick auf die jeweilige Checkbox die Subtasks als erledigt
    - Weisen Sie das Ticket rechts im Menü wieder dem Ticketersteller zu

## Aufgabe 5 - Ein Ticket reviewen

Sie haben nun ihr bearbeitetes Ticket wieder erhalten.

- Schauen Sie sich die Änderungen im Mergerequest an
- Kommentieren Sie Stellen, die nicht richtig in Markdown dargestellt werden (z.B. Links, Tabellen, Aufzählungen)
- Kommentieren Sie eine der Zeilen mit der Essens- oder Getränkeauswahl mit einem Verbesserungsvorschlag
- Fügen Sie ebenfalls noch einen allgemeinen Kommentar zum Mergerequest hinzu

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Gehen Sie auf den Mergerequest, den sie reviewen sollen. Das können Sie entweder
      links in der Navigation unter Merge requests oder oben rechts in der Kopfzeile.
      Dort gibt es ein Icon für Mergerequests, über das Sie sich alle Mergerequests
      anzeigen lassen können, bei denen Sie als Reviewer eingetrage sind.
    - Im Mergerquest können Sie sich die Änderungen unter dem Reiter "Changes" anzeigen lassen.
    - Wenn Sie mit der Maus über eine Zeile fahren, wird ihnen ein Icon angezeigt, mit dem Sie einzelne
      Änderungen direkt im Kontext kommentieren können.
    - Machen Sie eine Anmerkung zur Getränke oder Essensauswahl und klicken Sie auf "Start review"
    - Gehen Sie wieder auf den Reiter "Overview"
    - Schreiben Sie hier noch einen generellen Kommentar zum Mergerequest
    - Klicken Sie "Submit review" um alle Kommentare zu übernehmen

## Aufgabe 6 - Anmerkungen aus dem Review

Sie sollten nun ebenfalls Kommentare zu ihren Änderungen in ihrem Mergerequest erhalten haben.

- Bearbeiten Sie die Kommentare in ihrem Mergerequest
- Benachrichtigen Sie den Reviewer mit einem Kommentar, dass seine Anmerkungen umgesetzt wurden.

Am Ende sollten alle Kommentare "resolved" sein. Sie können entweder beim Antworten
  direkt die Checkbox anwählen oder nachträglich die einzelnen Diskussionen als gelöst markieren

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    - Gehen Sie auf den von ihnen erstellen Mergerequest. Das erreichen Sie entweder
      über die Navigation links unter Mergerequests oder über das Icon rechts oben
      in der Kopfzeile
    - Schauen Sie sich in der Mergerequest-Übersicht die Kommentare an
    - Beantworten Sie die Kommentare oder bearbeiten Sie die entsprechende Stelle in der Web IDE
    - Schauen Sie, dass alle Kommentare bearbeitet und gelöst sind. In der Mergerequest-Übersicht
      auf Höhe der Reiter "Overview", "Commits", etc. finden Sie eine Übersicht, wie viele Kommentare
      noch offen sind.

## Aufgabe 7 - Ein Ticket abschließen

Nach dem das Ticket und der Mergerequest eine Korrekturschleife gedreht haben, sollten
nun alle Anmerkungen gelöst sein. Damit ihr Ticket, das Sie anfangs erstellt haben, gelöst
werden kann, muss nun der dazugehörige Mergerequest abgeschlossen werden. Gehen Sie dazu
in den Mergerequest, den sie zuvor reviewen mussten.

- Approven Sie den Mergerequest
- Mergen Sie den Mergerequest
- Prüfen Sie, dass das Ticket ebenfalls geschlossen wurde

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Mergerequest approven:

    - Wenn alle ihre Anmerkungen umgesetzt oder beantwortet wurden, können Sie im
    Mergerequest in der Übersicht auf "Approve" klicken.
    - Nun kann der Mergerequest abgeschlossen werden. Klicken Sie auf "Merge"
    - Gehen Sie zum Schluss noch einmal auf das Board. Hier sehen Sie, dass ihr
     Ticket ebenfalls geschlossen wurde

Glückwunsch! Sie haben Gitlab erfolgreich verwendet um mit anderen zusammen zu arbeiten
