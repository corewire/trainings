# Vorlage für die Datei:

```
# Sommerfest 2019

## Übersicht

- Wann? 20.07.2019
- Wo? Im Innenhof

## Ansprechpartner

| Was?     | Wer? | Kontakt |
|----------|------|---------|
| Orga     |      |         |
| Catering |      |         |
| Musik    |      |         |

## Orga

- [x] Datum/Ort festlegen
- [x] Helfer finden
- [ ] Einladung verschicken
- [ ] ...

## Catering

TOOD: Kontaktdaten

### Essens und Getränkeauswahl

Getränke:

1. TODO
1. TODO

Essen:

1. TODO


## Musik

TODO: Kontaktdaten

```