# Der Lebenszyklus eines Tickets

!!! goal "Ziel"
    - In diesem Hands-on sollen Sie sich mit Gitlab und dem Arbeiten mit Tickets vertraut machen.
    - Sie werden ein Ticket erstellen, bearbeiten und schließen.

!!! tipp "Hilfsmittel"
    - Nehmen Sie sich Zeit und schauen Sie sich alles in Ruhe an.
    - Die Aufgabenbeschreibungen sind bewusst kurz gehalten. Versuchen Sie zuerst die Aufgaben ohne Hilfsmittel zu lösen
    - Sollten Sie Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem eine Schritt-für-Schritt-Anleitung beschrieben wird.

## Aufgabe 1 - Den User einrichten

- Loggen Sie sich mit ihrem Nutzer ein
    - URL: [https://gitlab.com/users/sign_in](https://gitlab.com/users/sign_in)
    - E-Mail: user_**XX**@corewire.de, XX muss durch die ihnen zugewiesene Zahl ersetzt werden
    - Password: Wird verteilt
- Ändern Sie ihren Anzeigenamen unter
    - "Edit profile" (im Dropdown Menü rechts oben)
    - "Main settings" -> Feld: "Full name"

## Aufgabe 2 - Ein Ticket erstellen

- Gehen Sie auf die "Ticket erstellen"-Maske
- Befüllen Sie das Ticket mit relevanten Informationen. Hierzu zählen der Title
  und die Beschreibung. Da nur Sie mit diesem Ticket arbeiten werden, ist der Inhalt beliebig.
- Weisen Sie sich das Ticket selbst zu
- Hängen Sie das Ticket an das Epic "Ticket Lebenszyklus"
- Markieren Sie das Ticket als Userstory
- Erstellen Sie das Ticket

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    "Ticket erstellen"-Maske:

    - Die "Ticket erstellen"-Maske erreichen Sie über das Plus-Symbol in der Kopfzeile oder in der Ticketübersicht (Navigation unter "Issues") mit dem Knopf "New Issue" rechts oben.

    Ticket befüllen:

    - Geben Sie in der "Ticket erstellen"-Maske einen Title und eine Beschreibung ein. Der Inhalt ist beliebig.

    Ticket zuweisen:

    - Tickets können einer oder mehreren Personen zugewiesen werden. Über die Auswahlliste unter "assignees" können Sie beliebige Kolleginnen und Kollegen zuweisen. Für den Augenblick genügt allerdings der Shortcut-Link neben der Auswahlliste "Assign to me". Weisen Sie sich damit das Ticket zu.

    Ticket an ein Epic hängen:

    - Über die Auswahlliste "Select epic" können Sie das Ticket an ein Epic hängen. Wählen Sie das Epic "Ticket Lebenszyklus" aus.

    Ticket als Userstory markieren:

    - Ein Ticket kann über Labels markiert/gruppiert werden. Klicken Sie auf die Auswahlliste unter "labels" und wählen Sie das Label "Userstory" aus.

    Ticket erstellen:

    - Erstellen Sie das Ticket durch einen Klick auf "Create Issue"

## Aufgabe 3 - Einen Mergerequest erstellen und bearbeiten

Sie haben das Ticket nun erstellt und sollten automatisch auf die Übersichtsseite zu diesem Ticket gelangt sein.
Am rechten Rand finden Sie alle wichtigen Einstellungen zum Ticket. In der Mitte wird der Diskussionsverlauf angezeigt.


Bearbeiten Sie nun das Ticket:

- Erstellen Sie einen Mergerequest

Der Mergerequest übernimmt die Labels vom Ticket und wird mit dem Ticket verlinkt.

- Öffnen Sie die Web IDE
- Erstellen Sie eine neue Datei mit einem zufälligen Namen
- Kopieren Sie [diesen Inhalt](./_file-template.md) in die Datei
- Committen Sie ihre Änderungen
- Betrachten Sie ihre Änderungen im Mergerequest
- Markieren Sie den Mergerequest als "Bereit" und tragen Sie Janosch Deurer als Reviewer ein

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"
    Einen Mergerequest erstellen:

    - Klicken Sie in der Ticketübersicht auf "Create mergerequest". Dadurch wird für Sie
    ein Mergerequest erstellt. Dieser wird mit dem Ticket verlinkt und Sie werden automatisch
    weitergeleitet.

    Die Web IDE öffnen:

    - Gitlab bietet eine Möglichkeit Dateien direkt im Browser zu bearbeiten - die Web IDE.
    Sie können Sie auf der Übersichtsseite zum Mergerequest mittig auf dem Bildschirm über den
    Knopf "Open in Web IDE" öffnen.

    Eine Datei erstellen:

    - In der Web IDE am linken Rand gibt es drei Reiter: "Edit", "Review" und "Commit". Stellen Sie sicher,
    dass sie sich auf "Edit" befinden.
    - Ebenfalls am linken Rand befindet sich das Symbol eine Datei anzulegen. Klicken Sie darauf.
    - In dem Dialog können Sie nun den Namen der Datei angeben. Wählen Sie einen zufälligen Namen und erstellen Sie die Datei.
    - Klicken Sie auf [diesen Link](./_file-template.md). Kopieren Sie die Vorlage über das Symbol oben rechts im hellen Bereich.
    - Fügen Sie den Inhalt in ihre erstellte Datei ein.

    Änderungen committen:

    - Committen Sie ihre Änderungen über den blauen Knopf mit der Aufschrift "Commit..." links unten
    - Sie sind nun auf den Reiter "Commit" gewechselt. Hier werden ihn nochmal alle Änderungen angezeigt.
    - Geben Sie links unten unter "Commit Message" eine Nachricht ein, die ihre Änderungen beschreibt.
    - Klicken Sie anschließend den grünen Knopf "Commit"

    Änderungen im Mergerequest ansehen:

    - Schließen Sie die Web IDE. Das können Sie entweder über einen Klick oben Links auf das aktuelle Projekt oder
    unten in der Fußzeile. Hier wird ihnen angezeigt, mit welchem Mergerequest sie arbeiten. Mit einem Klick auf
    "Merge request !{nummer}" gelangen Sie direkt zum Mergerequest.
    - Im Mergerequest sehen Sie unterhalb der Überschrift die Reitre "Overview", "Commits", "Changes".
    - Unter "Changes" können Sie alle Änderungen innerhalb des Mergerequestes sehen. Hier sollte Ihnen
    ihre Datei angezeigt werden.

    Mergerequest als "Bereit" markieren:

    - Ihr Mergerequest enthält aktuell noch "Draft" in der Überschrift. Damit wird
    signalisiert, dass der Mergerequest noch in Bearbeitung ist. Sie können entweder
    händisch die Überschrift bearbeiten oder den Knopf "Mark as ready" oben rechts klicken.

    Reviewer eintragen:

    - Tragen Sie nun noch Janosch Deurer als Reviewer für den Mergerequest ein. Dies können Sie über
    das Menü an der rechten Seite machen.

## Aufgabe 4 - Einen Mergerequest abschließen

Sie müssen nun warten, bis ihr Mergerequest genehmigt ("approved") wird. Das wird typischerweise
von einem der eingetragenen Reviewern gemacht, sobald er/sie mit den Änderungen
zufrieden ist.

Sobald ihr Mergerequest genehmigt wurde, können Sie fortfahren.

- Mergen Sie den Mergerequest
- Betrachten Sie das Ticket. Es sollte ebenfalls automatisch geschlossen worden sein

Sie haben nun erfolgreich einen vereinfachten Lebenszyklus eines Ticket durchgespielt.

??? help "Lösung (Klicken Sie auf den Pfeil, falls sie nicht weiterkommen)"

    Mergen:

    - Wenn bis hierher alles stimmt, dann ist im Mergerequest mittig im Bildschirm
    nun ein grüner Knopf mit der Aufschrift "Merge". Klicken Sie diesen.

    Ticket betrachten:

    - Gehen Sie wieder auf ihr anfänglich erstelltes Ticket. Im Änderungsverlauf
    des Tickets können Sie nun sehen, wie sie einen Mergerequest erstellt haben und
    mittels diesem auch das Ticket geschlossen haben.

Geschafft! Sie haben das Hands-on erfolgreich beendet.
