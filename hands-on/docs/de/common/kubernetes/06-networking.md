# Netzwerk

!!! goal "Ziel"
    In diesem Projekt werden Sie lernen, wie Sie verschiedene Netzwerkfunktionen
    in Kubernetes verwenden können. Dazu gehören Port-Forwarding, NodePorts und
    Ingress.

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe des [Cheatsheets](../../cheatsheets/06-networking/) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1: Port-Forwarding

### Aufgabe 1.1: Webserver Deployment und Service erstellen

- Erstellen Sie ein Deployment `web` in der Datei `port-forwarding.yaml`, das
das Image `nginx` verwendet und einen entsprechenden Service `web`. Vergessen
Sie nicht die Label-Selektoren, um die Pods dem Service zuzuordnen

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: web
    spec:
      replicas: 2
      selector:
        matchLabels:
          app: web
      template:
        metadata:
          labels:
            app: web
        spec:
          containers:
          - name: nginx
            image: nginx
            ports:
            - name: web
              containerPort: 80
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: web
    spec:
      selector:
        app: web
      ports:
      - protocol: TCP
        port: 80
        targetPort: web
    ```

- Deployen Sie das Deployment und den Service:

    ```
    kubectl apply -f port-forwarding.yaml
    ```

### Aufgabe 1.2: Port-Forward erstellen und testen

- Erstellen Sie ein Port-Forward von Port 8082 auf Ihrem Rechner auf den
Service-Port 80. Der Port-Forward-Prozess wird im Vordergrund Ihres Terminals
ausgeführt, bis Sie ihn mit `[STRG] + [C]` beenden.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```
    kubectl port-forward service/web 8082:80
    ```

- Öffnen Sie ein neues Terminal und verwenden Sie `curl`, um `http://127.0.0.1:8082` zu öffnen
- Öffnen Sie einen Browser-Tab unter `<VSCode-URL>/proxy/8081/wp-admin/install.php`, um sich mit dem nginx-Webserver zu verbinden
- Stoppen Sie den Port-Forward-Befehl mit `[STRG] + [C]`

## Aufgabe 2: NodePorts

- Kopieren Sie das Deployment und den Service aus der vorherigen Aufgabe in die Datei `node-ports.yaml`
- Ändern Sie den Service so, dass er einen `NodePort` öffnet der auf den Pod-Port 80 zeigt

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: web
    spec:
      type: NodePort
      selector:
        app: web
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
    ```

- Deployen Sie das Deployment und den Service
- Schauen Sie sich den Service an, um den zugewiesenen NodePort zu ermitteln

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```shell
    kubectl describe svc web
    ```

- Lassen Sie sich die IP-Adresse der Worker-Nodes anzeigen

```shell
kubectl get nodes -o wide
```

- Kopieren Sie sich eine der externen IP-Adressen. **Wichtig:** Verwenden Sie eine IP-Adresse von einem Worker, nicht von der Control-Plane. Welche Sie verwenden, ist egal.
- Öffnen Sie einen Browser-Tab unter `<WorkerIP>:<NodePort>`, um sich mit dem nginx-Webserver zu verbinden

## Aufgabe 3: Ingress

- Kopieren Sie das Deployment und den Service aus der ersten Aufgabe in die Datei `ingress.yaml`
- Erstellen Sie eine Ingress-Ressource, um den Traffic auf den Service zu routen
- Die Regeln für den Ingress sind:
    - host: `nginx.cluster.<VSCode-URL>`
    - path: /
    - pathType: Exact
    - backend: Service `web` auf Port 80

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```yaml
    apiVersion: networking.k8s.io/v1
    kind: Ingress
    metadata:
      name: mynginx
    spec:
      rules:
        - host: nginx.cluster.<VSCode-URL>
          http:
            paths:
              - path: /
                pathType: Exact
                backend:
                  service:
                    name:  web
                    port:
                      number: 80
    ```

- Deployen Sie das Deployment, den Service und den Ingress

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"

    ```shell
    kubectl apply -f ingress.yaml
    ```

- Öffnen Sie die URL `http://nginx.cluster.<VSCode-URL>` in Ihrem Browser und überprüfen Sie, ob Sie auf die Webanwendung zugreifen können
