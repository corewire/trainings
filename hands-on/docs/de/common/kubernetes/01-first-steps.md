# Erste Schritte

!!! goal "Ziel"
    In diesem Projekt geht es um die grundlegende Funktionalität von Kubernetes. Sie werden:

    - Pods starten und stoppen
    - Pods labeln
    - Pods mit einem Service erreichbar machen

!!! tipp "Hilfsmittel"

    - Versuchen Sie, die unten stehenden Aufgaben mit Hilfe der [Folien](../../02-first-steps/#/)
    und des [Cheatsheets](../../cheatsheets/01-first-steps/) eigenständig zu lösen.
    - Sollten Sie dabei Probleme haben, finden Sie bei jeder Aufgabe einen ausklappbaren Block, in dem der Lösungsweg beschrieben wird.

## Aufgabe 1: Bei VSCode anmelden

- Die Schulungsmaschinen sind unter `code-{ZAHL}.{{ domain }}` zu erreichen.
Ersetzen Sie `{ZAHL}` mit der Ihnen zugewiesenen Zahl.
  - Beispiel für die Zahl 5: `code-5.{{ domain }}`.
  - Beispiel für die Zahl 11: `code-11.{{ domain }}`.
- Geben Sie danach das Ihnen zugewiesene Passwort ein.
- Sie haben nun Zugriff auf die Schulungsmaschine.

-  Überprüfen Sie, dass Sie Zugriff auf ein Cluster haben. Der Output sollte in etwa folgendermaßen aussehen:

```shell
kubectl version
```

```shell
Client Version: v1.29.3
Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
Server Version: v1.28.8
```

## Aufgabe 2: Pods starten

- Öffnen Sie einen interaktiven Pod

```shell
kubectl run -it --image=alpine --rm my-first-pod
```

```shell
# An interactive shell starts, which you could investigate:
$ cat /etc/alpine-release
# 3.19.1
$ exit
# Session ended, resume using 'kubectl attach my-first-pod -c my-first-pod -i -t' command when the pod is running
# pod "my-first-pod" deleted
```

Folgende Dinge sind geschehen:

1. Das Alpine-Image wurde aus der Container-Registrierung heruntergeladen
1. Ein Pod wurde mit `kubectl run` erstellt und der Name `my-first-pod` wurde dem Pod zugewiesen
1. Ein Container wurde in dem Pod mit dem Alpine-Image gestartet (`--image=alpine`)
1. Eine Shell wurde gestartet (weil dies das Standardverhalten von Alpine-Image-Containern ist)
1. Eine Terminal-Sitzung wird für den Container zugewiesen (`-it`)
1. Das Beenden des Terminals beendet den Shell-Prozess und den Container
1. Der Pod wird sofort entfernt, sobald der Containerprozess beendet ist (`--rm`)

Beachten Sie, dass `kubectl attach my-first-pod -c my-first-pod -i -t` hier nicht funktioniert, da der Pod sofort entfernt wurde.

## Aufgabe 3: YAML File

Ein Pod (und andere Kubernetes-Ressourcen) können in YAML-Dateien beschrieben werden. Der allgemeine Aufbau einer solchen YAML-Datei ist wie folgt:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod-name
spec:
  containers:
  - name: my-container-name
    image: my-container-image
```

- Erstellen Sie eine YAML-Datei `my-simple-pod.yaml`, um Ihr Pod-Manifest zu beschreiben. Der Pod sollte ein nginx-Container-Image verwenden.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
    ```

- Starten Sie den Pod

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl apply -f my-simple-pod.yaml`

- Lassen Sie sich alle Pods ausgeben

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl get pods`

- Schauen Sie sich die Logs des Pods an

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl logs nginx`


## Aufgabe 4: Label

- Geben Sie Ihrem Pod das Label `app=webserver` und `version=1`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Label hinzufügen:
    ```yaml
    ...
    metadata:
      name: nginx
      labels:
        app: webserver
        version: "1"
    ...
    ```
    Dann `kubectl apply -f pod.yaml`

- Filtern Sie in allen Pods nach dem Label app=webserver

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl get pod -l app=webserver
    # Oder
    kubectl get pod -l 'app in (webserver)'
    # Oder
    kubectl get pods --selector app=webserver
    ```

- Überschreiben Sie das Label `version`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl label pod <name> version=2 --overwrite`

- Lassen Sie sich alle Label ausgeben

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl label pod <name> --list`

- Fügen Sie Annotationen hinzu

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    ...
    metadata:
      name: nginx
      labels:
        app: webserver
        version: 1
      annotations:
        name: My first webserver
    ...
    ```

- Lassen Sie sich die Annotationen ausgeben

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl describe pod <name>`


<!--
This can be added again if more content is needed
## Aufgabe 5: Gemischte Übung

- Definieren Sie einen weiteren Pod, der einen Apache Webserver (Image=httpd) startet

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: httpd
    spec:
      containers:
      - name: httpd
        image: httpd
    ```

- Fügen Sie Label und Annotationen nach Belieben hinzu, mindestens aber das Label `app=webserver`

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    ...
    metadata:
      name: apache
      labels:
        app: webserver
        version: 1
      annotations:
        name: My second Webserver
        description: apache webserver
    ...
    ```

- Starten Sie Ihren Apache-Pod

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl apply -f <name>.yaml`

- Lassen Sie sich alle Pods ausgeben

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl get pods`

- Löschen Sie den Apache-Pod wieder

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl delete pod <name>`
-->

## Aufgabe 5: Service

- Definieren Sie eine `service.yaml`-Datei so, dass der Service Pods mit dem Label `app=webserver` auf Port 80 erreichbar macht. Starten Sie den Service.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: web
      labels:
        exercise: services
    spec:
      selector:
        app: webserver
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
    ```

    Dann `kubectl apply -f service.yaml`

- Inspizieren Sie den Service. Achten Sie insbesondere auf die IP-Adressen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    `kubectl describe service web`

-  Löschen Sie den nginx Pod und starten Sie ihn neu. Betrachten Sie wieder die IP-Adressen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl delete pod nginx
    kubectl apply -f nginx.yaml
    kubectl describe service web
    ```
    Die IP-Adresse hat sich geändert.

- Verbinden Sie sich mit `curl` zu dem Service:

```shell
kubectl run -it --image=cmd.cat/bash/curl --rm -- bash
```

```shell
curl <service-name>
```


## Aufgabe 6: Load balancing testen

Wenn mehrere Pods vom Dienst ausgewählt werden, werden die Anfragen auf die Pods verteilt.

-  Stellen Sie einen zweiten Pod mit Apache bereit, damit er ebenfalls vom Service ausgewählt wird.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    apiVersion: v1
    kind: Pod
    metadata:
      name: apache
      labels:
        app: webserver
    spec:
      containers:
      - name: apache
        image: httpd
    ```

    ```
    kubectl apply -f <name>.yaml
    ```

-  Überprüfen Sie die IP-Adressen der Pods:

```
kubectl get pods -o=wide
```

-  Überprüfen Sie die IP-Adressen im Service:

```
kubectl describe service web
```

Beide Pods wurden zugewiesen, wie durch "Endpunkte" angezeigt.

-  Testen Sie die Verbindung zum Dienst und überprüfen Sie den Servernamen in der Antwort. Wiederholen Sie den Befehl `curl` mehrmals, um zu sehen, ob der eine oder andere Pod antwortet.

```
kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
curl web
```

## Cleanup

Bereinigen Sie Ihren Cluster, indem Sie alle Pods und den Service löschen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl delete pod <pod-name>
    ```

    - Um alle laufenden Pods zu erhalten nutzen Sie `kubectl get pods`
    - Um den Service zu löschen nutzen Sie:

    ```
    kubectl delete service <service-name>
    ```