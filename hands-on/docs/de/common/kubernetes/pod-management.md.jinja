
{% macro create_identical_pods(exercise_number) %}
## Aufgabe {{ exercise_number }}: Identische Pods erstellen

- Erstellen Sie ein ReplicaSet namens `webserver` in `replicaset.yaml` unter Verwendung des `nginx`-Images
- Rollen Sie das ReplicaSet mit `kubectl apply` aus

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Das YAML-File `replicaset.yaml` sollte so oder so ähnlich aussehen:
    ```yaml
    ---
    apiVersion: apps/v1
    kind: ReplicaSet
    metadata:
      name: webserver
    spec:
      replicas: 2
      selector:
        matchLabels:
          app: web
      template:
        metadata:
          labels:
            app: web
        spec:
          containers:
          - name: nginx
            image: nginx
    ```

    Anschließend wenden Sie das ReplicaSet an:

    ```
    kubectl apply -f replicaset.yaml
    ```

!!! tipp "ReplicaSets sind ein Wrapper für Pod-Beschreibungen"
    Beachten Sie, dass der Abschnitt `template` identisch mit der Pod-Beschreibung aus der vorherigen Übung ist,
    nur ohne die Felder `apiVersion` und `kind`.

- Verwenden Sie `kubectl`, wie Sie es zuvor gelernt haben, um die vom ReplicaSet erstellten Ressourcen, d.h. das ReplicaSet selbst und die Pods, anzuzeigen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - ReplicaSets anzeigen
    ```
    $ kubectl get replicasets
    NAME         DESIRED   CURRENT   READY   AGE
    webserver    2         2         2       8s
    ```

    - Pods anzeigen
    ```
    $ kubectl get pods
    webserver-f82ng   1/1     Running   0          41s
    webserver-lsc9f   1/1     Running   0          41s
    ```
    Es laufen zwei Pods, deren Namen sich aus dem Namen des ReplicaSet mit einem zufälligen Suffix ergeben.

    - ReplicaSet untersuchen
    ```
    $ kubectl describe replicaset webserver
    Name:         webserver
    Namespace:    testnamespace
    Selector:     app=web
    Labels:       exercise=scale
    Annotations:  <none>
    Replicas:     2 current / 2 desired
    Pods Status:  2 Running / 0 Waiting / 0 Succeeded / 0 Failed
    Pod Template:
      Labels:  app=web
      Containers:
      nginx:
        Image:        nginx
        Port:         <none>
        Host Port:    <none>
        Environment:  <none>
        Mounts:       <none>
      Volumes:        <none>
    Events:
      Type    Reason            Age   From                   Message
      ----    ------            ----  ----                   -------
      Normal  SuccessfulCreate  64s   replicaset-controller  Created pod: webserver-f82ng
      Normal  SuccessfulCreate  64s   replicaset-controller  Created pod: webserver-lsc9f
    ```

!!! tipp "Alle Ressourcen auf einmal anzeigen"
    Der folgende Befehl kann verwendet werden, um Pods und ReplicaSets auf einmal anzuzeigen:
    ```
    $ kubectl get all

    NAME                  READY   STATUS    RESTARTS   AGE
    pod/webserver-f82ng   1/1     Running   0          88s
    pod/webserver-lsc9f   1/1     Running   0          88s

    NAME                        DESIRED   CURRENT   READY   AGE
    replicaset.apps/webserver   2         2         2       88s
    ```

!!! tipp "Naming conventions"
    Beachten Sie, dass die Pods mit dem Namen des ReplicaSets und einem zufälligen Suffix benannt werden.
{% endmacro %}

{% macro scale_replicas(exercise_number) %}
## Aufgabe {{ exercise_number }}: Skalieren

-  Ändern Sie die Anzahl der Replikas in `replicaset.yaml` auf 3 und deployen Sie es.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```yaml
    ...
    spec:
        replicas: 3
    ....
    ```
    `kubectl apply -f replicaset.yaml`

-  Die Anzahl der Pods kann auch mit `kubectl scale` geändert werden:
    ```
    kubectl scale replicaset webserver --replicas=4
    ```
    Dadurch wird ein weiterer Pod hinzugefügt.

!!! tipp "Änderungen in Echtzeit beobachten"
    Führen Sie `watch kubectl get all` in einer separaten Konsole aus, um die Änderungen in Echtzeit zu beobachten.
{% endmacro %}

{% macro recover_lost_pods(exercise_number) %}
## Aufgabe {{ exercise_number }}: Recovery verlorener Pods

ReplicaSets behalten die gewünschte Anzahl von Pods bei, auch wenn Pods entfernt werden oder ausfallen.

-  Löschen Sie Pods mit `kubectl delete` und beobachten Sie die Wiederherstellung mit `watch kubectl get all`.

!!! tipp "Verwenden Sie Labels, um einen Pod auszuwählen und zu entfernen"
    Der folgende Befehl kann verwendet werden, um die ersten beiden Pods mit Hilfe eines Labels zu entfernen:
    ```
    kubectl get pod --selector app=web -o name | head -n 2 | xargs kubectl delete
    ```
{% endmacro %}

{% macro use_replicasets_with_services(exercise_number) %}
## Aufgabe {{ exercise_number }}: ReplicaSets mit Services nutzen

ReplicaSets sind ausschließlich für die Aufrechterhaltung der Skalierung verantwortlich, indem sie identische Pods auf Grundlage des Templates erstellen.

Loadbalancing und DNS werden über Services definiert, die Pods auf Basis ihrer Labels auswählen.

-  Definieren und starten Sie einen Service namens "web" mit dem Wissen aus dem vorherigen Kapitel. Wählen Sie die vom ReplicaSet erstellten Pods mit demselben Label aus, das im ReplicaSet verwendet wird.

!!! tipp "Hinweis"
    Nutzen Sie das Label `app=web` als Selektor im Service.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Die Beschreibung des Dienstes sollte in etwa so aussehen:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: web
    spec:
      selector:
        app: web
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
    ```

-  Überprüfen Sie, dass der Service korrekt funktioniert.

```shell
kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
curl web
```

- Beobachten Sie die Pods mit `watch kubectl get all`.
- Löschen Sie das ReplicaSet in einem zweiten Terminal und beobachten die Pods im ersten Terminal.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl delete replicaset webserver
    ```

Das ReplicaSet wird automatisch alle zugehörigen Pods entfernen.
{% endmacro %}

{% macro create_deployment(exercise_number, training="kubernetes") %}
## Aufgabe {{ exercise_number }}: Deployment erstellen

- Erstellen Sie ein Deployment mit dem Namen `web` in der Datei `deployment.yaml`, das zwei Pods mit dem Image nginx erstellt.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Die YAML-Datei `deployment.yaml` sollte so oder so ähnlich aussehen:
    ```yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: web
      labels:
        exercise: deployments
    spec:
      replicas: 2
      selector:
        matchLabels:
          app: web
      template:
        metadata:
          labels:
            app: web
        spec:
          containers:
          - name: nginx
            image: nginx
    ```
    Setzen Sie das Deployment um:
    ```
    kubectl apply -f deployment.yaml
    ```

{% if training == "kubernetes" %}
-  Vergleichen Sie `deployment.yaml` und `replicaset.yaml` aus der vorherigen Übung.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```diff
    $ diff -u deployment.yaml replicaset.yaml

    --- deployment.yaml     2024-02-21 09:03:08.903501225 +0100
    +++ replicaset.yaml     2024-02-21 09:23:36.668181993 +0100
    @@ -1,7 +1,7 @@
     apiVersion: apps/v1
    -kind: Deployment
    +kind: ReplicaSet
     metadata:
    -  name: web
    +  name: webserver
     spec:
       replicas: 2
       selector:
    ```
{% endif %}

-  Untersuchen Sie die erstellten Ressourcen mit `kubectl get all` sowie `kubectl describe` für die verschiedenen Ressourcentypen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Verwenden Sie den folgenden Befehl, um Deployments, ReplicaSets und Pods auf einmal anzuzeigen:
    ```
    $ kubectl get all

    NAME                      READY   STATUS    RESTARTS   AGE
    pod/web-76fd95c67-7cc4x   1/1     Running   0          4s
    pod/web-76fd95c67-vvh8v   1/1     Running   0          4s

    NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/web   2/2     2            2           4s

    NAME                            DESIRED   CURRENT   READY   AGE
    replicaset.apps/web-76fd95c67   2         2         2       4s
    ```
    - Untersuchen Sie das Deployment
    ```
    $ kubectl describe deployment web

    Name:                   web
    Namespace:              testnamespace
    CreationTimestamp:      Wed, 21 Feb 2024 09:32:23 +0100
    Labels:                 exercise=deployments
    Annotations:            deployment.kubernetes.io/revision: 1
    Selector:               app=web
    Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
    StrategyType:           RollingUpdate
    MinReadySeconds:        0
    RollingUpdateStrategy:  25% max unavailable, 25% max surge
    Pod Template:
      Labels:  app=web
      Containers:
      nginx:
        Image:        nginx
        Port:         <none>
        Host Port:    <none>
        Environment:  <none>
        Mounts:       <none>
      Volumes:        <none>
    Conditions:
      Type           Status  Reason
      ----           ------  ------
      Available      True    MinimumReplicasAvailable
      Progressing    True    NewReplicaSetAvailable
    OldReplicaSets:  <none>
    NewReplicaSet:   web-76fd95c67 (2/2 replicas created)
    Events:
      Type    Reason             Age   From                   Message
      ----    ------             ----  ----                   -------
      Normal  ScalingReplicaSet  3m4s  deployment-controller  Scaled up replica set web-76fd95c67 to 2
    ```
    Sie können hier sehen, dass...
    - das Deployment ein neues ReplicaSet `web-76fd95c67` erstellt hat, das 2 Pods hochskaliert
    - die Standardstrategie für die Aktualisierung von Pods `RollingUpdate` ist, was bedeutet, dass alte Pods eliminiert werden, sobald neue erstellt werden. Sie werden später mehr über Bereitstellungsstrategien erfahren.

!!! tipp "Naming conventions"
    Beachten Sie, wie ReplicaSets mit dem Namen des Deployments und einem zufälligen Suffix benannt werden.
    Beachten Sie, wie Pods mit dem Namen des ReplicaSets und einem weiteren zufälligen Suffix benannt werden.
{% endmacro %}

{% macro update_deployment(exercise_number) %}
## Aufgabe {{ exercise_number }}: Deployment updaten

-  Aktualisieren Sie unsere Anwendung, indem Sie das für das Deployment verwendete Image ändern. Fügen Sie dazu einen Tag wie `stable` oder `mainline` zum Image-Namen hinzu.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Bearbeiten Sie die Datei `deployment.yaml` und ersetzen Sie die Zeile `image: nginx` durch `image: nginx:stable`.

-  Öffnen Sie ein anderes Terminal mit "Split Terminal". Verwenden Sie `watch kubectl get all`, um die Änderungen in einem separaten Terminal zu beobachten. Wenden Sie anschließend die Änderung an.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Öffnen Sie ein weiteres Terminal und führen Sie `watch kubectl get all` aus.
    - Wenden Sie das Deployment an:
    ```
    kubectl apply -f deployment.yaml
    ```
    - Beobachten Sie, wie die Änderungen auf Deployments, ReplicaSets und Pods angewendet werden:
    ```
    $ watch kubectl get all

    NAME                       READY   STATUS              RESTARTS   AGE
    pod/web-5c7bdc66d4-fk4fx   1/1     Running             0          5s
    pod/web-5c7bdc66d4-lf4nw   0/1     ContainerCreating   0          2s
    pod/web-76fd95c67-vvh8v    1/1     Running             0          4h54m

    NAME          TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
    service/web   ClusterIP   10.42.71.163   <none>        80/TCP    118s

    NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
    deployment.apps/web   2/2     2            2           4h54m

    NAME                             DESIRED   CURRENT   READY   AGE
    replicaset.apps/web-5c7bdc66d4   2         2         2       5s
    replicaset.apps/web-76fd95c67    0         1         1       4h54m
    ```
    Sie können hier sehen, dass...
    - das existierende ReplicaSet `web-76fd95c67` auf die gewünschte Anzahl von 0 Pods herunter skaliert,
    - das bestehende ReplicaSet einen weiteren Pod `web-76fd95c67-vvh8v` hat, der noch läuft
    - ein neues ReplicaSet `web-5c7bdc66d4` ist fertig zum Hochskalieren
    - das neue ReplicaSet hat die gewünschte Anzahl von 2 Pods erstellt, `web-5c7bdc66d4-fk4fx` und `web-5c7bdc66d4-lf4nw`

-  Untersuchen Sie Ihr Deployment.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    $ kubectl describe deployment web

    Name:                   web
    Namespace:              testnamespace
    CreationTimestamp:      Wed, 21 Feb 2024 09:32:23 +0100
    Labels:                 exercise=deployments
    Annotations:            deployment.kubernetes.io/revision: 2
    Selector:               app=web
    Replicas:               2 desired | 2 updated | 2 total | 2 available | 0 unavailable
    StrategyType:           RollingUpdate
    MinReadySeconds:        0
    RollingUpdateStrategy:  25% max unavailable, 25% max surge
    Pod Template:
      Labels:  app=web
      Containers:
      nginx:
        Image:        nginx:stable
        Port:         <none>
        Host Port:    <none>
        Environment:  <none>
        Mounts:       <none>
      Volumes:        <none>
    Conditions:
      Type           Status  Reason
      ----           ------  ------
      Available      True    MinimumReplicasAvailable
      Progressing    True    NewReplicaSetAvailable
    OldReplicaSets:  web-76fd95c67 (0/0 replicas created)
    NewReplicaSet:   web-5c7bdc66d4 (2/2 replicas created)
    Events:
      Type    Reason             Age    From                   Message
      ----    ------             ----   ----                   -------
      Normal  ScalingReplicaSet  6m31s  deployment-controller  Scaled up replica set web-5c7bdc66d4 to 1
      Normal  ScalingReplicaSet  6m28s  deployment-controller  Scaled down replica set web-76fd95c67 to 1 from 2
      Normal  ScalingReplicaSet  6m28s  deployment-controller  Scaled up replica set web-5c7bdc66d4 to 2 from 1
      Normal  ScalingReplicaSet  6m26s  deployment-controller  Scaled down replica set web-76fd95c67 to 0 from 1
    ```

    Sie können hier sehen, dass...

    - die Revisionsnummer geändert wurde, wie in der Anmerkung `deployment.kubernetes.io/revision: 2` angegeben
    - das Image in der Pod-Vorlage auf `nginx:latest` geändert wurde
    - das OldReplicaSet `web-76fd95c6` wurde von 2 auf 1 Pod und von 1 auf 0 Pods verkleinert
    - das NewReplicaSet `web-5c7bdc66d4` wurde von 0 auf 1 Pod und von 1 auf 2 Pods vergrößert
{% endmacro %}

{% macro use_service_and_test_connectivity(exercise_number) %}
## Aufgabe {{ exercise_number }}: Service nutzen und Konnektivität testen

- Nutzen Sie den Service aus der vorherigen Aufgabe, um die Konnektivität zu den Pods zu testen
- Nutzen Sie folgenden Befehl, um sich mit dem Service zu verbinden:
```
kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
curl web
```
{% endmacro %}

{% macro use_deployments_with_services(exercise_number) %}
## Aufgabe {{ exercise_number }}: Deployments mit Services nutzen

Deployments sind für ausschließlich die Verwaltung der Pods mittels ReplicaSets verantwortlich.

Loadbalancing und DNS werden über Services definiert, die Pods auf Basis ihrer Labels auswählen.

-  Definieren und starten Sie einen Service namens "web" mit dem Wissen aus dem
vorherigen Kapitel. Wählen Sie die vom Deployment erstellten Pods mit demselben
Label aus, das im Deployment verwendet wird.

!!! tipp "Hinweis"
    Nutzen Sie das Label `app=web` als Selektor im Service.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    Die Beschreibung des Dienstes sollte in etwa so aussehen:
    ```yaml
    apiVersion: v1
    kind: Service
    metadata:
      name: web
    spec:
      selector:
        app: web
      ports:
      - protocol: TCP
        port: 80
        targetPort: 80
    ```

-  Überprüfen Sie, dass der Service korrekt funktioniert.

```shell
kubectl run -it --image=cmd.cat/bash/curl --rm --command -- bash
curl web
```

- Beobachten Sie die Pods mit `watch kubectl get all`.

{% endmacro %}

{% macro history_and_rollback(exercise_number) %}
## Aufgabe {{ exercise_number }}: History und Rollback

-  Nutzen Sie die interaktive Hilfe von `kubectl rollout`, um zu verstehen, wie Sie die History eines Deployments anzeigen kann.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl rollout --help
    kubectl rollout history --help
    ```

- Lassen Sie sich den Verlauf des Deployments anzeigen

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    $ kubectl rollout history deployment web
    deployment.apps/web
    REVISION  CHANGE-CAUSE
    1         <none>
    2         <none>
    ```
    Es gibt zwei Revisionen, die eine wurde in Aufgabe 5 erstellt, die andere in Aufgabe 6.

-  Wenn die Rollout-History nur eine einzige Revision enthält, wiederholen Sie bitte Aufgabe 6 zum Update des Deployments, um eine neue Revision zu erstellen.
-  Nutzen Sie die interaktive Hilfe von `kubectl rollout`, um die letzte Änderung des Deployments zurückzusetzen. Führen Sie ein Rollback zur vorherigen Revision des Deployments durch.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    - Schauen Sie sich Revision 1 an, die `Image: nginx` verwendet
    ```
    $ kubectl rollout history deployment web --revision=1

    deployment.apps/web with revision #1
    Pod Template:
      Labels:       app=web
                    exercise=deployments
                    pod-template-hash=76fd95c67
      Containers:
      nginx:
        Image:      nginx
        Port:       <none>
        Host Port:  <none>
        Environment:<none>
        Mounts:     <none>
      Volumes:      <none>
    ```
    - Schauen Sie sich Revision 2 an, die `Image: nginx:stable` verwendet
    ```
    $ kubectl rollout history deployment web --revision=2

    deployment.apps/web with revision #2
    Pod Template:
      Labels:       app=web
                    exercise=deployments
                    pod-template-hash=5c7bdc66d4
      Containers:
      nginx:
        Image:      nginx:stable
        Port:       <none>
        Host Port:  <none>
        Environment:<none>
        Mounts:     <none>
      Volumes:      <none>
    ```
    - Kehren Sie mit dem folgenden Befehl zur vorherigen Version des Deployments zurück:
    ```
    kubectl rollout undo deployment web
    ```
    - Have a look at the list of revisions:
    ```
    $ kubectl rollout history deployment web

    deployment.apps/web
    REVISION  CHANGE-CAUSE
    2         <none>
    3         <none>
    ```
    - Schauen Sie sich die neue Revision 3 an, die wieder `Image: nginx` verwendet:
    ```
    $ kubectl rollout history deployment web --revision=3

    deployment.apps/web with revision #3
    Pod Template:
      Labels:       app=web
                    exercise=deployments
                    pod-template-hash=76fd95c67
      Containers:
      nginx:
        Image:      nginx
        Port:       <none>
        Host Port:  <none>
        Environment:<none>
        Mounts:     <none>
      Volumes:      <none>
    ```
{% endmacro %}

{% macro cleanup() %}
## Cleanup

- Löschen Sie alle Objekte, die Sie erstellt haben, um die Umgebung zu bereinigen.
- Beobachten Sie, wie sie mit `watch kubectl get all` verschwinden.

Das ReplicaSet und Deployment wird automatisch alle Pods entfernen, die dem labelSelector entsprechen.

??? help "Lösung (Klicken Sie auf den Pfeil, falls Sie nicht weiterkommen)"
    ```
    kubectl delete deployment web
    kubectl delete service web
    ```
{% endmacro %}
