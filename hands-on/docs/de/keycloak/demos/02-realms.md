# Realms

- Hintergrund:
- Fokus:

## Admin-Console, Realm Export

- Keycloak starten
```
docker run -p 8889:8080 -e KC_BOOTSTRAP_ADMIN_USERNAME=admin   -e KC_BOOTSTRAP_ADMIN_PASSWORD=admin   quay.io/keycloak/keycloak:latest start-dev
```
- Client `myclient` (nur ClientId, sonst nichts) und User `demo` (mit PW) anlegen.
- Realm-Settings-> Action -> Partial Export

## Admin-Console, Realm-Import

- Docker-Compose von Slides kopieren, starten
- Create Realm via Import
- Client ist da, User nicht

## CLI Realm Export

- User wieder erstellen
- `mdkir out`
- Docker-compose erweitern
```
    volumes:
    - ./out:/out
```
- `docker compose exec -it keycloak bash`
- `cd /opt/keycloak/bin`
- `./kc.sh export --dir /out --users realm_file`
- Export in Online json formatter anschauen:
    - User mit Passwort Hash enthalten

## SMTP

- Docker-compose erweitern
```
  mail:
    # Use to display mails (txt/html) in the browser
    # Port 1025 -> SMTP-Server
    # Port 8025 -> WebUI
    image: maildev/maildev:2.1.0
    ports: [ "8025:8025" ]
    environment:
      - MAILDEV_INCOMING_USER=smtp_user
      - MAILDEV_INCOMING_PASS=pass
      - MAILDEV_WEB_PORT=8025
```
- `localhost:8025` testen
- SMTP-Server einrichten
    - host: mail
    - port: 1025
    - user: smtp_user
    - pass: pass
- Send Testmail

## Enable Passwort Reset

- Enable Passwort reset
- Login with Demo-User
- Reset password.

## Weitere Einstellungen

- Themes: später
- Keys: Rotation etc auch später
- Events: später
- Localization:
    - Anschalten, 2 Sprachen auswählen
    - Demo-User in der Accountconsole nutzen
- Security: Später
- Sessions: Kann man Hier einstellen
- Tokens:
    - General: Device Code -> OIDC Flow
    - Access Token: OIDC Flow
    - Action Token: User Flows (Password recovery, email verification)
- Client Policy: Später
- Attributes:
    - Neue Custom Attributes
- User registration: default roles,groups