# Cluster

- Hintergrund:
- Fokus:

## Voraussetzung

- Docker compose von vorher (DB + Keycloak-Dev)
```
services:
  postgres:
    image: postgres
    environment:
      POSTGRES_USER: keycloak
      POSTGRES_PASSWORD: secret
      POSTGRES_DB: keycloak

  keycloak:
    image: quay.io/keycloak/keycloak:latest
    environment:
      KC_BOOTSTRAP_ADMIN_USERNAME: admin
      KC_BOOTSTRAP_ADMIN_PASSWORD: admin
      KC_DB: postgres
      KC_DB_URL_HOST: postgres
      KC_DB_USERNAME: keycloak
      KC_DB_PASSWORD: secret
    command: [
      "start-dev"
    ]
    ports: [ "8888:8080" ]
    depends_on: [ "postgres" ]
```

## Prepare cluster

- `    scale: 2` bei Keycloak hinzufügen
- `ports` entfernen
- Docker compose starten, `docker inspect` auf einen Keycloak-Container für DNS Alias
- Load-balancer hinzufügen
    ```
    lb:
        image: nginx:alpine
        volumes:
        - ./nginx.conf:/etc/nginx/conf.d/default.conf
        ports:
        - "8888:8000"
    ```
- `nginx.conf`
    ```
    upstream backend {
        ip_hash;
        server keycloak-1:8080 fail_timeout=15s;
        server keycloak-2:8080 fail_timeout=15s;
    }

    server {
        listen       8000;
        server_name  localhost;
        access_log   off;

        location / {
            proxy_set_header    X-Forwarded-For    $proxy_add_x_forwarded_for;
            proxy_set_header    X-Forwarded-Proto  $scheme;
            proxy_set_header    X-Forwarded-Host   $host;
            proxy_set_header    X-Forwarded-Port   $server_port;

            proxy_pass              http://backend;
            proxy_connect_timeout   2s;

            proxy_buffer_size          128k;
            proxy_buffers              4 256k;
            proxy_busy_buffers_size    256k;
        }
    }
    ```
- Keycloak DNS anpassen
- Einloggen -> Redirect schlägt fehl
```
      KC_HOSTNAME: http://localhost:8888
      KC_PROXY_HEADERS: xforwarded
```
- Einloggen, Realm+User erstellen, mit User einloggen bei Account-Console -> geht kaputt
- Account-Console, Web-Origin "+" eintragen. Dann sollte es gehen

## Node-Ausfall simulieren

- Ein Keycloak-Container ausmachen, Dienst ist immer noch verfügbar.
