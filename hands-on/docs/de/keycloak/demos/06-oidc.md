# OIDC

- Hintergrund:
- Fokus:

## Voraussetzung

- Laufender Keycloak
- `Demo` Realm
- Demo-User

## Client erstellen

- ClientType "OpenID Connect"
- Client ID: `oidc-demo`
- Auth Flows:
  - Standard Flow: Auth Code Flow -> das will man!
  - Direct Access -> Keycloak hat Zugriff auf Username/password, verwenden wir gleich zum testen
- Client erstellen -> Client ist Public
- Client authentication einschalten -> Client ist confidential
- -> Jetzt gibts einen Credentials-Tab mit Client Secret

## Händischer Auth-Flow

- Code holen (private Browser)
```
http://localhost:8888/realms/demo/protocol/openid-connect/auth?client_id=oidc-demo&redirect_uri=http://localhost/callback&response_type=code&scope=openid&state=randomState123&nonce=randomNonce123
```
- Einloggen und URL nach dem Redirect rauskopieren
- Beispiel:
```
http://localhost/?state=randomState123&session_state=fdf4b873-5d6d-4a0c-8f1a-8129860c1a5e&iss=http%3A%2F%2Flocalhost%3A8888%2Frealms%2Fdemo&code=dd92b0b4-d1df-4eda-b03f-702bd47538c8.fdf4b873-5d6d-4a0c-8f1a-8129860c1a5e.97bada9d-043e-4826-bbf0-dd960a1b7ed9
```
- `code=<value>` -> value kopieren
- Mit Curl Token abholen:
```
curl -X POST "http://localhost:8888/realms/demo/protocol/openid-connect/token" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -d "grant_type=authorization_code" \
  -d "client_id=oidc-demo" \
  -d "client_secret=<secret>" \
  -d "redirect_uri=http://localhost/callback" \
  -d "code=<code>"
```
- Wir erhalten Tokens!
  - Access-Token
  - ID-Token
  - Refresh Token
- Token in https://jwt.io/ anschauen

## Direct Access Flow für einfacheres Testen

- Token über Direct Access Grant holen
```
curl -X POST "http://localhost:8888/realms/demo/protocol/openid-connect/token" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -d "grant_type=password" \
  -d "client_id=oidc-demo" \
  -d "client_secret=<secret>" \
  -d "username=<user>" \
  -d "password=<pass>"
```
- Hier: nur zum Testen (in einem Request), nicht produktiv nutzen!

- Ohne "scope=openid" -> kein Id Token

## Client Scopes

- Client Scope anlegen
  - Name: myscope
  - Type: -> Wird der Scope bei neuen Clients direkt zugewiesen
  - Include in token scope
  - Create
- Add Mapper, e.g. hardcoded claim
  - Name: mykey
  - Value: myvalue

- Client Scope bei Client hinterlegen
- Verschiedene Settings durchprobieren und Tokens erstellen

## Keycloak Evaluation

- -> Client -> ClientScopes -> Evaluate
- Damit kann man ebenfalls Token evaluieren

