# Cluster

- Hintergrund:
- Fokus:

## Voraussetzung

- Laufender Keycloak
- `Demo` Realm
- Demo-User

## Flow erstellen

- Neuer Flow: `myflow`
- `Add Step` -> `Cookie`, alternative
- `Add sub-flow` -> Generic, "auth", alternative
- "auth" -> `Add step` -> Username-Password Form
- Flow an `account-console` Client hängen
- Login im private Tab mit Demo-User

## Alternative vs. Required

- Demo-User ist eingeloggt
- Cookie und Auth auf required stellen
- Neu laden -> Muss neu User+PW eingeben (Cookie ist da + Form)
- Cookie löschen und neuladen -> Error, weil Cookie required
- Wieder zurück zu Alternative

## Conditional

- User-Password-Form löschen
- Cookie disabled, damit wir uns immer einloggen müssen
- `Username Form` und `Password Form` als Step bei Auth hinzufügen
- Zweistufigen Login testen

## TBD

- Play with WebAuthn Authenticator

![](./img/full-demo-auth-flow.png)

## Build-in Flows anschauen

