# Cluster

- Hintergrund:
- Fokus:

## Voraussetzung

- Laufender Keycloak, mit Emails

## Credentials

- Set password
- User > Credentials > "Credential Reset" > Configure OTP > Send Email

## Registrierung

- Private Tab, Login-Screen öffnen
- Realm-Settings > Login > User registration aktivieren
- Private Tab neu laden

## Attribute-Gruppe anlegen

- Gruppe anlegen
- Attribut anlegen
- Registrierungsscreen nach Änderungen zeigen

## Komplexes Attribute anlegen

- Attribute: JobTitle
- Permissions alle an
- Validation: options -> "Dev", "Support", "Manager"
- Annotations:
  - `inputType`=`select`
  - `inputOptionsFromValidation`=`options`
- Account-Seite von Demo-User zeigen

## Terms and Conditions

- Authentication -> Required Actions -> T&C aktivieren
- Flows - Registration -> Terms and contidions "Required"
- Neuen User anlegen. Muss T&C zustimmen
- User im Admin-Dashboard anschauen -> Timestamp von T&C
- Inhalt der T&C über Theming (`terms.ftl`)

## Required Actions

- Authentication -> Required Actions
  - "Enabled" = Feature Toggle
  - "Set as default" = für neue Nutzer
- Beispiel `Configure OTP` als Default
- Delete Account aktivieren
  - Rolle "delete-account" notwendig
- Required Action "Delete-User" an Nutzer hängen -> wird beim nächsten Login ausgeführt