# Realm Keys

- Hintergrund:
- Fokus:


## Setup

- `docker run --rm -p 8888:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin keycloak-demo start-dev`
- Demo Realm mit Demo Nutzer
- Demo-Nutzer ist eingeloggt in private Browser

## Neuer Key

- Mit Admin Account
- `Demo` > Realm-Settings > Keys
- hmac-generated anschauen, prio merken, id merken
- neuen hmac-generated mit gleicher Prio erstellen
- alten hmac (id checken) löschen
- Private Session mit Demo Nutzer neu laden -> ausgeloggt
- Das ganze nochmal
  - neuen Key erstellen
  - alten deaktivieren
  - einmal Demo-User neu laden
  - alten key löschen
