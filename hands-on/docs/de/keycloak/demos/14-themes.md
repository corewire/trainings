# Cluster

- Hintergrund:
- Fokus:

## Voraussetzung

- Laufender Keycloak
- `Demo` Realm
- Demo-User

## Standard Themes

- Login-Screen von Demouser zeigen
- Im Admin-Menu Login-Theme auf Base umstellen
- Seite neu laden

## Custom theme

- In einem Ordner folgendes anlegen

```
.
├── docker-compose.yml
└── themes
    └── mytheme
```

- Inhalt der Compose:

```docker-compose
services:
  postgres:
    image: postgres
    environment:
      POSTGRES_DB: keycloak
      POSTGRES_USER: keycloak
      POSTGRES_PASSWORD: password

  keycloak:
    image: quay.io/keycloak/keycloak
    command: start-dev
    environment:
      KC_DB: postgres
      KC_DB_URL_HOST: postgres
      KC_DB_USERNAME: keycloak
      KC_DB_PASSWORD: password
      KC_BOOTSTRAP_ADMIN_USERNAME: admin
      KC_BOOTSTRAP_ADMIN_PASSWORD: admin
    volumes:
      - ./themes:/opt/keycloak/themes
    ports:
      - 8080:8080
    depends_on:
      - postgres
```

- Theme anlegen:
  - Ornder `login` anlegen
  - In `login` Datei `theme.properties` anlegen

```
parent=base
```

- Theme kann jetz in der Oberfläche ausgewählt werden
- Login-Maske zeigen

- `theme.properties` anpassen:

```
parent=keycloak
styles=css/login.css css/styles.css
```

- `./resources/css/styles.css` anlegen

```
:root {
  --pf-global--primary-color--100: #f9b233;
  --pf-global--primary-color--200: #1d71b8;
}

.login-pf body {
  background-image: url("../img/corewire-bg.png");
}
```

- Bild nach `./img/` kopieren

