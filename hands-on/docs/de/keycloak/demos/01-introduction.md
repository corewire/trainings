# Einführung

- Hintergrund:
- Fokus:

## Keycloak zeigen

- Keycloak starten, Command egal, wird später erklärt

    ```
    docker run -p 8888:8080   -e KEYCLOAK_ADMIN=admin   -e KEYCLOAK_ADMIN_PASSWORD=admin   quay.io/keycloak/keycloak:latest start-dev
    ```

- `localhost:8888` öffnen
- User: `admin`, Password: `admin`

## Admin-Konsole zeigen

- Master-Realm -> Server Info
- Show `Manage`-section:
    - Clients, Client scopes, Realm-Roles, Users, Groups

    - Sessions -> active sessions
    - Events -> part of monitoring

- Realm-Settings, Authentication, Identity Providers, User federation

## Account-Konsole zeigen
- Oben rechts -> `Manage Account`
- Unterseiten zeigen
- Passwort ändern, Email eintragen
- Zurück zur Admin-Console

## User zeigen
- Email ist gesetzt
- Enable/Disable
- Required user actions

## Realm zeigen
- Neuen Realm `demo` erstellen
- Neues Set an User,Clients, etc.
- Demo-User erstellen, Passwort geben
- Unter Clients URL von Account Console kopieren, private Tab, einloggen
- Im private Tab in Demo-Admin-Console einloggen
- Demo-User -> Role-Mappings: "query-users", "query-clients", "manage-users", "manage-clients"
- Demo-Admin-Console refreshen