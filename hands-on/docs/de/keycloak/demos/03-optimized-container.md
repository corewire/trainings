# Optimized Container

- Hintergrund:
- Fokus:

## Voraussetzung

- Docker compose von vorher (DB + Keycloak-Dev)
```
services:
  postgres:
    image: postgres
    environment:
      POSTGRES_USER: keycloak
      POSTGRES_PASSWORD: secret
      POSTGRES_DB: keycloak

  keycloak:
    image: quay.io/keycloak/keycloak:latest
    environment:
      KC_BOOTSTRAP_ADMIN_USERNAME: admin
      KC_BOOTSTRAP_ADMIN_PASSWORD: admin
      KC_DB: postgres
      KC_DB_URL_HOST: postgres
      KC_DB_USERNAME: keycloak
      KC_DB_PASSWORD: secret
    command: [
      "start-dev"
    ]
    ports: [ "8888:8080" ]
    depends_on: [ "postgres" ]
```

## Test-Setup

- Docker compose anpassen:
- `start-dev` zu `start`
- Env-Variablen hinzufügen

```
    environment:
      KC_HTTP_ENABLED: true
      KC_HOSTNAME_STRICT: false
```

- Testscript `time.sh`:
```
docker compose up -d

echo "Starting messurement..."
start_time=$(date +%s)

# Wait for Keycloak to respond
until curl -s http://localhost:8888/ > /dev/null; do
  sleep 1
done

end_time=$(date +%s)
echo "Startup time: $((end_time - start_time)) seconds"

# Cleanup
docker compose down keycloak
```

## 1. Testrun

- Datenbank starten und checken bis verfügbar
- `docker compose up -d postgres && docker compose logs -f`
- `bash time.sh` (3x)
- 1. Datenbank init dauert (~22s), 2.+3. sollte schneller sein (~15s)

## Optimized Image

- Dockerfile von den Folien
- Image bauen `docker build -t kc-test .`
- Docker-compose anpassen:
    - `image: kc-test`
    - Command: `start`, `--optimized`
- sollte deutlich schneller sein (~5s)

## Image näher anschauen

- `docker image ls | grep keycloak`
- `docker image ls | grep kc-test`
- `dive kc-test`
- => Jar-Files are duplicated and inflate the size
- Test Dockerfile from docu: https://www.keycloak.org/server/containers
- `docker build -t kc-test2 -f Dockerfile2 .`
- => Seems more complicated, but does the same
- Dockerfile source: https://github.com/keycloak/keycloak/blob/main/quarkus/container/Dockerfile
- => Own image base on ubi9-micro
