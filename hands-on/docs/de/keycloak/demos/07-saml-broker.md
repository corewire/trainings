# SAML und Identity Broker

- Hintergrund:
- Fokus:

## Voraussetzung

- ` docker run --rm -p 8888:8888 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin -e KC_HTTP_PORT=8888 keycloak-demo start-dev`

## SAML Idp

- Realm: `demo` erstellen
- Realm `idp` erstellen
- In IDP einen User erstellen
- In `demo` einen Identity Provider anlegen
  - `http://localhost:8888/realms/idp/protocol/saml/descriptor` als SAML entity descriptor
- Erstellen und XML von ProviderMetadata speichern
- Realm `idp` > client > import client

## Testen

- Private tab: `http://localhost:8888/realms/demo/account` > login via saml
- User im Admin-Dashboard zeigen
- Saml Requests anzeigen

## Key tauschen

- `idp` > Client > keys > regenerate
  - Private Key wird heruntergeladen
  - Cert kopieren
- Login nicht mehr möglich
- `demo` > RealmSettings > Keys > Add provider
  - RSA
  - Cert von eben
  - Private RSA -> Inhalt der heruntergeladenen Datei

## OIDC Broker

- OIDC Client anlegen
