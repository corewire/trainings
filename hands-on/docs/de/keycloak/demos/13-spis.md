# SPIs

- Hintergrund:
- Fokus:


## Erste Erweiterung erstellen

- Basierend auf https://github.com/dasniko/keycloak-tokenmapper-example
- Ordner erstellen, Dateien kopieren
  - src/main/java/dasniko/keycloak/tokenmapper/LuckyNumberMapper.java
    - Package anpassen
  - src/main/resources/META-INF/services/org.keycloak.protocol.ProtocolMapper
  - pom.xml
    - Keycloak-Version anpassen
    - unnötige dependencies rauswerfen
- Compilen: ´docker run -it --rm -v "$(pwd)":/usr/src/mymaven -v /tmp/maven/cache:/root/.m2 -w /usr/src/mymaven maven:3.9.9-amazoncorretto-23 mvn compile`
- `compile` durch `package` ersetzen, Ergebnis mit `tree .` anzeigen
- .jar in tmp Verzeichnis kopieren
  ```
  mkdir -p /tmp/keycloak/provider
  cp target/*.jar /tmp/keycloak/provider
  ```

## Erweiterung testen

- Keycloak starten `docker run --rm -p 8888:8080 -v /tmp/keycloak/provider:/opt/keycloak/providers -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin quay.io/keycloak/keycloak:latest start-dev`
- Realm Info > Provider Info > oidc-lucky-number-mapper suchen
- Clients > account > Client Scopes > account-dedicated > Configure new mapper
- Evaluate token

## Build erweitern

- Dockerfile basierend auf Dockerfile von den Folien bauen

```
FROM maven:3.9.9-amazoncorretto-23 as builder

WORKDIR /user/src/mymaven

COPY pom.xml /user/src/mymaven
RUN mvn dependency:resolve
COPY src /user/src/mymaven/src
RUN mvn package

FROM quay.io/keycloak/keycloak:latest

COPY --from=builder --chown=keycloak:root /user/src/mymaven/target/*.jar /opt/keycloak/providers
# ENV KC_DB=postgres
RUN /opt/keycloak/bin/kc.sh build

# (Optional) Keycloak Konfiguration
# ENV KC_DB_URL_HOST=postgres
# ENV KC_DB_USERNAME=keycloak
```

- `docker build -t keycloak-demo .`
- `docker run --rm -p 8888:8080 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin keycloak-demo start --http-enabled=true --hostname-strict=false`

## Debugging

- Command in docker-compose überführen
```
services:
  keycloak:
    build: .
    environment:
      KC_BOOTSTRAP_ADMIN_USERNAME: admin
      KC_BOOTSTRAP_ADMIN_PASSWORD: admin
    command:
    - start
    - --http-enabled=true
    - --hostname-strict=false
    ports:
    - 8888:8080
```
- Env Vars + Port hinzufügen
```
      DEBUG_PORT: "*:8787"
      DEBUG: "true"
    [...]
    ports:
    - 8787:8787
```
- VSCode launch.json anlegen:
```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
"version": "0.2.0",
"configurations": [
    {
        "type": "java",
        "name": "Debug (Attach)",
        "projectName": "MyApplication",
        "request": "attach",
        "hostName": "localhost",
        "port": 8787
    }
]
}
```

## Custom SPI

- Folgende Dateien in `java/de/corewire/keycloak/tasks` erstellen:
- `ScheduledTaskSpi.java`
```
package de.corewire.keycloak.tasks;

import org.keycloak.provider.ProviderFactory;
import org.keycloak.provider.Spi;
import org.keycloak.provider.Provider;

public final class ScheduledTaskSpi implements Spi{
    @Override
    public boolean isInternal() {
        return true;
    }

    @Override
    public String getName() {
        return "scheduled-task";
    }

    @Override
    public Class<? extends Provider> getProviderClass() {
        return ScheduledTaskProvider.class;
    }

    @Override
    public Class<? extends ProviderFactory<ScheduledTaskProvider>> getProviderFactoryClass() {
        return ScheduledTaskProviderFactory.class;
    }
}
```
- `ScheduledTaskProviderFactory.java`
```
package de.corewire.keycloak.tasks;

import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderFactory;
import org.keycloak.timer.TimerProvider;

public abstract class ScheduledTaskProviderFactory implements ProviderFactory<ScheduledTaskProvider> {
    private KeycloakSessionFactory keycloakSessionFactory;

    @Override
    public final void postInit(KeycloakSessionFactory keycloakSessionFactory) {
        this.keycloakSessionFactory = keycloakSessionFactory;
        keycloakSessionFactory.register((event) -> {
            KeycloakSession session = keycloakSessionFactory.create();
            TimerProvider provider = session.getProvider(TimerProvider.class);
            ScheduledTaskProvider stp = create(session);
            provider.scheduleTask(stp.getScheduledTask(), stp.getInterval(), stp.getTaskName());
        });
    }

    @Override
    public final void close() {
        KeycloakSession session = keycloakSessionFactory.create();
        TimerProvider provider = session.getProvider(TimerProvider.class);
        ScheduledTaskProvider stp = this.create(session);
        provider.cancelTask(stp.getTaskName());
    }
}
```
- `ScheduledTaskProvider.java`
```
package de.corewire.keycloak.tasks;

import org.keycloak.provider.Provider;
import org.keycloak.timer.ScheduledTask;

interface ScheduledTaskProvider extends Provider {
    ScheduledTask getScheduledTask();
    long getInterval();
    String getTaskName();
}
```
- `CleanupScheduledTaskProviderFactory.java`
```
package de.corewire.keycloak.tasks;

import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;

public final class CleanupScheduledTaskProviderFactory extends ScheduledTaskProviderFactory {
    @Override
    public ScheduledTaskProvider create(KeycloakSession keycloakSession) {
        return new CleanupScheduledTaskProvider();
    }

    @Override
    public void init(Config.Scope scope) {
    }

    @Override
    public String getId() {
        return "cleanup";
    }

}
```
- `CleanupScheduledTaskProvider.java`
```
package de.corewire.keycloak.tasks;

import java.lang.invoke.MethodHandles;

import org.keycloak.timer.ScheduledTask;

public final class CleanupScheduledTaskProvider implements ScheduledTaskProvider {

    @Override
    public ScheduledTask getScheduledTask() {
        return (keycloakSession -> {
        });
    }

    @Override
    public long getInterval() {
        return 1000;
    }

    @Override
    public String getTaskName() {
        return "test";
    }

    @Override
    public void close() {
    }
}
```

- Build; checken, dass SPI geladen wird
- Log output hinzufügen
```
import org.jboss.logging.Logger;

# Als Property
    private static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass());

# In getScheduledTask
                logger.info("Hello from CleanupTask");
```

## Configuration

- `CleanupScheduledTaskProviderFactory.java` - Config in init hinzufügen
```
    public ScheduledTaskProvider create(KeycloakSession keycloakSession) {
        return new CleanupScheduledTaskProvider(this.interval);
    }

    private int interval;

    @Override
    public void init(Config.Scope config) {
        this.interval = config.getInt("interval-time", 1000);
    }
```
- `CleanupScheduledTaskProvider.java`
```
    private long interval;

    public CleanupScheduledTaskProvider(long interval) {
        this.interval = interval;
    }
```
- Docker compose anpassen: `--spi-scheduled-task-cleanup-interval-time=5000`

## ServerInfoAware

- `CleanupScheduledTaskProviderFactory.java`
```
public final class CleanupScheduledTaskProviderFactory extends ScheduledTaskProviderFactory implements ServerInfoAwareProviderFactory {

    @Override
    public Map<String, String> getOperationalInfo() {
        Map<String, String> ret = new LinkedHashMap<>();
        ret.put("interval", String.valueOf(this.interval));
        return ret;
    }
```

- Bauen, neu starten, Server info zeigen

## Custom Endpoint

- Ordner `rest` erstellen
- `CustomEndpointProviderFactory.java`
```
package de.corewire.keycloak.rest;

import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

public class CustomEndpointProviderFactory implements RealmResourceProviderFactory {

    @Override
    public RealmResourceProvider create(KeycloakSession keycloakSession) {
        return new CustomEndpointProvider(keycloakSession);
    }

    @Override
    public void init(Config.Scope scope) {
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return "custom-endpoint";
    }
}
```
- `CustomEndpointProvider.java`
```
package de.corewire.keycloak.rest;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import org.keycloak.models.KeycloakSession;
import org.keycloak.services.resource.RealmResourceProvider;

import java.util.Map;

public class CustomEndpointProvider implements RealmResourceProvider {
    private final KeycloakSession session;

    public CustomEndpointProvider(KeycloakSession session) {
        this.session = session;
    }

    @Override
    public Object getResource() {
        return this;
    }

    @Override
    public void close() {
    }

    @GET
    @Path("hello")
    public Response hello() {
        return Response.ok(Map.of("hello", "world")).build();
    }
}
```

- `http://localhost:8888/realms/master/custom-endpoint/hello`

- Erweitern:
```
        return Response.ok(Map.of(
            "hello", "world",
            "realm", session.getContext().getRealm().getName(),
            )
        ).build();
```
- Unterschiedliche Realms zeigen
- User:
```
        AuthResult auth = new AppAuthManager().authenticateIdentityCookie(session, session.getContext().getRealm());

        if (auth == null) {
            throw new NotAuthorizedException(CookieType.IDENTITY);
        }

        return Response.ok(Map.of(
            "hello", "world",
            "realm", session.getContext().getRealm().getName(),
            "user", auth.getUser().getUsername()
            )
        ).build();
```
