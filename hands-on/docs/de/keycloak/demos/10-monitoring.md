# Realm Keys

- Hintergrund:
- Fokus:


## Keycloak mit Metriken anmachen

- `docker run --rm -p 8888:8080 -p 9000:9000 -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin keycloak-demo start-dev --health-enabled=true --metrics-enabled=true --features=user-event-metrics --event-metrics-user-enabled=true`

## Prometheus anmachen

- cd /tmp && mkdir prom && cd prom
- nano `prometheus.yml`, target ip anpassen
```
global:
  scrape_interval: 15s # Adjust the scrape interval as needed

scrape_configs:
  - job_name: 'local_target'
    static_configs:
      - targets: ['172.17.0.2:9000']
```
- `docker run -it --rm -v /tmp/prom/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus`
- <prom-id>:9090/targets checken

## Metrics anschauen

- Grafana anmachen
- `docker run -it --rm -p 3000:3000 grafana/grafana`
- Einloggen (admin/admin) auf localhost:3100
- Datasource zu Prometheus eintragen (Ip nachschauen)
- Explore -> Metrics
  - `http_*`
  - `keycloak`
- Explore
  - keycloak_user_events_total event=login
  - keycloak_user_events_total event=login error=invalid_user_cred
