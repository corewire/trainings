# Keycloak Einführung

- [Zu den Aufgaben](./aufgaben/01-ci-cd-pipelines-in-gitlab.md)
- [Zu den Folien](./1-introduction/#/)

## Dauer
2 Tag

## Zielgruppe


## Voraussetzungen


## Kursziel


## Schulungsform
Die Schulungsinhalte werden vom Trainer mit Folien und Live-Demos vorgestellt. Zwischen den einzelnen Kapiteln dürfen die Teilnehmenden die Inhalte in praktischen Übungen selber anwenden und vertiefen. Die Aufteilung liegt bei **60% theoretischen Inhalten** und **40% praktischen Übungen**.

## Kursinhalt
