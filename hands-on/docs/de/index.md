---
title: Corewire Hands-On Collection
---

# Willkommen

Hier finden Sie sämtliche Unterlagen, die Sie für unsere Schulungen benötigen.

## Interesse an einer Schulung?

Interesse an einer der hier aufgeführten Schulungen oder einer themenverwandten Schulung rund um die Bereiche **DevOps**, **Cloud-Computing** und **Softwareentwicklung**? Fragen Sie einfach unverbindlich nach Themen und Terminen an unter <a href="mailto:training@corewire.de">training@corewire.de</a>.


## Direkt zu den Schulungsunterlagen:
- [AWS RDS](./aws-rds/)

- [Docker & Container](./docker/)

- [Git - Dezentrale Versionierung](./git/)

- [Projektmanagement mit Gitlab](./gitlab/)

- [CI/CD-Pipelines mit Gitlab](./gitlab-ci/)
