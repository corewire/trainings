# Docker & Container

**Important:** We are in the process of translating the material. So far, the slides link to the German material:

- [Exercises](./exercises/01-first-steps.md)
- [Cheat sheets](./cheatsheets/01-first-steps.md)
- [Slides](../../01-einfuehrung/#/)

## Duration

3 days

## Target audience

- Software developers
- Software architects
- System administrators
- DevOps engineers

## Prerequisites

- (optional) Some experience with Terminal/Bash
- (optional) Some experience with Docker

## Objectives

The course teaches the usage of Docker and provides technical background for it. On the first day, we cover use cases and basics of containers. The use cases provide insight into situations where containers are useful. The basics will enable participants to use containers confidently. The second day includes the creation of self-created images, as well as a deep insight into debugging with containers. With that, errors that can occur during the creation and operation of containers can be quickly found and fixed. The third day covers more in-depth topics as well as the use of containers in development environments, CI/CD systems and clusters. Thus, the course is explicitly aimed at teams with different experience levels with Docker and containers.

## Education concept

The trainer presents the content with slides and live demos. In addition, the participants will apply and deepen their understanding in practical exercises between the individual chapters. The split between these is **60% theoretical content** and **40% practical exercises**.

## Course content

### 1st day
- Motivation, basic concepts and use cases for containers
- Comparison of development environments with and without containers
- Comparison of deployment scenarios with and without containers
- Bare metal, VMs and containers
- Container vs. Docker
- Container lifecycle, starting/stopping existing containers
- Container images, tags, registry
- Volumes, volume types and persistence
- Networking and internal/external accessibility

### 2nd day
- Service architectures with Docker Compose
- Designing container-based applications
- Creating your own images (Dockerfile)
- Base image, layer and caching, multistage, Dockerfile best practices
- Basics of the Docker architecture (Docker CLI, Docker Daemon)
- Security best practices with securing images, containers, the Docker host, the Docker daemon
- Docker on Windows with Linux/Windows containers, WSL2, Docker Desktop

### 3rd day
- Advanced debugging of containers
- Docker architecture with containerd, runc, kernel (namespaces, cgroups)
- Containers without Docker/alternative runtimes
- Rootless Docker
- Container-based development environments
- CI/CD with and for containers
- Teasing next steps: container orchestration with Kubernetes
