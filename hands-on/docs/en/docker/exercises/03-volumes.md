# Dealing with volumes

!!! goal "Goal"
    This project is about volumes. You will:

    - start the demo application with different volume types
    - get to know the differences between managed volumes and bind mounts

!!! tipp "Tools"

    - Try to complete the tasks below with the help of the [slides](../../04-volumes/#/)
    and the [cheatsheet](../cheatsheets/03-volumes.md).
    - If you have any problems, you will find a fold-out block for each task describing the solution.

## Preparation

- Create a folder `volumes` as a subfolder of the `workspace` folder.

??? help "Solution (click on the arrow if you get stuck)"

    - If the Explorer bar of VSCode is not already displayed on the left, open Explorer of VSCode with `Ctrl + B` or alternatively by clicking on the file icon (above the magnifying glass) in the left bar.
    - Make sure that you are in the `workspace` folder.
    - Create a new folder with the name `volumes` via the Explorer.
        - Alternatively in the terminal: `mkdir volumes`. Again, make sure that you are in the `workspace` folder.

- Change to the `volumes` folder in the terminal.

??? help "Solution (click on the arrow if you get stuck)"
    - (Optional) Open a terminal (Menu > Terminal > New Terminal).
    - Change to the created folder with `cd volumes`. If you are still in the docker-demoapp folder from the last task, you must first go up one folder level with `cd ..` and then enter `cd volumes`.
    - Execute `pwd`. `pwd` returns the current directory. You
      should get the output `/home/coder/workspace/volumes`.
    - If you do not receive the output, change to the directory with
      `cd /home/coder/workspace/volumes`.


## Task 1 - Bind Mounts


### 1.1: Create bind mount

- Make sure that you are in the directory `/home/coder/workspace/volumes`.
- Start the demo application with the following properties:
    - in the background
    - with port forwarding (8080 -> 5000)
    - a bind mount
        - Host path: `${PWD}/data`
        - Container path: `/app/data/notes`
    - Note that the files are not mounted in read-only mode

??? help "Solution (click on the arrow if you get stuck)"
    - Create the folder `data` with `mkdir data` and start the image with the following command:
    ```
    docker run -d -p 8080:5000 --mount type=bind,source=${PWD}/data,target=/app/data/notes corewire/docker-demoapp
    ```


### 1.2: Check demo application

- Visit the demo application in the browser. It can be reached via your URL (`code-{NUMBER}.labs.corewire.de`)
  on port 8080. Use `http://` and not `https://`.
- Enter 1-2 notes.

??? help "Solution (click on the arrow if you get stuck)"
    - Try to open the webapp in a new tab under `http://code-0.labs.corewire.de:8080/`.
    - Important:
        - `http` **not** `https`
        - Replace `code-0` with your instance.

### 1.3: Check bind mount on the host

- In the Explorer in VSCode you will see the folder `volumes/data`. Check the contents.
- The folder contains your notes. These are persisted on the host system by the bind mount.
- Edit a note and save the file.
- Reload the demo application in the other tab with F5.
- You should see your modified note.


## Task 2 - Share bind mounts

You have successfully created a bind mount in task 1. The notes, i.e.
the state of the container, are now saved independently of the container itself.
This means that the container can now be started, stopped and deleted at will without losing the data.
the data being lost.

In the following, we now want to start a second container that accesses the same
directory.

### 2.1: Starting another demo application

- Make sure that you are in the directory `/home/coder/workspace/volumes`.
- Start the demo application with the following properties:
    - in the background
    - with port forwarding (**8081** -> 5000). **Important:** Port 8080 is already assigned.
    - a bind mount
        - Host path: `${PWD}/data`.
        - Container path: `/app/data/notes`
        - Mode: **Readonly**

??? help "Solution (click on the arrow if you get stuck)"
    - Start the image with the following command:
    ```
    docker run -d -p 8081:5000 --mount type=bind,source=${PWD}/data,target=/app/data/notes,readonly corewire/docker-demoapp
    ```

### 2.2: Check demo application

- Visit the demo application in a new tab.
- It contains the same notes as the demo application on port 8080.


### 2.3: Creating a note

- Try to create a note on the demo application on port 8081.
- You should receive an error because the application does not have write access to the directory.
- Create a note in the demo application on port 8080.
- Reload both demo applications.


### 2.4: Stop demo application

- Stop both demo applications.

!!! attention "VSCode Container"
    The containers `code-server` and `vscode-traefik` provide the VSCode program.
    Do **not** stop these containers, otherwise the instance will no longer be accessible.

??? help "Solution (click on the arrow if you get stuck)"
    - Execute the following command to display the running containers:
    ```
    docker ps
    ```
    - Search for the entries with `corewire/docker-demoapp` in the IMAGE column.
    - Copy either the `CONTAINER ID` at the beginning of the line or the `NAME` at the end of the line.
    - Stop both containers individually with :
    ```
    docker stop <ID or name>
    ```


## Task 3 - Managed Volumes

In the following, we want to implement the same scenario with Managed Volumes.

### 3.1: Starting the demo application with Managed Volumes

- Start the demo application with the following properties:
    - in the background
    - with port forwarding (8080 -> 5000)
    - a managed volume
        - Volume name: `demoapp_notes`.
        - Container path: `/app/data/notes`

??? help "Solution (click on the arrow if you get stuck)"
    - Create the volume and start the image with the following command:
    ```
    docker volume create demoapp_notes
    docker run -d -p 8080:5000 --mount source=demoapp_notes,target=/app/data/notes corewire/docker-demoapp
    ```

### 3.2: Check demo application

- Visit the demo application in a new tab.
- Write 1-2 notes.

### 3.3: Display volume

Display all volumes. You should get a comparable output:

```
DRIVER VOLUME NAME
local demoapp_notes
```

??? help "Solution (click on the arrow if you get stuck)"
    - Display all volumes with the following command:
    ```
    docker volume ls
    ```

Now also display the low-level details of the volume. These
contain, for example, the mountpoint under `/var/lib/docker`.

??? help "Solution (click on the arrow if you get stuck)"
    - Display the details with the following command:
    ```
    docker volume inspect demoapp_notes
    ```

### 3.4: Start another demo application

- Start the second demo application with the following properties:
    - in the background
    - with port forwarding (**8081** -> 5000). **Important:** Port 8080 is already assigned.
    - a managed volume
        - Volume name: `demoapp_notes`.
        - Container path: `/app/data/notes`
        - Mode: **Readonly**

??? help "Solution (click on the arrow if you get stuck)"
    - Start the image with the following command:
    ```
    docker run -d -p 8081:5000 --mount source=demoapp_notes,target=/app/data/notes,readonly corewire/docker-demoapp
    ```

### 3.5: Check demo application

- Visit both demo applications.
- Try to create notes in both demo applications.
- The behavior should be the same as before:
    - Demo application 1 can create new notes.
    - Demo application 2 can only display the notes.

### 3.6: Stop demo application

- As before, stop both demo applications.

!!! attention "VSCode Container"
    The containers `code-server` and `vscode-traefik` provide the VSCode program.
    Do **not** stop these containers, otherwise the instance will no longer be accessible.

??? help "Solution (click on the arrow if you get stuck)"
    - Execute the following command to display the running containers:
    ```
    docker ps
    ```
    - Search for the entries with `corewire/docker-demoapp` in the IMAGE column.
    - Copy either the `CONTAINER ID` at the beginning of the line or the `NAME` at the end of the line.
    - Stop both containers individually with :
    ```
    docker stop <ID or name>
    ```
