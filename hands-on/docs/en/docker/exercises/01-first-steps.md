# First steps with Docker


!!! goal "Goal"

    This project is about the basic functionality of Docker. You will:

    - start and stop containers
    - execute first commands
    - track the life cycle of containers


!!! tipp "Tools"

    - Try to complete the tasks below with the help of the [slides](../../02-erste-schritte/#/) and the [cheatsheet](../cheatsheets/01-first-steps.md).
    - If you have any problems, you will find a fold-out block for each task describing the solution.


## Task 1 - Set up the training machine


- The training machines can be accessed under `code-{NUMBER}.{{ domain }}`.
Replace `{NUMBER}` with the number assigned to you.
  - Example for the number 5: `code-5.{{ domain }}`.
  - Example for the number 11: `code-11.{{ domain }}`.
- Then enter the password assigned to you.
- You now have access to the training machine.


## Task 2 - The demo application


### 2.1: Starting the terminal

- Open a terminal (Menu > Terminal > New Terminal)


### 2.2: Starting the demo application

- Start the demo application `corewire/docker-demoapp` via the terminal. It is automatically downloaded from Dockerhub and started.

??? help "Solution (click on the arrow if you get stuck)"
    - Execute the following command:
    ```
    docker run corewire/docker-demoapp
    ```
    - You can close the container again with `Ctrl + c`.


### 2.2: Making the demo application externally accessible

The demo application is now running, but is not yet externally accessible. Stop the application and start it again. This time, however, with port forwarding from 8080 -> 5000.

??? help "Solution (click on the arrow if you get stuck)"

    - With `Ctrl + c` you can close the container again.
    - Execute the following command:
    ```
    docker run -p 8080:5000 corewire/docker-demoapp
    ```


### 2.3: Visit the demo application

- Visit the demo application in your browser
- It can be accessed via your URL (`code-{NUMBER}.labs.corewire.de`)
  on the port 8080. Use `http://` and not `https://`.
- Enter 1-2 notes.
- Check the logs in the VSCode terminal. You can see your access to the website.

??? help "Solution (click on the arrow if you get stuck)"
    - Open the webapp in a new tab under `http://code-0.labs.corewire.de:8080/`.
    - Important:
        - `http` **not** `https`
        - Replace `code-0` with your instance.
    - If successful, you can switch back to the VSCode instance. In the terminal you can see
      you can see in the log entries that you have accessed the webapp.

## Task 3 - Starting and stopping containers

### 3.1: Stopping containers in the foreground

- Stop your container.
- Check that the website is no longer accessible.

??? help "Solution (click on the arrow if you get stuck)"
    - Click on your terminal in which the container is running.
    - You can now close the container with `Ctrl + c`.


### 3.2: Start in the background

- Now start the demo application in the background so that the logs do not appear on the
  terminal.
- Visit the website again and check that it is accessible.
- Insert notes.

??? help "Solution (click on the arrow if you get stuck)"
    - Start container in **background**:
    ```
    docker run -d -p 8080:5000 corewire/docker-demoapp
    ```

### 3.3 Stopping the container in the background

- Now stop the container again. To do this, you must first find out what the
  container is called.

!!! attention "VSCode Container"
    The containers `code-server` and `vscode-traefik` provide the VSCode program.
    Do **not** stop these containers, otherwise the instance will no longer be accessible.

??? help "Solution (click on the arrow if you get stuck)"
    - Stop container if it was started in the **background**:
        - `docker stop <ID or name of container>`
        - You can look up the container name using `docker ps`.



## Task 4 - Restart container

`docker run` creates a new container each time. You can recognize this, for example
by the fact that the notes are no longer available for each new container.

In task 3.3 you looked up the name of the container. Now start this
container again (not via `docker run`) so that the notes are displayed again.

Check the accessibility of the website and whether your notes are displayed again.

??? help "Solution (click on the arrow if you get stuck)"
    - Restart the container: `docker start <ID or name of the container>`.


## Task 5 - Stop and delete the container

Now stop the container again. Then delete it.

??? help "Solution (click on the arrow if you get stuck)"
    - Stop the container as before:
        - `docker stop <ID or name of the container>`
        - You can look up the container name using `docker ps`.
    - Delete the container:
        - `docker rm <ID or name of the container>`
