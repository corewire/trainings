# Debugging

!!! goal "Goal"
    This project is about debugging and repairing a broken Docker setup. You will:

    - get a non-functional demo app up and running

!!! tipp "Tools"
    - Try the tasks below with the help of the [slides](../../10-debugging/#/)
    and the [cheatsheet](../cheatsheets/07-debugging.md).
    - If you have any problems, you will find a fold-out block for each task describing the solution.

## Task - Repair demo application

- Change to the folder `BROKEN-docker-demoapp`. This folder contains a setup for the docker-demoapp. Unfortunately, your colleague has broken something while trying to improve the application and now needs your help to get it up and running again.

Try to fix the errors yourself step by step until the application is online and the database connection is working. If you start the application with `docker compose up -d`, the container will be built automatically and you will see the errors that occur in the build process. Once these have been fixed, you can use `docker compose logs -f` to see live what is being logged in the container. Use the [cheatsheet](../cheatsheets/07-debugging.md) for everything else.

??? help "Solution (click on the arrow if you get stuck)"
    Here you will find solutions to various error messages that may occur during this process.

    - Error with any Docker Compose command:

    ??? failure "ERROR: The Compose file './docker-compose.yml' is invalid..."
        A `-` is missing in line 6 of `docker-compose.yml`. Change `./volumes/webapp-data:/data/notes` to `- ./volumes/webapp-data:/data/notes`.
    - Error while building the container:

    ??? failure "Error response from daemon: dockerfile parse error line 18: unknown instruction: COPI"
        Change the command `COPI` to `COPY` in line 18 in the Dockerfile.

    ??? failure "ERROR: Could not open requirements file: [Errno 2] No such file or directory: 'requirements.txt'"
        A typing error has crept into line 14 in the Dockerfile. Change `WORKDIR /ap` to `WORKDIR /app`. This will execute the `pip` command in the folder to which the `requirements.txt` was copied.

    ??? failure "ERROR: Could not install packages due to an OSError: [Errno 13] Permission denied: '/home/app-runner'"
        The command `pip install` should be executed as `root` so that it has the rights for the installation. To do this, move line 12 with `USER app-runner` under the `pip install` command.

    - Error after starting the container:

    ??? failure "ERROR: Connection to MariaDB Server could not be established: Unknown MySQL server host 'database' (-3)"
        The database network is not connected to the webapp. Extend the definition of the `webapp` service in `docker-compose.yml` to include the network:
        ```yaml
        networks:
          - database-network
        ```

    - Error after entering a note:

    ??? failure "PermissionError: [Errno 13] Permission denies: '/data/notes/note_....txt'"
        Since the mounted folder `webapp-data` only has a share for root and not for the user, the notes cannot be saved here. The rights of the folder must therefore be changed to the ID of the user.
        Therefore, change to the folder `volumes/webapp-data` and enter the following command:
        ```
        chown 10100:10100 .
        ```
