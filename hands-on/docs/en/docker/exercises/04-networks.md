# Dealing with networks

!!! goal "Goal"
    This project is about getting to know basic networking concepts of Docker. You will:

    - create your own network
    - establish the connection between two containers
    - create your own IPv6 network with specific subnets

!!! tipp "Tools"
    - Try to solve the tasks below independently with the help of the [slides](../../05-netzwerk/#/) and the [cheatsheet](../cheatsheets/04-networks).
    - If you have any problems, you will find a fold-out block for each task describing the solution.

## Preparation - Setting the VSCode

First make sure that you are back in the workspace folder. In this hands-on we will look at the connection between two containers in different scenarios. This is easiest if the two containers are open side by side in split terminal windows. To do this, open VSCode, open a terminal and click on ![Split Button](./img/split_button.png). Your VSCode should then look something like this:
![Split View](./img/split_cmd.png)

## Task 1 - Default bridge networks

### 1.1: Start container

Start a container in the left and right terminal with :
```shell
docker run -it --rm corewire/network-playground /bin/bash
```
For this hands-on, we have created a small Alpine-based container in which network diagnostic tools are pre-installed.

### 1.2: Find out network information

Find out the IP addresses for both containers with:
```shell
ip a
```

### 1.3: Test the container connection

- Execute the following command in the container in the right terminal:
```
tcpdump icmp
```
The container now monitors all ICMP traffic live, i.e. all PING packets.
- Execute a ping in the container in the left terminal to the container in the right terminal:
```
ping <container ip>
```
You can now see in the right-hand terminal how the traffic arrives at the container.

## Task 2 - User-defined bridge networks

### 2.1: Create your own network

- Exit the `tcpdump` in the right terminal with `Ctrl`+`c` and leave the container with `exit`.
- Create a new network with the name `my-network`.

??? help "Solution (click on the arrow if you get stuck)"
    Execute the following command in one of the two terminals:
    ```shell
    docker network create my-network
    ```

- Inspect your network and check which subnet is used in my-network.

??? help "Solution (click on the arrow if you get stuck)"
    Execute the following command in one of the two terminals:
    ```shell
    docker network inspect my-network
    ```

### 2.2: Start container with network

- Start the container again in the right-hand terminal, but this time with the network you have just created:
```shell
docker run -it --rm --network=my-network corewire/network-playground /bin/bash
```
- Start `tcpdump` again, query the **new** IP address with `ip a` and try, as above, to reach the right container from the left one. This is not successful in this case, as the containers are in different networks.

??? help "Solution (click on the arrow if you get stuck)"
    Execute the following command in the container in the right terminal:
    ```
    tcpdump icmp
    ```
    Now find out the **new** IP with the following command:
    ```
    ip a
    ```
    The container now monitors all ICMP traffic live, i.e. all PING packets.
    Ping the container in the left terminal to the container in the right terminal:
    ```
    ping <container ip>
    ```
    You will not see any traffic arriving at the container here, as the containers are in different networks.

- Stop the left container with `exit`.
- Start the left container in the new network.

??? help "Solution (click on the arrow if you get stuck)"
    To start the network in the left container, use the following command:
    ```shell
    docker run -it --rm --network=my-network corewire/network-playground /bin/bash
    ```

- Test the connection between the containers again with `tcpdump` and `ping`.
- The connection is now possible again.

### 2.3: Using the DNS

- Stop both containers again and also start them with a name. This can be set via `--name=<container name>`.
- Test the connection again with `tcpdump` and `ping`, using the assigned name as the destination address.

??? help "Solution (click on the arrow if you get stuck)"
    Left terminal:
    ```shell
    docker run -it --rm --network=my-network --name=left corewire/network-playground /bin/bash
    ```

    Right terminal:
    ```shell
    docker run -it --rm --network=my-network --name=right corewire/network-playground /bin/bash
    ```

    ```shell
    tcpdump icmp
    ```

    Left terminal:
    ```shell
    ping right
    ```

## Task 3: IPv6 networks

- Stop both containers again.
- Create a new network `my-ipv6-network` with the specified subnet `fd00:0:0:1::/64`

??? help "Solution (click on the arrow if you get stuck)"
    To create the network in one of the containers, use the following command:
    ```shell
    docker network create --ipv6 --subnet fd00:0:0:1::/64 my-ipv6-network
    ```

- Now start the two containers in the `my-ipv6-network` network.

??? help "Solution (click on the arrow if you get stuck)"
    Left terminal:
    ```shell
    docker run -it --rm --network=my-ipv6-network --name=left corewire/network-playground /bin/bash
    ```

    Right terminal:
    ```shell
    docker run -it --rm --network=my-ipv6-network --name=right corewire/network-playground /bin/bash
    ```

- You can now monitor the ipv6 packets with `tcpdump icmp6`.

??? help "Solution (click on the arrow if you get stuck)"
    Right terminal:
    ```shell
    tcpdump icmp6
    ```

    Left terminal:
    ```shell
    ping6 right
    ```
