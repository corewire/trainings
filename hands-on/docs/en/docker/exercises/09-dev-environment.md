# Developing with Docker

!!! goal "Goal"
    This project is about setting up or using Docker-based development environments 
    to set up or use development environments based on Docker. You will:

    - compile demo applications in containers
    - change the version of Docker under the demo application

!!! tipp "Tools"
    - Try to solve the tasks below on your own.
    - If you have any problems, you will find a fold-out block for each task describing the solution.


## Task 1 - Develop demo application with Docker

- Go to the `DEV-docker-demoapp` folder. This folder contains a setup for the docker-demoapp. Scripts for running the tests, linters and the application itself are already available here
- Take a look at how these scripts work
- Run the tests first:

```shell
./run-tests.sh
```

- Then run the linters:

```shell
./run-linter.sh
```

- Now start the application:

```
./run-app.sh
```

The demo application is now accessible on port 5000.

## Task 2 - Go demo application

Change to the `Go-demoapp` folder. Here you will find a simple Go Hello World. Go is a language that can be compiled into executable files. Since we do not want to install Go, we do not execute `go build` directly but in Docker.

### 2.1 - Compiling with Docker

- Create a new file `build-app.sh`
- Insert the following Docker command:
```shell
docker run --rm -v "$PWD":/usr/src/hello -w /usr/src/hello golang:1.20 go build
```

The script mounts the folder in the Docker container and then executes `go build`.

- Make the script executable:
```shell
chmod u+x build-app.sh
```

- Build the application:
```shell
./build-app.sh
```

- Run the Go application:
```shell
./hello
```

### 2.2 Developing the application with Docker

During development, we want to build and run the application in one step. To do this, create a new file `run-app.sh` with the following content:

```shell
docker run --rm -v "$PWD":/usr/src/hello -w /usr/src/hello golang:1.20 go run .
```

- Make the file executable:
```shell
chmod u+x run-app.sh
```

- Run the application without building it first:
```shell
./run-app.sh
```

Change the text that is to be displayed and run the application again.

## Task 3 - Node demo application

In this part of the hands-on we want to update the Node used in a Node demo application. Switch to the `Node-Demoapp` folder. Familiarize yourself with the application. The scripts `install.sh`, `run-app.sh` and `run-tests.sh` use `docker compose` to run the application.

### 3.1 - Using the development environment

- Install the dependencies of the application:
```shell
./install.sh
```

- Start the application
```shell
./run-app.sh
```

- The application can now be reached on port 5173.
- You can stop the application again with `Ctrl+c`.
- Execute the tests:
```shell
./run-tests.sh
```

### 3.2 - Update Node

We now want to update our project to a new node version. The node version is defined in the file `docker-compose.yml`.

- Change the node version to `20.0.0`.
- Run the application again:
```shell
./run-app.sh
```

- The application is accessible again on port 5173.

The update seems to have worked, but does everything work?

- Take a look at the tests:
```shell
./run-tests.sh
```

- The tests fail. This is because the new node version is not compatible with the application. We have installed a dummy test here which now fails. Change the test so that it works again.

??? help "Solution (click on the arrow if you get stuck)"
    The tests can be found in the file `src/__tests__/App.test.tsx`. In this file, change the version from `v18.16.0` to `v20.0.0`.

- Execute the tests again:

```shell
./run-tests.sh
```

- The tests should now be green again.

We have now successfully updated our project to a new Node version. The whole thing without installing Node.
