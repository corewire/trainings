# Dealing with images


!!! goal "Goal"

    This project is about the basic handling of Docker images. You will:

    - build images in different versions
    - build a given image and start the container



!!! tipp "Tools"

    - Try to complete the tasks below with the help of the [slides](../../03-images/#/) and the [cheatsheet](../cheatsheets/02-images.md).
    - If you have any problems, you will find a fold-out block for each task describing the solution.

## Task 1 - Display images

- Display the current images on the system.

??? help "Solution (click on the arrow if you get stuck)"
    - The images can be retrieved with
    ```shell
    docker image ls
    ```
- You will see the image for the VSCode container you are currently using and another one
for Traefik, a reverse proxy.


## Task 2 - Pulling different images

- Pull the image `nginx` with the tag `latest`.
- Pull the image `nginx` with the tag `mainline`.
    - The tags `latest` and `mainline` point to the same image. Docker notices this and does not download anything.
- Populate the image `nginx` with the tag `stable`.
    - Part of the layers are cached here.
- Display the current images on the system again.
    - The nginx images are now available with the cached tags.

??? help "Solution (click on the arrow if you get stuck)"
    - The images can be pulled with
    ```shell
    docker pull <image>:<tag>
    ```
    - The images can then be displayed with
    ```shell
    docker image ls
    ```

## Task 3 - Demo app in different versions

- Start the demo application `corewire/docker-demoapp` with the tag `1.1.1`.
- Call up the demo application in the browser.
- The version is displayed accordingly.
- Start the demo application with the tag `1.0`.
- Check which version is displayed in the browser.
- Start the demo application with the tag `latest`.
- Check in the browser which version is displayed.

??? help "Solution (click on the arrow if you get stuck)"
    To start a demo application, use the following command:
    ```shell
    docker run -p 8080:5000 corewire/docker-demoapp:<tag>
    ```

## Task 4 - Build images

- Change to the directory `docker-demoapp`.

??? help "Solution (click on the arrow if you get stuck)"
    The directory can be changed with `cd <path to directory>`:
    ```shell
    cd docker-demoapp
    ```
- Build the Docker image. More details on `docker build` will be explained later.
```shell
docker build -t my-great-image .
```
- Display the current images on the system again.
  - The image is now available locally.


## Task 5 - Starting the container

- Start the container with :
```shell
docker run -p 8080:5000 my-great-image
```

- Call up the demo application in the browser. It is accessable
  via your URL (`code-{NUMBER}.labs.corewire.de`)
  on port 8080. Use `http://` and not `https://`.
