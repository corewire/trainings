# Cheatsheet 4 - Networks

| Command                                                                                      | Description                                                                |
|----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| ```docker run --network <name> <image>```                                                    | Start container in the specified network                                   |
| ```docker network create <name>```                                                           | create network                                                             |
| ```docker network create --subnet <Netzadresse>/<Maske> <name>```                            | Create network with specified subnet                                       |
| ```docker network create --ipv6 --subnet <Netzadresse>/<Maske> <name>```                     | Create IPv6 network with specified subnet                                  |
| ```docker network connect <name> <containername>```                                          | Add existing container to a network                                        |
| ```docker network ls```                                                                      | show networks                                                              |
| ```docker network inspect <name>```                                                          | Show low-level details of network                                          |
| ```docker network rm <name>```                                                               | Delete single network                                                      |
| ```docker network prune```                                                                   | Delete all unused networks                                                 |

