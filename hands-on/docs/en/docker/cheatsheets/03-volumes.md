# Cheatsheet 3 - Docker Volumes

| Command                                                                                      | Description                                                                |
|----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| ```docker run --mount type=bind,source=<hostpath>,target=<containerpath> <image>```          | Mount path from host into a container                                      |
| ```docker run --mount type=bind,source=<hostpath>,target=<containerpath>,readonly <image>``` | Mount path from host into a container as **readonly**                      |
| ```docker run --mount type=bind,source=${PWD}/<hostpfad>,target=<containerpath> <image>```   | Mount path from host relative to the current directory into a container    |
| ```docker run --mount source=<volume>,target=<containerpath> <image>```                      | Mount volume into a container                                              |
| ```docker container inspect -f "{% raw %}{{.Mounts}}{% endraw %}" <container>```             | Show volumes of a container                                                |
| ```docker volume create <name>```                                                            | Create volume                                                              |
| ```docker volume ls```                                                                       | Show volumes                                                               |
| ```docker volume inspect <name>```                                                           | Show low-level details of volume                                           |
| ```docker volume rm <name>```                                                                | Delete single volume                                                       |
| ```docker volume prune```                                                                    | Delete all unused volumes                                                  |
| ```docker run -v ${PWD}/<hostpath>:<containerpath>:ro <image>```                             | Old spelling: Usage like --mount                                           |
