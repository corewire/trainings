# Cheatsheet 7.1 - Debugging with Docker Compose

| Command                                             | Description                                                       |
|-----------------------------------------------------|-------------------------------------------------------------------|
| ```docker compose up <service...>```                | Create and start one or more services in the foreground           |
| ```docker compose up -d <service...>```             | Create and start one or more services in the background           |
| ```docker compose down```                           | Stop and delete all services                                      |
| ```docker compose build <service...>```             | Build one or more services                                        |
| ```docker compose logs <service...>```              | Output the logs of one or more services                           |
| ```docker compose ps```                             | Show the running containers                                       |
| ```docker compose top <service...>```               | List the running processes of one or more services                |
| ```docker compose rm <service...>```                | Delete one or more services                                       |
| ```docker compose exec <service> <command>```       | Execute command in running container with TTY                     |
| ```docker compose exec -T <service> <command>```    | Execute command in running container without TTY                  |
| ```docker compose run <service> <command>```        | Start container and execute command                               |

# Cheatsheet 7.2 - Debugging with Docker (native)

| Command                                             | Description                                                  |
|-----------------------------------------------------|--------------------------------------------------------------|
| ```docker run <image>```                            | Start container in foreground                                |
| ```docker run -d <image>```                         | Start a container in the background (`detached`)             |
| ```docker stop```                                   | Stop a running container                                     |
| ```docker rm```                                     | Delete a stopped container                                   |
| ```docker build .```                                | Build an image from the Dockerfile in the current folder     |
| ```docker logs <container>```                       | Show logs of a container                                     |
| ```docker ps```                                     | Show running containers                                      |
| ```docker top <container>```                        | List running processes of a container                        |
| ```docker exec -ti <container> sh```                | Start interactive shell in container                         |
| ```docker exec -ti <container> sh -c "<command>"``` | Run command from a shell in container                        |
| ```docker stats <container>```                      | Show resource utilization of a container                     |
| ```docker events <container>```                     | Show status events of a container                            |
| ```docker inspect <container>```                    | Show low-level information of a container                    |
| ```docker history <image>```                        | Show individual layers of an image                           |
