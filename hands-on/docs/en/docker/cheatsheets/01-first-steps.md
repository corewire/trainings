# Cheatsheet 1 - First steps

| Command                                                  | Description                                                  |
|----------------------------------------------------------|--------------------------------------------------------------|
| ```docker run <image>```                                 | Start container in foreground                                |
| ```docker run -d <image>```                              | Start container in the background (`detached`)               |
| ```docker run --name <container-name> <image>```         | Start container and give it a name                           |
| ```docker run -p <host-port>:<container-port> <image>``` | Start container with port mapping from host system           |
| ```docker run -i <image>```                              | Start container interactively                                |
| ```docker run --rm <image>```                            | Delete container immediately after commpletion               |
| ```docker ps```                                          | Show running containers                                      |
| ```docker ps -a```                                       | Show all containers (running, stopped)                       |
| ```docker logs <container>```                            | Show logs of a container                                     |
| ```docker logs -f <container>```                         | Show logs of a container and wait for new entries            |
| ```docker inspect <container>```                         | Show low-level information of a container                    |
| ```docker start <container>```                           | Start a stopped container                                    |
| ```docker stop <container>```                            | Stop a running container                                     |
| ```docker restart <container>```                         | Restart a container                                          |
| ```docker rm <container>```                              | Delete a stopped container                                   |
