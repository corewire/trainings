# Cheatsheet 5.1 - Docker Compose commands

Services are defined in the `docker-compose.yml`. This must be located in the current directory.
`<service...>` can be replaced by one or more services.
You can omit it completely, in which case the command is applied to all services.

| Command                                             | Description                                                       |
|-----------------------------------------------------|-------------------------------------------------------------------|
| ```docker compose up -d <service...>```             | Create and start one or more services                             |
| ```docker compose down```                           | Stop and delete all services and networks                         |
| ```docker compose down -v```                        | Stop and delete all services, networks and volumes                |
| ```docker compose build <service...>```             | Build one or more services                                        |
| ```docker compose pull <service...>```              | Pull the images of one or more services                           |
| ```docker compose up -d --build --pull always <service...>``` | Build or pull services; Create and start services       |
| ```docker compose logs <service...>```              | Output the logs from one or more services                         |
| ```docker compose ps```                             | Display the running containers                                    |
| ```docker compose top <service...>```               | List the running processes of one or more services                |
| ```docker compose create <service...>```            | Create one or more services                                       |
| ```docker compose start <service...>```             | Start one or more services                                        |
| ```docker compose stop <service...>```              | Stop one or more services                                         |
| ```docker compose rm <service...>```                | Delete one or more services                                       |
| ```docker compose exec <service> <command>```       | Execute command in container with TTY                             |
| ```docker compose exec -T <service> <command>```    | Execute command in container without TTY                          |


# Cheatsheet 5.2 - docker-compose.yml

All entries beginning with `cw-` are freely selectable names.
All lines that begin with `#` are comments.

## Structure

```yml
services:
  # create one or more services
  cw-webapp:
    # build points to a directory containing the Dockerfile. This
    # file will be build
    build: ./path/to/directory
    # define the image with optional tag. Will be pulled if not present
    image: <image>:<tag>
    # Most of the time, it's either 'build' or 'image'. They can be used in combination though. See: https://docs.docker.com/reference/compose-file/build/

    # optional: overwrite entrypoint
    entrypoint: /bin/bash
    # optional: overwrite command
    command: echo $MYSQL_USER

    # List of dependend services
    ## This service (cw-webapp) will be started after all services it
    ## depends on have started
    depends_on:
      - cw-database

    # List of port forwarding entries
    ports:
      -  5000:5000

    # List of volumes
    volumes:
      - cw-webapp-volume:/app/data/notes

    # List of networks
    networks:
      - cw-database-network

    # List of environment variables
    environment:
      - "MYSQL_USER=example-user"
      - "MYSQL_PASSWORD=..."

  # second service:
  cw-database:
    [...]

# (optional) create one or more managed volumes
volumes:
  cw-webapp-volume:

# (optional) create one or more networks
networks:
  cw-database-network:
```
