# Cheatsheet 6.1 - Create Image from Dockerfile

| Command                                                    | Description                                              |
|------------------------------------------------------------|----------------------------------------------------------|
| ```docker build .```                                       | Build an image from the Dockerfile in the current folder |
| ```docker build -f <file> .```                             | Build an image from <file\> in the current folder        |
| ```docker build -t <name> .```                             | Build an image and tag it with <name\>:latest            |
| ```docker build -t <name>:<tag> .```                       | Build an image and tag it with <name\>:<tag\>            |
| ```docker build --no-cache .```                            | Build an image without cache                             |
| ```docker build --target <stage> .```                      | Build an image up to stage <stage\>                      |
| ```docker build --build-arg MYARG=myvalue .```             | Build an image with the build argument MYARG             |
| ```docker tag <image> <name>:<tag>```                      | Tag an image <image\> with another tag <tag\>            |
| ```docker tag <image> <registry>/<project>/<name>:<tag>``` | Tag an image <image\> for a private registry             |
| ```docker image push <name>:<tag>```                       | Push a tag to the registry                               |
| ```docker image push --all-tags <name>```                  | Push all tags to the registry                            |
| ```docker image prune -a --filter "until=24h"```           | Delete all unused images older than 24h                  |
| ```docker system prune```                                  | Delete all unused Docker objects                         |
| ```docker run <image> <args>```                            | Start an image with arguments. Overwrites CMD            |
| ```docker run --entrypoint="<command>" <image> ```         | Start an image with a different entrypoint               |

# Cheatsheet 6.2 - Commands in the Dockerfile

| Command                                     | Description                                                                |
|---------------------------------------------|----------------------------------------------------------------------------|
| ```FROM <baseimage>```                      | Set baseimage                                                              |
| ```FROM <baseimage> as <name>```            | Set baseimage and give the stage a name                                    |
| ```RUN <command>```                         | Execute a command in the container                                         |
| ```ENTRYPOINT ["<arg1>", "<arg2>"]```       | Set the entry point. Is executed each time a container is started          |
| ```CMD ["<arg1>", "<arg2>"]```              | Set default arguments for the entrypoint                                   |
| ```WORKDIR <path>```                        | Set the workdir in the container for all subsequent commands               |
| ```USER <user/user-id>```                   | Set the user in the container for all subsequent commands                  |
| ```EXPOSE <port>```                         | Document required ports                                                    |
| ```COPY <source> <target>```                | Copy <source\> from the host to the container                              |
| ```COPY --from=<stage> <source> <target>``` | Copy <source\> from a previous stage to the current stage                  |
| ```ADD <source> <target>```                 | Copy <source\> to the container. Can be a host path or URL                 |
| ```ARG <name>[=<default>]```                | Define build argument with optional default value                          |
| ```ENV <name>=<default>```                  | Define environment variable in container                                   |
