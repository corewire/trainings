# Cheatsheet 2 - Docker Images

| Command                                         | Description                                             |
|-------------------------------------------------|---------------------------------------------------------|
| ```docker pull <image>```                       | Download an image `<image>` with the tag `@latest`      |
| ```docker pull <image>:<tag>```                 | Download an image `<image>` with the tag `<tag>`        |
| ```docker pull <server>/<repo>/<image>:<tag>``` | Download an image from a private registry               |
| ```docker image ls```                           | Show locally available images                           |
| ```docker login [--username] <server>```        | Log in to private Docker registry                       |
