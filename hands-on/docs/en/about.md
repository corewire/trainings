## Impressum
Angaben gemäß § 5 TMG

corewire GmbH

Erbprinzenstr. 18

79098 Freiburg im Breisgau

Handelsregister: HRB 728307

Registergericht: Amtsgericht Freiburg im Breisgau

### Vertreten durch:

Janosch Deurer

Stefan Möhrle

## Kontakt

Telefon: +49 (0) 761 88 78 153 – 0

Telefax: +49 (0) 761 88 78 153 – 9

E-Mail: team@corewire.de

## Umsatzsteuer-ID
Umsatzsteuer-Identifikationsnummer gemäß § 27 a Umsatzsteuergesetz: DE359313480

## Verbraucherstreitbeilegung/Universalschlichtungsstelle
Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.
