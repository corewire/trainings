# Exoscale - Certified Solution Architect

- [Exercises](exercises/01-virtual-machines/)

## Duration

2 Tage

## Target audience

- Software developers
- Software architects
- System administrators
- DevOps engineers

## Prerequisites

- (optional) Some experience with Terminal/Bash
- (recommended) Certified Sales Professional - Course

## Objectives

Understand and apply technical concepts by leveraging Exoscale features and products to build and run modern applications and cloud-native workloads.

## Education concept

The trainer presents the content with slides and live demos. In addition, the participants will apply and deepen their understanding in practical exercises between the individual chapters. The split between these is **60% theoretical content** and **40% practical exercises**.

## Course contents

This course provides the technical foundation skills to build cloud computing architectures with Exoscale platform products, which are introduced at the beginning. Technological insights and building necessary knowledge on the Exoscale platform products and features and a structured approach to them is the purpose of this Level 200 Certified Solution Architect - Course. It will help you learn the core technical concepts of computing, storage, automation, scaling, and backup. You will also dive into networking components and configuration. The course closes with architectural guidance on cloud computing infrastructure and a comprehensive overview of the most critical cloud topics. Including some tips around database as a service concludes the training because database functionality is often a significant component of cloud architecture implementations. 

You learn about the technology's basic concepts:

- Compute
- Cloud-Init
- Automation
- Scaling
- Traffic
- Storage
- Backup

The necessary networking themes:

- Networking Basics
- Switching/Routing
- Private Networks
- Load Balancing

And selected advanced topics: 

- Cloud Challenges
- Architecture
- Database


## Certification

This course should prepare you for the Certified Solution Architect - Exam.