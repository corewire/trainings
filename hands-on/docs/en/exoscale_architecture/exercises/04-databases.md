# Managed Databases

!!! goal "Goal"
    - In this project you'll create a database and learn how to connect to it.

!!! tipp "Hints"

    - Try to solve the following tasks on your own without using the hints.
    - If you get stuck, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1 - Create a Database

- Create a postgres database with the UI or the CLI.

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `DBAAS` in the left sidebar
    - Click on `...` in the upper right corner
    - Click on `Add PostgreSQL Service`
    - Choose a name for your database, for example `my-first-db`
    - Click on `Create`

## Task 2 - Make the Database accessible

- Switch to your VSCode terminal
- Get the public IP of your VSCode instance with the CLI:

```
curl icanhazip.com
```

<!-- Other options for cur can be found here: https://linuxconfig.org/how-to-use-curl-to-get-public-ip-address -->

- Add the public IP of your VSCode instance to the database firewall with the UI or the CLI.

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `DBAAS` in the left sidebar
    - Click on the database you just created
    - On the left side click on `IP Filter`
    - Add the public IP of your VSCode instance to the field and press `+`

## Task 3 - Connect to the Database

- Connect to the database with the CLI:

```
pgsql <uri connection string>
```

??? help "Hints"
    - You'll find the connection string in the Exoscale UI in the database details.
    - The connection string looks like this: `postgresql://<username>:<password>@<host>:<port>/<database>`

- Exit the database shell with `\q` and press `Enter`.

## Task 4 - Create a Demoapp VM

- Create a Security Group with the CLI:

```
exo compute security-group add demoapp-sg
exo compute security-group rule add demoapp-sg --port 22 --network 0.0.0.0/0
exo compute security-group rule add demoapp-sg --port 80 --network 0.0.0.0/0
```

- Create a new VM with the CLI. Replace `<SSH Key name>` with the name of your SSH key and `<vm-name>` with a name for your VM:

```
exo compute instance create \
  --disk-size 10 \
  --instance-type standard.micro \
  --template "Linux Ubuntu 22.04 LTS 64-bit" \
  --security-group demoapp-sg --ssh-key <SSH Key name> <vm-name>
```

- Connect to the VM

```
ssh ubuntu@<ip>
```

- Become sudo

```
sudo su
```

- Install docker

```
apt update && apt install -y docker.io
```

- Run the demoapp with the DB credentials. Replace `<host>`, `<user>`, `<password>` and `<db name>` with the credentials of your database:

```
docker run -p 80:5000 --env "DATABASE_HOST=<host>" --env "DATABASE_PORT=21699" --env "DATABASE_USER=<user>" --env "DATABASE_USER_PASSWORD=<password>" --env "DATABASE_NAME=<db name>" corewire/docker-demoapp:exoscale
```

- Open the demoapp in your browser with the public IP of your VM.
- There is still an error message for the database. We'll fix that in the next task.

## Task 5 - Make the DB accessible from the VM

- Add the public IP of your VM to the database firewall with the UI or the CLI.

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `DBAAS` in the left sidebar
    - Click on the database you just created
    - On the left side click on `IP Filter`
    - Add the public IP of your VM to the field and press `+`

- Open the demoapp in your browser
- The demoapp should no longer show errors for the database. You can ignore the error for S3 for now.
