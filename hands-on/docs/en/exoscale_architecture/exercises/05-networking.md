# Networking

!!! goal "Goal"
    - In this project you'll create a database and learn how to connect to it.

!!! tipp "Hints"

    - Try to solve the following tasks on your own without using the hints.
    - If you get stuck, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Preparation - Configure VSCode

In this hands-on, we will look at connecting two VMs with private networks. This is easiest if the ssh connection to the two VMs are open side by side in split terminal windows. To do this, open VSCode, open a terminal, and click ![Split Button](./img/split_button.png). Your VSCode should look more or less like this:

![Split View](./img/split_cmd.png)

## Task 1 - Create the two VMs

- Create a security group with the CLI

```
exo compute security-group add network-handson-sg
exo compute security-group rule add network-handson-sg --port 22 --network
```

- Set a variable for your existing ssh key

```
SSH_KEY_NAME=<your ssh key name>
```

- Create the two VMs with the CLI

``` 
exo compute instance create \
  --disk-size 10 \
  --instance-type standard.micro \
  --template "Linux Ubuntu 22.04 LTS 64-bit" \
  --security-group network-handson-sg --ssh-key "$SSH_KEY_NAME" network-handson-vm1
```

- Wait for the first vm to be ready and then create the second one

``` 
exo compute instance create \
  --disk-size 10 \
  --instance-type standard.micro \
  --template "Linux Ubuntu 22.04 LTS 64-bit" \
  --security-group network-handson-sg --ssh-key "$SSH_KEY_NAME" network-handson-vm2
```

## Task 2 - Connect the two VMs

- Connect to the first VM in the left terminal

```
ssh ubuntu@<ip>
```

- Connect to the second VM in the right terminal

```
ssh ubuntu@<ip>
```

## Task 3 - Test initial network connection

- In the left terminal, ping the second VM

```
ping -c 3 <ip>
```

The VMs can not reach each other yet. We know already how to open up the security groups to make this work, however, in this hands-on we will first use private networks.

## Task 4 - Create a managed private network

- Create a managed private network with the UI or the CLI connecting the two VMs that we just created

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `Compute` in the left sidebar
    - Click on `Private Networks` in the left sidebar
    - Click on `Add`
    - Select `DE-FRA-1` as the zone
    - Give the network a name
    - Select `Managed` as the type
    - Leave the default ip configuration
    - Click on `Create`
    - Select the network that you just created
    - Click on `Attach`
    - Select the two VMs that we just created

## Task 5 - Enable the private IPs

The VMs we are using are not configured to use DHCP. Therefore we need to enable it manually.

- In both VMs, run

```
sudo su
dhclient eth1
```

## Task 5 - Test the private network

- In the right terminal run the following command to monitor all incoming icmp traffic

```
tcpdump -i eth1 icmp
```

- In the left terminal, ping the second VM, use the private IP that was assigned in the managed private network

```
ping -c 3 <ip>
```

- You should see the ping requests in the right terminal
- Stop the tcpdump in the right terminal with `Ctrl + C`

## Task 6 - Create an unmanaged private network

- Create an unmanaged private network with the UI or the CLI connecting the two VMs

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `Compute` in the left sidebar
    - Click on `Private Networks` in the left sidebar
    - Click on `Add`
    - Select `DE-FRA-1` as the zone
    - Give the network a name
    - Select `Manual` as the type
    - Click on `Create`
    - Select the network that you just created
    - Click on `Attach`
    - Select the two VMs that we just created

## Task 7 - Configure IP addresses for the VMs

- In both VMs, run

```
ip a
```

The VMs now have a third network interface. This interface is not configured yet.

- In the left terminal run:

```
ip addr add 192.168.0.1/24 dev eth2
ip link set eth2 up
```

- In the right terminal run:

```
ip addr add 192.168.0.2/24 dev eth2
ip link set eth2 up
```

- Start the tcpdump in the right terminal again

```
tcpdump -i eth2 icmp
```

- In the left terminal, ping the second VM

```
ping -c 3 192.168.0.2
```

- You should see the ping requests in the right terminal
- Stop the tcpdump in the right terminal with `Ctrl + C`


## Task 8 - Open the security groups

- Start the tcpdump in the right terminal again, this time for the eth0 interface

```
tcpdump -i eth0 icmp
```

- In the left terminal, ping the second VM

```
ping -c 3 <ip>
```

- This will not work yet, because the security groups are not open
- Open the security groups for icmp traffic from all instances in the same security group

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `Compute` in the left sidebar
    - Click on `Security Groups` in the left sidebar
    - Click on the security group that you created in task 1
    - Click on `Add Rule`
    - Click on `Add Custom Rule`
    - Select `ICMP` as the protocol
    - Select `Security Group` as the source type
    - Select the security group that you created in task 1
    - Click on `Create`

- In the left terminal, ping the second VM

```
ping -c 3 <ip>
```

- You should now see the ping requests in the right terminal