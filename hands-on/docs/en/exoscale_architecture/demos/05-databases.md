# Databases

## Start a Postgres instance

```
exo dbaas create --pg-version 14 pg startup-4 demoapp-db-pg-14
```

- Show the DB in the UI
- Add an IP filter for `0.0.0.0/0`

## Connect to the DB

- Connect to the DB with the CLI

```
pgsql <uri connection string>
```

- The next few steps are just to show that the DB is already usable
- Create a table

```
CREATE TABLE demoapp (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);
```

- Insert some data

```
INSERT INTO demoapp (name) VALUES ('Exoscale');
```

- Exit the postgres client

## Create a virtual machine with the CLI

- Create a security group with the CLI

```
exo compute security-group add demoapp-sg
exo compute security-group rule add demoapp-sg --port 22 --network 0.0.0.0/0
exo compute security-group rule add demoapp-sg --port 80 --network 0.0.0.0/0
```

- Create a VM with the CLI or use an existing one

```
exo compute instance create \
  --disk-size 10 \
  --instance-type standard.micro \
  --template "Linux Ubuntu 22.04 LTS 64-bit" \
  --security-group demoapp-sg --ssh-key <SSH Key name> <vm-name>
```

- Connect to the VM

```
ssh ubuntu@<ip>
```

- Become sudo

```
sudo su
```

- Install docker

```
apt update && apt install -y docker.io
```

- Run the demoapp

```
docker run -p 80:5000 corewire/docker-demoapp:exoscale
```

- Run the demoapp with the DB credentials

```
docker run -p 80:5000 --env "DATABASE_HOST=<host>" --env "DATABASE_PORT=21699" --env "DATABASE_USER=<user>" --env "DATABASE_USER_PASSWORD=<password>" --env "DATABASE_NAME=<db name>" corewire/docker-demoapp:exoscale
```

- Show the demoapp in the browser

## DB Upgrade

TODO: This part is not finished yet, skip to Cleanup for now

- Upgrade the DB by forking

```
 exo dbaas create pg startup-4 demoapp-db-pg-15 --pg-fork-from=demoapp-db-pg-14
```



## Cleanup

- Do this while the students are doing the exercises
- Delete the VM

```
exo compute instance delete <vm-name>
```

- Delete the security group

```
exo compute security-group delete demoapp-sg
```

- Delete the DB

```
exo dbaas update demoapp-db-pg-14 --termination-protection=false
exo dbaas update demoapp-db-pg-15 --termination-protection=false
exo dbaas delete demoapp-db-pg-14
exo dbaas delete demoapp-db-pg-15
```