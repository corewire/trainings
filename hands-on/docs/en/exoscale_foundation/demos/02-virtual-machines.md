# Virtual Machines

## Manual Setup

### Create VM

- Go trough the steps of creating a VM in the UI
- Show the different options
- When you get to the SSH key part, show how to create a new key pair

```
ssh-keygen -t ed25519 -a 100
```

<https://security.stackexchange.com/a/144044>


- When you get to the security group part, show how to create a new security group with port 80 and 22 open worldwide.

### SSH into VM

- Show how to SSH into the VM

### Nginx

- Install nginx

```
sudo apt install nginx
```

- Show the nginx welcome page in the browser

### Docker Demoapp

- Install docker

```
sudo apt update
sudo apt install docker.io
```

- Stop nginx

```
sudo systemctl stop nginx
```

- Run the demoapp

```
sudo docker run -p 80:5000 corewire/docker-demoapp:latest
```

### Slides

- Show the slides for compute
- Show the slides for Cloud-init

## Cloud-init

### Create VM

- Go trough the steps of creating a VM in the UI
- Use the following cloud-init script:

```
#!/bin/bash
apt update
apt install -y docker.io
docker run -d -p 80:5000 corewire/docker-demoapp:latest
```

- Show the demoapp in the browser

