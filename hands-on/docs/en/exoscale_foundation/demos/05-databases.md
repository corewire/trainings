# Databases

## Start a Postgres instance

- Get the IP of your vscode instance

```
curl icanhazip.com
```

- Start a Postgres instance with the in the UI
- Add an IP filter for your vscode instance
- Show the DB in the UI
- Show the commandline options with:

```
exo dbaas create pg --help-pg
```


## Connect to the DB

- Connect to the DB with the CLI

```
psql <uri connection string>
```

- The next few steps are just to show that the DB is already usable
- Create a table

```
CREATE TABLE demoapp (
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL
);
```

- Insert some data

```
INSERT INTO demoapp (name) VALUES ('Exoscale');
```

- Read the data

```
SELECT * FROM demoapp;
```

- Exit the postgres client

## Create a virtual machine with the CLI

- Create a security group with the CLI

```
exo compute security-group add demoapp-sg
exo compute security-group rule add demoapp-sg --port 22 --network 0.0.0.0/0
exo compute security-group rule add demoapp-sg --port 80 --network 0.0.0.0/0
```

- Create a VM with the CLI or use an existing one

```
exo compute instance create \
  --disk-size 10 \
  --instance-type standard.micro \
  --template "Linux Ubuntu 22.04 LTS 64-bit" \
  --security-group demoapp-sg --ssh-key <SSH Key name> <vm-name>
```

- Connect to the VM

```
ssh ubuntu@<ip>
```

- Become sudo

```
sudo su
```

- Install docker

```
apt update && apt install -y docker.io
```

- Run the demoapp

```
docker run -p 80:5000 corewire/docker-demoapp:exoscale
```

- Run the demoapp with the DB credentials

```
docker run -p 80:5000 --env "DATABASE_HOST=<host>" --env "DATABASE_PORT=21699" --env "DATABASE_USER=<user>" --env "DATABASE_USER_PASSWORD=<password>" --env "DATABASE_NAME=<db name>" corewire/docker-demoapp:exoscale
```

- Show the demoapp in the browser

## Cleanup

- Do this while the students are doing the exercises
- Delete the VM

```
exo compute instance delete <vm-name>
```

- Delete the security group

```
exo compute security-group delete demoapp-sg
```

- Delete the DB

```
exo dbaas update demoapp-db --termination-protection=false
exo dbaas delete demoapp-db
```