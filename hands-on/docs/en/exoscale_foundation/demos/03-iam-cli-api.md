# IAM CLI API

### Roles and Users

- Show how to create a role
- Create a role **Compute Admin Access** clicking through the web interface
- Create a user **Compute Admin** with the role **Compute Admin Access**
- Login with that user and show that you can start an instance but not use SOS
- Create a role **Compute Read Only Access** with the following permissions:

```
{
  "default-service-strategy": "deny",
  "services": {
    "compute": {
      "type": "rules",
      "rules": [
        {
          "expression": "operation.startsWith('list') || operation.startsWith('get')",
          "action": "allow"
        }
      ]
    }
  }
}
```

- Create a user **Compute Read Only** with the role **Compute Read Only Access**
- Login with that user and show that you can list instances but not start or stop them


### API Keys

- Show how to create an API key with Compute Read Only Access.

## CLI

- Configure the CLI:

```
exo config
```

We will explore the exoscale cli now. Show the help for the CLI on the way for multiple layers.

- List the running machines with the CLI:

```
exo compute instance list
```

- List the Security Groups with the CLI:

```
exo compute security-group list
```

- Show one of the security groups with the CLI:

```
exo compute security-group show <id>
```

- Show the json output of the security group:

```
exo compute security-group show <id> --output-format json
```

- Show the json output with jq:

```
exo compute security-group show <id> --output-format json | jq
```

- Filter the output with jq:

```
exo compute security-group show <id> --output-format json | jq .ingress_rules
```

- Try to scale an instance:

```
exo compute instance scale <id> tiny
```

- Create an IAM Key with full compute access:
- Add the key to the cli config:

```
exo config add
```
Make it the new default profile.

- Try to scale an instance:

```
exo compute instance scale <id> tiny
```

- Stop the instance:
  
  ```
  exo compute instance stop <id>
  ```

- Scale the instance:

```
exo compute instance scale <id> tiny
```

- Start the instance:

```
exo compute instance start <id>
```

- Create a new instance, show on the way how to get the details for the command:

```
exo compute instance create clitest --disk-size 10 --instance-type standard.micro --security-group allow-http-and-ssh --ssh-key Janosch
```


## API

Show the API documentation: <https://community.exoscale.com/api/>

Explain that this can be used in all programming languages and that there are
also SDKs available for python and go.







