# Simple Object Storage

In this demo, we will create a simple object storage bucket and upload a files to it.

## Single File Upload

- Create a new bucket in the UI
- Upload a file to the bucket
- Show the file in the browser, it will not be accessible
- Make the file public with a canned acl on the file
- Show the file in the browser, it will be accessible

## Website upload

- Download a website to the vscode instance
  - This can be the corewire website
- Explain, that folder upload is not possible via the UI
- Upload the website to the bucket

```
BUCKET_NAME=<bucket-name>
```

```
exo storage upload --acl public-read -r <directory>/ sos://${BUCKET_NAME}
``` 

- Show the website in the browser
- Explain the broken css
- Fix the css by setting the correct content type

```
exo storage headers add sos://${BUCKET_NAME}/styles.css --content-type "text/css; charset=utf-8"
```

```
exo storage headers add sos://${BUCKET_NAME}/fonts/fonts.css --content-type "text/css; charset=utf-8"
```

- Show the website in the browser again
- Fix the broken images by setting the correct content type

```
exo storage list --output-format=json -r ${BUCKET_NAME} | jq .[].name | grep .svg | xargs -I % exo storage headers add sos://$BUCKET_NAME/% --content-type image/svg+xml
```

- Show the website in the browser again

## Docker Demoapp

- Start a new instance
- Login via ssh
- Install docker:

```
sudo apt-get update
sudo apt-get install docker.io
```

- Start the demo app

```
sudo docker run -p 80:5000 corewire/docker-demoapp:latest
```

- Show the demo app in the browser
- Explain that we now want to attach the app to S3
- Create a bucket called `docker-demoapp-backup`
- Create a new IAM key with S3 access to the bucket:

```
{
  "default-service-strategy": "deny",
  "services": {
    "sos": {
      "type": "rules",
      "rules": [
        {
          "expression": "parameters.bucket == 'docker-demoapp-backup' && operation in ['list-objects', 'get-object', 'put-object', 'head-bucket']",
          "action": "allow"
        }
      ]
    }
  }
}
```

- Run the docker container with the new env var

```
sudo docker run -p 80:5000  -e S3_ENDPOINT=https://sos-de-fra-1.exo.io -e S3_BUCKET=docker-demoapp-backup -e S3_ACCESS_KEY= -e S3_SECRET_KEY= corewire/docker-demoapp:exoscale
```

- Show the demo app in the browser
- Create some data in the app
- Backup the data to the bucket
- Show the data in the bucket
- Restart the container
- Restore the data from the bucket
