# Intro and UI

## Intro

The idea of this demo is to give a first impression of the Exoscale UI. This is so that the participants can connect the things you are talking about in the presentation with the UI. 

This is intended to be very short demo. It should not take more than 5 minutes.