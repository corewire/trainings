# Virtual Machines

!!! goal "Goal"
    - In this project you'll get started with Virtual Machines, learn how to start them and run your first application on them.
    - Take your time and look around.

!!! tipp "Hints"

    - Try to solve the following tasks on your own without using the hints.
    - If you get stuck, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1 - Create an Exoscale account

- Create an Exoscale account with the link provided by your instructor.

## Task 2 - Login to the training environment

- Your machine for the training is reachable at `code-{NUMBER}.{{ domain }}`.
Replace `{NUMBER}` with the number that was assigned to you.
  - Example for the number 5: `code-5.{{ domain }}`.
  - Example for the number 11: `code-11.{{ domain }}`.
- Enter the password that was given to you.
- You now have access to your personal machine for the training.

## Task 3 - Create an SSH key

To be able to connect to your VMs later on, you'll need an SSH key. In the VSCode instance you'll find a terminal in the upper left corner. Open it and run the following command:

```bash
ssh-keygen -t ed25519 -a 100
```

This will create a new SSH key pair. You'll find the public key in `~/.ssh/id_ed25519.pub`. Print the contents of your public key to the terminal with the following command:

```bash
cat ~/.ssh/id_ed25519.pub
```

- Copy the output of the command 
- Got to the Exoscale UI and create a new SSH key with it

We will use this key later on when creating our VMs.

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `Compute` in the left sidebar
    - Click on `SSH Keys` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your key and paste the contents of your public key into the `Public Key` field

## Task 4 - Create a Security Group

A security group is a set of firewall rules that can be applied to multiple VMs. Create a security group with two rules. One rule that allows incoming traffic on port 80 (HTTP) and another rule that allows incoming traffic on port 22 (SSH). This will allow us to connect to our VMs via SSH and to access our application via HTTP.

We will use this security group in the next task when creating our VMs.

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `Compute` in the left sidebar
    - Click on `Security Groups` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your security group, for example `allow-http-and-ssh`
    - Click on `Create Group`
    - Click on the security group you just created
    - Click on `Add Rule` in the upper right corner
    - Click on `SSH`
    - Click on `Add Rule` in the upper right corner
    - Click on `Add Custom Rule`
    - Choose `TCP` as protocol
    - Enter `80` as start port
    - Enter `80` as end port
    - Click on `Create`


### Task 5 - Create a VM

- Go to the Exoscale UI
- Click on `Compute` in the left sidebar
- Click on `Instances` in the left sidebar
- Click on `Add` in the upper right corner
- Choose a name for your VM, for example `my-first-vm`
- Choose `Linux Ubuntu 22.04 LTS 64-bit` as template
- Choose `DE-FRA-1` as zone
- Choose `Micro` as instance type
- Choose `10GB` as disk size
- Choose the ssh key that you created as key pair 
- Choose the security group that you created
- Click on `Create`

### Task 6 - Connect to your VM via SSH

- Copy the `SSH command` from the VM details page
- Paste the command into the terminal of your VSCode instance
- You are now connected to your VM via SSH

### Task 7 - Install nginx

- Run the following command to install nginx:

```bash
sudo apt install nginx
```

- Enter the ip of the VM into your browser
- You should see the nginx welcome page
- Stop nginx with the following command:

```bash
sudo systemctl stop nginx
```

### Task 8 - Run a Docker container

- Run the following command to install docker:

```bash
sudo apt update
sudo apt install docker.io
```

- Run the following command to start the demoapp:

```bash
sudo docker run -p 80:5000 corewire/docker-demoapp:latest
```

- Enter the ip of the VM into your browser
- You should see the demoapp


## Task 9 - Create a VM with Userdata

- Go to the Exoscale UI
- Click on `Compute` in the left sidebar
- Click on `Instances` in the left sidebar
- Click on `Add` in the upper right corner
- Choose a name for your VM, for example `my-second-vm`
- Choose `Linux Ubuntu 22.04 LTS 64-bit` as template
- Choose `DE-FRA-1` as zone
- Choose `Micro` as instance type
- Choose `10GB` as disk size
- Choose the ssh key that you created as key pair
- Choose the security group that you created
- Enter the following script into the `Userdata` field:

```bash
#!/bin/bash
apt update
apt install -y nginx
systemctl start nginx
```

- Click on `Create`
- Enter the ip of the VM into your browser
- After a 1-2 minutes you should see the nginx welcome page