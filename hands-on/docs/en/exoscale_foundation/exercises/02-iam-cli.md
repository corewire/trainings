# IAM and CLI

!!! goal "Goal"
    - In this project you'll create your first API key and learn how to use it in the Exoscale CLI.

!!! tipp "Hints"

    - Try to solve the following tasks on your own without using the hints.
    - If you get stuck, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1 - Create a role

- Create an Exoscale role with full access to the Compute API.

??? help "Hints"

    - Go to the Exoscale UI
    - Click on `IAM` in the left sidebar
    - Click on `Roles` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your role, for example `compute-full-access`
    - Change the default service strategy to `DENY`
    - Click on `Add Service Class`
    - Choose `Compute` as service class
    - Leave the default service strategy for compute at `ALLOW`
    - Click on `Create`

## Task 2 - Create an API key

- Create an API key with the role you created in the previous task.
- The credentials shown after creating the API key are your API key and secret key. You'll need them in the next task.

??? help "Hints"

    - Click on `Keys` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your API key, for example `Compute Admin`
    - Choose the role you created in the previous task
    - Click on `Create`

## Task 3 - Configure the CLI

- Configure the CLI with the API key you created in the previous task. **Important:** Set a name for your config, otherwise your config will not be enabled.

```bash
exo config
```

- You'll be asked for your API key and secret key. Enter the credentials you got after creating the API key in the previous task.
- You'll be asked for a name for your config. Choose a name for your config, for example `compute-admin`.
- You'll be asked for a default zone. Choose `de-fra-1` as default zone.

## Task 4 - List running instances

- List the running instances with the CLI:

```
exo compute instance list
```

Look around and try out different cli commands.

## Task 5 - Create a Security Group

- Add a new secuiry group with the default settings named `cli-created-sg`.

??? help "Hints"
    ```
    exo compute security-group add cli-created-sg
    ```

## Task 6 - Create a VM

In this task you'll create a VM with the CLI. For that we need the following information:

- The name of the SSH key you created in the previous project
- The name of the security group you created in the previous task

Get the name of the SSH key with the CLI.

??? help "Hints"
    ```
    exo compute ssh-key list
    ```

- Create the instance. Replace `<SSH Key name>` and `<Security Group name>` with the names of the SSH key and security group you created in the previous tasks:

```
exo compute instance create \
  --disk-size 10 \
  --instance-type standard.micro \
  --template "Linux Ubuntu 22.04 LTS 64-bit" \
  --security-group <Security Group name> --ssh-key <SSH Key name> <vm-name>
```

- Try to connect to the machine via ssh. It will **not work** because of the security group configuration. Replace `<IP>` with the IP of your machine:

```
ssh ubuntu@<IP>
```

- Add a rule to the security group that allows incoming traffic on port 22 (SSH):



??? help "Hints"
    ```
    exo compute security-group rule add cli-created-sg --port 22 --network 0.0.0.0/0
    ```

- Try to connect to the machine via ssh again. It should work now:
    
```
ssh ubuntu@<IP>
```






