# Simple Object Storage

!!! goal "Goal"
    - In this project you'll learn how to work with the Exoscale Object Storage.

!!! tipp "Hints"

    - Try to solve the following tasks on your own without using the hints.
    - If you get stuck, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Manual File upload

### Task 1 - Create a new bucket

- Create a new bucket in the UI

??? help "Hints"
      - Go to the Exoscale UI
      - Click on `Storage` in the left sidebar
      - Click on `Add` in the upper right corner
      - Choose a name for your bucket, for example `my-first-bucket-<your-name>`
      - The bucket name must be unique across all exoscale accounts so you might
      need to add some random characters
      - Click on `Add`

### Task 2 - Upload a file

- Download the cloud image from the demo at `https://sos-de-fra-1.exo.io/corewire/Cloud-1.webp`
- You can also work with a different image if you want
- Upload the file to the bucket

??? help "Hints"
      - Go to the Exoscale UI
      - Click on `Storage` in the left sidebar
      - Click on the bucket you created in the previous task
      - Click on `Upload Object` in the upper right corner
      - Choose the file you downloaded in the previous step
      - Click on `Upload`
      - Choose the file that you downloaded

### Task 3 - Make the file public

- Look at the file in the browser, it will not be accessible
- Make the file publicly available with a quick acl on the file
- Look at the file in the browser again, it should now be accessible

??? help "Hints"
      - Go to the Exoscale UI
      - Click on `Storage` in the left sidebar
      - Click on the bucket you created in the previous task
      - Click on the file you uploaded in the previous task
      - Click on the tab `ACL`
      - Click on `Add Canned URL`
      - Click on `Public Read`

### Task 4 - Delete the file

- Delete the file you just oploaded

??? help "Hints"
      - Go to the Exoscale UI
      - Click on `Storage` in the left sidebar
      - Click on the bucket you created in the previous task
      - Click on the file you uploaded in the previous task
      - Click on `Delete` in the upper right corner
      - Confirm the deletion


### Task 5 - Upload the file via CLI

- Download the file in the vscode instance with:

```shell
wget https://sos-de-fra-1.exo.io/corewire/Cloud-1.webp
```
- Upload the file to the bucket with the CLI
- Make the file public with the CLI
- Look at the file in the browser

??? help "Hints"
    - Upload the file with:

    ```shell
    exo storage upload <file> sos://<bucket-name>
    ```

    - Make the file public with:

    ```shell
    exo storage setacl sos://<bucket-name>/<file-name> public-read 
    ```

## Docker Demoapp


### Task 6 - Create a VM

- Go to the Exoscale UI
- Click on `Compute` in the left sidebar
- Click on `Instances` in the left sidebar
- Click on `Add` in the upper right corner
- Choose a name for your VM, for example `docker-demoapp-vm`
- Choose `Linux Ubuntu 22.04 LTS 64-bit` as template
- Choose `DE-FRA-1` as zone
- Choose `Micro` as instance type
- Choose `10GB` as disk size
- Choose the ssh key that you created as key pair 
- Choose the security group that you created
- Click on `Create`

### Task 7 - Run the Docker Demoapp

- Login to your machine via ssh

??? help "Hints"
    - Click on `Compute` in the left sidebar
    - Click on the instance you created in the previous task
    - Copy the ssh command
    - Paste the ssh command into the terminal of your vscode instance

- Install docker:

```
sudo apt-get update
sudo apt-get install docker.io
```

- Start the demo app

```
sudo docker run -p 80:5000 corewire/docker-demoapp:exoscale
```

- Open the demo app in the browser

??? help "Hints"
    - Click on `Compute` in the left sidebar
    - Click on the instance you created in the previous task
    - Copy the ipv4 address
    - Enter the ip of the VM into your browser
    - You should see the demo app

### Task 8 - Create a backup bucket

- Create a bucket called `docker-demoapp-backup`

??? help "Hints"
    - Go to the Exoscale UI
    - Click on `Storage` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your bucket, for example `docker-demoapp-backup`
    - Click on `Add`

### Task 9 - Create an IAM key

- Create a new IAM role with S3 access to the bucket:

```
{
  "default-service-strategy": "deny",
  "services": {
    "sos": {
      "type": "rules",
      "rules": [
        {
          "expression": "parameters.bucket == 'docker-demoapp-backup' && operation in ['list-objects', 'get-object', 'put-object', 'head-bucket']",
          "action": "allow"
        }
      ]
    }
  }
}
```

??? help "Hints"
    - Go to the Exoscale UI
    - Click on `IAM` in the left sidebar
    - Click on `Roles` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your role, for example `docker-demoapp-backup`
    - Click on `Advanced Mode`
    - Copy the policy from above

- Create a new IAM key with with the role you created

??? help "Hints"
    - Click on `Keys` in the left sidebar
    - Click on `Add` in the upper right corner
    - Choose a name for your key, for example `docker-demoapp-backup`
    - Choose the role you created in the previous task
    - Click on `Create`

- Save the access key and secret key you will need them in the next task

### Task 10 - Connect the app to the bucket

- Run the docker container with the new env var

```
sudo docker run -p 80:5000  -e S3_ENDPOINT=https://sos-de-fra-1.exo.io -e S3_BUCKET=docker-demoapp-backup -e S3_ACCESS_KEY=<key> -e S3_SECRET_KEY=<key> corewire/docker-demoapp:exoscale
```

- View the demo app in the browser
- The Bucket should now be connected

### Task 11 - Backup and restore the data

- Create some notes in the app
- Backup the data to the bucket
- Look at the data in the bucket
- Restart the container
- Restore the data from the bucket
