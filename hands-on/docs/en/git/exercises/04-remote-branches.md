# Remote Branches

!!! goal "Goal"
    - In this Project you will learn how to work with branches and how to share them with other people.

!!! tipp "Hints"
    - Try to solve the following tasks with the help of the [slides](../../3-branches/#/) and the
    [Cheatsheets](../cheatsheets/3-branches.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- In Gitlab, search for the project `remote-branches` and clone it.
    Please ensure you have left any other git projects before cloning the repository.

??? help "Solution (Click on the arrow if you are stuck)"
    - Search in Gitlab in the left upper corner `Menu->Projects` for the project `hands-on/remote-branches`.
    - Copy the link from `Clone with HTTPS`.
    - Switch back to the terminal of the VSCode instance.
    - Ensure the current working directory is `/root/workspace`.
      Therefore move upwards from previous repositories with `cd ..` (alternatively it is possible to switch directly into the directory with: `cd /root/workspace`).
    - Clone the project with the command `git clone {URL}`.

## Task 2

- Create a new branch with your username and switch to it.

??? help "Solution (Click on the arrow if you are stuck)"
    - Switch into the new cloned project with `cd remote-branches`.
    - Create a new branch with `git switch -c user-{NUMBER}`.
    With this command you will automatically switch to this new branch.
    - With `git status` you can see and check that you are actually switched to the new branch.

## Task 3

- Commit a new file with a creative text inside.

??? help "Solution (Click on the arrow if you are stuck)"
    - Like in the previous hands-on, create a file with creative text inside and commit these changes.

## Task 4

- Push the new branch to the server.

??? help "Solution (Click on the arrow if you are stuck)"
    - Push the new branch with `git push`.
    - There will be an error as your branch is only known in the local repository
    and the server is not knowing where you want to push the branch to.
    - You can copy the right command from the output of the error.
    This command will tell git that you want to push your branch to `origin`.

## Task 5

- Get the branches from your colleagues and inspect some of the changes they made.

??? help "Solution (Click on the arrow if you are stuck)"
    - Execute the command `git fetch`. The output will tell you when there is a new branch available on `origin`.
    - Another option is to list all branches with `git branch -a`.
    - Check out a branch from someone else with `git switch {branch}`.
      Example: Remote has a branch `test`.

      ```shell
      $ git branch -a
      main
      remotes/origin/test
      ```

      Check out `test` with `git switch test`. Git will then create the local
      branch `test` and will track changes from `origin/test`.
    - Look at the creative contents in the filebrowser.
    - Repeat those steps for other participants.
    Remember to use `git fetch` from time to time. Maybe there are some new unseen branches for you.
