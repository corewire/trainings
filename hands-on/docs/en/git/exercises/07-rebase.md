# Rebase

!!! goal "Goal"
    - This project is about rebase. Rebasing is an alternative to merging.

!!! tipp "Hints"
    - Try to solve the following tasks with the help of the [Slides](../../4-branches-2/#/)
    and the [Cheatsheets](../cheatsheets/3-branches.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- Leave the repository `conflict` and create a new one:

   ```bash
   cd ..           # Leave the directory conflict
   mkdir rebase    # Create a new directory rebase
   cd rebase       # Change into that directory
   git init        # Create a new Git repository
   ```

??? help "Solution (Click on the arrow if you are stuck)"
    - Leave the directory `conflict` with the command `cd ..`.
    - Run the command `pwd` and you should see `/root/workspace` as a result.
    - If you got a different result, run `cd /root/workspace` to change into the right directory.
    - When you are in the right directory, run `mkdir rebase`.
    - Change into the newly created directory with `cd rebase`.
    - Run `git init` which creates a new git repository in the current directory.


## Task 2

- Create the following graph.

![Graph](tikz/rebase_graph.svg)

Each green node represents a commit. Proceed as follows for each commit/node:

1. Create the branch `us-42`.

    ??? help "Solution (Click on the arrow if you are stuck)"
        ```
        git switch -c us-42
        ```

1. Create a file with the node name as name (if it is not a merge commit). You can do this via the sidebar, or use the command `touch <filename>` in the terminal.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the first commit `C1`:
        ```
        touch C1
        ```

1. Add the file to the staging area and commit your change with the node name as the commit message.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the first commit `C1`:
        ```
        git add C1
        git commit -m "C1"
        ```

1. If more than one branch descends from a node, add a new branch if necessary.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the third commit `C3` starting from commit `C2`:
        ```
        git switch -c testing
        ```
        Then the third commit `C3` can be created. And then the branch can be changed.
        ```
        touch C3
        git add C3
        git commit -m "C3"
        git switch us-42
        ```

1. Complete the graph. In VSCode, it should then look like this or similar in the Git Graph plugin:

    ![](img/rebase_1.png)

??? help "Solution (Click on the arrow if you are stuck)"
    The complete solution is as follows:
    ```
    git switch -c us-42
    touch C1
    git add C1
    git commit -m "C1"
    touch C2
    git add C2
    git commit -m "C2"
    git switch -c testing
    touch C3
    git add C3
    git commit -m "C3"
    git switch us-42
    touch C4
    git add C4
    git commit -m "C4"
    ```

## Task 3

Do a rebase, such that the changes of `C3` are applied to `C4`. The branch `us-42` should point on the new `C3` commit.

In VSCode, it should then look like this or similar in the Git Graph plugin:
![](img/rebase_2.png)

??? help "Solution (Click on the arrow if you are stuck)"
    - You should now have a branch `us-42` and a branch `testing`.
    - The branch `testing` does not include the latest changes of `us-42`.
    - We want to change this by doing a rebase.
    - For that, switch to the branch `testing` with `git switch testing`.
    - Begin the rebase with `git rebase us-42`. Now, the changes of `testing` include all other changes of `us-42`.
    - As a last step, we want to integrate the changes of `testing` into `us-42`.
    - Switch to the branch `us-42` with `git switch us-42`.
    - Execute a fast forward merge with `git merge testing`.
    - Review the result with `git log`.
