# My first Project

!!! goal "Goal"
    - In this project you'll get familiar with the training environment, git on the commandline and Gitlab.
    - Take your time and look around.

## Task 1

- Your machine for the training is reachable at `code-{NUMBER}.{{ domain }}`.
Replace `{NUMBER}` with the number that was assigned to you.
  - Example for the number 5: `code-5.{{ domain }}`.
  - Example for the number 11: `code-11.{{ domain }}`.
- Enter the password that was given to you.
- You now have access to your personal machine for the training.

## Task 2

- Log into Gitlab too. You'll find the login button in the upper right corner.
Use `user-{NUMBER}` as a username and the password that you used before.
- You'll see multiple projects. For now we want to clone `my-first-project`.
- Open `my-first-project` by clicking on it. You now see an overview of the
  project with its name, some facts (number of commits, number of branches, tags,
  size) and below the files that are included.
- In between the project name and the file browser on the right hand side you'll find the blue button `Clone`.
- Click on it and copy the link at `Clone with HTTPS`.
- Go back to your VSCode instance.
- If there is no terminal open yet, open one with the tab in the upper left corner.
- Afterwards clone the project by running `git clone {URL}` in your terminal.
  Pasting into the terminal can be done using `Ctrl (+ Shift) + v`.
- If everything worked, you should see the folder `my-first-project` in the file browser of VSCode.

## Task 3 - First commands in git

- In the terminal switch to the directory of the project.

   ```bash
   cd my-first-project
   ```

- Run `git status`.

  ```bash
  $ git status
  On branch main

  nothing to commit, working tree clean
  ```

- If your output looks like this, everything worked.
  You successfully logged in, cloned the project and are now ready to begin working with the project.