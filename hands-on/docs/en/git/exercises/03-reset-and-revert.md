# Reset and Revert

!!! goal "Goal"
    - This project is about working with modified files and how to revert and reset them.

!!! tipp "Hints"
    - Try to solve the following tasks with the help of the [Slides](../../2-changes/#/)
    and the [Cheatsheets](../cheatsheets/2-changes.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- Leave the repository `my-first-project` and create a new one:

   ```bash
   cd ..                  # Leave the directory my-first-project
   mkdir reset_and_revert # Create a new directory reset_and_revert
   cd reset_and_revert    # Change into that directory
   git init               # Create a new Git repository
   ```

??? help "Solution (Click on the arrow if you are stuck)"
    - Leave the directory `my-first-project` with the command `cd ..` (`cd` stands for `Change Directory` and `..` for the parent directory).
    - Run the command `pwd` (`pwd` displays the current directory) and you should see `/root/workspace` as a result.
    - If you got a different result, run `cd /root/workspace` to change into the right directory.
    - When you are in the right directory, run `mkdir reset_and_revert` (`mkdir` stands for `make directory` and creates a new folder/directory).
    - Change into the newly created directory with `cd reset_und_revert`.
    - Run `git init` which creates a new git repository in the current directory.

## Task 2

- Commit a new file. It should be empty and have the name `myfile.txt`.

??? help "Solution (Click on the arrow if you are stuck)"
    - Create a file named `myfile.txt` in the directory `reset_and_revert`.
    - Add the file to the staging area with `git add .` (do not forget the `.`).
    - Review what happend with `git status`.
    - Create a new commit with `git commit -m "{Enter your commit message here}"`.
    - Review your work with `git log`.

## Task 3

- Change the content of your empty file and commit that change with the message "New changes".

??? help "Solution (Click on the arrow if you are stuck)"
    - Add some arbitrary content to the file `myfile.txt` and save it.
    - Add the file to the staging area.
    - Create a new commit with the message "New changes".
    - Review your history with `git log`.

## Task 4

- Edit the commit message into "Add new content to myfile.txt".

??? help "Solution (Click on the arrow if you are stuck)"
    - Run the command `git commit --amend`.
    - You see the same output as if you would create a new commit.
    - Edit the commit message, save and exit.
    - Review your history with `git log`.

## Task 5

- Remove the entire last commit. When you run `git log` **nothing** of the
  commit "Add new content to myfile.txt" should be visible anymore.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run the command `git reset HEAD~1`. This command removes the commit from
      the history. The actual changes to the files are still present in your
      workspace.
    - View the result with `git log`, `git status` and `git diff`.

## Task 6

- Repeat the steps of task 3 and 4.

??? help "Solution (Click on the arrow if you are stuck)"
    - Again, create a commit which adds some content to the file `myfile.txt`.

## Task 7

- Now, remove the commit without changing the history. When you run `git log` the
  commit "Add new content to myfile.txt" should still be present in your history.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run the command `git revert HEAD`. This command creates a new commit which
      reverts the changes of the latest commit.
    - Your file `myfile.txt` should be empty again. The history on how your file
      was edited is still present.
