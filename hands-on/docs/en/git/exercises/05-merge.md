# Merge

!!! goal "Goal"
    - This project is about working with branches and how to merge changes of multiple branches into one again.

!!! tipp "Hints"
    - Try to solve the following tasks with the help of the [Slides](../../3-branches/#/)
    and the [Cheatsheets](../cheatsheets/3-branches.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- For this small project it is a good idea to use a graph visualization plugin. To do this, click on the graph icon ![](img/Plugin.png) on the left sidebar.
- A SOURCE CONTROL bar will appear where you can find any Git repository already present in your VSCode.
- For various previous projects, click on the graph icon ![](img/Plugin_symbol.png) to the far right of the folder name.
- You should be able to see a graph analogous to your `git log --graph` command

## Task 2

- Leave the repository `remote-branches` and create a new one:

  ```bash
  cd ..        # Leave the directory remote-branches
  mkdir merge  # Create a new directory merge
  cd merge     # Change into that directory
  git init     # Create a new Git repository
  ```

??? help "Solution (Click on the arrow if you are stuck)"
    - Leave the directory `remote-branches` with the command `cd ..`.
    - Run the command `pwd` and you should see `/root/workspace` as a result.
    - If you got a different result, run `cd /root/workspace` to change into the right directory.
    - When you are in the right directory, run `mkdir merge`.
    - Change into the newly created directory with `cd merge`.
    - Run `git init` which creates a new git repository in the current directory.


## Task 3

- Create the following graph.

![Graph](tikz/project_graph.svg)

Each green node represents a commit. Proceed as follows for each commit/node:

1. Create a file with the node name as name (if it is not a merge commit). You can do this via the sidebar, or use the command `touch <filename>` in the terminal.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the first commit `C1`:
        ```
        touch C1
        ```

1. Add the file to the staging area and commit your change with the node name as the commit message.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the first commit `C1`:
        ```
        git add C1
        git commit -m "C1"
        ```

1. If more than one branch descends from a node, add a new branch if necessary, which you can access later with `git switch`.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the third commit `C3` descending of commit `C1`:
        ```
        git branch branch1
        ```
        Then the second commit `C2` can be created. And then the branch can be changed.
        ```
        touch C2
        git add C2
        git commit -m "C2"
        git switch branch1
        ```
        There C3 can be created as known.

1. If the node depends on two previous nodes, it is a merge commit. No file needs to be created for it, but the commit message should match the node name.

    ??? help "Solution (Click on the arrow if you are stuck)"
        e.g. for the fifth commit `C5` with previous commits `C2` and `C3`:
        ```
        git switch main
        git merge branch1
        ```

1. Complete the graph. In VSCode, it should then look like this or similar in the Git Graph plugin:

    ![](img/merge.png)


??? help "Solution (Click on the arrow if you are stuck)"
    The complete solution is as follows:
    ```
    touch C1
    git add C1
    git commit -m "C1"
    git branch branch1
    touch C2
    git add C2
    git commit -m "C2"
    git switch branch1
    touch C3
    git add C3
    git commit -m "C3"
    git switch main
    git merge branch1
    git switch branch1
    git branch branch2
    touch C4
    git add C4
    git commit -m "C4"
    git switch branch2
    touch C6
    git add C6
    git commit -m "C6"
    git switch branch1
    git merge branch2
    git switch main
    git merge branch1
    git branch -d branch1
    git branch -d branch2
    ```
