# Interactive Rebase

!!! goal "Goal"
    - This project is about the interactive rebase.

!!! tipp "Hints"
    - Try to solve the following tasks with the help of the
    [slides](../../5-history-rewriting/#/)
    and the [Cheatsheets](../cheatsheets/4-history-rewriting.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Preparation

Search in Gitlab for the project `interactive-rebase` and clone it:

- Search in Gitlab in the left upper corner `Menu->Projects` for the project `interactive-rebase`.
  If you don't find the project directly here, you can find it under `Your projects`.

- Copy the link from `Clone with HTTPS`.

- Switch back to the terminal of the VSCode instance.

- Ensure the current working directory is `/root/workspace`.
  Move upwards from previous repositories with `cd ..` or alternatively it is possible to switch directly into the directory with: `cd /root/workspace`.

- Clone the project with the command `git clone {URL}`.

## Task 1

A developer has written a small hello-world python script in the branch `feature-1`.
Your task is to get the changes of this feature-branch into the `main`-branch.
For a better understanding later there is some cleanup needed in the branch.
So you will need to 'squash' all the commits in the branch to a single commit.

1. Switch to the branch `feature-1`.

1. Look at the history of the branch to see what has been done.

1. Use the interactive rebase so there is only a single commit with one useful commit message left.

1. Check the history to see if the state of the branch is exactly how you want it to be.

1. Merge the branch `feature-1` into the `main`-branch without fast-forward.

??? help "Solution (Click on the arrow if you are stuck)"
    1. Part:
        - Switch into the `interactive rebase` folder.
        - Use `git switch feature-1` to switch to this given branch.
    2. Part:
        - Look in the filebrowser. There is a new file `hello-world.py`.
        - Look at the history with `git log --graph`.
        - Look at the single commits with `git show HEAD` or `git show HEAD~1` and `git show HEAD~2`.
    3. Part:
        - The feature is containing 3 commits, which we want to get into a single commit.
          Therefore run `git rebase -i HEAD~3`.
        - A list of commits will be shown. We now change the list so it looks like this:
             ```bash
             pick cf3b080 Add file where the feature should be implemented
             squash b88e281 Adds hello-world functionality
             squash 1d6f112 Fit typo in hello-world output
             ```
          Now the lower 2 commits will be integrated into the first commit.
        - You will now be presented with a list of combined commit messages.
          Remove all lines that have not been commented out and insert a useful message.
        - Save and close your commit message.
    4. Part:
        - Check the history to see if the state of the branch is now how you want it to be.
          To do this use the command `git log --graph`.
    5. Part:
        - Run `git switch main` and then `git merge feature-1` to merge the changes.

## Task 2

1. Use the command `git reflog` to undo your interactive rebase.
   Search in the reflog for the line `rebase -i (start)`. This is where
   the rebase was started. The line underneath is the reference to the state before.

1. Make a `git reset` to get to this state before the rebase.

??? help "Solution (Click on the arrow if you are stuck)"
    1. Part:
        - Run `git reflog`.
        - Search for the line where `rebase -i (start)` is stated. This is where the rebase has started.
        - The line underneath is the reference to the state before. Remember or copy the given refence (`HEAD@{??}`).
    2. Part:
        - Run `git reset --hard HEAD@{??}` and substitute the `??` with the number from the reflog.
        - Run `git log --graph` and you are now back in the state before the changes.

## Task 3

If you have some spare time you can check out the `feature-2`-branch and take a look at it.
This branch contains a small command line tool.

1. Switch to the branch `feature-2`.

1. Create a new branch so you can easily jump back to this state if something goes wrong after a rebase.

1. Take a look at the history.

1. Try different variants of `rebase -i`.
