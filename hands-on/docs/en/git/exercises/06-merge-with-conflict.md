# Merge with Conflicts

!!! goal "Goal"
    - This project is about working and resolving conflicts during a merge.

!!! tipp "Hints"
    - Try to solve the following tasks with the help of the [Slides](../../4-branches-2/#/)
    and the [Cheatsheets](../cheatsheets/3-branches.md).
    - If you still run into problems, you'll find an expandable hint block for
    every task with a detailed description of the solution.

## Task 1

- Leave the repository `merge` and create a new one:

   ```bash
   cd ..            # Leave the directory my-first-project
   mkdir conflict   # Create a new directory conflict
   cd conflict      # Change into that directory
   git init         # Create a new Git repository
   ```

??? help "Solution (Click on the arrow if you are stuck)"
    - Leave the directory `merge` with the command `cd ..`.
    - Run the command `pwd` and you should see `/root/workspace` as a result.
    - If you got a different result, run `cd /root/workspace` to change into the right directory.
    - When you are in the right directory, run `mkdir conflict`.
    - Change into the newly created directory with `cd conflict`.
    - Run `git init` which creates a new git repository in the current directory.


## Task 2

- Create a file `participants.txt` with the following content:
    ```txt
    Participants training 1:
      Philipp
      Lisa
      Rebecca
      Thomas
      Alexander
    ```

## Task 3

- Create a new commit with the created file.

??? help "Solution (Click on the arrow if you are stuck)"
    - Create a new commit with `git add .` and `git commit -m "{Enter your commit message here}"`.

## Task 4

- Create a new branch `training2` and change onto that branch.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run the command `git switch -c training2`.

## Task 5

- Add the following content to the file `participants.txt`:

    ```txt
    Participants training 2:
      Markus
      Ramona
      Till
      Juliane
    ```

## Task 6

- Create a commit with the changes.

??? help "Solution (Click on the arrow if you are stuck)"
    - Create a new commit with `git add .` and `git commit -m "{Enter your commit message here}"`.

## Task 7

- Change onto the `main` branch.

??? help "Solution (Click on the arrow if you are stuck)"
    - Run the command `git switch main`.

## Task 8

- Delete the participants `Thomas` and `Alexander`. Create a new commit.

??? help "Solution (Click on the arrow if you are stuck)"
    - Remove the names from `participants.txt`. Save the file.
    - Create a new commit with `git add .` and `git commit -m "{Enter your commit message here}"`.

## Task 9

- Merge the changes of branch `training2` into `main`. Resolve the conflicts.

??? help "Solution (Click on the arrow if you are stuck)"
    - Start the merge with `git merge training2`.
    - Open the file in the editor. VSCode automatically displays the areas with conflicts.
    - As you can see, the changes do not only include the changes made in `training2` but also the deleted names.
    - This is not a change we made on `training2` so we do not want to merge that into `main`.
    - Remove the names `Thomas` and `Alexander` such that only the list of training 2 will be added and then save the file.
    - Mark the conflict as resolved by running `git add participants.txt`.
    - Finish the merge with `git commit -m "{Enter your commit message here}"`.
