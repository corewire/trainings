# Cheatsheet 2 - reverting changes

## General

| Command                   | Action                                                          |
|---------------------------|-----------------------------------------------------------------|
| ```git blame <file>```    | Show the authors of all lines in `<file>`                       |
| ```git commit --amend```  | Correct a commit or commit-message                              |
| ```git revert <commit>``` | Adds a commit with the inverted changes of `<commit>`           |
| ```git reset <commit>```  | Set the state of the repository back to `<commit>`              |

## Git reset

| Command                            | changes head   | changes index   | changes working directory   |
|------------------------------------|----------------|-----------------|-----------------------------|
| ```git reset --soft <commit>```    | yes            | no              | no                          |
| ```git reset [--mixed] <commit>``` | yes            | yes             | no                          |
| ```git reset --hard <commit>```    | yes            | yes             | yes                         |

## Filestates

![filestates](tikz/filestates.svg)
