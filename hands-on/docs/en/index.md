---
title: Corewire Hands-On Collection
---

# Welcome

On this site you can find all materials for our trainings.

## Interested in a training?

If you are interested in any of the listed trainings or any related topic like **DevOps**, **cloud computing** and **software development**, feel
free to contact us at <a href="mailto:training@corewire.de">training@corewire.de</a>.


## Direct links to the material

- [Git](./git/)
- [Docker](./docker/)
