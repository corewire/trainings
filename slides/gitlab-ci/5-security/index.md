<section data-state="no-title-footer">

## Gitlab CI
### Security & Best Practices
### Corewire IT Consulting

---

### Security

- Remote-Code-Execution by Design
- Wer hat Zugriff auf die Repos?
<!-- .element: class="fragment"-->
- Welche Runner werden verwendet?
<!-- .element: class="fragment"-->

---

### Security - Jobs

- Job benötigt sensible Informationen
  - Environment Variablen
  - Remote Infrastruktur

⇨ Zugriff auf Job ⇨ Zugriff auf Secrets
<!-- .element: class="fragment"-->

---

### Security - Shell/SSH-Executor

- Nur für Trusted Repos
- Keine Isolation zwischen Jobs/Projekten
- Installation von Schadsoftware auf Host möglich

---

### Security - Docker-Executor

- Im nicht-privilegierten Modus sicher
- Runner: `pull_policy = always` (default)

---

### Security - Seiteneffekte

![](./img/docker_cache_1.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

![](./img/docker_cache_2.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

![](./img/docker_cache_3.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

![](./img/docker_cache_4.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

![](./img/docker_cache_5.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

Pull policy: `if-not-present`

![](./img/docker_cache_5.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

Pull policy: `if-not-present`

![](./img/docker_cache_6.svg)<!-- .element width="90%" -->

--

### Security - Seiteneffekte

Pull policy: `always`

![](./img/docker_cache_7.svg)<!-- .element width="90%" -->

---

### Security

- Separates Repo für `gitlab-ci.yml`
- "Limit CI_JOB_TOKEN access" aktivieren

---

### Job-Token

- `CI_JOB_TOKEN`
- Eindeutiger Token pro Job
- Zugriff auf:
<!-- .element: class="fragment" data-fragment-index="1" -->
  - Package/Container Registry
<!-- .element: class="fragment" data-fragment-index="1" -->
  - Pipeline Triggers
<!-- .element: class="fragment" data-fragment-index="1" -->
  - Artefakte
<!-- .element: class="fragment" data-fragment-index="1" -->
  - Andere private Projekte
<!-- .element: class="fragment" data-fragment-index="2" -->

⇨ Settings / CI/CD / Token Access
<!-- .element: class="fragment" data-fragment-index="3" -->
⇨ [Default soll sich mit Version 15.7 (22.12.22) ändern](https://gitlab.com/gitlab-org/gitlab/-/issues/340822)
<!-- .element: class="fragment" data-fragment-index="4" -->

---

### Secrets

- Sensible Informationen:
  - API-Token
  - Datenbanklogin
  - Private Keys
- CI/CD Variablen
<!-- .element: class="fragment"-->
- Externer Secret-Store
<!-- .element: class="fragment"-->

---

### Secrets - CI/CD Variablen

- Projekt-/Gruppen-level
- Protected Variables
- Masked Variables

---

### Secrets - Externer Secret-Store

![](./img/gitlab_vault_workflow_v13_4.png)<!-- .element width="90%" -->

---

### Secrets - HashiCorp Vault Integration

- Premium-Feature
- Notwendige Variablen:
  - `VAULT_SERVER_URL`
  - `VAULT_AUTH_ROLE`
  - `VAULT_AUTH_PATH`
  - `VAULT_NAMESPACE`
- Verwendung:

```yml
secrets:
  DATABASE_PASSWORD:
    vault: production/db/password@ops
```

---

### Best Practices - Jobs

- Workflow in kleine, logische Jobs unterteilen
- Komplexe Logik im Job vermeiden
- Einheitlicher Aufruf
<!-- .element: class="fragment" data-fragment-index="1"-->

```yml
myjob:
  script:
    - make build
    - ./scripts/build.sh
```
<!-- .element: class="fragment" data-fragment-index="1"-->

---

### Best Practices - Pipelines

- Laufzeit optimieren
- Schnelles Feedback
- Parallelisieren
<!-- .element: class="fragment" data-fragment-index="1"-->
- Timeouts
<!-- .element: class="fragment" data-fragment-index="1"-->
- Fail early, fail fast, fail loud
<!-- .element: class="fragment" data-fragment-index="1"-->

---

### Best Practices - Applikation

- Test/Review-Umgebung
- Identische Infrastruktur für Test und Prod
- Automatisierte Rollbacks
- Nach Sicherheitslücken scannen

---

### Failing Jobs

- Manchmal erwünscht

    ```yaml
    myjob:
      allow_failure: true
    ```

- Aufräumen mit `after_script`

---

### Troubleshooting

- Pipeline ist Code
- Potential für Bugs
- Hilfreiche Ausgabe für schnelle Fehleranalyse

---

### Debugging

- Print-Debugging
- Debug-Log
- Lokale Ausführung
- Interaktive Shell

---

### Print-Debugging

```yml
myjob:
  script:
    - echo $MY_VAR # Ist die Variable gesetzt?
    - pwd          # Bin ich im richtigen Verzeichnis?
    - ls -la       # Sind Dateien vorhanden?
```

---

### Debug-Log

Detailierte Ausgabe des Runners

```yml
myjob:
  variables:
    CI_DEBUG_TRACE: "true"
```

---

### Lokale Ausführung

```
gitlab-runner exec <executor> <job_name>
```

- Basierend auf lokaler `.gitlab-ci.yml`
- Limitiertes Feature-Set

[Zur Dokumentation](https://docs.gitlab.com/runner/commands/#limitations-of-gitlab-runner-exec)

---

### Interaktive Shell

![](./img/interactive_web_terminal_page.png)<!-- .element width="90%" -->

---

### Interaktive Shell

- Direkter Zugriff auf den Runner
- Nicht Out-of-the-Box
- Nicht in jedem Setup möglich

[Zur Dokumentation](https://docs.gitlab.com/ee/ci/interactive_web_terminal/)

---

### Knowledge Check

<iframe data-src="https://wall.sli.do/event/3QtyVtkq25KaccAfDcxQY1?section=c6563fec-1e08-42d8-8ede-1f8b19564cfd"></iframe><!-- .element style="width:50vw;height:50vh" -->
