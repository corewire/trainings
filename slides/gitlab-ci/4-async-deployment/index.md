<section data-state="no-title-footer">

## Gitlab CI
### Async-Deployment
### Corewire IT Consulting

---

### Szenario

![](./tikz/scenario_1.svg)<!-- .element width="80%" -->

--

### Szenario

![](./tikz/scenario_2.svg)<!-- .element width="80%" -->

--

### Szenario

![](./tikz/scenario_3.svg)<!-- .element width="80%" -->

--

### Szenario

![](./tikz/scenario_4.svg)<!-- .element width="80%" -->

--

### Szenario

![](./tikz/scenario_5.svg)<!-- .element width="80%" -->

---

### Job Artefakte

```yml
pdf:
  script: xelatex mycv.tex
  artifacts:
    paths:
      - mycv.pdf
    expire_in: 1 week
```

---

### Job Artefakte

- Jobs können Artefakte generieren
- Artefakte können
  - über die UI heruntergeladen werden
  - von späteren Jobs verwendet werden

---

### Job Artefakte - Wildcard

```yml
myjob:
  artifacts:
    paths:
      - path/to/file.txt



```

---

### Job Artefakte - Wildcard

```yml
myjob:
  artifacts:
    paths:
      - path/to/file.txt
      - path/to/*.txt


```

---

### Job Artefakte - Wildcard

```yml
myjob:
  artifacts:
    paths:
      - path/to/file.txt
      - path/to/*.txt
      - path/*/file.txt
```

---

### Job Artefakte - Name

```yml
myjob:
  artifacts:
    name: "job1-artifacts-file"
    paths:
      - binaries/





```

Default: `artifacts`/`artifacts.zip`

--

### Job Artefakte - Name

```yml
myjob:
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/





```

CI-Variablen werden unterstützt

--

### Job Artefakte - Exclude

```yml
myjob:
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
    exclude:
      - binaries/**/*.o



```

Ausschluss bestimmter Inhalte

--

### Job Artefakte - Untracked

```yml
myjob:
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
    exclude:
      - binaries/**/*.o
    untracked: true


```

Filter auf "Git Untracked"-Dateien

--

### Job Artefakte - When

```yml
myjob:
  artifacts:
    name: "$CI_JOB_STAGE-$CI_COMMIT_REF_NAME"
    paths:
      - binaries/
    exclude:
      - binaries/**/*.o
    untracked: true
    when: always
```

on_success (default), on_failure, always

---

### Job Artefakte - Reports

```yml
Terraform Plan:
  script:
    - gitlab-terraform plan-json

  artifacts:
    name: plan
    reports:
      terraform: deployment/plan.json
```

--

### Job Artefakte - Reports

Visuelle Aufbereitung in Merge Requests

![](./img/terraform_plan_widget_v13_2.png)<!-- .element width="90%" -->

---

### Services

```yml
Run Unittests:
  image: python:3.9


  script:
    - pytest demoapp
```

--

### Services

```yml
Run Unittests:
  image: python:3.9
  services:
    - mariadb:10.9.3
  script:
    - pytest demoapp
```

---

### Services

![](./img/services_1.svg)<!-- .element width="90%" -->

--

### Services

![](./img/services_2.svg)<!-- .element width="90%" -->

--

### Services

![](./img/services_3.svg)<!-- .element width="90%" -->

--

### Services

![](./img/services_4.svg)<!-- .element width="90%" -->

--

### Services

![](./img/services_5.svg)<!-- .element width="90%" -->

---

### Services

- Container-basierte Services
- Zugriff via Netzwerk
- Beispiel:
  - Datenbank

---

### Demo: Docker in der Pipeline

---

### Docker in der Pipeline

```yml
build:
  image: docker:20.10.16
  stage: build


  script:
    - docker build -t namespace/image:latest .
    - docker push namespace/image:latest
```

--

### Docker in der Pipeline

```yml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  script:
    - docker build -t namespace/image:latest .
    - docker push namespace/image:latest
```

---

### Container-Registry

Zugriff auf die Gitlab-Container-Registry:

```yml
myjob:
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
```

---

### Docker in der Pipeline

```yml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind


  script:

    - docker build -t namespace/image:latest .
    - docker push namespace/image:latest
```
---

### Docker in der Pipeline

```yml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind


  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t namespace/image:latest .
    - docker push namespace/image:latest
```

---

### Docker in der Pipeline

```yml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind


  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
```

---

### Docker in der Pipeline

```yml
build:
  image: docker:20.10.16
  stage: build
  services:
    - docker:20.10.16-dind
  variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $IMAGE_TAG .
    - docker push $IMAGE_TAG
```

---

### Tipps zu Docker

- Registry-Login in `before_script`
- A
<!-- .element class="placeholder" -->
- Langer Text fürs Layouuuuuuuuuuuuuuut
<!-- .element class="placeholder" -->
- A
<!-- .element class="placeholder" -->
- A
<!-- .element class="placeholder" -->

Note:

- Probleme mit lokalem Docker-Cache bei mehreren Runnern
- und parallel laufenden Pipelines vermeiden

---

### Tipps zu Docker

- Registry-Login in `before_script`
- Für einen Build: `docker build --pull`
- Expliziter `docker pull` vor `docker run`
- A
<!-- .element class="placeholder" -->
- A
<!-- .element class="placeholder" -->


Note:

- Probleme mit lokalem Docker-Cache bei mehreren Runnern
- und parallel laufenden Pipelines vermeiden

---

### Tipps zu Docker

- Registry-Login in `before_script`
- Für einen Build: `docker build --pull`
- Expliziter `docker pull` vor `docker run`
- Git-SHA als Image-Tag
- Latest-Tag nicht verwenden

Note:

- Probleme mit lokalem Docker-Cache bei mehreren Runnern
- und parallel laufenden Pipelines vermeiden

---

### Docker Beispiel - Setup

```yml
stages:
  - build
  - test
  - release

default:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
[...]
```

---

### Docker Beispiel - Build Stage

```yml
[...]
build:
  stage: build
  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE
[...]
```

---

### Docker Beispiel - Test Stage

```yml
[...]
test1:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE /script/to/run/tests

test2:
  stage: test
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker run $CONTAINER_TEST_IMAGE /script/to/run/another/test
[...]
```
---

### Docker Beispiel - Release Stage

```yml
[...]
release-image:
  stage: release
  script:
    - docker pull $CONTAINER_TEST_IMAGE
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push $CONTAINER_RELEASE_IMAGE
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

---

### Pipeline Architektur

- Jobs/Stages
  - Einfache Projekte
  - Alles an einer Stelle
- A
<!-- .element class="placeholder" -->
- A
<!-- .element class="placeholder" -->
- Langer Text für besseres Layouuuuuuuuuuut
<!-- .element class="placeholder" -->

--

### Pipeline Architektur

- Jobs/Stages
- Abhängigkeitsgraph
  - Größere, komplexe Projekte
  - Effiziente Ausführung
- A
<!-- .element class="placeholder" -->
- Langer Text für besseres Layouuuuuuuuuuut
<!-- .element class="placeholder" -->

--

### Pipeline Architektur

- Jobs/Stages
- Abhängigkeitsgraph
- Parent-Child Pipeline
  - Mono-Repos
- A
<!-- .element class="placeholder" -->
- Langer Text für besseres Layouuuuuuuuuuut
<!-- .element class="placeholder" -->

--
### Pipeline Architektur

- Jobs/Stages
- Abhängigkeitsgraph
- Parent-Child Pipeline
- Multi-project Pipeline
  - Projektübergreifende Abhängigkeiten
- Langer Text für besseres Layouuuuuuuuuuut
<!-- .element class="placeholder" -->

---

### Parent-Child Pipeline

```yaml
# .gitlab-ci.yml
stages:
  - triggers

trigger_a:
  stage: triggers
  trigger:
    include: a/.gitlab-ci.yml
  rules:
    - changes:
        - a/*

trigger_b:
  ...
```

---

### Multi-project Pipeline

- a
<!-- .element class="placeholder" -->

```yaml
# .gitlab-ci.yml
stages:
  - triggers

trigger_a:
  stage: triggers
  trigger:
    include: a/.gitlab-ci.yml




```

--

### Multi-project Pipeline

- a
<!-- .element class="placeholder" -->

```yaml
# .gitlab-ci.yml
stages:
  - triggers

trigger_a:
  stage: triggers
  trigger:
    include:
      - local: a/.gitlab-ci.yml



```

--

### Multi-project Pipeline

- a
<!-- .element class="placeholder" -->

```yaml
# .gitlab-ci.yml
stages:
  - triggers

trigger_a:
  stage: triggers
  trigger:
    include:
      - project: 'my-group/my-pipeline-library'



```

--

### Multi-project Pipeline

- a
<!-- .element class="placeholder" -->

```yaml
# .gitlab-ci.yml
stages:
  - triggers

trigger_a:
  stage: triggers
  trigger:
    include:
      - project: 'my-group/my-pipeline-library'
        ref: 'main'
        file: '/path/to/child-pipeline.yml'
```

--

### Multi-project Pipeline

- User benötigt Berechtigung für Downstream-Pipeline

```yaml
# .gitlab-ci.yml
stages:
  - triggers

trigger_a:
  stage: triggers
  trigger:
    include:
      - project: 'my-group/my-pipeline-library'
        ref: 'main'
        file: '/path/to/child-pipeline.yml'
```

---

### Dynamische Pipeline

```yaml
# .gitlab-ci.yml
generate-config:
  stage: build
  script: generate-ci-config > generated-config.yml
  artifacts:
    paths:
      - generated-config.yml








```

--

### Dynamische Pipeline

```yaml
# .gitlab-ci.yml
generate-config:
  stage: build
  script: generate-ci-config > generated-config.yml
  artifacts:
    paths:
      - generated-config.yml

child-pipeline:
  stage: test
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate-config
```

---

### Knowledge Check

<iframe data-src="https://wall.sli.do/event/s6bm5j2N72CJm6W47ZAv9a?section=2392448c-8d30-49df-a0ee-7edca295cfd5"></iframe><!-- .element style="width:50vw;height:50vh" -->

---

### Projekt

Gehen Sie auf: https://labs.corewire.de

Navigieren Sie nach:</br>
⇨ Gitlab CI/CD</br>
⇨ Aufgaben</br>
⇨ Async-Deployment
