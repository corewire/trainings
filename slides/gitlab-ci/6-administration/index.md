<section data-state="no-title-footer">

## Gitlab CI
### Administration
### Corewire IT Consulting

---

### Docker-Runner einrichten (1/3)

```shell

docker run -d                                       \


  gitlab/gitlab-runner:latest
```

--

### Docker-Runner einrichten (1/3)

```shell

docker run -d --name gitlab-runner --restart always \


  gitlab/gitlab-runner:latest
```

--

### Docker-Runner einrichten (1/3)

```shell

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \

  gitlab/gitlab-runner:latest
```

--

### Docker-Runner einrichten (1/3)

```shell

docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

--

### Docker-Runner einrichten (2/3)

```shell
docker volume create gitlab-runner-config
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v gitlab-runner-config:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```

---

### Docker-Runner einrichten (3/3)

```yml
# docker-compose.yml

services:
  gitlab-runner:
    image: 'gitlab/gitlab-runner:latest'
    restart: always
    volumes:
      - './config-runner:/etc/gitlab-runner'
      - '/var/run/docker.sock:/var/run/docker.sock'
```

---

### Docker-Runner registrieren

- Nur einmal pro Runner notwendig
- Füllt die `config.toml`

--

### Docker-Runner registrieren

```shell
docker run                                                          \
  gitlab/gitlab-runner register
```
--

### Docker-Runner registrieren

```shell
docker run --rm -it                                                 \
  gitlab/gitlab-runner register
```
--

### Docker-Runner registrieren

```shell
docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  gitlab/gitlab-runner register
```

--

### Docker-Runner registrieren

```shell
docker run --rm -it -v      gitlab-runner-config:/etc/gitlab-runner \
  gitlab/gitlab-runner register
```

---

### K8s-Runner einrichten

```bash
helm repo add gitlab https://charts.gitlab.io












```

--

### K8s-Runner einrichten

```bash
helm repo add gitlab https://charts.gitlab.io

# Configure Runner in values.yml










```

--

### K8s-Runner einrichten

```bash
helm repo add gitlab https://charts.gitlab.io

# Configure Runner in values.yml

# For Helm 2
helm init
helm install --namespace <NAMESPACE> --name gitlab-runner \
  -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner





```

--

### K8s-Runner einrichten

```bash
helm repo add gitlab https://charts.gitlab.io

# Configure Runner in values.yml

# For Helm 2
helm init
helm install --namespace <NAMESPACE> --name gitlab-runner \
  -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner

# For Helm 3
helm install --namespace <NAMESPACE> gitlab-runner \
  -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner
```

---

### Config.toml

```ini
concurrent = 1     # Anzahl paralleler Jobs
check_interval = 0 # Intervall für Anfragen nach Jobs
                   # Default: 3s, minimaler Wert 3s
[[runners]]
  name = "MyFirstRunner"
  url = "https://git.labs.corewire.de"
  id = 34
  token = "..."
  token_obtained_at = 2022-11-09T12:43:28Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
...
```

---

### Config.toml

```ini
...
  [runners.docker]
    image = "ubuntu:22.04"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
```

[Zur Dokumentation](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)

---

### Runner Types

- Shared Runner
- Group Runner
- Specific Runner

---

### Shared Runner

- Runner für eine ganze Gitlab-Instanz
- Kann für ganze Gruppen/Projekte (de-)aktiviert werden
- Fair Usage Queue
<!-- .element: class="fragment" data-fragment-index="1" -->
- CI/CD-Minuten Quota möglich
<!-- .element: class="fragment" data-fragment-index="1" -->

---

### Shared-Runner - Fair Usage Queue

- Job 1 for Project 1 **⇨ 1**
- Job 2 for Project 1
- Job 3 for Project 1
- Job 4 for Project 2
- Job 5 for Project 2
- Job 6 for Project 3

--

### Shared-Runner - Fair Usage Queue

- Job 1 for Project 1 **⇨ 1**
- Job 2 for Project 1
- Job 3 for Project 1
- Job 4 for Project 2 **⇨ 2**
- Job 5 for Project 2
- Job 6 for Project 3 **⇨ 3**

--

### Shared-Runner - Fair Usage Queue

- Job 1 for Project 1 **⇨ 1**
- Job 2 for Project 1 **⇨ 4**
- Job 3 for Project 1
- Job 4 for Project 2 **⇨ 2**
- Job 5 for Project 2
- Job 6 for Project 3 **⇨ 3**

--

### Shared-Runner - Fair Usage Queue

- Job 1 for Project 1 **⇨ 1**
- Job 2 for Project 1 **⇨ 4**
- Job 3 for Project 1
- Job 4 for Project 2 **⇨ 2**
- Job 5 for Project 2 **⇨ 5**
- Job 6 for Project 3 **⇨ 3**

--

### Shared-Runner - Fair Usage Queue

- Job 1 for Project 1 **⇨ 1**
- Job 2 for Project 1 **⇨ 4**
- Job 3 for Project 1 **⇨ 6**
- Job 4 for Project 2 **⇨ 2**
- Job 5 for Project 2 **⇨ 5**
- Job 6 for Project 3 **⇨ 3**

---

### Shared Runner

Admin Area ⇨ Overview ⇨ Runners

![](./img/shared_runner.png)<!-- .element width="25%" -->

---

### Group Runner

- Runner für eine Gruppe/alle Subgruppen
- FIFO

---

### Group Runner

Group ⇨ CI/CD ⇨ Runners

![](./img/group_runner.png)<!-- .element width="25%" -->

---

### Specific Runner

- Runner für einzelne Projekte
  - Untersützt mehrere Projekte
  - Muss für jedes Projekt einzeln aktiviert werden
- Starke CI/CD-Nutzung
<!-- .element: class="fragment" data-fragment-index="1" -->
- Spezielle Anforderungen
<!-- .element: class="fragment" data-fragment-index="1" -->
- FIFO
<!-- .element: class="fragment" data-fragment-index="2" -->

---

### Specific Runner

Project ⇨ Settings ⇨ CI/CD ⇨ Runners

![](./img/specific_runner.png)<!-- .element width="35%" -->

---

### Runner Tags

- Runner und Jobs können eine Liste von Tags erhalten
- Jobs laufen nur auf Runnern, die alle Tags enthalten

```yml





```

--

### Runner Tags

- Runner und Jobs können eine Liste von Tags erhalten
- Jobs laufen nur auf Runnern, die alle Tags enthalten

```yml
myjob:
  tags:
    - ruby
    - postgres
```

--

### Runner Tags

- Runner und Jobs können eine Liste von Tags erhalten
- Jobs laufen nur auf Runnern, die alle Tags enthalten

```yml
myjob:
  tags:
    - windows


```

--

### Runner Tags

- Runner und Jobs können eine Liste von Tags erhalten
- Jobs laufen nur auf Runnern, die alle Tags enthalten

```yml
myjob:
  tags:
    - big-machine


```

---

### Demo: Runner einrichten

---

### Knowledge Check

<iframe data-src="https://wall.sli.do/event/tdXjoNr2etxANe898wxJBd?section=5423f40a-20bf-4d63-bd66-ece0c6f51f3c"></iframe><!-- .element style="width:50vw;height:50vh" -->

---

### Projekt

Gehen Sie auf: https://labs.corewire.de

Navigieren Sie nach: Gitlab-CI ⇨ Aufgaben ⇨ Administration
