<style>
  .container{
    display: flex;
}
.col{
    width: 50%;
}
</style>

<section data-state="no-title-footer">

## Gitlab CI
### Erste Schritte
### Corewire IT Consulting

---

### Demo: Erste Pipeline

---

### Eine erste Pipeline

```yml
# .gitlab-ci.yml







```

---

### YAML

* `Key: Value` Mapping
* Endet auf `.yml` oder `.yaml`
* Hierarchie durch gleichmäßige Einrückung (Leerzeichen)

---

### YAML

```YAML
# Kommentar
Key1: Value
Key2:
  Subkey: "String"
  Subkey2: "#kein Kommentar"
List:
  - Key1
  - "String"





```

---

### YAML vs. Json

```YAML
# Kommentar
Key1: Value
Key2:
  Subkey: "String"
  Subkey2: "#kein Kommentar"
List:
  - Key1
  - "String"
```
<!-- .element: style="width: 48%; float: left" -->

```JSON
{
  "Key1": Value,
  "Key2": {
    "Subkey": "String",
    "Subkey2": "#kein Kommentar"
  },
  "List": [
    "Key1",
    "String"
  ]
}
```
<!-- .element: style="width: 48%; float: right" -->

---

### Eine erste Pipeline

```yml
# .gitlab-ci.yml







```

--

### Eine erste Pipeline

```yml
# .gitlab-ci.yml
run_tests:



  script:
    - pytest demoapp
```

---

### Wo läuft ein Job?

![](../../common/tikz/devops/runner_1.svg)<!-- .element width="80%" -->

--

### Wo läuft ein Job?

![](../../common/tikz/devops/runner_2.svg)<!-- .element width="80%" -->

--

### Wo läuft ein Job?

![](../../common/tikz/devops/runner_3.svg)<!-- .element width="80%" -->

--

### Wo läuft ein Job?

![](../../common/tikz/devops/runner_4.svg)<!-- .element width="80%" -->

--

### Wo läuft ein Job?

![](../../common/tikz/devops/runner_5.svg)<!-- .element width="80%" -->

--

### Wo läuft ein Job?

![](../../common/tikz/devops/runner_6.svg)<!-- .element width="80%" -->

---

### Gitlab Runner

- Separate Instanz(en)
- Go-Anwendung
- Betriebssystemunabhängig

---

### Executors

- Shell
- SSH
- Docker
- Kubernetes

---

### Executors

Unterscheiden sich durch
- Isolation der Build-Umgebung
- Zugriff auf den Host

⇨ Empfehlung: Docker oder Kubernetes
<!-- .element: class="fragment" data-fragment-index="1" -->

---

### Eine erste Pipeline

```yml
# .gitlab-ci.yml
run_tests:

  script:

    - pytest demoapp
```

--

### Eine erste Pipeline

```yml
# .gitlab-ci.yml
run_tests:
  image: python
  script:

    - pytest demoapp
```

--

### Eine erste Pipeline

```yml
# .gitlab-ci.yml
run_tests:
  image: python
  script:
    - pip install pytest
    - pytest demoapp
```

---

### Demo

---

### Pipeline Editor

- Web-Editor für `.gitlab-ci.yml`
- Visualisierung
- Validierung
- Linter
- Simulation

---

### Pipeline Struktur

- Jobs
  - Teilaufgabe
<!-- .element: class="fragment" data-fragment-index="1" -->
  - Parallele Ausführung
<!-- .element: class="fragment" data-fragment-index="1" -->
- Stages
  - Gruppe von Jobs
<!-- .element: class="fragment" data-fragment-index="2" -->
  - Sequenzielle Ausführung
<!-- .element: class="fragment" data-fragment-index="2" -->

---

### Stages

```yaml




build:

  script: echo "Building ..."

unittest:

  script: echo "Testing ..."

integrationtests:

  script: echo "Testing ..."
```

--

### Stages

```yaml
stages:
  - build
  - test

build:

  script: echo "Building ..."

unittest:

  script: echo "Testing ..."

integrationtests:

  script: echo "Testing ..."
```

--

### Stages

```yaml
stages:
  - build
  - test

build:
  stage: build
  script: echo "Building ..."

unittest:
  stage: test
  script: echo "Testing ..."

integrationtest:
  stage: test
  script: echo "Testing ..."
```

---

### Stages und Jobs

![](../../common/tikz/devops/stages_1.svg)<!-- .element width="80%" -->

--

### Stages und Jobs

![](../../common/tikz/devops/stages_2.svg)<!-- .element width="80%" -->

--

### Stages und Jobs

![](../../common/tikz/devops/stages_3.svg)<!-- .element width="80%" -->

--

### Stages und Jobs

![](../../common/tikz/devops/stages_4.svg)<!-- .element width="80%" -->

--

### Stages und Jobs

![](../../common/tikz/devops/stages_5.svg)<!-- .element width="80%" -->

---

### Abhängigkeitsgraph

- Job definiert seine Voraussetzungen
- Keyword: `needs`

---

### Abhängigkeitsgraph

```yaml
linux:build:
  script: echo "Building linux..."

mac:build:
  script: echo "Building mac..."

lint:
  needs: []
  script: echo "Linting..."

linux:rspec:
  needs: ["linux:build"]
  script: echo "Running rspec on linux..."

mac:rspec:
  needs: ["mac:build"]
  script: echo "Running rspec on mac..."
```

---

### Variablen

```yml
variables:
  DEPLOY_SITE: "https://example.com/"

deploy_job:
  stage: deploy
  script:
    - deploy-script --url $DEPLOY_SITE --path "/"

deploy_review_job:
  stage: deploy
  variables:
    REVIEW_PATH: "/review"
  script:
    - deploy-review-script --url $DEPLOY_SITE --path $REVIEW_PATH
```

---

### Variablen

- Name: A-Z,a-z,0-9,_
- Wert: String
- Verwendung: `$myvar`
- Wiederverwendbare Werte
<!-- .element: class="fragment" data-fragment-index="1" -->
- Variable Ausführung von Jobs
<!-- .element: class="fragment" data-fragment-index="1" -->
- Vermeidet Hard-Codierte Werte/URLs/User/...
<!-- .element: class="fragment" data-fragment-index="1" -->

---

### Vordefinierte Variablen

- In jeder Gitlab CI/CD verfügbar
- Enthalten Kontextinformationen über:
  - Commit
  - Job
  - Pipeline
  - Projekt

---

### Vordefinierte Variablen

Beispiele:
- `$CI_COMMIT_SHA`
- `$CI_DEFAULT_BRANCH`
- `$CI_PROJECT_NAME`
- `$CI_MERGE_REQUEST_ID`

[Zur Dokumentation](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)

---

### Demo

---

### Knowledge Check

<iframe data-src="https://wall.sli.do/event/hqkMbU3tSFo5Trq1ww7NyH?section=ea18a020-e52d-42af-aa9d-2f22e33c4d6a"></iframe><!-- .element style="width:50vw;height:50vh" -->

---

### Projekt

Gehen Sie auf: https://labs.corewire.de

Navigieren Sie nach:</br>
⇨ Gitlab CI/CD</br>
⇨ Aufgaben</br>
⇨ Eine erste Pipeline