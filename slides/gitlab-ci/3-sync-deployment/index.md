<section data-state="no-title-footer">

## Gitlab CI
### Sync Deployment
### Corewire IT Consulting

---

### Szenario

![](./tikz/scenario_1.svg)<!-- .element width="100%" -->

--

### Szenario

![](./tikz/scenario_2.svg)<!-- .element width="100%" -->

--

### Szenario

![](./tikz/scenario_3.svg)<!-- .element width="100%" -->

---

### Review Umgebung

Ansicht im Merge Request

![](./img/review_apps_preview_in_mr.png)<!-- .element width=80% -->

---

### Review Umgebung

Konfiguration:

```yml
deploy_review:
  stage: deploy





  script:
    - ...
```

--

### Review Umgebung

Konfiguration:

```yml
deploy_review:
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"



  script:
    - ...
```

--

### Review Umgebung

Konfiguration:

```yml
deploy_review:
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.example.com
  script:
    - ...
```

---

### Review Umgebung stoppen

```yml
deploy_review:
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.example.com

  script:
    - ...
```

--

### Review Umgebung stoppen

```yml
deploy_review:
  stage: deploy
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_ENVIRONMENT_SLUG.example.com
    on_stop: stop_review_app
  script:
    - ...
```

--

### Review Umgebung stoppen

```yml
stop_review_app:










```

--

### Review Umgebung stoppen

```yml
stop_review_app:
  stage: stop_review
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual






```

--

### Review Umgebung stoppen

```yml
stop_review_app:
  stage: stop_review
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop



```

--

### Review Umgebung stoppen

```yml
stop_review_app:
  stage: stop_review
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  script:
    - TODO # Review Umgebung stoppen
```

---

### Bedingte Jobs

- Keyword `rules`
- Definiert, wann ein Job ausgeführt wird

```yaml










```

- &nbsp;
<!-- .element: class="placeholder"-->
- &nbsp;
<!-- .element: class="placeholder"-->

--

### Bedingte Jobs

- Keyword `rules`
- Definiert, wann ein Job ausgeführt wird

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME != $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^feature/
      when: manual
      allow_failure: true
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
```

- Evaluierung bei Pipeline-Erstellung
<!-- .element: class="fragment" data-fragment-index="1"-->
- Evaluierung bis zur ersten Übereinstimmung
<!-- .element: class="fragment" data-fragment-index="1"-->

---
### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...
      changes: ...
      exists: ...
      allow_failure: ...
      variables: ...
      when: ...
```

--

### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...            # Bedingung
      changes: ...
      exists: ...
      allow_failure: ...
      variables: ...
      when: ...
```

--

### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...            # Bedingung
      changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
      exists: ...
      allow_failure: ...
      variables: ...
      when: ...
```

--

### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...            # Bedingung
      changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
      exists: ...        # Prüft, ob bestimmte Dateien existieren
      allow_failure: ...
      variables: ...
      when: ...
```

--

### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...            # Bedingung
      changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
      exists: ...        # Prüft, ob bestimmte Dateien existieren
      allow_failure: ... # Job darf fehlschlagen
      variables: ...
      when: ...
```

--

### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...            # Bedingung
      changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
      exists: ...        # Prüft, ob bestimmte Dateien existieren
      allow_failure: ... # Job darf fehlschlagen
      variables: ...     # Variablen setzen
      when: ...
```

--

### Bedingte Jobs

```yaml
myjob:
  script: echo "Hello, Rules!"
  rules:
    - if: ...            # Bedingung
      changes: ...       # Prüft, ob bestimmte Dateien verändert wurden
      exists: ...        # Prüft, ob bestimmte Dateien existieren
      allow_failure: ... # Job darf fehlschlagen
      variables: ...     # Variablen setzen
      when: ...          # "Wann" wird der Job ausgeführt
```

---

### Keyword `when`


```
myjob:
  rules:
    - when: on_success # Nur wenn alle vorhergehende Jobs erfolgreich sind
    - when: manual     # Manueller Trigger
    - when: always     # Unabhängig vom Status vorhergehender Jobs
    - when: on_failure # Nur wenn ein vorhergehender Job fehlgeschlagen ist
    - when: delayed    # Verzögerte Ausführung
    - when: never      # Niemals, negiert die Bedingung
```

---

### Bedingte Jobs - Beispiel

Wann wird diese Pipeline ausgeführt?

```yml
workflow:
  rules:
    - if: $CI_COMMIT_TITLE =~ /-draft$/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

---

### Bedingte Jobs

- Keywords: `only` und `except`
- Wurden von `rules` abgelöst

```yml
job1:
  script: echo
  only:
    - main
    - /^issue-.*$/
    - merge_requests

job2:
  script: echo
  except:
    - schedules
```

---

### Bedingte Workflows

- Globales Keyword: `workflow`
- Gültig für alle Jobs einer Datei

```







```

--

### Bedingte Workflows

- Globales Keyword: `workflow`
- Gültig für alle Jobs einer Datei

```yaml
workflow:
  rules:
    - if: $CI_COMMIT_TITLE =~ /-draft$/
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

---

### Pipeline aussetzen

Aktuelle Commit-Nachricht enthält</br> `[ci skip]` oder `[skip ci]`

⇨ Pipeline wird nicht ausgeführt

---

### Push Options

- Seit Git 2.10 verfügbar

```shell
git push --push-option=ci.skip
git push -o ci.skip


```

[Zur Dokumentation](https://docs.gitlab.com/ee/user/project/push_options.html)

---

### Push Options

- Seit Git 2.10 verfügbar

```shell
git push -o merge_request.create
  -o merge_request.target=my-target-branch
  -o merge_request.merge_when_pipeline_succeeds
```

[Zur Dokumentation](https://docs.gitlab.com/ee/user/project/push_options.html)

---



### SSH verwenden

- Zugriff auf
  - private Repositories/Submodules/Abhängigkeiten
  - Zielserver
- Muss manuell konfiguriert werden

---

### SSH verwenden

```yml
myjob:
  script:
    - 'command -v ssh-agent >/dev/null || ( apt-get update -y &&
        apt-get install openssh-client -y )'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
```

---

### CI/CD Variablen

Beispiel: `$SSH_PRIVATE_KEY`

Wo kann man Variablen setzen?
<!-- .element: class="fragment"-->
Was ist mit Secrets?
<!-- .element: class="fragment"-->
Was sind protected/masked Variablen?
<!-- .element: class="fragment"-->

---

### CI/CD Variablen

- `.gitlab-ci.yml`
- Projekt-level (Maintainer)
<!-- .element: class="fragment"-->
- Gruppen-level (Gruppen-Admin)
<!-- .element: class="fragment"-->
- Gitlab-Instanz (nur Self-hosted)
<!-- .element: class="fragment"-->

---

### Variable anlegen

Settings > CI/CD > Variables

![](./img/variable_dialog.png)<!-- .element width="60%" -->

---

### Default-Werte bei manueller Ausführung

```yml
# .gitlab-ci.yml
variables:
  TEST_SUITE:
    description: "Valid options: 'default', 'short', 'full'."
    value: "default"
  DEPLOY_ENVIRONMENT:
    description: "Valid options: 'canary', 'staging', 'production'."
```

![](./img/prefilled_vars.png)<!-- .element width="80%" -->
<!-- .element: class="fragment"-->
Nur bei globalen Variablen möglich
<!-- .element: class="fragment"-->

---

### Script-Tags

```yml






myjob:


  script:
    - echo "Script-Tag"



```

--

### Script-Tags

```yml






myjob:
  before_script:
    - echo "Ich laufe vor dem Script-Tag"
  script:
    - echo "Script-Tag"
  after_script:
    - echo "Ich laufe nach dem Script-Tag"
```

--

### Script-Tags

```yml
default:
  before_script:
    - echo "Ich laufe in jedem Job vor dem Script-Tag"
  after_script:
    - echo "Ich laufe in jedem Job nach dem Script-Tag"

myjob:


  script:
    - echo "Script-Tag"



```

--

### Script-Tags

```yml
default:
  before_script:
    - echo "Ich laufe in jedem Job vor dem Script-Tag"
  after_script:
    - echo "Ich laufe in jedem Job nach dem Script-Tag"

myjob:
  before_script:
    - echo "Ich überschreibe das default before_script-Tag"
  script:
    - echo "Script-Tag"



```

---

### Script-Tags

- `before_script`
    - Geteilte Shell mit `script`
- `after_script`
    - Getrennte Shell
    - Läuft auch, wenn der Job fehlschlägt
    - Status-Code hat keine Auswirkung

---

### Sonderzeichen in Befehle

Sonderzeichen können zu Problemen führen:

```yml
myjob:
  script:
    - echo Hallo: Welt
```

--

### Sonderzeichen in Befehle

Sonderzeichen können zu Problemen führen:

```yml
myjob:
  script:
    - echo "Hallo: Welt"
```

--

### Sonderzeichen in Befehle

Sonderzeichen können zu Problemen führen:

```yml
myjob:
  script:
    - 'echo Hallo: Welt'
```

---

### Mehrzeilige Befehle

`|`: Jede Zeile ist ein eigenständiger Befehl

```yml
myjob:
  script:
    - |
      echo "First command line."
      echo "Second command line."
      echo "Third command line."
```

---

### Mehrzeilige Befehle

`>`: Leere Zeile startet neuen Befehl

```yml
myjob:
  script:
    - >
      echo "First command line
      is split over two lines."

      echo "Second command line."
```

---

### YAML: Duplikation vermeiden

- Verstecke Konfigurationen (.)
- Anker (&)
- Alias (*)
- Map merging (<<)

---

### Demo: Review-Umgebung einrichten

---

### Knowledge Check

<iframe data-src="https://wall.sli.do/event/2RvQLdRcrTy33dz2Hip4UE?section=805c6395-321a-4be8-bfb9-9bb76cf917e9"></iframe><!-- .element style="width:50vw;height:50vh" -->

---

### Projekt

Gehen Sie auf: https://labs.corewire.de

Navigieren Sie nach:</br>
⇨ Gitlab CI/CD</br>
⇨ Aufgaben</br>
⇨ Sync-Deployment
