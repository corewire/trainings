# Git Schulung

## Gitlab

Folgende Repos müssen auf die Gitlab-Instanz:

* hands-on/git/initial (public!)
* hands-on/git/remote-branches (push-Rechte)
* hands-on/git/interactive-rebase (main, feature-1, feature-2)

Folgende Repos müssen pro User auf die Gitlab-Instanz:

* hands-on/git/my-first-project.md
* hands-on/microservices/api-users
* leeres Repo mit dem Namen git-lfs

## Schulungsmaschine

Folgende Repos müssen initial auf die Schulungsmaschine:

* hands-on/git/initial

### Installierte Software

* git
* tree
* git lfs
* unzip
