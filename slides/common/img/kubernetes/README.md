Source: https://github.com/kubernetes/community/tree/master/icons (License: CC-BY-SA 4.0)

The Kubernetes Icons Set is licensed under a choice of either Apache-2.0 or CC-BY-4.0 (Creative Commons Attribution 4.0 International). The Kubernetes logo is a registered trademark of The Linux Foundation, and use of it as a trademark is subject to The Linux Foundation's Trademark Usage Guidelines at https://www.linuxfoundation.org/trademark-usage/


Source: https://github.com/containernetworking/cni/blob/main/logo.png (License: Apache 2.0)
