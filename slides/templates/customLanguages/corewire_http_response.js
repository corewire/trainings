/*
Language: HTTP Response
Category: common
*/

RevealHighlight().hljs.registerLanguage('http_response', function(hljs) {
  let response_head = {
    className: 'response-head',
    begin: /^HTTP\//,
    returnBegin: true,
    end: /$/,
    contains: [
      {
        className: 'response-code',
        begin: / [0-9]+ /,
      },
    ]
  }

  let response_header = {
      className: 'response-header',
      begin: /^[A-Za-z-0-9]+: /,
      returnBegin: true,
      end: /$/,
      contains: [
        {
          className: 'header-name',
          begin: /^[A-Za-z-0-9]+/,
        }
      ]
    }

  return {
    contains: [
      response_head,
      response_header,
    ]
  }
});
  