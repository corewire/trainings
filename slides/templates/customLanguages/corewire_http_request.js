/*
Language: HTTP Request
Category: common
*/

RevealHighlight().hljs.registerLanguage('http_request', function(hljs) {
  let request_head = {
    className: 'request-head',
    begin: /^[A-Z]+ \//,
    returnBegin: true,
    contains: [
      {
        className: 'request-method',
        begin: /^[A-Z]+ /,
      },
      {
        className: 'request-path',
        begin: /\/[^ \n\r]*/,
      },
      {
        className: 'request-version',
        begin: /HTTP\//,
        end: /$/
      }
    ]
  }

  let request_header = {
      className: 'request-header',
      begin: /^[A-Za-z-0-9]+: /,
      returnBegin: true,
      end: /$/,
      contains: [
        {
          className: 'header-name',
          begin: /^[A-Za-z-0-9]+/,
        }
      ]
    }

  return {
    contains: [
      request_head,
      request_header,
    ]
  }
});
  