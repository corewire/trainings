/*
Language: Corewire Shell Session
*/

RevealHighlight().hljs.registerLanguage('gitshell', function() {
  return {
    contains: [
      {
        className: 'command',
        begin: /^\$/,
        end: /\n\$/,
        subLanguage: 'git',
        returnEnd: true,
        returnBegin: true,
        contains: [
          {
            className: 'meta',
            begin: /^\$/,
          },
        ]
      },
      {
        className: 'command',
        begin: /^Stage this hunk/,
        end: /\n\$/,
        subLanguage: 'git',
        returnEnd: true,
        returnBegin: true,
        contains: [
          {
            className: 'patch',
            begin: /^\@\@/,
            end: /\@\@$/,
          },
          {
            className: 'added',
            begin: /^\+/,
            end: /$/,
          },
          {
            className: 'removed',
            begin: /^[-−]/,
            end: /$/,
          },
        ]
      }
    ]
  }
});
