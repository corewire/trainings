/*
Language: Git log session
Category: common
*/
RevealHighlight().hljs.registerLanguage('git', function(hljs) {
  let git_status = {
    className: 'git-status',
    begin: /^On branch/,
    end: /^\$/,
    contains: [
      {
        className: 'staged',
        begin: /^Changes to be committed:/,
        end: /^[A-Z]/,
        excludeBegin: true,
        returnEnd: true,
        contains: [
          {
            className: 'staged-files',
            begin: /^    /,
            end: /$/,
          }
        ]
      },
      {
        className: 'modified',
        begin: /^Changes not staged for commit:/,
        end: /^[A-Z]/,
        excludeBegin: true,
        returnEnd: true,
        contains: [
          {
            className: 'modified-files',
            begin: /^    /,
            end: /$/,
          }
        ]
      },
      {
        className: 'untracked',
        begin: /^Untracked files:/,
        end: /^[A-Z]/,
        excludeBegin: true,
        contains: [
          {
            className: 'untracked-files',
            begin: /^    /,
            end: /$/,
          }
        ]
      },
    ]
  }

  let git_log = {
    className: 'git-log',
    begin: /^commit [a-f0-9]+( \()?/,
    end: /(\))?$/,
    contains: [
      {
        begin: /[^,\)]*->/,
        className: 'head',
        end: '[,)]',
        excludeEnd: true,
        contains: [
          {
            className: 'head-branch',
            begin: /[a-zA-Z]/
          }
        ]
      },
      {
        className: 'branch',
        begin: /[a-zA-Z\/]+/,
      },
    ]
  }

  let git_log_graph = {
    className: 'git-log-graph',
    begin: /^[*|][^a-f0-9]+/,
    end: /(\))?$/,
    contains: [
      {
        className: 'commit-id',
        begin: /[a-f0-9]+ \(/,
        end: /\)/,
        contains: [
          {
            className: 'tag',
            begin: /tag: [0-9\.\-a-z]+/,
          },
          {
            begin: /[^,\)]*->/,
            className: 'head',
            end: ',',
            excludeEnd: true,
            contains: [
              {
                className: 'head-branch',
                begin: /[a-zA-Z]/
              }
            ]
          },
          {
            className: 'branch',
            begin: /[a-zA-Z\-\/]+/,
          },
        ]
      },
      {
        className: 'commit-id',
        begin: /[a-f0-9]{6,}/,
        end: /$/,
        contains: [
          {
            className: 'commit-msg',
            begin: /[a-zA-Z ->]+/,
            end: /$/,
            endsParent: true,
            returnEnd: true,
          }
        ]
      },
    ]
  }

  let git_diff = {
    className: 'git-diff',
    begin: /^diff --git/,
    end: /^\$/,
    contains: [
      {
        className: 'patch',
        begin: /^\@\@/,
        end: /\@\@$/,
      },
      {
        className: 'added',
        begin: /^\+/,
        end: /$/,
      },
      {
        className: 'removed',
        begin: /^[-−]/,
        end: /$/,
      },
    ]
  }

  return {
    aliases: ['git'],
    contains: [
      git_status,
      git_log,
      git_log_graph,
      git_diff,
    ]
  }
});
