<section data-state="no-title-footer">

## Docker & Container
### Zombie Reaping
### Corewire IT Consulting

---

### Eigene Startup-Skripte

```Dockerfile
FROM ubuntu

...

CMD ["meinskript.sh"]
```

---

### Eigene Startup-Skripte

* Vorbereitungscode ausführen, bevor das eigentliche
  Programm startet
* Optionen auswerten (Umgebungsvariablen)
* mehrere Programme starten
* auf externe Services warten

---

### Zombie Processes

![](./tikz/process_tree_1.svg)<!-- .element width="100%" -->

---

### Zombie Processes

![](./tikz/process_tree_1_1.svg)<!-- .element width="100%" -->

---

### Zombie Processes

![](./tikz/process_tree_2.svg)<!-- .element width="100%" -->

---

### Zombie Processes

![](./tikz/process_tree_2_1.svg)<!-- .element width="100%" -->

---

### Zombie Processes

![](./tikz/process_tree_3.svg)<!-- .element width="100%" -->

---

### Zombie Processes

![](./tikz/process_tree_4.svg)<!-- .element width="100%" -->

---

### Process Addoption

![](./tikz/process_tree_5.svg)<!-- .element width="15%" .element: class="fragment" -->
![](./tikz/process_tree_6.svg)<!-- .element width="15%" .element: class="fragment" -->
![](./tikz/process_tree_7.svg)<!-- .element width="15%" .element: class="fragment" -->

---

### Zombie Reaping in Docker

![](./tikz/process_tree_9.svg)<!-- .element width="30%" -->
![](./tikz/process_tree_10.svg)<!-- .element width="22%" --> <!-- .element: class="fragment" -->
![](./tikz/process_tree_11.svg)<!-- .element width="22%" --> <!-- .element: class="fragment" -->

---

### Initialization Scripts

![](./tikz/process_tree_13.svg)<!-- .element width="50%" -->

---

### Init Processes

* **tini** since Docker 1.13
* phusion/baseimage-docker
* dockerfy (markriggins/dockerfy on Github)

---

### Tini Init Process

* reaps zombies
* signal handling
* no configuration

```shell
docker run --init ...
```

<!-- .element: class="fragment" -->

```yaml
# docker-compose.yml
services:
  testme:
      image: some/image
          init: true
```

<!-- .element: class="fragment" -->

---
