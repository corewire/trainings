\documentclass[crop,tikz]{standalone}
\usepackage{tikz}
\input{../../../templates/common.tex}
\usepackage{ifthen}
\usetikzlibrary{fit, arrows, shapes.arrows, shapes.geometric, calc, positioning}
\makeatletter
\tikzset{
  >=stealth',
  fitting node/.style={
    inner sep=0pt,
    fill=none,
    draw=none,
    reset transform,
    fit={(\pgf@pathminx,\pgf@pathminy) (\pgf@pathmaxx,\pgf@pathmaxy)}
  },
  reset transform/.code={\pgftransformreset}
}

\begin{document}
  \foreach \n in {1,...,3} {
    % Wdith defintions and helpers
    \def\width{10}
    \def\height{5}
    \def\firstcircle{(\width/4,\height/2) circle (\height/2-0.5)}
    \def\secondcircle{(3*\width/4,\height/2) circle (\height/2-0.5)}
    \def\thirdcircle{(\width/3+0.5,\height/2) circle (\height/2-0.5)}
    \def\forthcircle{(2*\width/3-0.5,\height/2) circle (\height/2-0.5)}
    
    \begin{tikzpicture}[
    filled/.style={fill=corewireblue!10, draw=corewireblue!50, thick},
    outline/.style={draw=corewireblue!50, thick}
]

\draw [draw=none, fill=gray!10](0,0) rectangle (\width,\height); 
    
    \ifthenelse{\n = 1}{
        \begin{scope}
            \clip \firstcircle;
            \fill[filled] \secondcircle;
        \end{scope}
        \draw[outline] \firstcircle node[text width=2cm, align=center] {{\Huge{\faUser}} \\ Development};
        \draw[outline] \secondcircle node[text width=2cm, align=center] {{\Huge{\faUser}} \\ Operations};
    }{}
    
    \ifthenelse{\n > 1}{
        \begin{scope}
            \clip \thirdcircle;
            \fill[filled] \forthcircle;
        \end{scope}
        \draw[outline] \thirdcircle node[text width=3.2cm, align=left] {{\Huge{\faUser}} \\ Development};
        \draw[outline] \forthcircle node[text width=3.2cm, align=right] {{\Huge{\faUser}} \\ Operations};
    }{}
    
    \ifthenelse{\n = 3}{
        \node[text width=2cm, align=center] at (\width/2, \height/2) {{\Huge{\faUser}} \\ DevOps};
    }{}


\end{tikzpicture}
  }
\end{document}
